

import UIKit

class RightViewCell: UITableViewCell {

    @IBOutlet weak var messageContainerView: UIView!
    @IBOutlet weak var textMessageLabel: UILabel!
    @IBOutlet weak var lblTiming: UILabel!
    @IBOutlet weak var imgviewUser: UIImageView!
    @IBOutlet weak var textMessage: UITextView!
    @IBOutlet weak var progressIndicator: UIProgressView!
    
    @IBOutlet weak var labelProgressIndicator: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        messageContainerView.setRoundCorner(cornerRadious: 12.0)
        messageContainerView.layer.cornerRadius = 10
        messageContainerView.backgroundColor = .white
        //messageContainerView.setBorder(width: 1, borderColor: UIColor.purple, cornerRadious: 2)
        self.progressIndicator.isHidden = true
        self.labelProgressIndicator.isHidden = true
        self.labelProgressIndicator.numberOfLines = 0
        
        self.textMessage.isScrollEnabled = false
        self.textMessage.isEditable = false
        self.textMessage.isSelectable = true
        self.textMessage.sizeToFit()
        
        //self.editButton.isHidden = true
        //self.editButton.addTarget(self, action: #selector(), for: .touchUpInside)
        //self.editButton.backgroundColor = UIColor.green
        //self.editButton.setBorder(width: 1, borderColor: UIColor.black, cornerRadious: 10)
        
        
        //self.cancelButton.isHidden = true
        
        //self.cancelButton.backgroundColor = UIColor.red
        //self.cancelButton.setBorder(width: 1, borderColor: UIColor.black, cornerRadious: 10)
        
        contentView.backgroundColor = .clear
        backgroundColor = .clear
    }
    
    func getDay(value: Int) -> String {
        switch value {
        case 1:
            return "Sunday"
        case 2:
            return "Monday"
        case 3:
            return "Tuesday"
        case 4:
            return "Wednesday"
        case 5:
            return "Thursday"
        case 6:
            return "Friday"
        case 7:
            return "Saturday"
        default:
            return ""
        }
    }
    func getDateDayAndTime(timestamp: NSNumber) -> String {
           let date  = Date(timeIntervalSince1970: Double(truncating: timestamp)/1000)
           let calendar = Calendar.current
    
           if calendar.isDateInToday(date) {
               let dateFormatter = DateFormatter()
               dateFormatter.timeZone = NSTimeZone.local
               dateFormatter.dateFormat = "hh:mm a"
               let time = dateFormatter.string(from: date)
               return time
           }else if calendar.isDateInYesterday(date) {
               return "Yesterday"
           }
           else if calendar.isDateInWeekend(date) {
               let component = calendar.component(Calendar.Component.weekday, from: date)
               return self.getDay(value: component)
           } else {
               let dateFormatter = DateFormatter()
               dateFormatter.timeZone = NSTimeZone.local
               dateFormatter.dateFormat = "dd/MM/YY"
               let time = dateFormatter.string(from: date)
               return time
           }
       }
    func configureCell(message: Chat1) {
//        let chatMessage = message.msg?.decode()
//        textMessageLabel.text = chatMessage?.decode()
        DispatchQueue.global().async {
            let data = try! Data(contentsOf: URL(string: "https://br.aescare.com/user/avatar/\(message.user_key ?? "")")!)
            
            DispatchQueue.main.async {
                self.imgviewUser.image = UIImage(data: data)
            }
        }
        
        let chatMessage = message.content?.decode()
        self.textMessage.text = chatMessage?.decode()
//               if let timeStamp = message.timeStamp{
//               let time = self.getDateDayAndTime(timestamp: timeStamp)
//                   lblTiming.text = time
//               }else{
        lblTiming.text = message.send_date ?? ""
               //}
    }
}
class RightImageCell: UITableViewCell {

    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var imgViewChat: UIImageView!
    @IBOutlet weak var sender: UIImageView!
   
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //messageContainerView.rounded(radius: 12)
        viewContainer.backgroundColor = UIColor.white
        
        contentView.backgroundColor = .clear
        backgroundColor = .clear
    }
    
    
}
class RightMultipleImageCell: UITableViewCell {

    //@IBOutlet weak var messageContainerView: UIView!
   
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //messageContainerView.rounded(radius: 12)
        //messageContainerView.backgroundColor = UIColor(hexString: "E1F7CB")
        
        contentView.backgroundColor = .clear
        backgroundColor = .clear
    }
    
    
}
