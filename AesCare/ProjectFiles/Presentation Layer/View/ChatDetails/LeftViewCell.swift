

import UIKit
import WebKit
import ActiveLabel

class LeftViewCell: UITableViewCell {
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblTiming: UILabel!
    @IBOutlet weak var txtChatMsg: UITextView!
    @IBOutlet weak var imgviewUser: UIImageView!
    @IBOutlet weak var linkViewer: UIStackView!
    
    var linkPreview: AESCareLinkPreview?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.txtChatMsg.isScrollEnabled = false
        self.txtChatMsg.isEditable = false
        self.txtChatMsg.isSelectable = true
        self.txtChatMsg.sizeToFit()
                
        self.linkPreview = AESCareLinkPreview()
        
        self.lblUserName.numberOfLines = 0
        self.lblUserName.lineBreakMode = NSLineBreakMode.byWordWrapping
        
        contentView.backgroundColor = .clear
        backgroundColor = .clear
    }
    
    func getDay(value: Int) -> String {
        switch value {
        case 1:
            return "Sunday"
        case 2:
            return "Monday"
        case 3:
            return "Tuesday"
        case 4:
            return "Wednesday"
        case 5:
            return "Thursday"
        case 6:
            return "Friday"
        case 7:
            return "Saturday"
        default:
            return ""
        }
    }
    func getDateDayAndTime(timestamp: NSNumber) -> String {
           let date  = Date(timeIntervalSince1970: Double(truncating: timestamp)/1000)
           let calendar = Calendar.current
    
           if calendar.isDateInToday(date) {
               let dateFormatter = DateFormatter()
               dateFormatter.timeZone = NSTimeZone.local
               dateFormatter.dateFormat = "hh:mm a"
               let time = dateFormatter.string(from: date)
               return time
           }else if calendar.isDateInYesterday(date) {
               return "Yesterday"
           }
           else if calendar.isDateInWeekend(date) {
               let component = calendar.component(Calendar.Component.weekday, from: date)
               return self.getDay(value: component)
           } else {
               let dateFormatter = DateFormatter()
               dateFormatter.timeZone = NSTimeZone.local
               dateFormatter.dateFormat = "dd/MM/YY"
               let time = dateFormatter.string(from: date)
               return time
           }
       }
    
    func configureCell(message: Chat1) {
        let chatMessage = message.content?.decode()
        var data = chatMessage?.decode()
        
        DispatchQueue.global().async {
            let data = try! Data(contentsOf: URL(string: "https://br.aescare.com/user/avatar/\(message.user_key ?? "")")!)
            
            DispatchQueue.main.async {
                self.imgviewUser.image = UIImage(data: data)
            }
        }
                
        DispatchQueue.main.async {
            self.linkPreview?.retriveMetaData(&data, extractLinkFromString: true, requestHandler: {
                metadata in
                if let _ = metadata {
                    self.linkViewer.insertArrangedSubview(metadata!, at: 0)
                    self.linkViewer.isHidden = false
                }
                else {
                    self.linkViewer.isHidden = true
                }
            })
        }
        
        self.txtChatMsg.text = chatMessage?.decode()

        lblTiming.text = message.send_date ?? ""
        lblUserName.text = message.username
        
    }
    
    func linkDetector(data: String) -> String? {
        let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
        let matches = detector.matches(in: data, options: [], range: NSRange(location: 0, length: data.utf16.count))
        var link: String? = ""
        
        for match in matches {
            guard let range = Range(match.range, in: data) else { continue }
            let url = data[range]
            print("URL::: \(url)")
            link = String(url)
        }
        
        return link
    }
    
}
class LeftImageView: UITableViewCell {

    @IBOutlet weak var messageContainerView: UIView!
   @IBOutlet weak var lblTime: UILabel!
   @IBOutlet weak var imgViewChat: UIImageView!
    @IBOutlet weak var sender: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        //messageContainerView.rounded(radius: 12)
        messageContainerView.backgroundColor = UIColor.clear
        
        contentView.backgroundColor = .clear
        backgroundColor = .clear
    }
    
    
}
class LeftImageCell: UITableViewCell {

    //@IBOutlet weak var messageContainerView: UIView!
   @IBOutlet weak var lblTime: UILabel!
   @IBOutlet weak var imgViewChat: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //messageContainerView.rounded(radius: 12)
        //messageContainerView.backgroundColor = UIColor(hexString: "E1F7CB")
        
        contentView.backgroundColor = .clear
        backgroundColor = .clear
    }
    
//    func configureCell(message: Message) {
//       
//    }
}
