//
//  AdvancedSearchBarView.swift
//  AesCare
//
//  Created by Thiago on 26/04/21.
//

import Foundation
import UIKit
import SideMenu

class AdvancedSearchBarView: UIView {
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var textFieldSearch: UITextField!
    @IBOutlet weak var buttonSearch: UIButton!
    @IBOutlet weak var buttonSideMenu: UIButton!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.loadViewFromNib()
        self.setupGradient()
        self.setupViews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        self.loadViewFromNib()
        self.setupGradient()
        self.setupViews()
    }
    
    func loadViewFromNib() {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "AdvancedSearchBarView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(view);
    }
    
    private func setupViews() {
        let buttonSearch = UIButton()
        buttonSearch.setImage(UIImage(named: "search"), for: .normal)
        buttonSearch.addTarget(self, action: #selector(self.buttonSearchPressed(_:)), for: .touchUpInside)
        
        self.textFieldSearch.placeholder = "Faça uma pesquisa"
        self.textFieldSearch.borderStyle = .roundedRect
        self.textFieldSearch.backgroundColor = .white
        self.textFieldSearch.returnKeyType = .search
        self.textFieldSearch.setIcon(UIImage(named: "logo-aescare")!, viewPosition: .leftSide, viewMode: .whileEditing)
        self.textFieldSearch.setButton(buttonSearch, viewPosition: .rightSide, viewMode: .always)
        self.textFieldSearch.delegate = self
        
        self.buttonSideMenu.contentMode = .scaleToFill
        self.buttonSideMenu.clipsToBounds = true
        self.buttonSideMenu.addTarget(self, action: #selector(self.buttonSideMenuPressed(_:)), for: .touchUpInside)
    }
    
    private func setupGradient() {
        let gradient = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = [UIColor(red: 168/255, green: 72/255, blue: 217/255, alpha: 1.0).cgColor, UIColor(red: 105/255, green: 108/255, blue: 220/255, alpha: 1.0).cgColor]
        gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        self.contentView.layer.insertSublayer(gradient, at: 0)
    }
}

extension AdvancedSearchBarView {
    @objc func buttonSearchPressed(_ sender: UIButton) {
        if !self.textFieldSearch.text!.isEmpty {
            let advancedSearch = AESCareAdvancedSearch()
            advancedSearch.frame = CGRect(x: UIScreen.main.bounds.origin.x, y: UIScreen.main.bounds.origin.y, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
            advancedSearch.searchText = self.textFieldSearch.text!

            UIApplication.shared.windows.first(where: { $0.isKeyWindow })?.addSubview(advancedSearch)
        }
    }
    
    @objc func buttonSideMenuPressed(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(identifier: "MenuViewController") as! MenuViewController
        
        let menu = SideMenuNavigationController(rootViewController: controller)
        menu.leftSide = true
        menu.animationOptions = .curveEaseIn
        menu.blurEffectStyle = .light
        menu.allowPushOfSameClassTwice = false
                
        UIApplication.shared.windows.first?.rootViewController?.present(menu, animated: true, completion: nil)
        
    }
}

extension AdvancedSearchBarView: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()

        if !textField.text!.isEmpty {
            let advancedSearch = AESCareAdvancedSearch()
            advancedSearch.frame = CGRect(x: UIScreen.main.bounds.origin.x, y: UIScreen.main.bounds.origin.y, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
            advancedSearch.searchText = self.textFieldSearch.text!
            
            UIApplication.shared.windows.first(where: { $0.isKeyWindow })?.addSubview(advancedSearch)
        }
        
        return true
    }
}
