//
//  UserSendMessageView.swift
//  AesCare
//
//  Created by Thiago on 19/04/21.
//

import UIKit
import Loady

protocol UserSendMessageDelegate: class {
    
}

class UserSendMessageView: UIView {
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var buttonSendUserMessage: LoadyButton!
    @IBOutlet weak var textViewUserMessage: UITextView!
    @IBOutlet weak var imageViewUserIcon: UIImageView!

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.initSubviews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        self.initSubviews()
        
    }
    
    func initSubviews() {
        let nib = UINib(nibName: "UserSendMessageView", bundle: nil)
        nib.instantiate(withOwner: self, options: nil)
        contentView.frame = bounds
        self.setupViews()
        addSubview(contentView)
    }
    
    func setupViews() {
        self.buttonSendUserMessage.layer.cornerRadius = self.buttonSendUserMessage.frame.height / 2
        self.buttonSendUserMessage.layer.borderWidth = 1
        self.buttonSendUserMessage.layer.borderColor = UIColor(red: 175/255, green: 68/255, blue: 217/255, alpha: 1.0).cgColor
        self.buttonSendUserMessage.backgroundFillColor = UIColor(red: 175/255, green: 68/255, blue: 217/255, alpha: 1.0)
        self.buttonSendUserMessage.setAnimation(LoadyAnimationType.backgroundHighlighter())
        
        self.textViewUserMessage.setBorder(width: 2, borderColor: .lightGray, cornerRadious: 10)
        self.textViewUserMessage.textColor = .lightGray
        self.textViewUserMessage.text = "Faça um comentario"
        //self.textViewUserMessage.delegate = self
        self.textViewUserMessage.isScrollEnabled = false
        self.textViewUserMessage.showsVerticalScrollIndicator = false
        self.textViewUserMessage.showsHorizontalScrollIndicator = false
        self.textViewUserMessage.sizeToFit()
        
        self.imageViewUserIcon.setBorder(width: 2, borderColor: UIColor(red: 175/255, green: 68/255, blue: 217/255, alpha: 1.0), cornerRadious: self.imageViewUserIcon.frame.height / 2)
        self.contentView.setBorder(width: 2, borderColor: UIColor(red: 175/255, green: 68/255, blue: 217/255, alpha: 1.0), cornerRadious: 10)
        self.imageViewUserIcon.sd_setImage(with: URL(string: usrerModelOBJ.user!.perfil_image!), completed: nil)
    }
}
