//
//  AdvancedSearchMedicInfoTableViewCell.swift
//  AesCare
//
//  Created by Thiago on 25/04/21.
//

import UIKit

class AdvancedSearchMedicInfoTableViewCell: UITableViewCell {
    @IBOutlet weak var imageViewIcon: UIImageView!
    @IBOutlet weak var labelMedicName: UILabel!
    @IBOutlet weak var labelMedicSpecialty: UILabel!
    @IBOutlet weak var labelMedicLocation: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setupViews()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    private func setupViews() {
        self.imageViewIcon.setBorder(width: 2, borderColor: .purple, cornerRadious: self.imageViewIcon.frame.height / 2)
        
        self.labelMedicName.numberOfLines = 0
        self.labelMedicName.lineBreakMode = .byWordWrapping
        
        self.labelMedicSpecialty.numberOfLines = 0
        self.labelMedicSpecialty.lineBreakMode = .byWordWrapping
        
        self.labelMedicLocation.numberOfLines = 0
        self.labelMedicLocation.lineBreakMode = .byWordWrapping
    }
    
}
