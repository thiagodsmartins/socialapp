//
//  WhoWeAreView.swift
//  AesCare
//
//  Created by Thiago on 04/05/21.
//

import Foundation
import UIKit
import FSPagerView

class WhoWeAreView: UIView {
    var blurEffect: UIBlurEffect!
    var blurVisualEffect: UIVisualEffectView!
    var buttonClose: UIButton!
    var labelMessage: UILabel!
    var pagerView: FSPagerView!
    var pageControl: FSPageControl!
    
    var presentationImages: [UIImage] {
        let images = [(UIImage(named: "aescare_image_medics")!),
                      (UIImage(named: "wwa_image1")!),
                      (UIImage(named: "wwa_image2")!),
                      (UIImage(named: "wwa_image3")!)]
        
        return images
    }
    
    var presentationText: [String] {
        let text = [
            """
                Seja bem-vindo(a) a AesCare. Conheça aqui nossas metas e objetivos para com o público.
            """,
            """
                AesCare é uma comunidade que promove o compartilhamento colaborativo de informações
                reais de procedimentos relacionados à autoimagem e auxilia na interação entre pacientes
                e especialistas.
            """,
            """
                Surgimos a partir da pecepção que havia um certo desgaste na busca de informações sobre
                sobre procedimentos estéticos. Já somos mais de 600 mil usuárias colaborando com informações
                e conquistando nossos sonhos!
            """,
            """
                Tire dúvidas, busque informações, encontre resultados dos especialistas, participe de grupos,
                agende sua consulta e interaja com pessoas que possuem o mesmo objetivo que você.
            """
        ]
        
        return text
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
    }
    
    override func didMoveToSuperview() {
        self.alpha = 0
        
        UIView.transition(with: self, duration: 1.2, options: .transitionCrossDissolve, animations: {
            self.alpha = 1
            self.setupBlurEffect()
        }, completion: {
            _ in
            self.setupViews()
        })
    }
    
    private func setupViews() {
        self.setupComponents()
        self.setupSlideShow()
        self.setContraints()
    }
    
    private func setupBlurEffect() {
        self.blurEffect = UIBlurEffect(style: .light)
        self.blurVisualEffect = UIVisualEffectView(effect: self.blurEffect)
        
        self.blurVisualEffect.frame = self.bounds
        self.blurVisualEffect.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        self.addSubview(self.blurVisualEffect)
    }
    
    private func setupSlideShow() {
        self.pagerView = FSPagerView(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        self.pageControl = FSPageControl()
        
        self.pagerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
        self.pagerView.automaticSlidingInterval = 0
        self.pagerView.bounces = true
        self.pagerView.itemSize = CGSize(width: self.pagerView.bounds.size.width, height: self.pagerView.bounds.size.height)
        self.pagerView.interitemSpacing = 10
        self.pagerView.transformer = FSPagerViewTransformer(type: .zoomOut)
        self.pagerView.delegate = self
        self.pagerView.dataSource = self
        self.pagerView.translatesAutoresizingMaskIntoConstraints = false
        
        self.pageControl.itemSpacing = 5
        self.pageControl.setFillColor(.purple, for: .normal)
        self.pageControl.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(self.pagerView)
        self.addSubview(self.pageControl)
    }
    
    private func setupComponents() {
        self.buttonClose = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        self.labelMessage = UILabel()
        
        self.buttonClose.setTitle("X", for: .normal)
        self.buttonClose.setBorder(width: 2, borderColor: .purple, cornerRadious: self.buttonClose.frame.height / 2)
        self.buttonClose.addTarget(self, action: #selector(self.buttonClosePressed(_:)), for: .touchUpInside)
        self.buttonClose.translatesAutoresizingMaskIntoConstraints = false
        
        self.labelMessage.numberOfLines = 0
        self.labelMessage.lineBreakMode = .byWordWrapping
        self.labelMessage.textColor = .black
        self.labelMessage.translatesAutoresizingMaskIntoConstraints = false
        self.labelMessage.font = UIFont(name: self.labelMessage.font.fontName, size: 20.0)
        self.labelMessage.textAlignment = .center
        self.addSubview(self.buttonClose)
        self.addSubview(self.labelMessage)
    }
    
    private func setContraints() {
        NSLayoutConstraint.activate([
            self.pagerView.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            self.pagerView.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            self.pagerView.widthAnchor.constraint(equalToConstant: self.bounds.width),
            self.pagerView.heightAnchor.constraint(equalToConstant: 300),
            
            self.pageControl.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            self.pageControl.topAnchor.constraint(equalTo: self.pagerView.bottomAnchor, constant: -10),
            self.pageControl.widthAnchor.constraint(equalToConstant: 100),
            self.pageControl.heightAnchor.constraint(equalToConstant: 50),
            
            self.labelMessage.topAnchor.constraint(equalTo: self.pagerView.bottomAnchor, constant: 20),
            self.labelMessage.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 10),
            self.labelMessage.widthAnchor.constraint(equalTo: self.widthAnchor, constant: -10),
            
            self.buttonClose.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            self.buttonClose.bottomAnchor.constraint(equalTo: self.pagerView.topAnchor, constant: -80),
            self.buttonClose.widthAnchor.constraint(equalToConstant: 40),
            self.buttonClose.heightAnchor.constraint(equalToConstant: 40)
        ])
    }
    
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size

        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height

        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)

        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return newImage!
    }
    
}

extension WhoWeAreView: FSPagerViewDelegate, FSPagerViewDataSource {
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        self.pageControl.numberOfPages = self.presentationImages.count
        return self.presentationImages.count
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        
        cell.imageView?.image = self.resizeImage(image: self.presentationImages[index], targetSize: CGSize(width: self.frame.width, height: self.frame.height))
        cell.imageView?.contentMode = .scaleAspectFit
        self.labelMessage.text = self.presentationText[index]

        self.pageControl.currentPage = index
        
        return cell
    }
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        pagerView.deselectItem(at: index, animated: true)
    }
}

extension WhoWeAreView {
    @objc func buttonClosePressed(_ sender: UIButton) {
        UIView.transition(with: self, duration: 1.2, options: .curveEaseOut, animations: {
            self.alpha = 0
        }, completion: {
            _ in
            self.removeFromSuperview()
        })
    }
}
