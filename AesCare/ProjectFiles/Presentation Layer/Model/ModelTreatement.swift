//
//  ModelTreatement.swift
//  AesCare
//
//  Created by Gali Srikanth on 12/10/20.
//

import Foundation
struct ModelTreatement : Codable {
    let worth_it_no_count : Int?
    let active : Bool?
    let image : String?
    let slug : String?
    let name_en : String?
    let name : String?
    let body_parts : [Int]?
    let created_at : String?
    let worth_it_dontknow_count : Int?
    let worth_it_yes_count : Int?
    let thumbnail : String?
    let description : String?
    let name_es : String?
    let treatment_id : Int?
    let updated_at : String?
    let testimonials_count : Int?

    enum CodingKeys: String, CodingKey {

        case worth_it_no_count = "worth_it_no_count"
        case active = "active"
        case image = "image"
        case slug = "slug"
        case name_en = "name_en"
        case name = "name"
        case body_parts = "body_parts"
        case created_at = "created_at"
        case worth_it_dontknow_count = "worth_it_dontknow_count"
        case worth_it_yes_count = "worth_it_yes_count"
        case thumbnail = "thumbnail"
        case description = "description"
        case name_es = "name_es"
        case treatment_id = "treatment_id"
        case updated_at = "updated_at"
        case testimonials_count = "testimonials_count"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        worth_it_no_count = try values.decodeIfPresent(Int.self, forKey: .worth_it_no_count)
        active = try values.decodeIfPresent(Bool.self, forKey: .active)
        image = try values.decodeIfPresent(String.self, forKey: .image)
        slug = try values.decodeIfPresent(String.self, forKey: .slug)
        name_en = try values.decodeIfPresent(String.self, forKey: .name_en)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        body_parts = try values.decodeIfPresent([Int].self, forKey: .body_parts)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        worth_it_dontknow_count = try values.decodeIfPresent(Int.self, forKey: .worth_it_dontknow_count)
        worth_it_yes_count = try values.decodeIfPresent(Int.self, forKey: .worth_it_yes_count)
        thumbnail = try values.decodeIfPresent(String.self, forKey: .thumbnail)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        name_es = try values.decodeIfPresent(String.self, forKey: .name_es)
        treatment_id = try values.decodeIfPresent(Int.self, forKey: .treatment_id)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
        testimonials_count = try values.decodeIfPresent(Int.self, forKey: .testimonials_count)
    }

}
