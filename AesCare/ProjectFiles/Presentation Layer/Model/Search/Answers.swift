/* 
 Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com
 
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 
 For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar
 
 */

import Foundation
struct Answers : Codable {

    let medic_id : Int?
    let name : String?
    let user_id : Int?
    let image_url:String?
    let evaluations_count : Int?
    let status : String?
    let specialty_id : Int?
    let cover_media : Cover_media?
    let name_with_title : String?
    let evaluation_average_formatted : String?
    
    
    let card_type : String?
    let payment_type : String?
    let treatment_value : String?
    let evaluation_result : Int?
    let medic_name : String?
    let medic_key : String?
    
    
    let medic_text : String?
    let medic : Medic?
    var isClickedEye : Bool = false
    let post_id : Int?
    //let content : Content?
    let created_at_formatted : String?
    let city_name : String?
    
    
    
    enum CodingKeys: String, CodingKey {
        case medic_id = "medic_id"
        case name = "name"
        case user_id = "user_id"
        case evaluations_count = "evaluations_count"
        case status = "status"
        case specialty_id = "specialty_id"
        case cover_media = "cover_media"
        case name_with_title = "name_with_title"
        case evaluation_average_formatted = "evaluation_average_formatted"
        
        case city_name = "city_name"
        case card_type = "card_type"
        case payment_type = "payment_type"
        case treatment_value = "treatment_value"
        case evaluation_result = "evaluation_result"
        case medic_name = "medic_name"
        case medic_key = "medic_key"
        
        
        case medic_text = "medic_text"
        case medic = "medic"
        case image_url = "image_url"
        case post_id = "post_id"
       // case content = "content"
        case created_at_formatted = "created_at_formatted"
    }
    
}
