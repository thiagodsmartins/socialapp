//
//  ModelSearchedCity.swift
//  AesCare
//
//  Created by Gali Srikanth on 22/09/20.
//

import Foundation
struct ModelSearchedCity : Codable {
    let google_place_id : String?
    let name : String?
    let preposition : String?
    let slug : String?
    let slug_original : String?
    let city_id : Int?
    let state_abbr : String?
    let country_abbr : String?
    let timezone : String?
    let latitude : Double?
    let longitude : Double?
    let results_count : Int?
    let state : State_searchCity?

    enum CodingKeys: String, CodingKey {

        case google_place_id = "google_place_id"
        case name = "name"
        case preposition = "preposition"
        case slug = "slug"
        case slug_original = "slug_original"
        case city_id = "city_id"
        case state_abbr = "state_abbr"
        case country_abbr = "country_abbr"
        case timezone = "timezone"
        case latitude = "latitude"
        case longitude = "longitude"
        case results_count = "results_count"
        case state = "state"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        google_place_id = try values.decodeIfPresent(String.self, forKey: .google_place_id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        preposition = try values.decodeIfPresent(String.self, forKey: .preposition)
        slug = try values.decodeIfPresent(String.self, forKey: .slug)
        slug_original = try values.decodeIfPresent(String.self, forKey: .slug_original)
        city_id = try values.decodeIfPresent(Int.self, forKey: .city_id)
        state_abbr = try values.decodeIfPresent(String.self, forKey: .state_abbr)
        country_abbr = try values.decodeIfPresent(String.self, forKey: .country_abbr)
        timezone = try values.decodeIfPresent(String.self, forKey: .timezone)
        latitude = try values.decodeIfPresent(Double.self, forKey: .latitude)
        longitude = try values.decodeIfPresent(Double.self, forKey: .longitude)
        results_count = try values.decodeIfPresent(Int.self, forKey: .results_count)
        state = try values.decodeIfPresent(State_searchCity.self, forKey: .state)
    }

}
struct State_searchCity : Codable {
    let abbr : String?

    enum CodingKeys: String, CodingKey {

        case abbr = "abbr"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        abbr = try values.decodeIfPresent(String.self, forKey: .abbr)
    }

}
