//
//  ModelUser.swift
//  AesCare
//
//  Created by Gali Srikanth on 20/08/20.
//

import Foundation
class ZeroIndexObject : NSObject{
//    var first : [ResultsFirst] = []
//    var second: [Posts] = []
//    var third : [Posts] = []
//    var fourth : [Posts] = []
    var arrayOrderSet : [AnyObject] = []
    var total = 0
    
}

class Page : NSObject{
    var perPage:Int = 10
    var pageNumber:Int = 1
    var pageDoctor: Int = 1
    var totalData:Int?
}
struct ModelUser : Codable {
    //let redirect : String?
    let user_key : String?
    let user : User?
    let session_key : String?
    // let messages : [String]?
    //let current_jorney : Int?
    // let message : String?
    //let code : Int?
    
    enum CodingKeys: String, CodingKey {
        
        // case redirect = "redirect"
        case user_key = "user_key"
        case user = "user"
        case session_key = "session_key"
        //case messages = "messages"
        // case current_jorney = "current_jorney"
        // case message = "message"
        // case code = "code"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        //redirect = try values.decodeIfPresent(String.self, forKey: .redirect)
        user_key = try values.decodeIfPresent(String.self, forKey: .user_key)
        user = try values.decodeIfPresent(User.self, forKey: .user)
        session_key = try values.decodeIfPresent(String.self, forKey: .session_key)
        // messages = try values.decodeIfPresent([String].self, forKey: .messages)
        // current_jorney = try values.decodeIfPresent(Int.self, forKey: .current_jorney)
        // message = try values.decodeIfPresent(String.self, forKey: .message)
        // code = try values.decodeIfPresent(Int.self, forKey: .code)
    }
    
}

struct User : Codable {
    let key : String?
    let role : String?
    let name : String?
    var phone : String?
    var email : String?
    let encrypted_password : String?
    var gender : String?
    let status : String?
    let user_id : Int?
    var medic_id : String?
    var medic_key : String?
    let username : String?
    let firstname : String?
    var is_medic : Bool?
    let url_image : String?
    var birthdate : String?
    var age : Int?
    let created : String?
    let created_at : String?
    let perfil_image : String?
    let assistant_content_balance : Int?
    let review_count : Int?
    var country_code : String?
    let treatments : [String]?
    var latitude : Double?
    var longitude : Double?
    var last_latitude : Double?
    var last_longitude : Double?
    let acronym_name : String?
    var city_id : Int?
    var city : String?
    let abbr : String?
    let city_name : String?
    let state_abbr : String?
    var current_journey_step : Int?
    var is_contract_accepted : Bool?
    var blocked_users_id : String?

    enum CodingKeys: String, CodingKey {

        case key = "key"
        case role = "role"
        case name = "name"
        case phone = "phone"
        case email = "email"
        case encrypted_password = "encrypted_password"
        case gender = "gender"
        case status = "status"
        case user_id = "user_id"
        case medic_id = "medic_id"
        case medic_key = "medic_key"
        case username = "username"
        case firstname = "firstname"
        case is_medic = "is_medic"
        case url_image = "url_image"
        case birthdate = "birthdate"
        case age = "age"
        case created = "created"
        case created_at = "created_at"
        case perfil_image = "perfil_image"
        case assistant_content_balance = "assistant_content_balance"
        case review_count = "review_count"
        case country_code = "country_code"
        case treatments = "treatments"
        case latitude = "latitude"
        case longitude = "longitude"
        case last_latitude = "last_latitude"
        case last_longitude = "last_longitude"
        case acronym_name = "acronym_name"
        case city_id = "city_id"
        case city = "city"
        case abbr = "abbr"
        case city_name = "city_name"
        case state_abbr = "state_abbr"
        case current_journey_step = "current_journey_step"
        case is_contract_accepted = "is_contract_accepted"
        case blocked_users_id = "blocked_users_id"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        key = try values.decodeIfPresent(String.self, forKey: .key)
        role = try values.decodeIfPresent(String.self, forKey: .role)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        phone = try values.decodeIfPresent(String.self, forKey: .phone)
        email = try values.decodeIfPresent(String.self, forKey: .email)
        encrypted_password = try values.decodeIfPresent(String.self, forKey: .encrypted_password)
        gender = try values.decodeIfPresent(String.self, forKey: .gender)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
        medic_id = try values.decodeIfPresent(String.self, forKey: .medic_id)
        medic_key = try values.decodeIfPresent(String.self, forKey: .medic_key)
        username = try values.decodeIfPresent(String.self, forKey: .username)
        firstname = try values.decodeIfPresent(String.self, forKey: .firstname)
        is_medic = try values.decodeIfPresent(Bool.self, forKey: .is_medic)
        url_image = try values.decodeIfPresent(String.self, forKey: .url_image)
        birthdate = try values.decodeIfPresent(String.self, forKey: .birthdate)
        age = try values.decodeIfPresent(Int.self, forKey: .age)
        created = try values.decodeIfPresent(String.self, forKey: .created)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        perfil_image = try values.decodeIfPresent(String.self, forKey: .perfil_image)
        assistant_content_balance = try values.decodeIfPresent(Int.self, forKey: .assistant_content_balance)
        review_count = try values.decodeIfPresent(Int.self, forKey: .review_count)
        country_code = try values.decodeIfPresent(String.self, forKey: .country_code)
        treatments = try values.decodeIfPresent([String].self, forKey: .treatments)
        latitude = try values.decodeIfPresent(Double.self, forKey: .latitude)
        longitude = try values.decodeIfPresent(Double.self, forKey: .longitude)
        last_latitude = try values.decodeIfPresent(Double.self, forKey: .last_latitude)
        last_longitude = try values.decodeIfPresent(Double.self, forKey: .last_longitude)
        acronym_name = try values.decodeIfPresent(String.self, forKey: .acronym_name)
        city_id = try values.decodeIfPresent(Int.self, forKey: .city_id)
        city = try values.decodeIfPresent(String.self, forKey: .city)
        abbr = try values.decodeIfPresent(String.self, forKey: .abbr)
        city_name = try values.decodeIfPresent(String.self, forKey: .city_name)
        state_abbr = try values.decodeIfPresent(String.self, forKey: .state_abbr)
        current_journey_step = try values.decodeIfPresent(Int.self, forKey: .current_journey_step)
        is_contract_accepted = try values.decodeIfPresent(Bool.self, forKey: .is_contract_accepted)
        blocked_users_id = try values.decodeIfPresent(String.self, forKey: .blocked_users_id)
    }
    
    

}

struct User1 : Codable {
    let key : String?
    let username : String?
    let name : String?
    let image_url : String?
    var isSelected :Bool = false
    let chatImage :String?

    enum CodingKeys: String, CodingKey {

        case key = "key"
        case username = "username"
        case name = "name"
        case image_url = "image_url"
        case chatImage = "url_image"
    }

    

}
struct ChatUser : Codable {
    let user_id : Int?
    let username : String?
    let user_key : String?
    //let image_url : String?

    enum CodingKeys: String, CodingKey {

        case user_id = "user_id"
        case username = "username"
        case user_key = "user_key"
        //case image_url = "image_url"
    }

    

}
