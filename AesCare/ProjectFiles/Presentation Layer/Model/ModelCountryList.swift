//
//  ModelCountryList.swift
//  AesCare
//
//  Created by Gali Srikanth on 26/09/20.
//

import Foundation
struct ModelCountryList : Codable {
    let capital_name : String?
    let name_pt : String?
    let code : String?
    let name : String?
    let phone_code : String?
    let name_es : String?
    let code3 : String?
    let country_id : Int?

    enum CodingKeys: String, CodingKey {

        case capital_name = "capital_name"
        case name_pt = "name_pt"
        case code = "code"
        case name = "name"
        case phone_code = "phone_code"
        case name_es = "name_es"
        case code3 = "code3"
        case country_id = "country_id"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        capital_name = try values.decodeIfPresent(String.self, forKey: .capital_name)
        name_pt = try values.decodeIfPresent(String.self, forKey: .name_pt)
        code = try values.decodeIfPresent(String.self, forKey: .code)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        phone_code = try values.decodeIfPresent(String.self, forKey: .phone_code)
        name_es = try values.decodeIfPresent(String.self, forKey: .name_es)
        code3 = try values.decodeIfPresent(String.self, forKey: .code3)
        country_id = try values.decodeIfPresent(Int.self, forKey: .country_id)
    }

}
