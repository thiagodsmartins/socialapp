/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct FacebookPost : Codable {
	let evaluations_facebook_id : Int?
	let facebook_user : String?
	let content : String?
	let evaluation : Int?
	let date : String?
	let published_at_formated : String?

	enum CodingKeys: String, CodingKey {

		case evaluations_facebook_id = "evaluations_facebook_id"
		case facebook_user = "facebook_user"
		case content = "content"
		case evaluation = "evaluation"
		case date = "date"
		case published_at_formated = "published_at_formated"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		evaluations_facebook_id = try values.decodeIfPresent(Int.self, forKey: .evaluations_facebook_id)
		facebook_user = try values.decodeIfPresent(String.self, forKey: .facebook_user)
		content = try values.decodeIfPresent(String.self, forKey: .content)
		evaluation = try values.decodeIfPresent(Int.self, forKey: .evaluation)
		date = try values.decodeIfPresent(String.self, forKey: .date)
		published_at_formated = try values.decodeIfPresent(String.self, forKey: .published_at_formated)
	}

}
