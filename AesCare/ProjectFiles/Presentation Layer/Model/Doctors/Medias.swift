/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct Medias : Codable {
	let key : String?
	let created_at : String?
	let created_at_formatted : String?
	let user_id : Int?
	let status : String?
	let hash : String?
	let _hash : String?
	let type : String?
	let _type : String?
	let testimonial_id : String?
	let post_id : Int?
	let content_id : String?
	let video_url : String?
	let image_url : String?
	let thumbnail_url : String?
	let cover_url : String?

	enum CodingKeys: String, CodingKey {

		case key = "key"
		case created_at = "created_at"
		case created_at_formatted = "created_at_formatted"
		case user_id = "user_id"
		case status = "status"
		case hash = "hash"
		case _hash = "_hash"
		case type = "type"
		case _type = "_type"
		case testimonial_id = "testimonial_id"
		case post_id = "post_id"
		case content_id = "content_id"
		case video_url = "video_url"
		case image_url = "image_url"
		case thumbnail_url = "thumbnail_url"
		case cover_url = "cover_url"
	}


}
struct MediasResult : Codable {
//let device_id : String?
//let google_labels : String?
//let device_key : String?
//let youtube_video_id : String?
//let created_at : String?
//let ticket_id : String?
//let created_at_formatted : String?
//let key : String?
let image_url : String?
//let google_faces : [[[Int]]]?
//let type : String?
//let media_id : Int?
//let instagram_url : String?
//let processed_at : String?
//let lead_lead_key : String?
//let params : String?
//let has_dash : String?
//let hash : String?
//let user_edit_blur : String?
//let transcribe : String?
//let _type : String?
//let medic_id : String?
//let _hash : String?
//let name : String?
//let is_private : Bool?
//let is_protected : Bool?
//let status : String?
//let google_adult : Int?
//let medic_medic_key : String?
let cover_url : String?
//let content_id : String?
//let thumbnail_url : String?
//let booking_id : String?
//let tags : String?
//let has_transcribe : String?
//let user_id : Int?
//let updated_at : String?
//let has_hsl : String?
//let has_mp4 : String?
//let testimonial_photos_id : Int?
//let video_url : String?
//let testimonial_id : Int?
//let has_cover : String?
//let post_id : String?

enum CodingKeys: String, CodingKey {

  //  case device_id = "device_id"
//    case google_labels = "google_labels"
//    case device_key = "device_key"
//    case youtube_video_id = "youtube_video_id"
//    case created_at = "created_at"
//    case ticket_id = "ticket_id"
//    case created_at_formatted = "created_at_formatted"
//    case key = "key"
    case image_url = "image_url"
//    case google_faces = "google_faces"
//    case type = "type"
//    case media_id = "media_id"
//    case instagram_url = "instagram_url"
//    case processed_at = "processed_at"
//    case lead_key = "lead_key"
//    case params = "params"
//    case has_dash = "has_dash"
//    case hash = "hash"
//    case user_edit_blur = "user_edit_blur"
//    case transcribe = "transcribe"
//    case _type = "_type"
//    case medic_id = "medic_id"
//    case _hash = "_hash"
//    case name = "name"
//    case is_private = "is_private"
//    case is_protected = "is_protected"
//    case status = "status"
//    case google_adult = "google_adult"
//    case medic_key = "medic_key"
    case cover_url = "cover_url"
//    case content_id = "content_id"
//    case thumbnail_url = "thumbnail_url"
//    case booking_id = "booking_id"
//    case tags = "tags"
//    case has_transcribe = "has_transcribe"
//    case user_id = "user_id"
//    case updated_at = "updated_at"
//    case has_hsl = "has_hsl"
//    case has_mp4 = "has_mp4"
//    case testimonial_photos_id = "testimonial_photos_id"
//    case video_url = "video_url"
//    case testimonial_id = "testimonial_id"
//    case has_cover = "has_cover"
//    case post_id = "post_id"
}
}
struct Media_Base : Codable {
    let medias : [MediasResult]?

    enum CodingKeys: String, CodingKey {

        case medias = "medias"
    }

    

}
