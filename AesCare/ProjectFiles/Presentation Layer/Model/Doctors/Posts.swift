/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct Posts : Codable {
    let post_id : Int?
    let key : String?
    let status : String?
    let score : Double?
    let order : String?
    let body : String?
    let medic : Medic?
    //	let medic_key : String?
    //	let medic_id : String?
    let content_id : Int?
    let content : Content?
    let user : User1?
    let user_id : Int?
    let medias : [Medias]?
    let comments_count : Int?
    var comments : [Comments]?
    //let treatment_ids : [String]?
    let created_at : String?
    let created_at_formatted : String?
    //let treatments_name : [String]?
    var postType:Int?
    
    enum CodingKeys: String, CodingKey {
        
        case post_id = "post_id"
        case key = "key"
        case status = "status"
        case score = "score"
        case order = "order"
        case body = "body"
        case medic = "medic"
        //		case medic_key = "medic_key"
        //	case medic_id = "medic_id"
        case content_id = "content_id"
        case content = "content"
        case user = "user"
        case user_id = "user_id"
        case medias = "medias"
        case comments_count = "comments_count"
        case comments = "comments"
        //case treatment_ids = "treatment_ids"
        case created_at = "created_at"
        case created_at_formatted = "created_at_formatted"
        //case treatments_name = "treatments_name"
    }
    
    
    
}
