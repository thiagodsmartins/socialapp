

import Foundation

struct Medics : Codable {
//    let address_number : Int?
    let evaluation_average : Double?
//    let sbcp_nome : String?
//    let evaluation_facebook : String?
//    let booking_price : Int?
//    let facebook_page : String?
//    let follow_count : Int?
    let sbcp_categoria : String?
//    let evaluation_facebook_update_at : String?
//    let is_hidden_price : Bool?
//    let has_profile : Bool?
    let posts_count : Int?
//    let sbcp_cidade : String?
//    let evaluation_google : String?
    let medical_insurance : String?
//    let has_calendar : Bool?
//    let posts_instagram_count : Int?
//    let sbcp_estado : String?
//    let evaluation_google_update_at : String?
//    let count_profile_viewed_month : Int?
//    let has_interest_scheduling : Bool?
//    let last_post_created_at : String?
//    let sbcp_address : String?
//    let instagram_url : String?
//    let count_reviews_viewed_month : Int?
//    let has_waiting_list : Bool?
//    let processed_at : String?
//    let sbcp_phone : String?
//    let has_instagram_sync : Bool?
//    let count_contents_viewed_month : Int?
//    let has_noshow_fee : Bool?
//    let next_process_at : String?
//    let sbcp_email : String?
//    let count_stories : Int?
//    let count_bookings : Int?
//    let noshow_fee : String?
//    let processed_instagram_at : String?
//    let sbd_code : Int?
//    let evaluations_google_count : Int?
//    let cover_media_id : Int?
//    let has_online_service : Bool?
//    let next_process_instagram_at : String?
//    let sbd_name : String?
    let evaluations_facebook_count : Int?
//    let count_waiting_list : Int?
//    let has_calendar_enabled_at : String?
    let city_id : Int?
//    let sbd_phone : String?
//    let google_name : String?
//    let count_waiting_list_notification_read : Int?
//    let testimonials_count : Int?
//    let state_id : Int?
//    let sbd_address : String?
//    let google_address : String?
//    let count_waiting_list_notification_sent : Int?
//    let contents_count : Int?
//    let postal_code : String?
//    let sbd_district : String?
//    let google_phone : String?
//    let tags : [String]?
//    let score : Double?
//    let created_at : String?
//    let sbd_rqe : String?
//    let google_latitude : String?
//    let google_longitude : String?
//    let score_updated_at : String?
//    let updated_at : String?
//    let sbd_crm : String?
//    let latitude : String?
    let evaluations_count : Int?
//    let sbcp_json : Sbcp_json?
//    let status : String?
//    let specialty_id : Int?
//    let longitude : String?
//    let evaluations_with_contents_count : Int?
//    let sbcp_codigo : Int?
//    let place_id : String?
//    let whatsapp_phone : String?
//    let last_story_at : String?
//    let order : Double?
    let medic_id : Int?
//    let key : String?
    let distance : Double?
    let name : String?
//    let name_unaccent : String?
    let user_id : Int?
//    let old_id : Int?
//    let crm : String?
//    let gender : String?
//    let email : String?
//    let phone : String?
    let address : String?
    //let user : String?
    //let city : City?
    //let state : State?
    let cover_media : Cover_media?
     
//    let addresses : [String]?
//    let state_abbr : String?
//    let city_name : String?
//    let name_with_title : String?
//    let has_calendar_enabled_at_formatted : String?
//    let lat : Double?
//    let lng : Double?
//    let slug : String?
    let results_count : Int?
//    let eval_count : Int?
//    let evaluation_average_formatted : String?
    let calendar : [ModelCalender]? //= []
    lazy var arrayString : [String]? = {
        var fullNameArr = self.medical_insurance?.components(separatedBy: "#")
        return fullNameArr
    }()
    
    

    enum CodingKeys: String, CodingKey {

//        case address_number = "address_number"
        case evaluation_average = "evaluation_average"
//        case sbcp_nome = "sbcp_nome"
//        case evaluation_facebook = "evaluation_facebook"
 //       case booking_price = "booking_price"
  //      case facebook_page = "facebook_page"
 //       case follow_count = "follow_count"
        case sbcp_categoria = "sbcp_categoria"
//        case evaluation_facebook_update_at = "evaluation_facebook_update_at"
//        case is_hidden_price = "is_hidden_price"
//        case has_profile = "has_profile"
        case posts_count = "posts_count"
//        case sbcp_cidade = "sbcp_cidade"
//        case evaluation_google = "evaluation_google"
        case medical_insurance = "medical_insurance"
//        case has_calendar = "has_calendar"
//        case posts_instagram_count = "posts_instagram_count"
//        case sbcp_estado = "sbcp_estado"
//        case evaluation_google_update_at = "evaluation_google_update_at"
//        case count_profile_viewed_month = "count_profile_viewed_month"
//        case has_interest_scheduling = "has_interest_scheduling"
//        case last_post_created_at = "last_post_created_at"
//        case sbcp_address = "sbcp_address"
//        case instagram_url = "instagram_url"
//        case count_reviews_viewed_month = "count_reviews_viewed_month"
//        case has_waiting_list = "has_waiting_list"
//        case processed_at = "processed_at"
//        case sbcp_phone = "sbcp_phone"
//        case has_instagram_sync = "has_instagram_sync"
//        case count_contents_viewed_month = "count_contents_viewed_month"
//        case has_noshow_fee = "has_noshow_fee"
//        case next_process_at = "next_process_at"
//        case sbcp_email = "sbcp_email"
//        case count_stories = "count_stories"
//        case count_bookings = "count_bookings"
//        case noshow_fee = "noshow_fee"
//        case processed_instagram_at = "processed_instagram_at"
//        case sbd_code = "sbd_code"
//        case evaluations_google_count = "evaluations_google_count"
//        case cover_media_id = "cover_media_id"
//        case has_online_service = "has_online_service"
//        case next_process_instagram_at = "next_process_instagram_at"
//        case sbd_name = "sbd_name"
        case evaluations_facebook_count = "evaluations_facebook_count"
//        case count_waiting_list = "count_waiting_list"
//        case has_calendar_enabled_at = "has_calendar_enabled_at"
        case city_id = "city_id"
//        case sbd_phone = "sbd_phone"
//        case google_name = "google_name"
//        case count_waiting_list_notification_read = "count_waiting_list_notification_read"
//        case testimonials_count = "testimonials_count"
//        case state_id = "state_id"
//        case sbd_address = "sbd_address"
//        case google_address = "google_address"
//        case count_waiting_list_notification_sent = "count_waiting_list_notification_sent"
//        case contents_count = "contents_count"
//        case postal_code = "postal_code"
//        case sbd_district = "sbd_district"
//        case google_phone = "google_phone"
//        case tags = "tags"
//         case score = "score"
//        case created_at = "created_at"
//        case sbd_rqe = "sbd_rqe"
//        case google_latitude = "google_latitude"
//        case google_longitude = "google_longitude"
//        case score_updated_at = "score_updated_at"
//        case updated_at = "updated_at"
//        case sbd_crm = "sbd_crm"
//        case latitude = "latitude"
        case evaluations_count = "evaluations_count"
//        case sbcp_json = "sbcp_json"
//        case status = "status"
//        case specialty_id = "specialty_id"
//        case longitude = "longitude"
//        case evaluations_with_contents_count = "evaluations_with_contents_count"
//        case sbcp_codigo = "sbcp_codigo"
//        case place_id = "place_id"
//        case whatsapp_phone = "whatsapp_phone"
//        case last_story_at = "last_story_at"
//        case order = "order"
        case medic_id = "medic_id"
//        case key = "key"
        case distance = "distance"
        case name = "name"
//        case name_unaccent = "name_unaccent"
        case user_id = "user_id"
//        case old_id = "old_id"
//        case crm = "crm"
//        case gender = "gender"
//        case email = "email"
//        case phone = "phone"
        case address = "address"
        //case user = "user"
        //case city = "city"
        //case state = "state"
        case cover_media = "cover_media"
         
//        case addresses = "addresses"
//        case state_abbr = "state_abbr"
//        case city_name = "city_name"
//        case name_with_title = "name_with_title"
//        case has_calendar_enabled_at_formatted = "has_calendar_enabled_at_formatted"
//        case lat = "lat"
//        case lng = "lng"
//        case slug = "slug"
       case results_count = "results_count"
//        case eval_count = "eval_count"
//        case evaluation_average_formatted = "evaluation_average_formatted"
        case calendar = "calendar"
 
    }
}
/*
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
      //  address_number = try values.decodeIfPresent(Int.self, forKey: .address_number)
        evaluation_average = try values.decodeIfPresent(Double.self, forKey: .evaluation_average)
//        sbcp_nome = try values.decodeIfPresent(String.self, forKey: .sbcp_nome)
//        evaluation_facebook = try values.decodeIfPresent(String.self, forKey: .evaluation_facebook)
//        booking_price = try values.decodeIfPresent(Int.self, forKey: .booking_price)
//        facebook_page = try values.decodeIfPresent(String.self, forKey: .facebook_page)
//        follow_count = try values.decodeIfPresent(Int.self, forKey: .follow_count)
        sbcp_categoria = try values.decodeIfPresent(String.self, forKey: .sbcp_categoria)
//        evaluation_facebook_update_at = try values.decodeIfPresent(String.self, forKey: .evaluation_facebook_update_at)
//        is_hidden_price = try values.decodeIfPresent(Bool.self, forKey: .is_hidden_price)
//        has_profile = try values.decodeIfPresent(Bool.self, forKey: .has_profile)
 //       posts_count = try values.decodeIfPresent(Int.self, forKey: .posts_count)
//        sbcp_cidade = try values.decodeIfPresent(String.self, forKey: .sbcp_cidade)
//        evaluation_google = try values.decodeIfPresent(String.self, forKey: .evaluation_google)
        medical_insurance = try values.decodeIfPresent(String.self, forKey: .medical_insurance)
//        has_calendar = try values.decodeIfPresent(Bool.self, forKey: .has_calendar)
//        posts_instagram_count = try values.decodeIfPresent(Int.self, forKey: .posts_instagram_count)
//        sbcp_estado = try values.decodeIfPresent(String.self, forKey: .sbcp_estado)
//        evaluation_google_update_at = try values.decodeIfPresent(String.self, forKey: .evaluation_google_update_at)
//        count_profile_viewed_month = try values.decodeIfPresent(Int.self, forKey: .count_profile_viewed_month)
//        has_interest_scheduling = try values.decodeIfPresent(Bool.self, forKey: .has_interest_scheduling)
//        last_post_created_at = try values.decodeIfPresent(String.self, forKey: .last_post_created_at)
//        sbcp_address = try values.decodeIfPresent(String.self, forKey: .sbcp_address)
//        instagram_url = try values.decodeIfPresent(String.self, forKey: .instagram_url)
//
//        count_reviews_viewed_month = try values.decodeIfPresent(Int.self, forKey: .count_reviews_viewed_month)
//        has_waiting_list = try values.decodeIfPresent(Bool.self, forKey: .has_waiting_list)
//        processed_at = try values.decodeIfPresent(String.self, forKey: .processed_at)
//        sbcp_phone = try values.decodeIfPresent(String.self, forKey: .sbcp_phone)
//        has_instagram_sync = try values.decodeIfPresent(Bool.self, forKey: .has_instagram_sync)
//        count_contents_viewed_month = try values.decodeIfPresent(Int.self, forKey: .count_contents_viewed_month)
//        has_noshow_fee = try values.decodeIfPresent(Bool.self, forKey: .has_noshow_fee)
//        next_process_at = try values.decodeIfPresent(String.self, forKey: .next_process_at)
//        sbcp_email = try values.decodeIfPresent(String.self, forKey: .sbcp_email)
//        count_stories = try values.decodeIfPresent(Int.self, forKey: .count_stories)
//        count_bookings = try values.decodeIfPresent(Int.self, forKey: .count_bookings)
//        noshow_fee = try values.decodeIfPresent(String.self, forKey: .noshow_fee)
//        processed_instagram_at = try values.decodeIfPresent(String.self, forKey: .processed_instagram_at)
//        sbd_code = try values.decodeIfPresent(Int.self, forKey: .sbd_code)
//        evaluations_google_count = try values.decodeIfPresent(Int.self, forKey: .evaluations_google_count)
//        cover_media_id = try values.decodeIfPresent(Int.self, forKey: .cover_media_id)
//        has_online_service = try values.decodeIfPresent(Bool.self, forKey: .has_online_service)
//        next_process_instagram_at = try values.decodeIfPresent(String.self, forKey: .next_process_instagram_at)
//        sbd_name = try values.decodeIfPresent(String.self, forKey: .sbd_name)
//        evaluations_facebook_count = try values.decodeIfPresent(Int.self, forKey: .evaluations_facebook_count)
//        count_waiting_list = try values.decodeIfPresent(Int.self, forKey: .count_waiting_list)
//        has_calendar_enabled_at = try values.decodeIfPresent(String.self, forKey: .has_calendar_enabled_at)
 
city_id = try values.decodeIfPresent(Int.self, forKey: .city_id)
//        sbd_phone = try values.decodeIfPresent(String.self, forKey: .sbd_phone)
//        google_name = try values.decodeIfPresent(String.self, forKey: .google_name)
//        count_waiting_list_notification_read = try values.decodeIfPresent(Int.self, forKey: .count_waiting_list_notification_read)
//        testimonials_count = try values.decodeIfPresent(Int.self, forKey: .testimonials_count)
//        state_id = try values.decodeIfPresent(Int.self, forKey: .state_id)
//        sbd_address = try values.decodeIfPresent(String.self, forKey: .sbd_address)
//        google_address = try values.decodeIfPresent(String.self, forKey: .google_address)
//        count_waiting_list_notification_sent = try values.decodeIfPresent(Int.self, forKey: .count_waiting_list_notification_sent)
//        contents_count = try values.decodeIfPresent(Int.self, forKey: .contents_count)
//        postal_code = try values.decodeIfPresent(String.self, forKey: .postal_code)
//        sbd_district = try values.decodeIfPresent(String.self, forKey: .sbd_district)
//        google_phone = try values.decodeIfPresent(String.self, forKey: .google_phone)
//        tags = try values.decodeIfPresent([String].self, forKey: .tags)
//        score = try values.decodeIfPresent(Double.self, forKey: .score)
//        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
//        sbd_rqe = try values.decodeIfPresent(String.self, forKey: .sbd_rqe)
//        google_latitude = try values.decodeIfPresent(String.self, forKey: .google_latitude)
//        google_longitude = try values.decodeIfPresent(String.self, forKey: .google_longitude)
//        score_updated_at = try values.decodeIfPresent(String.self, forKey: .score_updated_at)
//        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
//        sbd_crm = try values.decodeIfPresent(String.self, forKey: .sbd_crm)
//        latitude = try values.decodeIfPresent(String.self, forKey: .latitude)
//        evaluations_count = try values.decodeIfPresent(Int.self, forKey: .evaluations_count)
//        sbcp_json = try values.decodeIfPresent(Sbcp_json.self, forKey: .sbcp_json)
//
//        status = try values.decodeIfPresent(String.self, forKey: .status)
//        specialty_id = try values.decodeIfPresent(Int.self, forKey: .specialty_id)
//        longitude = try values.decodeIfPresent(String.self, forKey: .longitude)
//        evaluations_with_contents_count = try values.decodeIfPresent(Int.self, forKey: .evaluations_with_contents_count)
//        sbcp_codigo = try values.decodeIfPresent(Int.self, forKey: .sbcp_codigo)
//        place_id = try values.decodeIfPresent(String.self, forKey: .place_id)
//        whatsapp_phone = try values.decodeIfPresent(String.self, forKey: .whatsapp_phone)
//        last_story_at = try values.decodeIfPresent(String.self, forKey: .last_story_at)
//        order = try values.decodeIfPresent(Double.self, forKey: .order)
        medic_id = try values.decodeIfPresent(Int.self, forKey: .medic_id)
//        key = try values.decodeIfPresent(String.self, forKey: .key)
        distance = try values.decodeIfPresent(Double.self, forKey: .distance)
        name = try values.decodeIfPresent(String.self, forKey: .name)
//        name_unaccent = try values.decodeIfPresent(String.self, forKey: .name_unaccent)
        user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
//        old_id = try values.decodeIfPresent(Int.self, forKey: .old_id)
//        crm = try values.decodeIfPresent(String.self, forKey: .crm)
//        gender = try values.decodeIfPresent(String.self, forKey: .gender)
//        email = try values.decodeIfPresent(String.self, forKey: .email)
//        phone = try values.decodeIfPresent(String.self, forKey: .phone)
     
 address = try values.decodeIfPresent(String.self, forKey: .address)
        //user = try values.decodeIfPresent(String.self, forKey: .user)
        //city = try values.decodeIfPresent(City.self, forKey: .city)
        //state = try values.decodeIfPresent(State.self, forKey: .state)
        cover_media = try values.decodeIfPresent(Cover_media.self, forKey: .cover_media)
        
//        addresses = try values.decodeIfPresent([String].self, forKey: .addresses)
//        state_abbr = try values.decodeIfPresent(String.self, forKey: .state_abbr)
//        city_name = try values.decodeIfPresent(String.self, forKey: .city_name)
//        name_with_title = try values.decodeIfPresent(String.self, forKey: .name_with_title)
//        has_calendar_enabled_at_formatted = try values.decodeIfPresent(String.self, forKey: .has_calendar_enabled_at_formatted)
//        lat = try values.decodeIfPresent(Double.self, forKey: .lat)
//        lng = try values.decodeIfPresent(Double.self, forKey: .lng)
//        slug = try values.decodeIfPresent(String.self, forKey: .slug)
//        results_count = try values.decodeIfPresent(Int.self, forKey: .results_count)
//        eval_count = try values.decodeIfPresent(Int.self, forKey: .eval_count)
//        evaluation_average_formatted = try values.decodeIfPresent(String.self, forKey: .evaluation_average_formatted)
//        if let calendar = try values.decodeIfPresent([ModelCalender].self, forKey: .calendar){
//            if calendar.count == 0{
//                self.calendar = []
//            }else{
//            self.calendar = calendar
//            }
//        }else{
//            self.calendar = []
//        }
 
    }

}

struct Medics : Codable {
    let medic_id : Int?
    let user_id : Int?
    let crm : String?
    let gender : String?
    
    let count_waiting_list : Int?
    let email : String?
    let phone : String?
    let state_abbr : String?
    let city_name : String?
    let name : String?
    let key : String?
    let medic_key : String?
   
    let city_id : Int?
    let name_with_title : String?
    let slug : String?
    let cover_media_id : Int?
    let cover_media : Cover_media?
    let image_url : String?
    let has_profile : Bool?
    let evaluation_average : Double?
    let evaluation_average_formatted : String?
    let testimonials_count : Int?
    let results_count : Int?
    let has_interest_scheduling : Bool?
    let has_waiting_list : Bool?
    let has_calendar : Bool?
    let has_online_service : Bool?
    let has_noshow_fee : Bool?
    let noshow_fee : Double?
    let instagram_url : String?
    let posts_count : Int?
    let posts_instagram_count : Int?
    let count_stories : Int?
    let count_waiting_list_notification_read : Int?
    let count_waiting_list_notification_sent : Int?
    let has_instagram_sync : Bool?
    let specialties : [Specialties]?

    enum CodingKeys: String, CodingKey {

        case medic_id = "medic_id"
        case user_id = "user_id"
        case crm = "crm"
        case gender = "gender"
       
        case count_waiting_list = "count_waiting_list"
        case email = "email"
        case phone = "phone"
        case state_abbr = "state_abbr"
        case city_name = "city_name"
        case name = "name"
        case key = "key"
        case medic_key = "medic_key"
         
        case city_id = "city_id"
        case name_with_title = "name_with_title"
        case slug = "slug"
        case cover_media_id = "cover_media_id"
        case cover_media = "cover_media"
        case image_url = "image_url"
        case has_profile = "has_profile"
        case evaluation_average = "evaluation_average"
        case evaluation_average_formatted = "evaluation_average_formatted"
        case testimonials_count = "testimonials_count"
        case results_count = "results_count"
        case has_interest_scheduling = "has_interest_scheduling"
        case has_waiting_list = "has_waiting_list"
        case has_calendar = "has_calendar"
        case has_online_service = "has_online_service"
        case has_noshow_fee = "has_noshow_fee"
        case noshow_fee = "noshow_fee"
        case instagram_url = "instagram_url"
        case posts_count = "posts_count"
        case posts_instagram_count = "posts_instagram_count"
        case count_stories = "count_stories"
        case count_waiting_list_notification_read = "count_waiting_list_notification_read"
        case count_waiting_list_notification_sent = "count_waiting_list_notification_sent"
        case has_instagram_sync = "has_instagram_sync"
        case specialties = "specialties"
 
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        medic_id = try values.decodeIfPresent(Int.self, forKey: .medic_id)
        user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
        crm = try values.decodeIfPresent(String.self, forKey: .crm)
        gender = try values.decodeIfPresent(String.self, forKey: .gender)
        
        count_waiting_list = try values.decodeIfPresent(Int.self, forKey: .count_waiting_list)
        email = try values.decodeIfPresent(String.self, forKey: .email)
        phone = try values.decodeIfPresent(String.self, forKey: .phone)
        state_abbr = try values.decodeIfPresent(String.self, forKey: .state_abbr)
        city_name = try values.decodeIfPresent(String.self, forKey: .city_name)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        key = try values.decodeIfPresent(String.self, forKey: .key)
        medic_key = try values.decodeIfPresent(String.self, forKey: .medic_key)
        
        city_id = try values.decodeIfPresent(Int.self, forKey: .city_id)
        name_with_title = try values.decodeIfPresent(String.self, forKey: .name_with_title)
        slug = try values.decodeIfPresent(String.self, forKey: .slug)
        cover_media_id = try values.decodeIfPresent(Int.self, forKey: .cover_media_id)
        //cover_media = try values.decodeIfPresent(Cover_media.self, forKey: .cover_media)
        image_url = try values.decodeIfPresent(String.self, forKey: .image_url)
        has_profile = try values.decodeIfPresent(Bool.self, forKey: .has_profile)
        evaluation_average = try values.decodeIfPresent(Double.self, forKey: .evaluation_average)
        evaluation_average_formatted = try values.decodeIfPresent(String.self, forKey: .evaluation_average_formatted)
        testimonials_count = try values.decodeIfPresent(Int.self, forKey: .testimonials_count)
        results_count = try values.decodeIfPresent(Int.self, forKey: .results_count)
        has_interest_scheduling = try values.decodeIfPresent(Bool.self, forKey: .has_interest_scheduling)
        has_waiting_list = try values.decodeIfPresent(Bool.self, forKey: .has_waiting_list)
        has_calendar = try values.decodeIfPresent(Bool.self, forKey: .has_calendar)
        has_online_service = try values.decodeIfPresent(Bool.self, forKey: .has_online_service)
        has_noshow_fee = try values.decodeIfPresent(Bool.self, forKey: .has_noshow_fee)
        noshow_fee = try values.decodeIfPresent(Double.self, forKey: .noshow_fee)
        instagram_url = try values.decodeIfPresent(String.self, forKey: .instagram_url)
        posts_count = try values.decodeIfPresent(Int.self, forKey: .posts_count)
        posts_instagram_count = try values.decodeIfPresent(Int.self, forKey: .posts_instagram_count)
        count_stories = try values.decodeIfPresent(Int.self, forKey: .count_stories)
        count_waiting_list_notification_read = try values.decodeIfPresent(Int.self, forKey: .count_waiting_list_notification_read)
        count_waiting_list_notification_sent = try values.decodeIfPresent(Int.self, forKey: .count_waiting_list_notification_sent)
        has_instagram_sync = try values.decodeIfPresent(Bool.self, forKey: .has_instagram_sync)
        specialties = try values.decodeIfPresent([Specialties].self, forKey: .specialties)
 
    }

}
 */
