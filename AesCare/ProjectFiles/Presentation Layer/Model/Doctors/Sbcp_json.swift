/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct Sbcp_json : Codable {
	let afiliacao1 : String?
	let afiliacao2 : String?
	let afiliacao3 : String?
	let afiliacao4 : String?
	let afiliacao5 : String?
	let cRM : String?
	let categoria : String?
	let cidade : String?
	let cidade2 : String?
	let email : String?
	let endereco : String?
	let endereco2 : String?
	let erro : String?
	let especializacoes : String?
	let formacao : String?
	let homePage : String?
	let imagem : String?
	let nome : String?
	let perfilMedico : String?
	let telefone1 : String?
	let telefone2 : String?
	let uF : String?
	let uF2 : String?
	let video : String?

	enum CodingKeys: String, CodingKey {

		case afiliacao1 = "Afiliacao1"
		case afiliacao2 = "Afiliacao2"
		case afiliacao3 = "Afiliacao3"
		case afiliacao4 = "Afiliacao4"
		case afiliacao5 = "Afiliacao5"
		case cRM = "CRM"
		case categoria = "Categoria"
		case cidade = "Cidade"
		case cidade2 = "Cidade2"
		case email = "Email"
		case endereco = "Endereco"
		case endereco2 = "Endereco2"
		case erro = "Erro"
		case especializacoes = "Especializacoes"
		case formacao = "Formacao"
		case homePage = "HomePage"
		case imagem = "Imagem"
		case nome = "Nome"
		case perfilMedico = "PerfilMedico"
		case telefone1 = "Telefone1"
		case telefone2 = "Telefone2"
		case uF = "UF"
		case uF2 = "UF2"
		case video = "Video"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		afiliacao1 = try values.decodeIfPresent(String.self, forKey: .afiliacao1)
		afiliacao2 = try values.decodeIfPresent(String.self, forKey: .afiliacao2)
		afiliacao3 = try values.decodeIfPresent(String.self, forKey: .afiliacao3)
		afiliacao4 = try values.decodeIfPresent(String.self, forKey: .afiliacao4)
		afiliacao5 = try values.decodeIfPresent(String.self, forKey: .afiliacao5)
		cRM = try values.decodeIfPresent(String.self, forKey: .cRM)
		categoria = try values.decodeIfPresent(String.self, forKey: .categoria)
		cidade = try values.decodeIfPresent(String.self, forKey: .cidade)
		cidade2 = try values.decodeIfPresent(String.self, forKey: .cidade2)
		email = try values.decodeIfPresent(String.self, forKey: .email)
		endereco = try values.decodeIfPresent(String.self, forKey: .endereco)
		endereco2 = try values.decodeIfPresent(String.self, forKey: .endereco2)
		erro = try values.decodeIfPresent(String.self, forKey: .erro)
		especializacoes = try values.decodeIfPresent(String.self, forKey: .especializacoes)
		formacao = try values.decodeIfPresent(String.self, forKey: .formacao)
		homePage = try values.decodeIfPresent(String.self, forKey: .homePage)
		imagem = try values.decodeIfPresent(String.self, forKey: .imagem)
		nome = try values.decodeIfPresent(String.self, forKey: .nome)
		perfilMedico = try values.decodeIfPresent(String.self, forKey: .perfilMedico)
		telefone1 = try values.decodeIfPresent(String.self, forKey: .telefone1)
		telefone2 = try values.decodeIfPresent(String.self, forKey: .telefone2)
		uF = try values.decodeIfPresent(String.self, forKey: .uF)
		uF2 = try values.decodeIfPresent(String.self, forKey: .uF2)
		video = try values.decodeIfPresent(String.self, forKey: .video)
	}

}