//
//  Calender.swift
//  AesCare
//
//  Created by Gali Srikanth on 11/09/20.
//

import Foundation
struct ModelCalender : Codable {
    var user_id : Int? = 0
    let date_start : String?
    let medic_id : Int?
    let is_online_service : Bool?
    let noshow_fee : String?
    let created_by_ip : String?
    let has_noshow_fee : Bool?
    let cep : String?
    let street : String?
    let key : String?
    let date_end : String?
    let timezone : String?
    let medic_calendar_slot_id : Int?
    let created_at : String?
    let created_by_user_id : Int?
    let specialty_id : Int?
    let city_id : Int?
    let address_number : Int?
    let status : String?

    enum CodingKeys: String, CodingKey {

        case user_id = "user_id"
        case date_start = "date_start"
        case medic_id = "medic_id"
        case is_online_service = "is_online_service"
        case noshow_fee = "noshow_fee"
        case created_by_ip = "created_by_ip"
        case has_noshow_fee = "has_noshow_fee"
        case cep = "cep"
        case street = "street"
        case key = "key"
        case date_end = "date_end"
        case timezone = "timezone"
        case medic_calendar_slot_id = "medic_calendar_slot_id"
        case created_at = "created_at"
        case created_by_user_id = "created_by_user_id"
        case specialty_id = "specialty_id"
        case city_id = "city_id"
        case address_number = "address_number"
        case status = "status"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
        date_start = try values.decodeIfPresent(String.self, forKey: .date_start)
        medic_id = try values.decodeIfPresent(Int.self, forKey: .medic_id)
        is_online_service = try values.decodeIfPresent(Bool.self, forKey: .is_online_service)
        noshow_fee = try values.decodeIfPresent(String.self, forKey: .noshow_fee)
        created_by_ip = try values.decodeIfPresent(String.self, forKey: .created_by_ip)
        has_noshow_fee = try values.decodeIfPresent(Bool.self, forKey: .has_noshow_fee)
        cep = try values.decodeIfPresent(String.self, forKey: .cep)
        street = try values.decodeIfPresent(String.self, forKey: .street)
        key = try values.decodeIfPresent(String.self, forKey: .key)
        date_end = try values.decodeIfPresent(String.self, forKey: .date_end)
        timezone = try values.decodeIfPresent(String.self, forKey: .timezone)
        medic_calendar_slot_id = try values.decodeIfPresent(Int.self, forKey: .medic_calendar_slot_id)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        created_by_user_id = try values.decodeIfPresent(Int.self, forKey: .created_by_user_id)
        specialty_id = try values.decodeIfPresent(Int.self, forKey: .specialty_id)
        city_id = try values.decodeIfPresent(Int.self, forKey: .city_id)
        address_number = try values.decodeIfPresent(Int.self, forKey: .address_number)
        status = try values.decodeIfPresent(String.self, forKey: .status)
    }

}
