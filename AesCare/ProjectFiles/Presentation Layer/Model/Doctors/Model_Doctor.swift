//
//  Model_Doctor.swift
//  AesCare
//
//  Created by Gali Srikanth on 01/09/20.
//

import Foundation
struct Model_Doctor : Codable {
    let code : Int?
    let medics : [Medics]?
  
    enum CodingKeys: String, CodingKey {

        case code = "code"
        case medics = "medics"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        medics = try values.decodeIfPresent([Medics].self, forKey: .medics)
    }
}
