/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct Content : Codable {
	let content_id : Int?
	let key : String?
	let url_youtube : String?
	let media_type : String?
	let cover_image_url : String?
	let title : String?
	let is_media : String?
	let summary : String?
    let youtube_video_id : String?
    var isLoaded:Bool = false

	enum CodingKeys: String, CodingKey {

		case content_id = "content_id"
		case key = "key"
		case url_youtube = "url_youtube"
		case media_type = "media_type"
		case cover_image_url = "cover_image_url"
		case title = "title"
		case is_media = "is_media"
		case summary = "summary"
        case youtube_video_id = "youtube_video_id"
	}

//	init(from decoder: Decoder) throws {
//		let values = try decoder.container(keyedBy: CodingKeys.self)
//		content_id = try values.decodeIfPresent(Int.self, forKey: .content_id)
//		key = try values.decodeIfPresent(String.self, forKey: .key)
//		url_youtube = try values.decodeIfPresent(String.self, forKey: .url_youtube)
//		media_type = try values.decodeIfPresent(String.self, forKey: .media_type)
//		cover_image_url = try values.decodeIfPresent(String.self, forKey: .cover_image_url)
//		title = try values.decodeIfPresent(String.self, forKey: .title)
//		is_media = try values.decodeIfPresent(String.self, forKey: .is_media)
//		summary = try values.decodeIfPresent(String.self, forKey: .summary)
//	}

}
