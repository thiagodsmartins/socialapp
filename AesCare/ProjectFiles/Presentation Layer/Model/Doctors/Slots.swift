/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct Slots : Codable {
	let medic_calendar_slot_id : Int?
	let key : String?
	let city_id : Int?
	let user_id : String?
	let user_name : String?
	let specialty_id : Int?
	let city_name : String?
	let state_abbr : String?
	let noshow_fee : Int?
	let noshow_fee_formatted : String?
	let has_noshow_fee : Bool?
	let medic : Medic?
	let medic_key : String?
	let medic_id : Int?
	let booking : String?
	let address_number : Int?
	let street : String?
	let cep : String?
	let status : String?
	let is_online_service : Bool?
	let date_start : String?
	let date_end : String?
	let timezone : String?
	let is_cancellable : Bool?
	let date_start_formatted : String?
	let date_end_formatted : String?
	let created_by_user_id : Int?
	let created_by_username : String?
	let created_at : String?
	let created_at_formatted : String?

	enum CodingKeys: String, CodingKey {

		case medic_calendar_slot_id = "medic_calendar_slot_id"
		case key = "key"
		case city_id = "city_id"
		case user_id = "user_id"
		case user_name = "user_name"
		case specialty_id = "specialty_id"
		case city_name = "city_name"
		case state_abbr = "state_abbr"
		case noshow_fee = "noshow_fee"
		case noshow_fee_formatted = "noshow_fee_formatted"
		case has_noshow_fee = "has_noshow_fee"
		case medic = "medic"
		case medic_key = "medic_key"
		case medic_id = "medic_id"
		case booking = "booking"
		case address_number = "address_number"
		case street = "street"
		case cep = "cep"
		case status = "status"
		case is_online_service = "is_online_service"
		case date_start = "date_start"
		case date_end = "date_end"
		case timezone = "timezone"
		case is_cancellable = "is_cancellable"
		case date_start_formatted = "date_start_formatted"
		case date_end_formatted = "date_end_formatted"
		case created_by_user_id = "created_by_user_id"
		case created_by_username = "created_by_username"
		case created_at = "created_at"
		case created_at_formatted = "created_at_formatted"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		medic_calendar_slot_id = try values.decodeIfPresent(Int.self, forKey: .medic_calendar_slot_id)
		key = try values.decodeIfPresent(String.self, forKey: .key)
		city_id = try values.decodeIfPresent(Int.self, forKey: .city_id)
		user_id = try values.decodeIfPresent(String.self, forKey: .user_id)
		user_name = try values.decodeIfPresent(String.self, forKey: .user_name)
		specialty_id = try values.decodeIfPresent(Int.self, forKey: .specialty_id)
		city_name = try values.decodeIfPresent(String.self, forKey: .city_name)
		state_abbr = try values.decodeIfPresent(String.self, forKey: .state_abbr)
		noshow_fee = try values.decodeIfPresent(Int.self, forKey: .noshow_fee)
		noshow_fee_formatted = try values.decodeIfPresent(String.self, forKey: .noshow_fee_formatted)
		has_noshow_fee = try values.decodeIfPresent(Bool.self, forKey: .has_noshow_fee)
		medic = try values.decodeIfPresent(Medic.self, forKey: .medic)
		medic_key = try values.decodeIfPresent(String.self, forKey: .medic_key)
		medic_id = try values.decodeIfPresent(Int.self, forKey: .medic_id)
		booking = try values.decodeIfPresent(String.self, forKey: .booking)
		address_number = try values.decodeIfPresent(Int.self, forKey: .address_number)
		street = try values.decodeIfPresent(String.self, forKey: .street)
		cep = try values.decodeIfPresent(String.self, forKey: .cep)
		status = try values.decodeIfPresent(String.self, forKey: .status)
		is_online_service = try values.decodeIfPresent(Bool.self, forKey: .is_online_service)
		date_start = try values.decodeIfPresent(String.self, forKey: .date_start)
		date_end = try values.decodeIfPresent(String.self, forKey: .date_end)
		timezone = try values.decodeIfPresent(String.self, forKey: .timezone)
		is_cancellable = try values.decodeIfPresent(Bool.self, forKey: .is_cancellable)
		date_start_formatted = try values.decodeIfPresent(String.self, forKey: .date_start_formatted)
		date_end_formatted = try values.decodeIfPresent(String.self, forKey: .date_end_formatted)
		created_by_user_id = try values.decodeIfPresent(Int.self, forKey: .created_by_user_id)
		created_by_username = try values.decodeIfPresent(String.self, forKey: .created_by_username)
		created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
		created_at_formatted = try values.decodeIfPresent(String.self, forKey: .created_at_formatted)
	}

}
