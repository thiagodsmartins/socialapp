/* 
 Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com
 
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 
 For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar
 
 */

import Foundation
struct Cover_media : Codable {
    var key : String? = ""
    var created_at : String? = ""
    //	let created_at_formatted : String?
    let user_id : Int?
    //	let status : String?
    //	let hash : String?
    //	let _hash : String?
    var type : String? = ""
    //	let _type : String?
    //let testimonial_id : String?
    var post_id : String? = ""
    var content_id : String? = ""
    var video_url : String? = ""
    let image_url : String?
    let thumbnail_url : String?
    let cover_url : String?
    
    enum CodingKeys: String, CodingKey {
        
        case key = "key"
        case created_at = "created_at"
        //		case created_at_formatted = "created_at_formatted"
        case user_id = "user_id"
        //		case status = "status"
        //		case hash = "hash"
        //		case _hash = "_hash"
        case type = "type"
        //		case _type = "_type"
        //case testimonial_id = "testimonial_id"
        case post_id = "post_id"
        case content_id = "content_id"
        case video_url = "video_url"
        case image_url = "image_url"
        case thumbnail_url = "thumbnail_url"
        case cover_url = "cover_url"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
        key = try values.decodeIfPresent(String.self, forKey: .key)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        type = try values.decodeIfPresent(String.self, forKey: .type)
        post_id = try values.decodeIfPresent(String.self, forKey: .post_id)
        content_id = try values.decodeIfPresent(String.self, forKey: .content_id)
        video_url = try values.decodeIfPresent(String.self, forKey: .video_url)
        image_url = try values.decodeIfPresent(String.self, forKey: .image_url)
        thumbnail_url = try values.decodeIfPresent(String.self, forKey: .thumbnail_url)
        cover_url = try values.decodeIfPresent(String.self, forKey: .cover_url)
        
    }
    
    
}
