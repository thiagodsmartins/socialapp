/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct Comments : Codable {
	let comment_id : Int?
	let related_id : Int?
	let content : String?
	let likes_count : Int?
	let created_at : String?
	let comments_count : Int?
	let liked : Bool?
	let user : User?

	enum CodingKeys: String, CodingKey {

		case comment_id = "comment_id"
		case related_id = "related_id"
		case content = "content"
		case likes_count = "likes_count"
		case created_at = "created_at"
		case comments_count = "comments_count"
		case liked = "liked"
		case user = "user"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		comment_id = try values.decodeIfPresent(Int.self, forKey: .comment_id)
		related_id = try values.decodeIfPresent(Int.self, forKey: .related_id)
		content = try values.decodeIfPresent(String.self, forKey: .content)
		likes_count = try values.decodeIfPresent(Int.self, forKey: .likes_count)
		created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
		comments_count = try values.decodeIfPresent(Int.self, forKey: .comments_count)
		liked = try values.decodeIfPresent(Bool.self, forKey: .liked)
		user = try values.decodeIfPresent(User.self, forKey: .user)
	}

}