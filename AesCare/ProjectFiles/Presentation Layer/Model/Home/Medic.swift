/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct Medic : Codable {
//	let medic_id : Int?
//	let user_id : String?
//	let crm : String?
//	let gender : String?
//	let count_waiting_list : Int?
//	let email : String?
//	let phone : String?
//	let state_abbr : String?
//	let city_name : String?
	let name : String?
//	let key : String?
//	let medic_key : String?
//	let city_id : Int?
//	let name_with_title : String?
//	let slug : String?
//	let cover_media_id : Int?
	let cover_media : Cover_media?
	let image_url : String?
//	let has_profile : Bool?
//	let evaluation_average : Double?
//	let evaluation_average_formatted : String?
//	let testimonials_count : Int?
//	let results_count : Int?
//	let has_interest_scheduling : Bool?
//	let has_waiting_list : Bool?
//	let has_calendar : Bool?
//	let has_online_service : String?
//	let has_noshow_fee : Bool?
//	let noshow_fee : String?
//	let instagram_url : String?
//	let posts_count : Int?
//	let posts_instagram_count : Int?
//	let count_stories : Int?
//	let count_waiting_list_notification_read : Int?
//	let count_waiting_list_notification_sent : Int?
//	let has_instagram_sync : Bool?
//	let specialties : [Specialties]?

	enum CodingKeys: String, CodingKey {

//		case medic_id = "medic_id"
//		case user_id = "user_id"
//		case crm = "crm"
//		case gender = "gender"
//		case count_waiting_list = "count_waiting_list"
//		case email = "email"
//		case phone = "phone"
//		case state_abbr = "state_abbr"
//		case city_name = "city_name"
		case name = "name"
//		case key = "key"
//		case medic_key = "medic_key"
//		case city_id = "city_id"
//		case name_with_title = "name_with_title"
//		case slug = "slug"
//		case cover_media_id = "cover_media_id"
		case cover_media = "cover_media"
		case image_url = "image_url"
//		case has_profile = "has_profile"
//		case evaluation_average = "evaluation_average"
//		case evaluation_average_formatted = "evaluation_average_formatted"
//		case testimonials_count = "testimonials_count"
//		case results_count = "results_count"
//		case has_interest_scheduling = "has_interest_scheduling"
//		case has_waiting_list = "has_waiting_list"
//		case has_calendar = "has_calendar"
//		case has_online_service = "has_online_service"
//		case has_noshow_fee = "has_noshow_fee"
//		case noshow_fee = "noshow_fee"
//		case instagram_url = "instagram_url"
//		case posts_count = "posts_count"
//		case posts_instagram_count = "posts_instagram_count"
//		case count_stories = "count_stories"
//		case count_waiting_list_notification_read = "count_waiting_list_notification_read"
//		case count_waiting_list_notification_sent = "count_waiting_list_notification_sent"
//		case has_instagram_sync = "has_instagram_sync"
//		case specialties = "specialties"
	}


}
