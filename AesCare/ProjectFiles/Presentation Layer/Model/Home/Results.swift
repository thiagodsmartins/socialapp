/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct ResultsFirst : Codable {
	let card_type : String?
	let title : String?
	let testimonial_id : Int?
	let result_id : Int?
	let user_id : Int?
	let key : String?
	let liked : Bool?
	let likes_count : Int?
	let published : Bool?
	let status : String?
	let image_id : String?
	let worth_it : String?
	let image_key : String?
	let payment_type : String?
	let order : String?
	let distance : Double?
	let auto_title : String?
	let description : String?
	let keywords : String?
	let cover_media_key : String?
	let followed : Bool?
	let treatment_value : String?
	let evaluation_result : Int?
	let comments_count : Int?
	let created_at_formated : String?
	let city : City?
	let city_id : Int?
	let city_name : String?
	let state_abbr : String?
	let treatments : [Treatments]?
	let slug : String?
	let medic_name : String?
	let medic_id : Int?
	let medic_key : String?
	let medic_text : String?
	let medic : Medic?
	//let user : User?
	let image_url : String?
	var comments : [Comments]?
	let treatment_name : String?
	let specialty_id : Int?
	let specialty_name : String?
	let specialty : Specialty?
	let cover_media_id : Int?
    var isClickedEye : Bool = false
    var evaluation_average :Double?

	enum CodingKeys: String, CodingKey {

		case card_type = "card_type"
		case title = "title"
		case testimonial_id = "testimonial_id"
		case result_id = "result_id"
		case user_id = "user_id"
		case key = "key"
		case liked = "liked"
		case likes_count = "likes_count"
		case published = "published"
		case status = "status"
		case image_id = "image_id"
		case worth_it = "worth_it"
		case image_key = "image_key"
		case payment_type = "payment_type"
		case order = "order"
		case distance = "distance"
		case auto_title = "auto_title"
		case description = "description"
		case keywords = "keywords"
		case cover_media_key = "cover_media_key"
		case followed = "followed"
		case treatment_value = "treatment_value"
		case evaluation_result = "evaluation_result"
		case comments_count = "comments_count"
		case created_at_formated = "created_at_formated"
		case city = "city"
		case city_id = "city_id"
		case city_name = "city_name"
		case state_abbr = "state_abbr"
		case treatments = "treatments"
		case slug = "slug"
		case medic_name = "medic_name"
		case medic_id = "medic_id"
		case medic_key = "medic_key"
		case medic_text = "medic_text"
		case medic = "medic"
	//	case user = "user"
		case image_url = "image_url"
        case comments = "comments"
		case treatment_name = "treatment_name"
		case specialty_id = "specialty_id"
		case specialty_name = "specialty_name"
		case specialty = "specialty"
		case cover_media_id = "cover_media_id"
        case evaluation_average = "evaluation_average"
	}

}
