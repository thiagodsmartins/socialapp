//
//  File.swift
//  AesCare
//
//  Created by Gali Srikanth on 20/10/20.
//

import Foundation
struct ModelAppintement : Codable {
    let leads : [Leads]?
    let code : Int?

    enum CodingKeys: String, CodingKey {

        case leads = "leads"
        case code = "code"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        leads = try values.decodeIfPresent([Leads].self, forKey: .leads)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
    }

}
struct Leads : Codable {
    let created_at : String?
    let date_start_formatted : String?
    let medic_calendar_slot_id : Int?
    let has_noshow_fee : Bool?
    let subject : String?
    let is_cancellable : Bool?
    let zoom_meeting_password : String?
    let description : String?
    let hours_slot : String?
    let key : String?
    let date_slot : String?
    let duration : String?
    let type : String?
    let user_key : String?
    let noshow_fee_status_description : String?
    let date_end : String?
    let noshow_fee_status : String?
    let status_description : String?
    let period_level : String?
    let email : String?
    let schedule : Schedule?
    let date_end_formatted : String?
    let medic_id : Int?
    let name : String?
    let medic : Medic?
    let interest_level : String?
    let status : String?
    let zoom_meeting_id : String?
    let zoom_join_url : String?
    let period_level_description : String?
    let medic_name : String?
    let booking_id : Int?
    let slot_id : Int?
    let user_id : Int?
    let city_name : String?
    let payment_types_formatted : String?
    let zoom_start_url : String?
    let interest_level_description : String?
    let payment_types : [String]?
    let phone : String?
    let date_start : String?
    let timezone : String?
    let time_to_start : Double?
    let state_abbr : String?

    enum CodingKeys: String, CodingKey {

        case created_at = "created_at"
        case date_start_formatted = "date_start_formatted"
        case medic_calendar_slot_id = "medic_calendar_slot_id"
        case has_noshow_fee = "has_noshow_fee"
        case subject = "subject"
        case is_cancellable = "is_cancellable"
        case zoom_meeting_password = "zoom_meeting_password"
        case description = "description"
        case hours_slot = "hours_slot"
        case key = "key"
        case date_slot = "date_slot"
        case duration = "duration"
        case type = "type"
        case user_key = "user_key"
        case noshow_fee_status_description = "noshow_fee_status_description"
        case date_end = "date_end"
        case noshow_fee_status = "noshow_fee_status"
        case status_description = "status_description"
        case period_level = "period_level"
        case email = "email"
        case schedule = "schedule"
        case date_end_formatted = "date_end_formatted"
        case medic_id = "medic_id"
        case name = "name"
        case medic = "medic"
        case interest_level = "interest_level"
        case status = "status"
        case zoom_meeting_id = "zoom_meeting_id"
        case zoom_join_url = "zoom_join_url"
        case period_level_description = "period_level_description"
        case medic_name = "medic_name"
        case booking_id = "booking_id"
        case slot_id = "slot_id"
        case user_id = "user_id"
        case city_name = "city_name"
        case payment_types_formatted = "payment_types_formatted"
        case zoom_start_url = "zoom_start_url"
        case interest_level_description = "interest_level_description"
        case payment_types = "payment_types"
        case phone = "phone"
        case date_start = "date_start"
        case timezone = "timezone"
        case time_to_start = "time_to_start"
        case state_abbr = "state_abbr"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        date_start_formatted = try values.decodeIfPresent(String.self, forKey: .date_start_formatted)
        medic_calendar_slot_id = try values.decodeIfPresent(Int.self, forKey: .medic_calendar_slot_id)
        has_noshow_fee = try values.decodeIfPresent(Bool.self, forKey: .has_noshow_fee)
        subject = try values.decodeIfPresent(String.self, forKey: .subject)
        is_cancellable = try values.decodeIfPresent(Bool.self, forKey: .is_cancellable)
        zoom_meeting_password = try values.decodeIfPresent(String.self, forKey: .zoom_meeting_password)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        hours_slot = try values.decodeIfPresent(String.self, forKey: .hours_slot)
        key = try values.decodeIfPresent(String.self, forKey: .key)
        date_slot = try values.decodeIfPresent(String.self, forKey: .date_slot)
        duration = try values.decodeIfPresent(String.self, forKey: .duration)
        type = try values.decodeIfPresent(String.self, forKey: .type)
        user_key = try values.decodeIfPresent(String.self, forKey: .user_key)
        noshow_fee_status_description = try values.decodeIfPresent(String.self, forKey: .noshow_fee_status_description)
        date_end = try values.decodeIfPresent(String.self, forKey: .date_end)
        noshow_fee_status = try values.decodeIfPresent(String.self, forKey: .noshow_fee_status)
        status_description = try values.decodeIfPresent(String.self, forKey: .status_description)
        period_level = try values.decodeIfPresent(String.self, forKey: .period_level)
        email = try values.decodeIfPresent(String.self, forKey: .email)
        schedule = try values.decodeIfPresent(Schedule.self, forKey: .schedule)
        date_end_formatted = try values.decodeIfPresent(String.self, forKey: .date_end_formatted)
        medic_id = try values.decodeIfPresent(Int.self, forKey: .medic_id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        medic = try values.decodeIfPresent(Medic.self, forKey: .medic)
        interest_level = try values.decodeIfPresent(String.self, forKey: .interest_level)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        zoom_meeting_id = try values.decodeIfPresent(String.self, forKey: .zoom_meeting_id)
        zoom_join_url = try values.decodeIfPresent(String.self, forKey: .zoom_join_url)
        period_level_description = try values.decodeIfPresent(String.self, forKey: .period_level_description)
        medic_name = try values.decodeIfPresent(String.self, forKey: .medic_name)
        booking_id = try values.decodeIfPresent(Int.self, forKey: .booking_id)
        slot_id = try values.decodeIfPresent(Int.self, forKey: .slot_id)
        user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
        city_name = try values.decodeIfPresent(String.self, forKey: .city_name)
        payment_types_formatted = try values.decodeIfPresent(String.self, forKey: .payment_types_formatted)
        zoom_start_url = try values.decodeIfPresent(String.self, forKey: .zoom_start_url)
        interest_level_description = try values.decodeIfPresent(String.self, forKey: .interest_level_description)
        payment_types = try values.decodeIfPresent([String].self, forKey: .payment_types)
        phone = try values.decodeIfPresent(String.self, forKey: .phone)
        date_start = try values.decodeIfPresent(String.self, forKey: .date_start)
        timezone = try values.decodeIfPresent(String.self, forKey: .timezone)
        time_to_start = try values.decodeIfPresent(Double.self, forKey: .time_to_start)
        state_abbr = try values.decodeIfPresent(String.self, forKey: .state_abbr)
    }

}
struct Schedule : Codable {
    let user_name : String?
    let medic : Medic?
    let is_online_service : Bool?
    let address_number : Int?
    let city_id : Int?
    let status : String?
    let date_start : String?
    let user_id : Int?
    let date_end_formatted : String?
    let is_cancellable : Bool?
    let street : String?
    let date_end : String?
    let has_noshow_fee : Bool?
    let medic_key : String?
    let medic_calendar_slot_id : Int?
    let city_name : String?
    let created_by_username : String?
    let created_at_formatted : String?
    let cep : String?
    let specialty_id : String?
    let booking : Booking?
    let date_start_formatted : String?
    let noshow_fee : Int?
    let noshow_fee_formatted : String?
    let timezone : String?
    let state_abbr : String?
    let medic_id : Int?
    let created_at : String?
    let created_by_user_id : Int?
    let key : String?

    enum CodingKeys: String, CodingKey {

        case user_name = "user_name"
        case medic = "medic"
        case is_online_service = "is_online_service"
        case address_number = "address_number"
        case city_id = "city_id"
        case status = "status"
        case date_start = "date_start"
        case user_id = "user_id"
        case date_end_formatted = "date_end_formatted"
        case is_cancellable = "is_cancellable"
        case street = "street"
        case date_end = "date_end"
        case has_noshow_fee = "has_noshow_fee"
        case medic_key = "medic_key"
        case medic_calendar_slot_id = "medic_calendar_slot_id"
        case city_name = "city_name"
        case created_by_username = "created_by_username"
        case created_at_formatted = "created_at_formatted"
        case cep = "cep"
        case specialty_id = "specialty_id"
        case booking = "booking"
        case date_start_formatted = "date_start_formatted"
        case noshow_fee = "noshow_fee"
        case noshow_fee_formatted = "noshow_fee_formatted"
        case timezone = "timezone"
        case state_abbr = "state_abbr"
        case medic_id = "medic_id"
        case created_at = "created_at"
        case created_by_user_id = "created_by_user_id"
        case key = "key"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        user_name = try values.decodeIfPresent(String.self, forKey: .user_name)
        medic = try values.decodeIfPresent(Medic.self, forKey: .medic)
        is_online_service = try values.decodeIfPresent(Bool.self, forKey: .is_online_service)
        address_number = try values.decodeIfPresent(Int.self, forKey: .address_number)
        city_id = try values.decodeIfPresent(Int.self, forKey: .city_id)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        date_start = try values.decodeIfPresent(String.self, forKey: .date_start)
        user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
        date_end_formatted = try values.decodeIfPresent(String.self, forKey: .date_end_formatted)
        is_cancellable = try values.decodeIfPresent(Bool.self, forKey: .is_cancellable)
        street = try values.decodeIfPresent(String.self, forKey: .street)
        date_end = try values.decodeIfPresent(String.self, forKey: .date_end)
        has_noshow_fee = try values.decodeIfPresent(Bool.self, forKey: .has_noshow_fee)
        medic_key = try values.decodeIfPresent(String.self, forKey: .medic_key)
        medic_calendar_slot_id = try values.decodeIfPresent(Int.self, forKey: .medic_calendar_slot_id)
        city_name = try values.decodeIfPresent(String.self, forKey: .city_name)
        created_by_username = try values.decodeIfPresent(String.self, forKey: .created_by_username)
        created_at_formatted = try values.decodeIfPresent(String.self, forKey: .created_at_formatted)
        cep = try values.decodeIfPresent(String.self, forKey: .cep)
        specialty_id = try values.decodeIfPresent(String.self, forKey: .specialty_id)
        booking = try values.decodeIfPresent(Booking.self, forKey: .booking)
        date_start_formatted = try values.decodeIfPresent(String.self, forKey: .date_start_formatted)
        noshow_fee = try values.decodeIfPresent(Int.self, forKey: .noshow_fee)
        noshow_fee_formatted = try values.decodeIfPresent(String.self, forKey: .noshow_fee_formatted)
        timezone = try values.decodeIfPresent(String.self, forKey: .timezone)
        state_abbr = try values.decodeIfPresent(String.self, forKey: .state_abbr)
        medic_id = try values.decodeIfPresent(Int.self, forKey: .medic_id)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        created_by_user_id = try values.decodeIfPresent(Int.self, forKey: .created_by_user_id)
        key = try values.decodeIfPresent(String.self, forKey: .key)
    }

}
struct Booking : Codable {
    let related_class : String?
    let created_at : String?
    let related_id : Int?
    let has_noshow_fee : Bool?
    let medic_calendar_slot_id : Int?
    let subject : String?
    let url : String?
    let description : String?
    let zoom_meeting_password : String?
    let key : String?
    let user_key : String?
    let date_end : String?
    let processed_at : String?
    let treatments : String?
    let noshow_fee_status : String?
    let period_level : String?
    let email : String?
    let _type : String?
    let medic_id : Int?
    let name : String?
    let zoom_meeting_json : String?
    let indication_code : String?
    let interest_level : String?
    let zoom_join_url : String?
    let price : String?
    let zoom_meeting_id : String?
    let next_process_at : String?
    let booking_id : Int?
    let noshow_fee : String?
    let status : String?
    let user_id : Int?
    let is_online_service : Bool?
    let city_id : Int?
    let zoom_start_url : String?
    let date_start : String?
    let payment_types : [String]?
    let phone : String?
    let timezone : String?

    enum CodingKeys: String, CodingKey {

        case related_class = "related_class"
        case created_at = "created_at"
        case related_id = "related_id"
        case has_noshow_fee = "has_noshow_fee"
        case medic_calendar_slot_id = "medic_calendar_slot_id"
        case subject = "subject"
        case url = "url"
        case description = "description"
        case zoom_meeting_password = "zoom_meeting_password"
        case key = "key"
        case user_key = "user_key"
        case date_end = "date_end"
        case processed_at = "processed_at"
        case treatments = "treatments"
        case noshow_fee_status = "noshow_fee_status"
        case period_level = "period_level"
        case email = "email"
        case _type = "_type"
        case medic_id = "medic_id"
        case name = "name"
        case zoom_meeting_json = "zoom_meeting_json"
        case indication_code = "indication_code"
        case interest_level = "interest_level"
        case zoom_join_url = "zoom_join_url"
        case price = "price"
        case zoom_meeting_id = "zoom_meeting_id"
        case next_process_at = "next_process_at"
        case booking_id = "booking_id"
        case noshow_fee = "noshow_fee"
        case status = "status"
        case user_id = "user_id"
        case is_online_service = "is_online_service"
        case city_id = "city_id"
        case zoom_start_url = "zoom_start_url"
        case date_start = "date_start"
        case payment_types = "payment_types"
        case phone = "phone"
        case timezone = "timezone"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        related_class = try values.decodeIfPresent(String.self, forKey: .related_class)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        related_id = try values.decodeIfPresent(Int.self, forKey: .related_id)
        has_noshow_fee = try values.decodeIfPresent(Bool.self, forKey: .has_noshow_fee)
        medic_calendar_slot_id = try values.decodeIfPresent(Int.self, forKey: .medic_calendar_slot_id)
        subject = try values.decodeIfPresent(String.self, forKey: .subject)
        url = try values.decodeIfPresent(String.self, forKey: .url)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        zoom_meeting_password = try values.decodeIfPresent(String.self, forKey: .zoom_meeting_password)
        key = try values.decodeIfPresent(String.self, forKey: .key)
        user_key = try values.decodeIfPresent(String.self, forKey: .user_key)
        date_end = try values.decodeIfPresent(String.self, forKey: .date_end)
        processed_at = try values.decodeIfPresent(String.self, forKey: .processed_at)
        treatments = try values.decodeIfPresent(String.self, forKey: .treatments)
        noshow_fee_status = try values.decodeIfPresent(String.self, forKey: .noshow_fee_status)
        period_level = try values.decodeIfPresent(String.self, forKey: .period_level)
        email = try values.decodeIfPresent(String.self, forKey: .email)
        _type = try values.decodeIfPresent(String.self, forKey: ._type)
        medic_id = try values.decodeIfPresent(Int.self, forKey: .medic_id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        zoom_meeting_json = try values.decodeIfPresent(String.self, forKey: .zoom_meeting_json)
        indication_code = try values.decodeIfPresent(String.self, forKey: .indication_code)
        interest_level = try values.decodeIfPresent(String.self, forKey: .interest_level)
        zoom_join_url = try values.decodeIfPresent(String.self, forKey: .zoom_join_url)
        price = try values.decodeIfPresent(String.self, forKey: .price)
        zoom_meeting_id = try values.decodeIfPresent(String.self, forKey: .zoom_meeting_id)
        next_process_at = try values.decodeIfPresent(String.self, forKey: .next_process_at)
        booking_id = try values.decodeIfPresent(Int.self, forKey: .booking_id)
        noshow_fee = try values.decodeIfPresent(String.self, forKey: .noshow_fee)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
        is_online_service = try values.decodeIfPresent(Bool.self, forKey: .is_online_service)
        city_id = try values.decodeIfPresent(Int.self, forKey: .city_id)
        zoom_start_url = try values.decodeIfPresent(String.self, forKey: .zoom_start_url)
        date_start = try values.decodeIfPresent(String.self, forKey: .date_start)
        payment_types = try values.decodeIfPresent([String].self, forKey: .payment_types)
        phone = try values.decodeIfPresent(String.self, forKey: .phone)
        timezone = try values.decodeIfPresent(String.self, forKey: .timezone)
    }

}
