//
//  ModelPost.swift
//  AesCare
//
//  Created by Gali Srikanth on 23/09/20.
//

import Foundation
struct ModelMyPost : Codable {
    let posts : [Posts_Media]?
    
    enum CodingKeys: String, CodingKey {
        
        case posts = "posts"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        posts = try values.decodeIfPresent([Posts_Media].self, forKey: .posts)
    }
    
}
struct Posts_Media : Codable {
    let post_id : Int?
    let key : String?
    let status : String?
    let medias : [Medias_post]?
    let created_at_formatted : String?
    let publisher : Publisher?
    let body : String?
    enum CodingKeys: String, CodingKey {
        
        case post_id = "post_id"
        case key = "key"
        case status = "status"
        case medias = "medias"
        case created_at_formatted = "created_at_formatted"
        case publisher = "user"
        case body = "body"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        post_id = try values.decodeIfPresent(Int.self, forKey: .post_id)
        key = try values.decodeIfPresent(String.self, forKey: .key)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        medias = try values.decodeIfPresent([Medias_post].self, forKey: .medias)
        created_at_formatted = try values.decodeIfPresent(String.self, forKey: .created_at_formatted)
        publisher = try values.decodeIfPresent(Publisher.self, forKey: .publisher)
        body = try values.decodeIfPresent(String.self, forKey: .body)
        
    }
    
}

struct Medias_post : Codable {
    let key : String?
    let created_at : String?
    let created_at_formatted : String?
    let user_id : Int?
    let status : String?
    let hash : String?
    let _hash : String?
    let type : String?
    let _type : String?
    //let testimonial_id : Int?
    let post_id : Int?
    //let content_id : Int?
    let video_url : String?
    let image_url : String?
    let thumbnail_url : String?
    let cover_url : String?
    
    enum CodingKeys: String, CodingKey {
        
        case key = "key"
        case created_at = "created_at"
        case created_at_formatted = "created_at_formatted"
        case user_id = "user_id"
        case status = "status"
        case hash = "hash"
        case _hash = "_hash"
        case type = "type"
        case _type = "_type"
        //case testimonial_id = "testimonial_id"
        case post_id = "post_id"
        //case content_id = "content_id"
        case video_url = "video_url"
        case image_url = "image_url"
        case thumbnail_url = "thumbnail_url"
        case cover_url = "cover_url"
    }
    
    
}
struct Publisher : Codable {
    let key : String?
    let username : String?
    let name : String?
    let image_url : String?
    
    enum CodingKeys: String, CodingKey {
        
        case key = "key"
        case username = "username"
        case name = "name"
        case image_url = "image_url"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        key = try values.decodeIfPresent(String.self, forKey: .key)
        username = try values.decodeIfPresent(String.self, forKey: .username)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        image_url = try values.decodeIfPresent(String.self, forKey: .image_url)
    }
    
}
