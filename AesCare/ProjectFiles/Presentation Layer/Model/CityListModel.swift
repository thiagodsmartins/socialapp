//
//  CityListModel.swift
//  AesCare
//
//  Created by Gali Srikanth on 20/08/20.
//

import Foundation

struct CityListModel : Codable {
    let city_list : [City_list]?

    enum CodingKeys: String, CodingKey {

        case city_list = "city_list"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        city_list = try values.decodeIfPresent([City_list].self, forKey: .city_list)
    }
}
struct City_list : Codable {
    let name : String?
    let state : State?
    let city_id : Int?
    
    /*
    let google_place_id : String?
    let processed_at : String?
    let timezone : String?
    let ddd : String?
    let slug : String?
    
    let latitude : String?
    let slug_original : String?
   
    let longitude : String?
    let preposition : String?
    let name_pt : String?
    let results_count : String?
    let state_id : Int?
    let name_pt_unaccent : String?
    let status : String?
    let state_abbr : String?
    let name_en : String?
    let created_at : String?
    let country_abbr : String?
    let name_unaccent : String?
    let updated_at : String?
    */

    enum CodingKeys: String, CodingKey {
case name = "name"
         case city_id = "city_id"
        case state = "state"
        /*
        case google_place_id = "google_place_id"
        case processed_at = "processed_at"
        case timezone = "timezone"
        case ddd = "ddd"
        case slug = "slug"
       
        case latitude = "latitude"
        case slug_original = "slug_original"
        
        case longitude = "longitude"
        case preposition = "preposition"
        case name_pt = "name_pt"
        case results_count = "results_count"
        case state_id = "state_id"
        case name_pt_unaccent = "name_pt_unaccent"
        case status = "status"
        case state_abbr = "state_abbr"
        case name_en = "name_en"
        case created_at = "created_at"
        case country_abbr = "country_abbr"
        case name_unaccent = "name_unaccent"
        case updated_at = "updated_at"
        */
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try values.decodeIfPresent(String.self, forKey: .name)
         city_id = try values.decodeIfPresent(Int.self, forKey: .city_id)
        state = try values.decodeIfPresent(State.self, forKey: .state)
        
        /*
        
        
        google_place_id = try values.decodeIfPresent(String.self, forKey: .google_place_id)
        processed_at = try values.decodeIfPresent(String.self, forKey: .processed_at)
        timezone = try values.decodeIfPresent(String.self, forKey: .timezone)
        ddd = try values.decodeIfPresent(String.self, forKey: .ddd)
        slug = try values.decodeIfPresent(String.self, forKey: .slug)
       
        latitude = try values.decodeIfPresent(String.self, forKey: .latitude)
        slug_original = try values.decodeIfPresent(String.self, forKey: .slug_original)
        
        longitude = try values.decodeIfPresent(String.self, forKey: .longitude)
        preposition = try values.decodeIfPresent(String.self, forKey: .preposition)
        name_pt = try values.decodeIfPresent(String.self, forKey: .name_pt)
        results_count = try values.decodeIfPresent(String.self, forKey: .results_count)
        state_id = try values.decodeIfPresent(Int.self, forKey: .state_id)
        name_pt_unaccent = try values.decodeIfPresent(String.self, forKey: .name_pt_unaccent)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        state_abbr = try values.decodeIfPresent(String.self, forKey: .state_abbr)
        name_en = try values.decodeIfPresent(String.self, forKey: .name_en)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        country_abbr = try values.decodeIfPresent(String.self, forKey: .country_abbr)
        name_unaccent = try values.decodeIfPresent(String.self, forKey: .name_unaccent)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
        */
    }

}
struct State : Codable {
    let name_en : String?
    let name : String?
    let updated_at : String?
    let latitude : String?
    let abbr : String?
    let name_pt : String?
    let state_id : Int?
    let created_at : String?
    let longitude : String?
    let country_abbr : String?

    enum CodingKeys: String, CodingKey {

        case name_en = "name_en"
        case name = "name"
        case updated_at = "updated_at"
        case latitude = "latitude"
        case abbr = "abbr"
        case name_pt = "name_pt"
        case state_id = "state_id"
        case created_at = "created_at"
        case longitude = "longitude"
        case country_abbr = "country_abbr"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name_en = try values.decodeIfPresent(String.self, forKey: .name_en)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
        latitude = try values.decodeIfPresent(String.self, forKey: .latitude)
        abbr = try values.decodeIfPresent(String.self, forKey: .abbr)
        name_pt = try values.decodeIfPresent(String.self, forKey: .name_pt)
        state_id = try values.decodeIfPresent(Int.self, forKey: .state_id)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        longitude = try values.decodeIfPresent(String.self, forKey: .longitude)
        country_abbr = try values.decodeIfPresent(String.self, forKey: .country_abbr)
    }

}
