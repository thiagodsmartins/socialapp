//
//  Chat.swift
//  ChatSample
//
//  Created by Hafiz on 22/10/2019.
//  Copyright © 2019 Nibs. All rights reserved.
//

import Foundation

struct Chat {
    var isHimself: Bool?
    var avatar :String?
    var displayName:String?
    var message:String?
    var active:Int?
    var isNew:Int?
    var timeStamp : NSNumber?
    var userID:Int?
    var chatAutoID:String?
    var firebaseID :String?
    
    
}
