/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct Owner : Codable {
	let is_medic : Bool?
	let last_latitude : Double?
	let birthdate : String?
	let state_abbr : String?
	let created_at : String?
	let abbr : String?
	let created : String?
	let encrypted_password : String?
	let url_image : String?
	let assistant_content_balance : Int?
	let key : String?
	let role : String?
	//let treatments : [String]?
	let perfil_image : String?
	let email : String?
	let acronym_name : String?
	let medic_key : String?
	let name : String?
	//let medic_id : String?
	let longitude : Double?
	let status : String?
	let username : String?
	let city : String?
	let user_id : Int?
	let gender : String?
	let country_code : String?
	let city_name : String?
	let firstname : String?
	let age : Int?
	let review_count : Int?
	let last_longitude : Double?
	let city_id : Int?
	let is_woman : Bool?
	let phone : String?
	let latitude : Double?

	enum CodingKeys: String, CodingKey {

		case is_medic = "is_medic"
		case last_latitude = "last_latitude"
		case birthdate = "birthdate"
		case state_abbr = "state_abbr"
		case created_at = "created_at"
		case abbr = "abbr"
		case created = "created"
		case encrypted_password = "encrypted_password"
		case url_image = "url_image"
		case assistant_content_balance = "assistant_content_balance"
		case key = "key"
		case role = "role"
		//case treatments = "treatments"
		case perfil_image = "perfil_image"
		case email = "email"
		case acronym_name = "acronym_name"
		case medic_key = "medic_key"
		case name = "name"
		//case medic_id = "medic_id"
		case longitude = "longitude"
		case status = "status"
		case username = "username"
		case city = "city"
		case user_id = "user_id"
		case gender = "gender"
		case country_code = "country_code"
		case city_name = "city_name"
		case firstname = "firstname"
		case age = "age"
		case review_count = "review_count"
		case last_longitude = "last_longitude"
		case city_id = "city_id"
		case is_woman = "is_woman"
		case phone = "phone"
		case latitude = "latitude"
	}

//	init(from decoder: Decoder) throws {
//		let values = try decoder.container(keyedBy: CodingKeys.self)
//		is_medic = try values.decodeIfPresent(Bool.self, forKey: .is_medic)
//		last_latitude = try values.decodeIfPresent(Double.self, forKey: .last_latitude)
//		birthdate = try values.decodeIfPresent(String.self, forKey: .birthdate)
//		state_abbr = try values.decodeIfPresent(String.self, forKey: .state_abbr)
//		created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
//		abbr = try values.decodeIfPresent(String.self, forKey: .abbr)
//		created = try values.decodeIfPresent(String.self, forKey: .created)
//		encrypted_password = try values.decodeIfPresent(String.self, forKey: .encrypted_password)
//		url_image = try values.decodeIfPresent(String.self, forKey: .url_image)
//		assistant_content_balance = try values.decodeIfPresent(Int.self, forKey: .assistant_content_balance)
//		key = try values.decodeIfPresent(String.self, forKey: .key)
//		role = try values.decodeIfPresent(String.self, forKey: .role)
//		treatments = try values.decodeIfPresent([String].self, forKey: .treatments)
//		perfil_image = try values.decodeIfPresent(String.self, forKey: .perfil_image)
//		email = try values.decodeIfPresent(String.self, forKey: .email)
//		acronym_name = try values.decodeIfPresent(String.self, forKey: .acronym_name)
//		medic_key = try values.decodeIfPresent(String.self, forKey: .medic_key)
//		name = try values.decodeIfPresent(String.self, forKey: .name)
//		medic_id = try values.decodeIfPresent(String.self, forKey: .medic_id)
//		longitude = try values.decodeIfPresent(Double.self, forKey: .longitude)
//		status = try values.decodeIfPresent(String.self, forKey: .status)
//		username = try values.decodeIfPresent(String.self, forKey: .username)
//		city = try values.decodeIfPresent(String.self, forKey: .city)
//		user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
//		gender = try values.decodeIfPresent(String.self, forKey: .gender)
//		country_code = try values.decodeIfPresent(String.self, forKey: .country_code)
//		city_name = try values.decodeIfPresent(String.self, forKey: .city_name)
//		firstname = try values.decodeIfPresent(String.self, forKey: .firstname)
//		age = try values.decodeIfPresent(String.self, forKey: .age)
//		review_count = try values.decodeIfPresent(Int.self, forKey: .review_count)
//		last_longitude = try values.decodeIfPresent(Double.self, forKey: .last_longitude)
//		city_id = try values.decodeIfPresent(Int.self, forKey: .city_id)
//		is_woman = try values.decodeIfPresent(Bool.self, forKey: .is_woman)
//		phone = try values.decodeIfPresent(String.self, forKey: .phone)
//		latitude = try values.decodeIfPresent(Double.self, forKey: .latitude)
//	}

}
