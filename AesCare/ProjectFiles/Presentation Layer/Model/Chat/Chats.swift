/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct Chats : Codable {
	
	let image_url : String?
	let status : String?
	let title : String?
	let owner_device_key : String?
	let tags : String?
	let owner_id : Int?
//	let user_id : Int?
	//let updated_at : String?
	let last_message_at : String?
	let date_last_message : String?
	let owner : Owner?
	let related_id : Int?
	let file_url_image : String?
	let is_msg_read : Bool?
	let chat_id : Int?
	let image_key : String?
	let timestamp : Int?
	let related_class : String?
	let first_participant_user_id : Int?
	let created_at : String?
	let photo : String?
	let participants_count : Int?
	let owner_username : String?
	let last_message : String?
	let key : String?

	enum CodingKeys: String, CodingKey {

		
		case image_url = "image_url"
		case status = "status"
		case title = "title"
		case owner_device_key = "owner_device_key"
		case tags = "tags"
		case owner_id = "owner_id"
		//case user_id = "user_id"
		//case updated_at = "updated_at"
		case last_message_at = "last_message_at"
		case date_last_message = "date_last_message"
		case owner = "owner"
		case related_id = "related_id"
		case file_url_image = "file_url_image"
		case is_msg_read = "is_msg_read"
		case chat_id = "chat_id"
		case image_key = "image_key"
		case timestamp = "timestamp"
		case related_class = "related_class"
		case first_participant_user_id = "first_participant_user_id"
		case created_at = "created_at"
		case photo = "photo"
		case participants_count = "participants_count"
		case owner_username = "owner_username"
		case last_message = "last_message"
		case key = "key"
	}

}
struct Chat1 : Codable {
    let parent_comment_id : Int?
    //let link_preview : String?
    let send_date : String?
    let device_key : String?
    let created_at : String?
    //let tags : String?
    let user_id : Int?
    //let files_id : [String]?
    let key : String?
    let chat_message_id : Int?
    let files : [String]?
    let user_key : String?
    let updated_at : String?
    let username : String?
    let chat_id : Int?
    let is_my : Bool?
    let user : ChatUser?
    let content : String?

    enum CodingKeys: String, CodingKey {

        case parent_comment_id = "parent_comment_id"
        //case link_preview = "link_preview"
        case send_date = "send_date"
        case device_key = "device_key"
        case created_at = "created_at"
       // case tags = "tags"
        case user_id = "user_id"
       // case files_id = "files_id"
        case key = "key"
        case chat_message_id = "chat_message_id"
        case files = "files"
        case user_key = "user_key"
        case updated_at = "updated_at"
        case username = "username"
        case chat_id = "chat_id"
        case is_my = "is_my"
        case user = "user"
        case content = "content"
    }

//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        parent_comment_id = try values.decodeIfPresent(Int.self, forKey: .parent_comment_id)
//       // link_preview = try values.decodeIfPresent(String.self, forKey: .link_preview)
//        send_date = try values.decodeIfPresent(String.self, forKey: .send_date)
//        device_key = try values.decodeIfPresent(String.self, forKey: .device_key)
//        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
//        //tags = try values.decodeIfPresent(String.self, forKey: .tags)
//        user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
//       // files_id = try values.decodeIfPresent([String].self, forKey: .files_id)
//        key = try values.decodeIfPresent(String.self, forKey: .key)
//        chat_message_id = try values.decodeIfPresent(Int.self, forKey: .chat_message_id)
//        //files = try values.decodeIfPresent([String].self, forKey: .files)
//        user_key = try values.decodeIfPresent(String.self, forKey: .user_key)
//        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
//        username = try values.decodeIfPresent(String.self, forKey: .username)
//        chat_id = try values.decodeIfPresent(Int.self, forKey: .chat_id)
//        is_my = try values.decodeIfPresent(Bool.self, forKey: .is_my)
//        user = try values.decodeIfPresent(ChatUser.self, forKey: .user)
//        content = try values.decodeIfPresent(String.self, forKey: .content)
//    }

}
struct FilesMessage : Codable {
    let key : String?
    let public_url : String?

    enum CodingKeys: String, CodingKey {

        case key = "key"
        case public_url = "public_url"
    }

    
}
