//
//  UserProfileEditVC.swift
//  AesCare
//
//  Created by Gali Srikanth on 24/09/20.
//

import UIKit
import Kingfisher

protocol UserProfileEditVCDelegate: class {
    func didCompleteProfileEdit()
}

class UserProfileEditVC: BaseViewController {
    var gradientLayer: CAGradientLayer!
    @IBOutlet weak var vwNavigation: UIView!
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var tblUser: UITableView!
    
    weak var delegate: UserProfileEditVCDelegate!
    
    var imagePicker = UIImagePickerController()
    //var publication_multipartData : [MultiPartDataFormatStructure] = [MultiPartDataFormatStructure]()
    var userImage : UIImage!
    var userName = ""
    
    var userContactHeader = ["Email*","Telefone","Celular*","Pais*"]
    //var selectedCountryForEdit:ModelCountryList!
    var selectedCityForEdit = 0
    var arrContactInfo = [String]()
    var arrPersonalInfo = [String]()
    var arrPassword = ["",""]
    var arrNotification = [false,false,false,false]
    enum User_ContactInfo:Int{
        case Email = 0
        case Telefone
        case Celular
        case Pais
        case total
        func getPlaceholder () -> String {
            switch self {
            case .Email:
                return "Seu e-mail"
            case .Telefone:
                return "Seu Telefone"
            case .Celular:
                return "+XX DDD XXXXXXX"
            case .Pais:
                return "Pais"
            default:
                return ""
            }
        }
    }
    enum User_PersonalInfo:Int{
        case Gender = 0
        case Profile
        case Interest
        case DOB
        case FullName
        case City
        case total
        func getPlaceholder () -> String {
            switch self {
            case .Gender:
                return "Gênero*"
            case .Profile:
                return "Perfil*"
            case .Interest:
                return "Interesses"
            case .DOB:
                return "Data de Nascimento*"
            case .FullName:
                return "Nome Comleto"
            case .City:
                return "Cidade*"
            default:
                return ""
            }
         }
    }
    enum User_SettingsInfo:Int{
        case NewPassword = 0
        case ConfirmNewPassword
        
        case total
        func getPlaceholder () -> String {
            switch self {
            case .NewPassword:
                return "Nova senha"
            case .ConfirmNewPassword:
                return "Confirme nova senha"
            default:
                return ""
            }
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //createGradientLayer()
        // Do any additional setup after loading the view.
        for _ in 0 ..< User_ContactInfo.total.rawValue {
            arrContactInfo.append("")
        }
        for _ in 0 ..< User_PersonalInfo.total.rawValue {
            arrPersonalInfo.append("")
        }
        
        arrPersonalInfo[User_PersonalInfo.Gender.rawValue] = "0"
        arrPersonalInfo[User_PersonalInfo.Profile.rawValue] = "0"
        
        refreshUser()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        ///// Call Custom Tabbar
        //gradientLayer.frame = self.vwNavigation.bounds
    }
      
    func createGradientLayer() {
        gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.vwNavigation.bounds
        gradientLayer.colors = [UIColor(named: "AppPurpleColor")!.cgColor, UIColor(named: "AppMoveColor")!.cgColor]
        self.vwNavigation.layer.addSublayer(gradientLayer)
        self.vwNavigation.bringSubviewToFront(self.btnBack)
        self.vwNavigation.bringSubviewToFront(self.lblHeader)
    }
    
    @IBAction func backNavigationPressed(_ sender: Any) {
        backNavigation()
    }
    
    func refreshUser() {
        userName = usrerModelOBJ.user?.username ?? ""
        arrContactInfo[User_ContactInfo.Email.rawValue] = usrerModelOBJ.user?.email ?? ""
        arrContactInfo[User_ContactInfo.Pais.rawValue] = usrerModelOBJ.user?.country_code ?? ""
        arrContactInfo[User_ContactInfo.Celular.rawValue] = usrerModelOBJ.user?.phone ?? ""
        arrPersonalInfo[User_PersonalInfo.Profile.rawValue] = (usrerModelOBJ.user?.is_medic ?? false) ? "1" : "0"
        arrPersonalInfo[User_PersonalInfo.DOB.rawValue] = usrerModelOBJ.user?.birthdate ?? ""
        arrPersonalInfo[User_PersonalInfo.FullName.rawValue] = usrerModelOBJ.user?.name ?? ""
        arrPersonalInfo[User_PersonalInfo.City.rawValue] = usrerModelOBJ.user?.city ?? ""
        selectedCityForEdit = usrerModelOBJ.user?.city_id ?? 0
        arrPersonalInfo[User_PersonalInfo.Profile.rawValue] = (usrerModelOBJ.user?.is_medic == false) ? "0" : "1"
        arrPersonalInfo[User_PersonalInfo.Gender.rawValue] = usrerModelOBJ.user?.gender == "F" ? "0" : "1"
        
        if let url = URL.init(string: (usrerModelOBJ.user?.perfil_image)!) {
            let resource = ImageResource(downloadURL: url)
            
            KingfisherManager.shared.retrieveImage(with: resource, options: nil, progressBlock: nil) { result in
                switch result {
                case .success(let value):
                    self.userImage = value.image
                    self.tblUser.reloadData()
                case .failure:
                    print("Error")
                }
            }
        }
        else {
            tblUser.reloadData()
        }
    }
}
//
//Mark: table work
//
extension UserProfileEditVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 0 ? 0 : 70
    }
    /*
     func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
     if(section == 0){
     
     }else{
     let header = view as! UITableViewHeaderFooterView
     header.textLabel?.textColor = UIColor(named: "AppPurpleColor")
     header.textLabel?.font = UIFont(name: "OpenSans-Bold ", size: 25)
     header.textLabel?.text = section == 1 ? "Informações de Contato" : (section == 2 ? "Informações Pessoais" : (section == 3 ?  "Configurações" : "Notificações") )
     }
     }*/
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if(section != 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: "CommonCellTableViewCell4") as! CommonCellTableViewCell
            cell.lblHeader.text = section == 1 ? "Informações de Contato" : (section == 2 ? "Informações Pessoais" : (section == 3 ?  "Configurações" : "Notificações") )
            return cell
        }
        return nil
    }
    /*
     func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
     return section == 0 ?  "" : "Informações de Contato"
     }*/
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? 2 : (section == 1 ?  User_ContactInfo.total.rawValue : (section == 2 ?  User_PersonalInfo.total.rawValue : (section == 3 ?  User_SettingsInfo.total.rawValue : 1)))
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0 
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section == 0){
            if(indexPath.row == 0){
                let cell: CommonCellTableViewCell = self.tblUser.dequeueReusableCell(withIdentifier: "CommonCellTableViewCell") as! CommonCellTableViewCell
                
                if let _ = userImage{
                    cell.imgContent.image = userImage
                }
                cell.imgContent.contentMode = .scaleAspectFill
                return cell
            }else{
                let cell: CommonCellTableViewCell = self.tblUser.dequeueReusableCell(withIdentifier: "CommonCellTableViewCell1") as! CommonCellTableViewCell
                cell.lblHeader.text = "Nome de Usuário*"
                
                cell.cellSelectionText.isSecureTextEntry = false
                cell.cellSelectionText.text = userName
                cell.cellSelectionText.placeholder = "Nome"
                cell.cellSelectionText.removeTarget(nil,
                                                    action: nil,
                                                    for: .allEvents)
                cell.cellSelectionText.addTarget(self, action: #selector(self.textFieldNameValueChange(_:)), for: .editingChanged)
                
                return cell
            }
        } else if(indexPath.section == 1){
            if(indexPath.row == User_ContactInfo.Pais.rawValue){
                let cell: CommonCellTableViewCell = self.tblUser.dequeueReusableCell(withIdentifier: "CommonCellTableViewCell5") as! CommonCellTableViewCell
                
                cell.btnInstance.removeTarget(nil,action: nil,for: .allEvents)
                cell.lblHeader.text = userContactHeader[indexPath.row]
                let type = User_ContactInfo.init(rawValue: indexPath.row)
                cell.cellSelectionText.placeholder = type?.getPlaceholder()
                cell.cellSelectionText.text = arrContactInfo [indexPath.row]
                cell.btnInstance.addTarget(self,
                                action: #selector(self.openSearchCountry(_:)),
                                           for: .touchUpInside)
                return cell
            } else {
                let cell: CommonCellTableViewCell = self.tblUser.dequeueReusableCell(withIdentifier: "CommonCellTableViewCell1") as! CommonCellTableViewCell
                
                cell.lblHeader.text = userContactHeader[indexPath.row]
                
                cell.cellSelectionText.isSecureTextEntry = false
                let type = User_ContactInfo.init(rawValue: indexPath.row)
                cell.cellSelectionText.placeholder = type?.getPlaceholder()
                cell.cellSelectionText.text = arrContactInfo [indexPath.row]
                cell.cellSelectionText.tag = indexPath.row
                
                cell.cellSelectionText.removeTarget(nil,
                                                    action: nil,
                                                    for: .allEvents)
                
                cell.cellSelectionText.addTarget(self, action: #selector(self.textFieldContactInfoValueChange(_:)), for: .editingChanged)
                
                return cell
            }
        }else if(indexPath.section == 2){
            if(indexPath.row == 0 || indexPath.row == 1){
                let cell: CommonCellTableViewCell = self.tblUser.dequeueReusableCell(withIdentifier: "CommonCellTableViewCell2") as! CommonCellTableViewCell
                
                let type = User_PersonalInfo.init(rawValue: indexPath.row)
                cell.lblHeader.text = type?.getPlaceholder()
                
                cell.btnInstance.setImage(UIImage(named: "unselectCircle"), for: .normal)
                cell.btnInstance2.setImage(UIImage(named: "unselectCircle"), for: .normal)
                
                
                if(indexPath.row == 0) {
                    cell.lblInfoExtra1.text = "Feminino"
                    cell.lblInfoExtra2.text = "Masculino"
                    cell.btnInstance.tag = 1
                    cell.btnInstance2.tag = 2
                    
                    
                } else {
                    cell.lblInfoExtra1.text = "Paciente"
                    cell.lblInfoExtra2.text = "Profissinal"
                    cell.btnInstance.tag = 3
                    cell.btnInstance2.tag = 4
                }
                
                cell.btnInstance.addTarget(self, action: #selector(self.selectGenderOption(_:)), for: .touchUpInside)
                cell.btnInstance2.addTarget(self, action: #selector(self.selectGenderOption(_:)), for: .touchUpInside)
                
                if let index = Int(arrPersonalInfo[indexPath.row]){
                    
                    if(index == 0){
                        cell.btnInstance.setImage(UIImage(named: "selectCircle"), for: .normal)
                    }else if(index == 1){
                        cell.btnInstance2.setImage(UIImage(named: "selectCircle"), for: .normal)
                    }
                }
                
                return cell
            }else{
                if(indexPath.row == User_PersonalInfo.DOB.rawValue){
                    let cell: CommonCellTableViewCell = self.tblUser.dequeueReusableCell(withIdentifier: "CommonCellTableViewCell5") as! CommonCellTableViewCell
                    
                    
                    let type = User_PersonalInfo.init(rawValue: indexPath.row)
                    cell.lblHeader.text = type?.getPlaceholder()
                    cell.cellSelectionText.placeholder = type?.getPlaceholder()
                    cell.cellSelectionText.text = arrPersonalInfo[User_PersonalInfo.DOB.rawValue]
                    cell.btnInstance.removeTarget(nil,action: nil,for: .allEvents)
                    cell.btnInstance.addTarget(self,
                                action: #selector(self.openDOB(_:)),
                                               for: .touchUpInside)
                    return cell
                }else if(indexPath.row == User_PersonalInfo.City.rawValue){
                    let cell: CommonCellTableViewCell = self.tblUser.dequeueReusableCell(withIdentifier: "CommonCellTableViewCell5") as! CommonCellTableViewCell
                    
                    
                    let type = User_PersonalInfo.init(rawValue: indexPath.row)
                    cell.lblHeader.text = type?.getPlaceholder()
                    cell.cellSelectionText.placeholder = type?.getPlaceholder()
                    cell.cellSelectionText.text = arrPersonalInfo [indexPath.row]
                    cell.btnInstance.removeTarget(nil,action: nil,for: .allEvents)
                    cell.btnInstance.addTarget(self,
                                action: #selector(self.openSearchCity),
                                               for: .touchUpInside)
                    return cell
                }else{
                    let cell: CommonCellTableViewCell = self.tblUser.dequeueReusableCell(withIdentifier: "CommonCellTableViewCell1") as! CommonCellTableViewCell
                    
                    let type = User_PersonalInfo.init(rawValue: indexPath.row)
                    cell.lblHeader.text = type?.getPlaceholder()
                    
                    cell.cellSelectionText.isSecureTextEntry = false
                    cell.cellSelectionText.placeholder = type?.getPlaceholder()
                    cell.cellSelectionText.text = arrPersonalInfo[indexPath.row]
                    cell.cellSelectionText.tag = indexPath.row
                    
                    cell.cellSelectionText.removeTarget(nil,
                                                        action: nil,
                                                        for: .allEvents)
                    cell.cellSelectionText.addTarget(self, action: #selector(self.textFieldPersonalInfoValueChange(_:)), for: .editingChanged)
                    
                    return cell
                }
            }
        }else  if(indexPath.section == 3){
            let cell: CommonCellTableViewCell = self.tblUser.dequeueReusableCell(withIdentifier: "CommonCellTableViewCell1") as! CommonCellTableViewCell
            
            let type = User_SettingsInfo.init(rawValue: indexPath.row)
            cell.lblHeader.text = type?.getPlaceholder()
            cell.cellSelectionText.placeholder = type?.getPlaceholder()
            
            cell.cellSelectionText.isSecureTextEntry = true
            cell.cellSelectionText.text = arrPassword[indexPath.row]
            cell.cellSelectionText.tag = indexPath.row
            
            cell.cellSelectionText.removeTarget(nil,
                                                action: nil,
                                                for: .allEvents)
            
            cell.cellSelectionText.addTarget(self, action: #selector(self.textFieldPasswordValueChange(_:)), for: .editingChanged)
            
            return cell
        }else{
            let cell: CommonCellTableViewCell = self.tblUser.dequeueReusableCell(withIdentifier: "CommonCellTableViewCell3") as! CommonCellTableViewCell
            
            cell.btnInstance.setImage(UIImage(named: "unchecked_box"), for: .normal)
            cell.btnInstance2.setImage(UIImage(named: "unchecked_box"), for: .normal)
            cell.btnInstance3.setImage(UIImage(named: "unchecked_box"), for: .normal)
            cell.btnInstance4.setImage(UIImage(named: "unchecked_box"), for: .normal)
            if(arrNotification[0]){
                cell.btnInstance.setImage(UIImage(named: "checked_box"), for: .normal)
            }
            if(arrNotification[1]){
                cell.btnInstance2.setImage(UIImage(named: "checked_box"), for: .normal)
            }
            if(arrNotification[2]){
                cell.btnInstance3.setImage(UIImage(named: "checked_box"), for: .normal)
            }
            
            if(arrNotification[3]){
                cell.btnInstance4.setImage(UIImage(named: "checked_box"), for: .normal)
            }
            
            cell.btnInstance.addTarget(self, action: #selector(self.selectNotification(_:)), for: .touchUpInside)
            cell.btnInstance2.addTarget(self, action: #selector(self.selectNotification(_:)), for: .touchUpInside)
            cell.btnInstance3.addTarget(self, action: #selector(self.selectNotification(_:)), for: .touchUpInside)
            cell.btnInstance4.addTarget(self, action: #selector(self.selectNotification(_:)), for: .touchUpInside)
            
            cell.btnInstance5.addTarget(self, action: #selector(self.SubmitData(_:)), for: .touchUpInside)
            return cell
        }
    }
}
extension UserProfileEditVC{
    
    @objc func textFieldNameValueChange(_ txt: UITextField)  {
        //print("textFieldNameValueChange")
        if let str = txt.text, str.count > 0 {
            userName = str
        }else{
            userName = ""
        }
    }
    
    @objc func openSearchCountry(_ sender: UIButton)  {
        let optionView = CountrySearchPopUp()
        
        optionView.frame = self.view.frame
        
        optionView.tag = 1000
        optionView.basicSetup()
        optionView.fadeIn()
        optionView.delegate = self
        optionView.isSearchforCountry = true
        self.view.bringSubviewToFront(optionView)
        self.view.addSubview(optionView)
    }
    @objc func openSearchCity(_ sender: UIButton)  {
        let optionView = CountrySearchPopUp()
        
        optionView.frame = self.view.frame
        
        optionView.tag = 1000
        optionView.basicSetup()
        optionView.fadeIn()
        optionView.delegate = self
        optionView.isSearchforCountry = false
        self.view.bringSubviewToFront(optionView)
        self.view.addSubview(optionView)
    }
    @objc func textFieldContactInfoValueChange(_ txt: UITextField)  {
        //print("textFieldContactInfoValueChange")
        if let str = txt.text, str.count > 0 {
            self.arrContactInfo[txt.tag] = str
        }else{
            self.arrContactInfo[txt.tag] = ""
        }
    }
    @objc func textFieldPersonalInfoValueChange(_ txt: UITextField)  {
        //print("textFieldPersonalInfoValueChange")
        if let str = txt.text, str.count > 0 {
            self.arrPersonalInfo[txt.tag] = str
        }else{
            self.arrPersonalInfo[txt.tag] = ""
        }
    }
    @objc func selectGenderOption(_ sender:UIButton){
        if(sender.tag == 1 || sender.tag == 2){
            arrPersonalInfo[User_PersonalInfo.Gender.rawValue] = sender.tag == 1 ? "0": "1"
        }else{
            arrPersonalInfo[User_PersonalInfo.Profile.rawValue] = sender.tag == 3 ? "0": "1"
        }
        
        self.tblUser.reloadData()
    }
    @objc func textFieldPasswordValueChange(_ txt: UITextField)  {
        //print("textFieldPasswordValueChange")
        if let str = txt.text, str.count > 0 {
            self.arrPassword[txt.tag] = str
        }else{
            self.arrPassword[txt.tag] = ""
        }
    }
    @objc func openDOB(_ sender:UIButton){
        MyBasics.showDatePickerDropDown(PickerType: .date, ParentViewC: self, setRestriction: "")
    }
    
    @objc func selectNotification(_ sender:UIButton){
        arrNotification[sender.tag] = !arrNotification[sender.tag]
        
        self.tblUser.reloadData()
    }
    @objc func SubmitData(_ sender:UIButton){
        if(!isValidData()){
            return
        }
        
        var param:[String:AnyObject] = [String:AnyObject]()
        
        param["username"] = usrerModelOBJ.user?.username as AnyObject
        param["name"] = userName as AnyObject
        param["email"] = arrContactInfo[User_ContactInfo.Email.rawValue] as AnyObject
        param["phone"] = arrContactInfo[User_ContactInfo.Celular.rawValue] as AnyObject
        param["gender"] = (arrPersonalInfo[User_PersonalInfo.Gender.rawValue] == "0" ? "F" : "M" ) as AnyObject
        param["is_medic"] = (arrPersonalInfo[User_PersonalInfo.Profile.rawValue] == "0" ? false : true ) as AnyObject
        param["country_code"] = self.arrContactInfo[User_ContactInfo.Pais.rawValue] as AnyObject
        param["birthdate"] = arrPersonalInfo[User_PersonalInfo.DOB.rawValue] as AnyObject
        param["city_id"] = selectedCityForEdit as AnyObject
        
        if(!arrPassword[0].isEmpty){
            param["password"] = (arrPassword[0]) as AnyObject
        }
        
        //param["notifications_selected"] = [] as AnyObject
        
        //param["all_notification_types"] = [] as AnyObject
        var paramOuter:[String:AnyObject] = [String:AnyObject]()
        paramOuter["user"] = param  as AnyObject
        print(paramOuter)
        postUserApi(param: paramOuter)
    }
    func isValidData() -> Bool {
        if(!arrPassword[0].isEmpty || !arrPassword[1].isEmpty){
            if(arrPassword[0] != arrPassword[1]){
                showAlertMessage(title: "Ooops", message: ALERT_MESSAGE_MANDETORY_FIELD, vc: self)
                return false
            }
        }
        /*
        guard !arrPassword[0].isEmpty else {
            showAlertMessage(title: "Ooops", message: ALERT_MESSAGE_MANDETORY_FIELD, vc: self)
            return false
        }
        guard (arrPassword[0] == arrPassword[1]) else {
            showAlertMessage(title: "Ooops", message: ALERT_MESSAGE_PASS_NOT_MATCH, vc: self)
            return false
        }
        */
        guard !arrContactInfo[User_ContactInfo.Email.rawValue].isEmpty else {
            showAlertMessage(title: "Ooops", message: ALERT_MESSAGE_MANDETORY_FIELD, vc: self)
            return false
        }
        guard !arrContactInfo[User_ContactInfo.Celular.rawValue].isEmpty else {
            showAlertMessage(title: "Ooops", message: ALERT_MESSAGE_MANDETORY_FIELD, vc: self)
            return false
        }
        guard !arrContactInfo[User_ContactInfo.Pais.rawValue].isEmpty else {
            showAlertMessage(title: "Ooops", message: ALERT_MESSAGE_MANDETORY_FIELD, vc: self)
            return false
        }
        guard !arrPersonalInfo[User_PersonalInfo.Profile.rawValue].isEmpty else {
            showAlertMessage(title: "Ooops", message: ALERT_MESSAGE_MANDETORY_FIELD, vc: self)
            return false
        }
        guard !arrPersonalInfo[User_PersonalInfo.DOB.rawValue].isEmpty else {
            showAlertMessage(title: "Ooops", message: ALERT_MESSAGE_MANDETORY_FIELD, vc: self)
            return false
        }
        guard !arrPersonalInfo[User_PersonalInfo.City.rawValue].isEmpty else {
            showAlertMessage(title: "Ooops", message: ALERT_MESSAGE_MANDETORY_FIELD, vc: self)
            return false
        }
        
        return true
    }
}
extension UserProfileEditVC : DatePickerDelegate{
    func selectedDate(selectedDate: Date) {
        let formatter = DateFormatter()
        formatter.dateFormat = "YYYY-MM-dd"
        arrPersonalInfo[User_PersonalInfo.DOB.rawValue] = formatter.string(from: selectedDate)
        tblUser.reloadData()
    }
}
extension UserProfileEditVC : PopupCountrySelectionDelegate{
    func didSelectCityPressed(_ selectedCity: ModelSearchedCity) {
        self.arrPersonalInfo[User_PersonalInfo.City.rawValue] = selectedCity.name!
        selectedCityForEdit = selectedCity.city_id!
        tblUser.reloadData()
    }
    
    func didSelectCountryPressed(_ selectedCountry: ModelCountryList) {
        self.arrContactInfo[User_ContactInfo.Pais.rawValue] = selectedCountry.code ?? ""
        //selectedCountryForEdit = selectedCountry
        tblUser.reloadData()
    }
}

///
/////mark: Upload publication content
////
extension UserProfileEditVC:UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    @IBAction func selctImage(_ btn:UIButton) {
        self.systemAlertofAttachmentForPublication(header: App_Title, message: "Escolha a sua foto")
    }
    ////alert to represent attachment option
    func systemAlertofAttachmentForPublication(header:String , message:String) {
        
        let alertController:UIAlertController = UIAlertController(title: header, message: message, preferredStyle: .alert)
        
        
        let actionVideoGallery = UIAlertAction(title: "Torar foto", style: .default, handler: { (action) -> Void in
            // Get TextFields text
            self.openCamera()
            
        })
        let actionImageGallery = UIAlertAction(title: "Escolher foto", style: .default, handler: { (action) -> Void in
            // Get TextFields text
            self.openPhotoGallary()
            
        })
        let cancelAction = UIAlertAction(title: "Cancelher foto", style: .destructive, handler: { (action) -> Void in
            
        })
        
        alertController.addAction(actionVideoGallery)
        alertController.addAction(actionImageGallery)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(.camera)){
            self.imagePicker.delegate = self
            self.imagePicker.sourceType = .camera
            self.imagePicker.allowsEditing = true
            UIApplication.shared.keyWindow?.rootViewController?.present(imagePicker, animated: true, completion: nil)
        }else{
            showAlertMessage(title: "Ooops", message: "unable to open Camera", vc: self)
        }
    }
    func openPhotoGallary(){
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            self.imagePicker.delegate = self
            self.imagePicker.allowsEditing = true
            self.imagePicker.sourceType = .savedPhotosAlbum//.photoLibrary
            self.imagePicker.mediaTypes = ["public.image"]
            
            UIApplication.shared.keyWindow?.rootViewController?.present(self.imagePicker, animated: true, completion: nil)
        } else {
            showAlertMessage(title: "Ooops", message: "unable to open PhotoAlbum", vc: self)
        }
        
    }
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)
        if let pickedimage = info[.editedImage] as? UIImage {
                let data = pickedimage.jpegData(compressionQuality: 1)
                let timeInterval = NSDate().timeIntervalSince1970
                let theFileName : String = "\(timeInterval).jpeg"
                let imageFileInfo = [MultiPartDataFormatStructure.init(key: "image", mimeType: MultiPartDataFormatStructure.MIME_TYPE.image_jpeg, data: data, name: theFileName)]
                self.view.setNeedsLayout()
                
                self.userImage = pickedimage
                
                self.tblUser.reloadData()
                deleteUserPhotoApi(multipartData: imageFileInfo)
                //postUserPhotoApi(multipartData: [imageFileInfo])
                
            }else{
            showAlertMessage(title: "Ooops", message: "unable to select image", vc: self)
        }
        
        self.dismiss(animated: true, completion: nil)
        self.view.setNeedsLayout()
    }
    
}

///
/////mark: Api call for user photo delete and request
////
extension UserProfileEditVC{
    
    
    func deleteUserPhotoApi(multipartData:[MultiPartDataFormatStructure]) {
        let url = (API.userPhoto.getURL()?.absoluteString ?? "")
        let operation = WebServiceOperation.init(url, nil, .WEB_SERVICE_MULTI_PART,nil)
        //(url, nil, .WEB_SERVICE_GET, nil)
        
        operation.completionBlock =
            {
                DispatchQueue.main.async {
                    
                    print(operation.responseData?.dictionary?.prettyprintedJSON ?? "")
                    guard let dictResponse = operation.responseData?.dictionary, dictResponse.count > 0 else {
                        UtilityClass.tosta(message: "Something went wrong", duration: 2.0, vc: self)
                        return
                    }
                    
                    if let responceCode = dictResponse["code"] ,responceCode as! Int == 1 , let msg = dictResponse["message"] as? String{
                        self.postUserPhotoApi(multipartData: multipartData)
                        UtilityClass.tosta(message: msg, duration: 2.0, vc: self)
                    }else{
                        UtilityClass.tosta(message: "Something wentwrong", duration: 2.0, vc: self)
                    }
                    
                }
        }
        appDel.operationQueue.addOperation(operation)
    }
    
    func postUserPhotoApi(multipartData:[MultiPartDataFormatStructure]) {
        let url = (API.userPhoto.getURL()?.absoluteString ?? "")
        let operation = WebServiceOperation.init(url, nil, .WEB_SERVICE_MULTI_PART, multipartData)
        
        operation.completionBlock =
            {
                DispatchQueue.main.async {
                    
                    print(operation.responseData?.dictionary?.prettyprintedJSON ?? "")
                    guard let dictResponse = operation.responseData?.dictionary, dictResponse.count > 0 else {
                        UtilityClass.tosta(message: "Something went wrong", duration: 2.0, vc: self)
                        return
                    }
                    if let responceCode = dictResponse["code"] ,responceCode as! Int == 1 , let msg = dictResponse["message"] as? String{
                        UtilityClass.tosta(message: msg, duration: 2.0, vc: self)
                    }else{
                        UtilityClass.tosta(message: "Something wentwrong", duration: 2.0, vc: self)
                    }
                    
                }
        }
        appDel.operationQueue.addOperation(operation)
    }
    
    func postUserApi(param : [String:AnyObject]) {
        
        let url = (API.postUser.getURL()?.absoluteString ?? "")
        let operation =  WebServiceOperation.init(url,param, .WEB_SERVICE_POST,nil)
        //let operation = WebServiceOperation.init(url, param, .WEB_SERVICE_MULTI_PART, nil)
        operation.completionBlock =
            {
                DispatchQueue.main.async {
                    
                    print(operation.responseData?.dictionary?.prettyprintedJSON ?? "")
                    guard let dictResponse = operation.responseData?.dictionary, dictResponse.count > 0 else {
                        UtilityClass.tosta(message: "Something went wrong", duration: 2.0, vc: self)
                        return
                    }
                    if let responceCode = dictResponse["code"] ,responceCode as! Int == 1 , let msg = dictResponse["message"] as? String{
                        
                        if UserDefaults.standard.bool(forKey: "isUserDataComplete") {
                            self.backByToast(message: msg)
                        }
                        else {
                            UserDefaults.standard.setValue(true, forKey: "isUserDataComplete")
                            self.dismiss(animated: true, completion: {
                                self.delegate.didCompleteProfileEdit()
                            })
                        }
                    }else{
                        UtilityClass.tosta(message: "Something wentwrong", duration: 2.0, vc: self)
                    }
                    
                }
        }
        
        appDel.operationQueue.addOperation(operation)
    }
    
}
