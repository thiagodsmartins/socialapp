//
//  UserProfileVC.swift
//  AesCare
//
//  Created by Gali Srikanth on 23/09/20.
//

import UIKit
import SideMenu
import AMTabView

class UserProfileVC: BaseViewController, TabItem {
    var gradientLayer: CAGradientLayer!
    @IBOutlet weak var vwNavigation: UIView!
    @IBOutlet weak var tblPost: UITableView!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var lbl1: UILabel!
    @IBOutlet weak var lbl2: UILabel!
    @IBOutlet weak var lbl3: UILabel!
    @IBOutlet weak var lbl4: UILabel!
    @IBOutlet weak var lbl5: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var vwContainer: UIView!
    @IBOutlet weak var imgToblur: UIImageView!
    @IBOutlet weak var containerSearch: designableView!
    @IBOutlet weak var lblPost: UILabel!
    var myPost:ModelMyPost!
    var swipeGesture: UISwipeGestureRecognizer?
    var tabImage: UIImage? {
        return UIImage(named: "profile-Active")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.swipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(self.openSideMenuWithSwipe(_:)))
        self.swipeGesture?.direction = .right
        
        createGradientLayer()
        imgToblur.blurEffect(context: CIContext(options: nil))
        
        self.view.addGestureRecognizer(self.swipeGesture!)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        ////// GET USER PROFILE DATA
        getUserProfileApi()
        
        ///GET USER PUBLISH DATA
        getUserPostApi()
        
        
    }
    
    override func viewLayoutMarginsDidChange() {
        
        self.view.bringSubviewToFront(self.vwContainer)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        ///// Call Custom Tabbar
        //gradientLayer.frame = self.vwNavigation.bounds
    }
    
    func createGradientLayer() {
        gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.vwNavigation.bounds
        gradientLayer.colors = [UIColor(named: "AppPurpleColor")!.cgColor, UIColor(named: "AppMoveColor")!.cgColor]
        self.vwNavigation.layer.addSublayer(gradientLayer)
        self.vwNavigation.bringSubviewToFront(self.btnMenu)
        self.vwNavigation.bringSubviewToFront(self.containerSearch)
    }
    
    @objc func openSideMenuWithSwipe(_ swipeGesture: UISwipeGestureRecognizer) {
        if swipeGesture.state == .ended {
            self.openSidePanel()
        }
    }
    
    @IBAction func openSideMenu(_ sender: Any) {
        openSidePanel()
    }
    
    @IBAction func editProfile(_ sender: Any) {
        if #available(iOS 13.0, *) {
            let vc = self.storyboard?.instantiateViewController(identifier: "UserProfileEditVC") as! UserProfileEditVC
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            // Fallback on earlier versions
            let vc = UserProfileEditVC.init(nibName: "UserProfileEditVC", bundle: nil)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func refreshUser(){
        if let savedUser = UserDefaults.standard.object(forKey: "LOGGED_IN_USER_DATA") as? Data {
            let decoder = JSONDecoder()
            if let replaceobj = try? decoder.decode(ModelUser.self, from: savedUser) {
                usrerModelOBJ = replaceobj
                lbl1.text = (replaceobj.user?.city_name ?? "") + "," + (replaceobj.user?.state_abbr ?? "")
                lbl4.text = "@" + (replaceobj.user?.firstname ?? "")
                lbl5.text = replaceobj.user?.firstname
                
                if let url = URL(string:replaceobj.user?.perfil_image ?? ""){
                    self.imgProfile.kf.indicatorType = .activity
                    self.imgProfile.kf.setImage(with: url)
                }
            }
        }
        imgProfile.layer.cornerRadius = self.imgProfile.frame.size.height / 2
        imgProfile.clipsToBounds = true
    }
}

//
//Mark: table work
//
extension UserProfileVC:UITableViewDelegate,UITableViewDataSource
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let mdObj = myPost, let arr = mdObj.posts{
            self.lblPost.text = String(format: "%02d", arr.count)
            return arr.count
        }
        return 0
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: PublishMediaCell = self.tblPost.dequeueReusableCell(withIdentifier: "PublishMediaCell") as! PublishMediaCell
        if let mdObj = myPost ,let post = mdObj.posts?[indexPath.row], let url = URL(string:post.publisher?.image_url ?? ""){
            cell.imgContent.kf.indicatorType = .activity
            cell.imgContent.kf.setImage(with: url)
            cell.imgContent.contentMode = .scaleAspectFill
        }
        
        
        
        var body = " "
        
        if let mdObj = myPost , let post = mdObj.posts?[indexPath.row] , let bodyTemp = post.body{
            body = bodyTemp
        }
        cell.lblInfo.text  = body
        var imgArr = [UIImage]()
        
        
        
        if let mdObj = myPost ,let post = mdObj.posts?[indexPath.row], let mediaArray = post.medias , mediaArray.count > 0 {
            
            
            do{
                let imgData =  try Data(contentsOf: URL(string: mediaArray[0].image_url!)! as URL)
                           imgArr.append(UIImage(data: imgData)!)
            }catch _ {
                
            }
           
            /*
            for(_,object) in mediaArray.enumerated(){
                do{
                    let imgData =  try Data(contentsOf: URL(string: object.image_url!)! as URL)
                    
                    imgArr.append(UIImage(data: imgData)!)
                }catch _{
                    
                }
            }*/
        }
        cell.imgArr = imgArr
        cell.colVWImageAdd.reloadData()
        
        
        
        cell.btnInstance2.tag = indexPath.row
        cell.btnInstance2.addTarget(self, action: #selector(deletePost(_:)), for: .touchUpInside)
        
        cell.btnInstance.tag = indexPath.row
        cell.btnInstance.addTarget(self, action: #selector(editPost(_:)), for: .touchUpInside)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        self.navigateToEditPostByIndex(index: indexPath.row)
    }
}
extension UserProfileVC{
    @objc func editPost(_ sender:UIButton){
        self.navigateToEditPostByIndex(index: sender.tag)
    }
    
    @objc func deletePost(_ sender:UIButton){
        if let mdObj = self.myPost , let arrPost = mdObj.posts ,arrPost.count > 0,  let selectedPostKey = arrPost[sender.tag].key{
            
            var param:[String:String] = [String:String]()
            param["key"] = selectedPostKey as String
            param["status"] = "d" as String
            self.deletePostApi(param: param)
        }
    }
    func navigateToEditPostByIndex(index:NSInteger) {
        let selectedPost = myPost.posts![index]
        if #available(iOS 13.0, *) {
            let vc = self.storyboard?.instantiateViewController(identifier: "PublishEditVC") as! PublishEditVC
            vc.myPost = selectedPost
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            let vc = PublishEditVC.init(nibName: "PublishEditVC", bundle: nil)
            vc.myPost = selectedPost
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

////
///Mark : Api
////
extension UserProfileVC{
    func deletePostApi(param:[String:String]) {
        let url = (API.deletePost.getURL()?.absoluteString ?? "")
        let operation = WebServiceOperation.init(url, param as Dictionary<String, AnyObject>, .WEB_SERVICE_MULTI_PART, nil)
        operation.completionBlock =
            {
                DispatchQueue.main.async {
                    
                    print(operation.responseData?.dictionary?.prettyprintedJSON ?? "")
                    guard let dictResponse = operation.responseData?.dictionary, dictResponse.count > 0 else {
                        UtilityClass.tosta(message: "Something went wrong", duration: 2.0, vc: self)
                        return
                    }
                    if let responceCode = dictResponse["code"] ,responceCode as! Int == 1 , let msg = dictResponse["message"] as? String{
                        
                        UtilityClass.tosta(message: msg, duration: 2.0, vc: self)
                        self.getUserPostApi()
                    }else{
                        UtilityClass.tosta(message: "Something wentwrong", duration: 2.0, vc: self)
                    }
                    
                }
        }
        appDel.operationQueue.addOperation(operation)
    }
    func getUserProfileApi() {
        let url = (API.profileDetail.getURL()?.absoluteString ?? "")
        let operation = WebServiceOperation.init(url, nil, .WEB_SERVICE_GET, nil)
        
        operation.completionBlock =
            {
                DispatchQueue.main.async {
                    
                    //print(operation.responseData?.dictionary?.prettyprintedJSON ?? "")
                    guard let dictResponse = operation.responseData?.dictionary, dictResponse.count > 0 else {
                        UtilityClass.tosta(message: "Something went wrong", duration: 2.0, vc: self)
                        return
                    }
                    if let dataUserDetails = dictResponse.data, dataUserDetails.count > 0 {
                        do{
                            
                            let userModel = try JSONDecoder().decode(ModelUser.self, from: dataUserDetails)
                            usrerModelOBJ = userModel
                            let encoder = JSONEncoder()
                            if let encoded = try? encoder.encode(userModel) {
                                let defaults = UserDefaults.standard
                                defaults.set(encoded, forKey: "LOGGED_IN_USER_DATA")
                            }
                        }catch let e{
                            print(e.localizedDescription)
                            showAlertMessage(title: App_Title, message: "Something went wrong", vc: self)
                        }
                    }
                    
                    
                    DispatchQueue.main.async {
                        self.refreshUser()
                    }
                }
        }
        appDel.operationQueue.addOperation(operation)
    }
    
    func getUserPostApi() {
        let url = (API.userPost.getURL()?.absoluteString ?? "") + "?with_treatments_name=false&is_my=true"
        let operation = WebServiceOperation.init(url, nil, .WEB_SERVICE_GET, nil)
        
        operation.completionBlock =
            {
                DispatchQueue.main.async {
                    
                    //print(operation.responseData?.dictionary?.prettyprintedJSON ?? "")
                    guard let dictResponse = operation.responseData?.dictionary, dictResponse.count > 0 else {
                        UtilityClass.tosta(message: "Something went wrong", duration: 2.0, vc: self)
                        return
                    }
                    if let dataUserDetails = dictResponse.data, dataUserDetails.count > 0 {
                        do{
                            
                            self.myPost = try JSONDecoder().decode(ModelMyPost.self, from: dataUserDetails)
                            
                            self.tblPost.reloadData()
                            
                        }catch let e{
                            print(e.localizedDescription)
                            showAlertMessage(title: App_Title, message: "Something went wrong", vc: self)
                        }
                    }
                }
        }
        appDel.operationQueue.addOperation(operation)
    }
}
