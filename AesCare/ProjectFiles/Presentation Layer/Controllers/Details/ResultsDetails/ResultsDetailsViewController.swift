//
//  ResultsDetailsViewController.swift
//  AesCare
//
//  Created by Thiago on 25/03/21.
//

import Foundation
import UIKit
import JJFloatingActionButton
import Alamofire
import Agrume

class ResultsDetailsViewController: UIViewController {
    @IBOutlet weak var tableViewResultsDetails: UITableView!
    @IBOutlet weak var buttonCloseWindow: UIButton!
    @IBOutlet weak var imageViewGradient: UIView!
    @IBOutlet weak var imageViewIcon: UIImageView!

    var nibFiles: [UINib]?
    var floatButton: JJFloatingActionButton!
    var textViewMessage: UITextView!
    var resultsData: ResultsModel?
    var scrollViewImage: UIScrollView!
    var pageControl: UIPageControl!
    var imageToPreview: UIImageView!
    var gestureRecognizer: UITapGestureRecognizer?
    var index = 0
    var isViewLoading = true
    var resultsMessage: UserSendMessageView!
    var gesture: UITapGestureRecognizer!
    var buttonUserMessageView: UIButton!
    var isUserMessageViewHidden = false
    var previousPosition: CGRect = CGRect.zero
    
    //var resultsComments: [[ResultsUserCommentsResponse?]?]!
    var resultsComments: ResultsUserCommentsResponse?
    var workItem: DispatchWorkItem!
    var semaphore: DispatchSemaphore!
    var queue: DispatchQueue!
    
    lazy var mediasData = [MediasContent]()
    lazy var imagesURL = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupTableView()
        self.setupViews()
        
        //self.resultsComments = [[ResultsUserCommentsResponse?]]()
        //self.resultsComments.insert(nil, at: 0)
        
        self.setupBackgroundResultsRequest()
        
        self.requestMedia((self.resultsData?.result_id)!, completion: {
            response in
            if response {
                print("media acquired")
                for url in self.mediasData {
                    self.imagesURL.append(url.cover_url!)
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                    print(self.imagesURL)
                    let cell = self.tableViewResultsDetails.cellForRow(at: IndexPath(item: 0, section: 0)) as! ResultsDetailsImagesTableViewCell
                    
                    cell.imagesURL = self.imagesURL
                    cell.pagerView.reloadData()
                })
            }
        })
        

    }
    
    
    func starRatingResult(_ rate: Int) -> String {
        switch rate {
        case 1:
            return "Fraco"
        case 2:
            return "Bom"
        case 3:
            return "Muito Bom"
        case 4:
            return "Ótimo"
        case 5:
            return "Excelente"
        default:
            return ""
        }
    }
        
    private func setupTableView() {
        self.nibFiles = [UINib]()
        self.nibFiles?.append(UINib(nibName: "ResultsDetailsImagesTableViewCell", bundle: nil))
        self.nibFiles?.append(UINib(nibName: "ResultsDetailsInfoTableViewCell", bundle: nil))
        self.nibFiles?.append(UINib(nibName: "ResultsDetailsCommentsTableViewCell", bundle: nil))
        self.nibFiles?.append(UINib(nibName: "EmptyTableViewCell", bundle: nil))
        
        self.tableViewResultsDetails.register(self.nibFiles![0], forCellReuseIdentifier: "ResultsDetailsImagesTableViewCell")
        self.tableViewResultsDetails.register(self.nibFiles![1], forCellReuseIdentifier: "ResultsDetailsInfoTableViewCell")
        self.tableViewResultsDetails.register(self.nibFiles![2], forCellReuseIdentifier: "ResultsDetailsCommentsTableViewCell")
        self.tableViewResultsDetails.register(self.nibFiles![3], forCellReuseIdentifier: "EmptyTableViewCell")
        self.tableViewResultsDetails.rowHeight = UITableView.automaticDimension
        self.tableViewResultsDetails.estimatedRowHeight = 300
        self.tableViewResultsDetails.delegate = self
        self.tableViewResultsDetails.dataSource = self
    }
    
    private func setupScrollView() {
        let viewHeight: CGFloat = self.view.bounds.size.height
        let viewWidth: CGFloat = self.view.bounds.size.width
        
        self.scrollViewImage = UIScrollView(frame: CGRect(x: 0, y: 0, width: viewWidth, height: viewHeight))
        self.scrollViewImage.delegate = self
        self.scrollViewImage.isPagingEnabled = true
        self.scrollViewImage.minimumZoomScale = 1.0
        self.scrollViewImage.maximumZoomScale = 10.0
        self.scrollViewImage.alwaysBounceVertical = false
        self.scrollViewImage.alwaysBounceHorizontal = false
        self.scrollViewImage.flashScrollIndicators()
        self.scrollViewImage.zoomScale = 1.0
        self.scrollViewImage.backgroundColor = UIColor.clear
        self.scrollViewImage.isHidden = true
        self.scrollViewImage.addGestureRecognizer(self.gestureRecognizer!)
        self.scrollViewImage.delegate = self
        self.scrollViewImage.addSubview(self.imageToPreview)
        
        self.view.addSubview(self.scrollViewImage)
    }
    
    private func setupViews() {
        self.gesture = UITapGestureRecognizer()
        self.gesture.addTarget(self, action: #selector(self.tapView(_:)))
        
        self.textViewMessage = UITextView()
        self.textViewMessage.isHidden = true
        
        self.textViewMessage.delegate = self
        
        self.imageToPreview = UIImageView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
        self.imageToPreview.image = nil
        self.imageToPreview.clipsToBounds = false
        self.gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.tapToClosePreview(_:)))
        self.gestureRecognizer?.numberOfTapsRequired = 2
        
        self.keyboardNotifications()
        
        self.buttonCloseWindow.addTarget(self, action: #selector(self.buttonCloseWindowPressed(_:)), for: .touchUpInside)
        self.buttonCloseWindow.backgroundColor = UIColor(red: 175/255, green: 68/255, blue: 217/255, alpha: 1.0)
        self.buttonCloseWindow.tintColor = .white
        self.buttonCloseWindow.layer.cornerRadius = self.buttonCloseWindow.frame.size.width / 2
        self.buttonCloseWindow.clipsToBounds = true
        
        self.imageViewIcon.layer.cornerRadius = self.imageViewIcon.frame.size.width / 2
        self.imageViewIcon.layer.masksToBounds = false
        self.imageViewIcon.clipsToBounds = true
        self.imageViewIcon.image = UIImage(named: "detailimage")
        
        //self.view.addGestureRecognizer(self.gesture)
        self.setupUserMessageView()
        self.gradientView()
        
//        self.floatButton = JJFloatingActionButton()
//        self.floatButton.addItem(title: "Comentar", image: UIImage(named: "comment-image")?.withRenderingMode(.alwaysTemplate)) { item in
//            self.textViewMessage.becomeFirstResponder()
//            self.textViewMessage.isHidden = false
//        }
//
//        self.floatButton.addItem(title: "Compartilhar", image: UIImage(named: "share-image")?.withRenderingMode(.alwaysTemplate)) { item in
//            self.shareResult()
//        }
//
//        self.view.addSubview(self.floatButton)
//
//        self.floatButton.translatesAutoresizingMaskIntoConstraints = false
//        self.floatButton.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -16).isActive = true
//        self.floatButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -16).isActive = true
        
        
        //self.sendMessageKeyboardButton()
    }
    
    private func setupBackgroundResultsRequest() {
        self.queue = DispatchQueue.global(qos: .userInitiated)
        self.semaphore = DispatchSemaphore(value: 0)
        self.workItem = DispatchWorkItem {
            while !self.workItem.isCancelled {
                self.requestComments(self.resultsData!.result_id! , completion: {
                    data in
                    if let data = data {
                        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(15), execute: {
                            self.resultsComments = data
                            self.tableViewResultsDetails.reloadSections(IndexSet(integer: 2), with: .none)
                            
                            self.semaphore.signal()
                        })
                    }
                    else {
                        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(5), execute: {
                            print("No data")
                            self.semaphore.signal()
                        })
                    }
                })
                
                self.semaphore.wait()
            }
            
            self.workItem = nil
        }
        
        self.queue.async(execute: self.workItem)
    }
    
    private func setupUserMessageView() {
        self.buttonUserMessageView = UIButton(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        self.resultsMessage = UserSendMessageView()
        
        buttonUserMessageView.setTitle("X", for: .normal)
        buttonUserMessageView.setTitleColor(.red, for: .normal)
        buttonUserMessageView.layer.cornerRadius = buttonUserMessageView.frame.height / 2
        buttonUserMessageView.layer.borderWidth = 2
        buttonUserMessageView.layer.borderColor = UIColor.red.cgColor
        buttonUserMessageView.addTarget(self, action: #selector(self.buttonHideUserSendMessagePressed(_:)), for: .touchUpInside)
        buttonUserMessageView.translatesAutoresizingMaskIntoConstraints = false
        
        self.resultsMessage.buttonSendUserMessage.addTarget(self, action: #selector(self.buttonUserSendMessagePressed(_:)), for: .touchUpInside)
        self.resultsMessage.buttonSendUserMessage.isEnabled = false
        self.resultsMessage.textViewUserMessage.delegate = self
        self.resultsMessage.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addSubview(self.resultsMessage)
        self.view.addSubview(buttonUserMessageView)
        self.view.bringSubviewToFront(buttonUserMessageView)
        
        self.resultsMessage.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: -10).isActive = true
        self.resultsMessage.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: 10).isActive = true
        self.resultsMessage.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: -10).isActive = true
        //self.resultsMessage.heightAnchor.constraint(equalTo: self.resultsMessage.heightAnchor, multiplier: 1.0).isActive = true
        //self.resultsMessage.heightAnchor.constraint(equalToConstant: 60).isActive = true
        
        buttonUserMessageView.bottomAnchor.constraint(equalTo: self.resultsMessage.topAnchor, constant: -5).isActive = true
        buttonUserMessageView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        buttonUserMessageView.heightAnchor.constraint(equalToConstant: 20).isActive = true
        buttonUserMessageView.widthAnchor.constraint(equalToConstant: 20).isActive = true
    }
    
    private func gradientView() {
        let gradient = CAGradientLayer()
        gradient.frame = self.imageViewGradient.bounds
        gradient.colors = [UIColor(red: 168/255, green: 72/255, blue: 217/255, alpha: 1.0).cgColor, UIColor(red: 105/255, green: 108/255, blue: 220/255, alpha: 1.0).cgColor]
        gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        self.imageViewGradient.layer.addSublayer(gradient)
        self.imageViewGradient.addSubview(self.buttonCloseWindow)
        self.imageViewGradient.addSubview(self.imageViewIcon)
    }
    
    private func keyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: UIResponder.keyboardDidShowNotification , object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
//    private func sendMessageKeyboardButton() {
//        let toolBar =  UIToolbar(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 35))
//        toolBar.barStyle = .default
//        toolBar.sizeToFit()
//
//        let sendButton = UIBarButtonItem(title: "Enviar", style: .plain, target: self, action: #selector(self.buttonSendMessagePressed(_:)))
//        let closeButton = UIBarButtonItem(title: "Fechar", style: .plain, target: self, action: #selector(self.buttonCloseMessagePressed(_:)))
//        toolBar.items = [sendButton, closeButton]
//        toolBar.isUserInteractionEnabled = true
//        self.textViewMessage.inputAccessoryView = toolBar
//    }
    
    private func shareResult() {
        let text = "AESCare"
        let image = UIImage(named: "illustration-medico")
        let myWebsite = NSURL(string: "https://br.aescare.com/")
        let shareAll = [text, image!, myWebsite as Any] as Any
        let activityViewController = UIActivityViewController(activityItems: shareAll as! [Any], applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
}
//         0
// [[array(1, 2, 3]]

extension ResultsDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 || section == 1{
            return 1
        }
        else if section == 2 {
            if self.resultsComments == nil {
                return 1
            }
            else {
                return self.resultsComments!.comments!.count
            }
        }
        else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = self.tableViewResultsDetails.dequeueReusableCell(withIdentifier: "ResultsDetailsImagesTableViewCell") as! ResultsDetailsImagesTableViewCell
            print("TABLEVIEW --- \(self.imagesURL)")
            cell.delegate = self
            cell.imagesURL = self.imagesURL
            cell.starRating.rating = Double(self.resultsData?.evaluation_result ?? 1)
            cell.starRating.text = self.starRatingResult(self.resultsData?.evaluation_result ?? 1)
            self.resultsData!.liked! ? cell.likeButton.setStatus(.liked) : cell.likeButton.setStatus(.unliked)
            return cell
        }
        else if indexPath.section == 1 {
            let cell = self.tableViewResultsDetails.dequeueReusableCell(withIdentifier: "ResultsDetailsInfoTableViewCell") as! ResultsDetailsInfoTableViewCell
            cell.infoData = self.resultsData
            return cell
        }
        else if indexPath.section == 2 {
            if let _ = self.resultsComments {
                if !self.resultsComments!.comments!.isEmpty {
                    let cell = self.tableViewResultsDetails.dequeueReusableCell(withIdentifier: "ResultsDetailsCommentsTableViewCell") as! ResultsDetailsCommentsTableViewCell
                    cell.labelUsername.text = self.resultsComments!.comments![indexPath.row].user!.username!
                    cell.textViewMessage.text = self.resultsComments!.comments![indexPath.row].content!
                    cell.imageViewIcon.sd_setImage(with: URL(string: self.resultsComments!.comments![indexPath.row].user!.url_image!), completed: nil)
                    cell.labelPostDate.text = self.resultsComments!.comments![indexPath.row].created_at!
                    cell.delegate = self
                    self.index = indexPath.row
                    return cell
                }
                else {
                    let cell = self.tableViewResultsDetails.dequeueReusableCell(withIdentifier: "EmptyTableViewCell") as! EmptyTableViewCell
                    cell.labelMessage.text = "Nenhum Comentário"
                    return cell
                }
            }
            else {
                let cell = self.tableViewResultsDetails.dequeueReusableCell(withIdentifier: "EmptyTableViewCell") as! EmptyTableViewCell
                cell.labelMessage.text = "Nenhum Comentário"
                
                return cell
            }
        }
        else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 240
        }
        else if indexPath.section == 1 {
            return 180
        }
        else if indexPath.section == 2 {
            return UITableView.automaticDimension
        }
        else {
            return tableView.rowHeight
        }
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .destructive, title:  "Apagar", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            
            self.deleteResultsCommentary(self.resultsComments!.comments![indexPath.row].comment_id!, response: {
                data in
                
                if data {
                    self.resultsComments!.comments?.remove(at: indexPath.row)
                    tableView.deleteRows(at: [IndexPath(row: indexPath.row, section: 2)], with: .none)
                }
            })
            
            success(true)
        })
        
        deleteAction.backgroundColor = .red
        
        if indexPath.section == 2 {
            if self.resultsComments!.comments![indexPath.row].user!.user_id! == usrerModelOBJ.user?.user_id! {
                return UISwipeActionsConfiguration(actions: [deleteAction])
            }
            else {
                return nil
            }
        }
        else {
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let viewHeader = UITableViewHeaderFooterView()
        viewHeader.textLabel?.textColor = .black
        viewHeader.contentView.backgroundColor = UIColor(red: 228/255, green: 233/255, blue: 242/255, alpha: 1.0)
        
        return viewHeader
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "Imagens"
        case 1:
            return "Informaçōes"
        case 2:
            return "Comentários"
        default:
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, willBeginEditingRowAt indexPath: IndexPath) {
        print("Editing begin")
    }
    
    func tableView(_ tableView: UITableView, didEndEditingRowAt indexPath: IndexPath?) {
        print("Editing ended")
        self.queue.resume()
    }
}

extension ResultsDetailsViewController {
    @objc func keyboardWillShow(_ notification: Notification) {
        //let userInfo:NSDictionary = notification.userInfo! as NSDictionary
        //let keyboardFrame:NSValue = userInfo.value(forKey: UIResponder.keyboardFrameEndUserInfoKey) as! NSValue
        //let keyboardRectangle = keyboardFrame.cgRectValue
        //let keyboardHeight = keyboardRectangle.height
        //let keyboardWidth = keyboardRectangle.width
        
        self.view.layoutIfNeeded()
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        self.view.layoutIfNeeded()
    }
    
    @objc func buttonCloseWindowPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
        
    @objc func buttonCloseMessagePressed(_ sender: UIButton) {
        self.textViewMessage.resignFirstResponder()
    }
    
    @objc func tapToClosePreview(_ tapGesture: UIGestureRecognizer) {
        self.scrollViewImage.isHidden = true
    }
    
    @objc func tapView(_ tapGesture: UIGestureRecognizer) {
        print("Screen Tapped")
    }
    
    @objc func buttonUserSendMessagePressed(_ sender: UIButton) {
        print("Button Pressed!!!")

        self.resultsMessage.buttonSendUserMessage.isEnabled = false
        self.resultsMessage.buttonSendUserMessage.startLoading()
        self.resultsMessage.buttonSendUserMessage.update(percent: 0)
        
        self.publishResultsCommentary(self.resultsMessage.textViewUserMessage.text, resultId: self.resultsData!.result_id!, relatedClass: "results", response: {
            result in
            
            if result {
                self.resultsMessage.buttonSendUserMessage.update(percent: 100)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2), execute: {
                    self.resultsMessage.buttonSendUserMessage.stopLoading()
                    self.resultsMessage.textViewUserMessage.textColor = .lightGray
                    self.resultsMessage.textViewUserMessage.text = "Faça um comentario"
                    self.resultsMessage.buttonSendUserMessage.isEnabled = true
                })
                
            }
            else {
                print("Error!!!")
                self.resultsMessage.buttonSendUserMessage.stopLoading()
                self.resultsMessage.buttonSendUserMessage.isEnabled = true
            }
        })
    }
    
    @objc func buttonHideUserSendMessagePressed(_ sender: UIButton) {
        self.isUserMessageViewHidden = !self.isUserMessageViewHidden
        sender.isEnabled = false
        
        UIView.animate(withDuration: 1.2, animations: {
            self.resultsMessage.alpha = self.isUserMessageViewHidden ? 0 : 1
        }, completion: {_ in
            self.resultsMessage.isHidden = self.isUserMessageViewHidden
            
            if self.isUserMessageViewHidden {
                sender.setTitle("O", for: .normal)
                sender.setTitleColor(.green, for: .normal)
                sender.layer.borderColor = UIColor.green.cgColor
            }
            else {
                sender.setTitle("X", for: .normal)
                sender.setTitleColor(.red, for: .normal)
                sender.layer.borderColor = UIColor.red.cgColor
            }
            
            sender.isEnabled = true
        })
    }
}

extension ResultsDetailsViewController: ResultsDetailsImagesTableViewCellDelegate {
    func imagesToPreview(_ selectedImage: UIImage?, selectedImage index: Int, listOfImages images: [UIImage?], numberOfImages counter: Int) {
        
        var imageList = [UIImage]()
        
        for img in images {
            imageList.append(img!)
        }
        
        let imageViewer = Agrume(images: imageList, startIndex: index, background: .blurred(.light), dismissal: Dismissal.withPhysics, overlayView: nil)
        
        imageViewer.show(from: self)
    }
    
    func didSelectImageToPreview(show image: UIImage?) {
        if let _ = image {
            self.imageToPreview.image = image
            self.scrollViewImage.isHidden = false
            self.view.layoutIfNeeded()
        }
        else {
            print("No image")
        }
    }
    
    func didPressLike(_ like: Bool, unlike: Bool) {
        if like {
            if self.isViewLoading {
                print("Like loaded")
                self.isViewLoading = false
            }
            else {
                self.postResultLike(self.resultsData!.result_id!, like: "l", response: {
                    data in
                    
                    if data {
                        print("Like saved")
                    }
                })
            }
        }
        else if unlike {
            if self.isViewLoading {
                print("Like loaded")
                self.isViewLoading = false
            }
            else {
                self.postResultLike(self.resultsData!.result_id!, like: "d", response: {
                    data in
                    
                    if data {
                        print("Unlike saved")
                    }
                })
            }
        }
    }
}

extension ResultsDetailsViewController: ResultsDetailsCommentsTableViewCellDelegate {
    func testTextView(_ cell: ResultsDetailsCommentsTableViewCell, textView: UITextView) {
        self.updateHeightOfRow(cell, textView)
    }
    
    func didTouchScreen() {
        print("SCREEN TOUCHED FROM CELL")
    }
    
    
}

extension ResultsDetailsViewController: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.imageToPreview
    }
}

extension ResultsDetailsViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.textColor = .black
            textView.text = ""
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.textColor = .lightGray
            textView.text = "Faça um comentario"
        }
    }
    
    func updateHeightOfRow(_ cell: ResultsDetailsCommentsTableViewCell, _ textView: UITextView) {
            let size = textView.bounds.size
        let newSize = self.tableViewResultsDetails.sizeThatFits(CGSize(width: size.width, height: CGFloat.greatestFiniteMagnitude))
            if size.height != newSize.height {
                UIView.setAnimationsEnabled(false)
                self.tableViewResultsDetails.beginUpdates()
                self.tableViewResultsDetails.endUpdates()
                UIView.setAnimationsEnabled(true)
                if let thisIndexPath = self.tableViewResultsDetails.indexPath(for: cell) {
                    self.tableViewResultsDetails.scrollToRow(at: thisIndexPath, at: .bottom, animated: false)
                }
            }
        }
    
    func textViewDidChange(_ textView: UITextView) {
        if textView.text.isEmpty {
            self.resultsMessage.buttonSendUserMessage.isEnabled = false
        }
        else {
            self.resultsMessage.buttonSendUserMessage.isEnabled = true
        }
    }
}
