//
//  ResultsDetailsCommentsTableViewCell.swift
//  AesCare
//
//  Created by Thiago on 27/03/21.
//

import UIKit

protocol ResultsDetailsCommentsTableViewCellDelegate: class {
    func testTextView(_ cell: ResultsDetailsCommentsTableViewCell, textView: UITextView)
}

class ResultsDetailsCommentsTableViewCell: UITableViewCell {
    @IBOutlet weak var imageViewIcon: UIImageView!
    @IBOutlet weak var textViewMessage: UITextView!
    @IBOutlet weak var labelUsername: UILabel!
    @IBOutlet weak var buttonTest: UIButton!
    @IBOutlet weak var labelPostDate: UILabel!
    
    var usersComments: ResultsModel!
    var textView: UITextView!
    
    weak var delegate: ResultsDetailsCommentsTableViewCellDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setupViews()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
            
    private func setupViews() {
        self.imageViewIcon.setBorder(width: 2, borderColor: .purple, cornerRadious: self.imageViewIcon.frame.height / 2)
        self.imageViewIcon.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleBottomMargin, .flexibleRightMargin, .flexibleLeftMargin, .flexibleTopMargin]
        self.imageViewIcon.contentMode = .scaleAspectFill
        self.imageViewIcon.clipsToBounds = true
        
        self.textViewMessage.delegate = self
        self.textViewMessage.isScrollEnabled = false
        
        self.labelPostDate.textColor = .lightGray
    }
}

extension ResultsDetailsCommentsTableViewCell: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        self.delegate.testTextView(self, textView: textView)
    }
}
