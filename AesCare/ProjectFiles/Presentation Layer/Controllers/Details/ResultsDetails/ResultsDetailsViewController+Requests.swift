//
//  ResultsDetailsViewController+Requests.swift
//  AesCare
//
//  Created by Thiago on 30/03/21.
//

import Foundation
import Alamofire

extension ResultsDetailsViewController {
    func requestMedia(_ resultId: Int, completion: @escaping (Bool) -> Void) {
        let url = "\(BaseUrl)/media/result?result_id=\(resultId)"
        
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default).validate(statusCode: 200 ..< 299).responseJSON { [unowned self] AFdata in
                do {
                    guard let jsonObject = try JSONSerialization.jsonObject(with: AFdata.data!) as? [String: Any] else {
                        print("Error: Cannot convert data to JSON object")
                        return
                    }
                    guard let prettyJsonData = try? JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted) else {
                        print("Error: Cannot convert JSON object to Pretty JSON data")
                        completion(false)
                        return
                    }
                    guard String(data: prettyJsonData, encoding: .utf8) != nil else {
                        print("Error: Could print JSON in String")
                        completion(false)
                        return
                    }
                    
                    let data = try! JSONDecoder().decode(MediasModel.self, from: AFdata.data!)
                    
                    if data.medias!.isEmpty {
                        completion(false)
                    }
                    else {
                        self.mediasData.append(contentsOf: data.medias!)
                        completion(true)
                    }
                } catch {
                    print("Error: Trying to convert JSON data to string")
                    completion(false)
                    return
                }
            }
    }
    
    func publishResultsCommentary(_ message: String, resultId: Int, relatedClass: String, response: @escaping (Bool) -> Void) {
        let headers: HTTPHeaders = ["Content-Type": "multipart/form-data", "Cookie": "aescare_session_key=\(usrerModelOBJ.session_key!)"]
        let messageToPost: Parameters = ["content": message,
                                         "related_id": String(resultId),
                                         "related_class": relatedClass] as Parameters
        let url = "\(BaseUrl)/comment"
        
        Alamofire.upload(multipartFormData: {
            multipartFormData in
            for (key, value) in messageToPost {
                print(value)
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to: url, method: .post, headers: headers, encodingCompletion: {
            encodingResult in
            
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { data in
                    print(data.result.value!)
                    response(true)
                }
            case .failure(let encodingError):
                print(encodingError)
                response(false)
            }
        })
    }
    
    func requestComments(_ resultId: Int, completion: @escaping (ResultsUserCommentsResponse?) -> Void) {
        //let url = "\(BaseUrl)/user/results?user_id=\(userId)"
        let headers: HTTPHeaders = ["Content-Type": "multipart/form-data", "Cookie": "aescare_session_key=\(usrerModelOBJ.session_key!)"]
        let url = "\(BaseUrl)/result/detail?result_id=\(resultId)"
        
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers).validate(statusCode: 200 ..< 299).responseJSON { AFdata in
                do {
                    guard let jsonObject = try JSONSerialization.jsonObject(with: AFdata.data!) as? [String: Any] else {
                        print("Error: Cannot convert data to JSON object")
                        return
                    }
                    guard let prettyJsonData = try? JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted) else {
                        print("Error: Cannot convert JSON object to Pretty JSON data")
                        completion(nil)
                        return
                    }
                    guard String(data: prettyJsonData, encoding: .utf8) != nil else {
                        print("Error: Could print JSON in String")
                        completion(nil)
                        return
                    }
                    
                    let data = try! JSONDecoder().decode(ResultsUserComments.self, from: AFdata.data!)
                    
                    if let _ = data.result {
                        if data.result!.comments!.isEmpty {
                            completion(nil)
                        }
                        else {
                            completion(data.result)
                        }
                    }
                    else {
                        completion(nil)
                    }
                } catch {
                    print("Error: Trying to convert JSON data to string")
                    completion(nil)
                    return
                }
            }
    }
    
    func deleteResultsCommentary(_ commentId: Int, response: @escaping (Bool) -> Void) {
        let headers: HTTPHeaders = ["Content-Type": "multipart/form-data",
                                    "Cookie": "aescare_session_key=\(usrerModelOBJ.session_key!)"]
                                    //"Cookie": "aescare_user_key=\(usrerModelOBJ.user_key!)"]
        let messageToPost: Parameters = ["comment_id": String(commentId)] as Parameters
        let url = "\(BaseUrl)/comment"
        
        Alamofire.upload(multipartFormData: {
            multipartFormData in
            for (key, value) in messageToPost {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to: url, method: .delete, headers: headers, encodingCompletion: {
            encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { data in
                    print(data.result.value!)
                    response(true)
                }
            case .failure(let encodingError):
                print(encodingError)
                response(false)
            }
        })
    }
    
    func postResultLike(_ resultId: Int, like: String, response: @escaping (Bool) -> Void) {
        let headers: HTTPHeaders = ["Content-Type": "multipart/form-data", "Cookie": "aescare_session_key=\(usrerModelOBJ.session_key!)"]
        let messageToPost: Parameters = ["result_id": String(resultId), "action": like] as Parameters
        let url = "\(BaseUrl)/result/like"
        
        Alamofire.upload(multipartFormData: {
            multipartFormData in
            for (key, value) in messageToPost {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to: url, method: .delete, headers: headers, encodingCompletion: {
            encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { data in
                    print(data.result.value ?? "")
                    response(true)
                }
            case .failure(let encodingError):
                print(encodingError)
                response(false)
            }
        })
    }
}
