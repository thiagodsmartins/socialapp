//
//  ResultsDetailsInfoTableViewCell.swift
//  AesCare
//
//  Created by Thiago on 26/03/21.
//

import UIKit
import Magnetic

class ResultsDetailsInfoTableViewCell: UITableViewCell {
    @IBOutlet weak var spriteViewInfo: MagneticView!
    var infoData: ResultsModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setupSpriteView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    private func setupSpriteView() {
        let node1 = Node(text: "Doutor", image: UIImage(named: "doutor"), color: .purple, radius: 35)
        let node2 = Node(text: "Pagamento", image: UIImage(named: "pagamento"), color: .green, radius: 35)
        let node3 = Node(text: "Valor", image: UIImage(named: "valor"), color: .green, radius: 35)
        let node4 = Node(text: "Data", image: UIImage(named: "data"), color: .orange, radius: 35)
        let node5 = Node(text: "Cidade", image: UIImage(named: "cidade"), color: .orange, radius: 35)
        node1.name = "doutor"
        node2.name = "pagamento"
        node3.name = "valor"
        node4.name = "data"
        node5.name = "cidade"
        
        //self.spriteViewInfo.contentScaleFactor = CGFloat(10)
        self.spriteViewInfo.magnetic.addChild(node1)
        self.spriteViewInfo.magnetic.addChild(node2)
        self.spriteViewInfo.magnetic.addChild(node3)
        self.spriteViewInfo.magnetic.addChild(node4)
        self.spriteViewInfo.magnetic.addChild(node5)
        self.spriteViewInfo.magnetic.magneticDelegate = self
    }
}

extension ResultsDetailsInfoTableViewCell: MagneticDelegate {
    func magnetic(_ magnetic: Magnetic, didSelect node: Node) {
        if node.name == "doutor" {
            node.update(radius: CGFloat(40))
            node.label.text = self.infoData?.medic?.name_with_title!
        }
        else if node.name == "pagamento" {
            node.update(radius: CGFloat(40))
            node.label.text = self.infoData?.payment_type!
        }
        else if node.name == "valor" {
            node.update(radius: CGFloat(40))
            node.label.text = self.infoData?.treatment_value!
        }
        else if node.name == "data" {
            node.update(radius: CGFloat(40))
            node.label.text = self.infoData?.treatment_date_formated!
        }
        else if node.name == "cidade" {
            node.update(radius: CGFloat(40))
            node.label.text = self.infoData?.city?.name!
        }
    }
    
    func magnetic(_ magnetic: Magnetic, didDeselect node: Node) {
        if node.name == "doutor" {
            node.update(radius: CGFloat(35))
            node.label.text = "Doutor"
        }
        else if node.name == "pagamento" {
            node.update(radius: CGFloat(35))
            node.label.text = "Pagamento"
        }
        else if node.name == "valor" {
            node.update(radius: CGFloat(35))
            node.label.text = "Valor"
        }
        else if node.name == "data" {
            node.update(radius: CGFloat(35))
            node.label.text = "Data"
        }
        else if node.name == "cidade" {
            node.update(radius: CGFloat(35))
            node.label.text = "Cidade"
        }
    }
}
