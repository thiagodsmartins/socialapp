//
//  ResultsDetailsMessage.swift
//  AesCare
//
//  Created by Thiago on 12/04/21.
//

import Foundation
import UIKit

class ResultsDetailsMessage: UIView {
    @IBOutlet weak var imageViewUserIcon: UIImageView!
    @IBOutlet weak var textViewMessage: UITextView!
    @IBOutlet weak var buttonSendMessage: UIButton!
    @IBOutlet weak var viewResultsDetails: UIView!
    
    var view: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        loadViewFromNib()
        //self.setupViews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        loadViewFromNib()
        //self.setupViews()
    }
    
    private func loadViewFromNib() {
//        let bundle = Bundle(for: type(of: self))
//        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
//        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
//        view.frame = bounds
//        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
//        addSubview(view)
        //self.view = view
        Bundle.main.loadNibNamed("ResultsDetailsMessage", owner: self, options: nil)
        self.setupViews()
    }
    
    private func setupViews() {
        self.view.setBorder(width: 1.0, borderColor: .lightGray, cornerRadious: 20)
        
        self.imageViewUserIcon.layer.cornerRadius = self.imageViewUserIcon.frame.height / 2
        self.imageViewUserIcon.layer.borderColor = UIColor(red: 175/255, green: 68/255, blue: 217/255, alpha: 1.0).cgColor
        self.imageViewUserIcon.layer.borderWidth = 2.0
        
        self.textViewMessage.placeholder = "Escreva um comentário"
        
        self.buttonSendMessage.backgroundColor = .white
        self.buttonSendMessage.layer.cornerRadius = 10
        self.buttonSendMessage.titleLabel?.adjustsFontSizeToFitWidth = true
    }
}
