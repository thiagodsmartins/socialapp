//
//  ResultsDetailsImagesTableViewCell.swift
//  AesCare
//
//  Created by Thiago on 25/03/21.
//

import UIKit
import FSPagerView
import SDWebImage
import Cosmos
import SweetLike

protocol ResultsDetailsImagesTableViewCellDelegate: class {
    func didSelectImageToPreview(show image: UIImage?)
    func imagesToPreview(_ selectedImage: UIImage?, selectedImage index: Int, listOfImages images: [UIImage?], numberOfImages counter: Int)
    func didPressLike(_ like: Bool, unlike: Bool)
}
class ResultsDetailsImagesTableViewCell: UITableViewCell {
    @IBOutlet weak var pagerView: FSPagerView!
    @IBOutlet weak var starRating: CosmosView!
    @IBOutlet weak var likeButton: SweetLike!
    
    var pageControl: FSPageControl!
    var itemSelected = false
    var imagesURL = [String]()
    var imageManager: SDWebImageDownloader!
    var imagesList = [UIImage?]()
    
    weak var delegate: ResultsDetailsImagesTableViewCellDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setupPagerView()
        self.setupImageManager()
        self.setupStarRating()
        self.setupLikeButton()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    private func setupPagerView() {
        self.selectionStyle = .none
        
        self.pagerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
        self.pagerView.automaticSlidingInterval = 3.0
        self.pagerView.bounces = true
        self.pagerView.itemSize = CGSize(width: 310, height: 160)
        //self.pagerView.itemSize = FSPagerView.automaticSize
        self.pagerView.interitemSpacing = 10
        self.pagerView.transformer = FSPagerViewTransformer(type: .coverFlow)
        self.pagerView.delegate = self
        self.pagerView.dataSource = self
        
        self.pageControl = FSPageControl(frame: CGRect(x: (self.contentView.frame.origin.x + 320), y: (self.contentView.frame.origin.y + 140), width: 50, height: 50))
        self.pageControl.setFillColor(.purple, for: .normal)
        self.pageControl.setStrokeColor(.black, for: .normal)
        self.pageControl.numberOfPages = 3
        self.pageControl.isHidden = true
        self.contentView.addSubview(self.pageControl)
    }
    
    private func setupImageManager() {
        self.imageManager = SDWebImageDownloader()
        self.imageManager.setValue("aescare_session_key=\(usrerModelOBJ.session_key!)", forHTTPHeaderField: "Cookie")
    }
    
    private func setupStarRating() {
        self.starRating.rating = 5
        self.starRating.settings.filledColor = UIColor(red: 175/255, green: 68/255, blue: 217/255, alpha: 1.0)
        self.starRating.settings.updateOnTouch = false
    }
    
    private func setupLikeButton() {
        self.likeButton.delegate = self
    }
}

extension ResultsDetailsImagesTableViewCell: FSPagerViewDelegate, FSPagerViewDataSource, UICollectionViewDelegate {
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        self.pageControl.numberOfPages = imagesURL.count
        return imagesURL.count
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = self.pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)

        if let imageCache = SDImageCache.shared.imageFromCache(forKey: self.imagesURL[index]) {
            cell.imageView?.image = imageCache
            cell.textLabel?.text = "Imagem \(index + 1)"
            self.imagesList.append(imageCache)
        }
        else {
            self.imageManager.downloadImage(with: URL(string: self.imagesURL[index]), completed: {
                (image, data, cacheType, finished) in
                
                if finished {
                    if let image = image {
                        cell.imageView?.image = image
                        self.imagesList.append(image)
                        SDImageCache.shared.storeImage(toMemory: image, forKey: self.imagesURL[index])
                        cell.textLabel?.text = "Imagem \(index + 1)"
                    }
                    else {
                        cell.imageView?.image = UIImage()
                        cell.textLabel?.text = "Imagem \(index + 1)"
                        self.imagesList.append(UIImage())
                    }
                }
            })
        }
        
        return cell
    }
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        pagerView.deselectItem(at: index, animated: true)
    
        let cell = pagerView.cellForItem(at: index)
        //self.delegate.didSelectImageToPreview(show: cell?.imageView!.image)
        self.delegate.imagesToPreview(cell?.imageView!.image, selectedImage: index, listOfImages: self.imagesList, numberOfImages: self.imagesList.count)
//        self.delegate.didSelectImageToPreview(show: pagerView[index].)
    }
    
    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
        self.pageControl.isHidden = false
        self.pageControl.currentPage = targetIndex
    }
    
    func pagerViewDidEndScrollAnimation(_ pagerView: FSPagerView) {
        self.pageControl.isHidden = true
    }
}

extension ResultsDetailsImagesTableViewCell: SweetLikeDelegate {
    func likeAction() {
        self.likeButton.likedColor = .red
        self.delegate.didPressLike(true, unlike: false)
    }
    
    func unlikeAction() {
        //zself.delegate.didPressLike(false, unlike: true)
    }
}
