//
//  StartingPageViewController.swift
//  AesCare
//
//  Created by Thiago on 12/04/21.
//

import Foundation
import UIKit
import FSPagerView

class StartingPageViewController: UIViewController {
    @IBOutlet weak var pagerViewPresentation: FSPagerView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelMessage: UILabel!
    @IBOutlet weak var buttonStart: UIButton!
    @IBOutlet weak var pageControlPresentation: FSPageControl!
    
    var presentationImages: [UIImage]!
    var presentationMessages: [String:String]!
    var welcomeViewController: StartingPageWelcomeViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupImages()
        self.setupMessages()
        self.setupViews()
        self.setupSlideView()
    }
    
    private func setupSlideView() {
        self.pagerViewPresentation.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
        self.pagerViewPresentation.automaticSlidingInterval = 0
        self.pagerViewPresentation.bounces = true
        self.pagerViewPresentation.itemSize = CGSize(width: self.pagerViewPresentation.bounds.size.width, height: self.pagerViewPresentation.bounds.size.height)
        
        self.pagerViewPresentation.interitemSpacing = 10
        self.pagerViewPresentation.transformer = FSPagerViewTransformer(type: .zoomOut)
        self.pagerViewPresentation.delegate = self
        self.pagerViewPresentation.dataSource = self
    }
    
    private func setupImages() {
        self.presentationImages = [UIImage]()
        self.presentationImages.insert(UIImage(named: "slide-image1")!, at: 0)
        self.presentationImages.insert(UIImage(named: "slide-image2")!, at: 1)
        self.presentationImages.insert(UIImage(named: "slide-image3")!, at: 2)
    }
    
    private func setupMessages() {
        self.presentationMessages = [String:String]()
        self.presentationMessages["title1"] = StartingPageMessage.slideTitle1.message()
        self.presentationMessages["title2"] = StartingPageMessage.slideTitle2.message()
        self.presentationMessages["title3"] = StartingPageMessage.slideTitle3.message()
        self.presentationMessages["message1"] = StartingPageMessage.slideMessage1.message()
        self.presentationMessages["message2"] = StartingPageMessage.slideMessage2.message()
        self.presentationMessages["message3"] = StartingPageMessage.slideMessage3.message()
    }
    
    private func setupViews() {
        self.buttonStart.isHidden = true
        self.buttonStart.tintColor = .white
        self.buttonStart.addTarget(self, action: #selector(self.buttonStartPressed(_:)), for: .touchUpInside)
        self.gradientEffect()
        
        self.pageControlPresentation.contentHorizontalAlignment = .center
        self.pageControlPresentation.numberOfPages = 3
    }
    
    private func gradientEffect() {
        let gradient = CAGradientLayer()
        gradient.frame = self.buttonStart.bounds
        gradient.colors = [UIColor(red: 168/255, green: 72/255, blue: 217/255, alpha: 1.0).cgColor, UIColor(red: 105/255, green: 108/255, blue: 220/255, alpha: 1.0).cgColor]
        gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        gradient.cornerRadius = 10
        
        self.buttonStart.layer.addSublayer(gradient)
    }
    
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size

        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height

        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)

        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return newImage!
    }
    
}

extension StartingPageViewController: FSPagerViewDelegate, FSPagerViewDataSource {
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return self.presentationImages.count
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        
        cell.imageView?.image = self.resizeImage(image: self.presentationImages[index], targetSize: CGSize(width: self.view.frame.width, height: self.view.frame.height))
        cell.imageView?.contentMode = .scaleAspectFill
        
        self.labelTitle.text = self.presentationMessages["title\(index + 1)"]
        self.labelMessage.text = self.presentationMessages["message\(index + 1)"]
        self.pageControlPresentation.currentPage = index
        
        index == 2 ? (self.buttonStart.isHidden = false) : (self.buttonStart.isHidden = true)
        
        return cell
    }
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        pagerView.deselectItem(at: index, animated: true)
    }
}

extension StartingPageViewController {
    @objc func buttonStartPressed(_ sender: UIButton) {
        self.welcomeViewController = StartingPageWelcomeViewController()
        self.welcomeViewController.modalPresentationStyle = .fullScreen
    
        self.present(self.welcomeViewController, animated: false, completion: nil)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(10), execute: {
            self.welcomeViewController.dismiss(animated: false, completion: nil)
            
            let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let loginViewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            
            self.navigationController?.pushViewController(loginViewController, animated: true)
        })
    }
}


