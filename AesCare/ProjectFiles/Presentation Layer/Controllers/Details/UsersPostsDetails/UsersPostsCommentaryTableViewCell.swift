//
//  UsersPostsCommentaryTableViewCell.swift
//  AesCare
//
//  Created by Thiago on 03/04/21.
//

import UIKit

class UsersPostsCommentaryTableViewCell: UITableViewCell {
    @IBOutlet weak var imageViewUserIcon: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelUsername: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var textViewMessage: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setupViews()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    private func setupViews() {
        //self.imageViewUserIcon.setBorder(width: 1.0, borderColor: .purple, cornerRadious: 10)
        self.imageViewUserIcon.layer.cornerRadius = self.imageViewUserIcon.frame.height / 2
        self.imageViewUserIcon.layer.borderWidth = 2.0
        self.imageViewUserIcon.layer.borderColor = UIColor(red: 175/255, green: 68/255, blue: 217/255, alpha: 1.0).cgColor
//        self.imageViewUserIcon.layer.backgroundColor =
        
        self.labelTitle.numberOfLines = 0
        self.labelTitle.lineBreakMode = .byWordWrapping
        
        self.labelUsername.numberOfLines = 0
        self.labelUsername.lineBreakMode = .byWordWrapping
        
        self.textViewMessage.setBorder(width: 1.0, borderColor: UIColor(red: 232/255, green: 233/255, blue: 235/255, alpha: 1.0), cornerRadious: 10)
        
    }
}
