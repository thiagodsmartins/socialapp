//
//  UsersPostsInformationTableViewCell.swift
//  AesCare
//
//  Created by Thiago on 14/04/21.
//

import UIKit
import ASCollectionView
import GlidingCollection
import Player

protocol UsersPostsInformationTableViewCellDelegate: class {
    func playSelectedVideo(_ videoUrl: String)
    func openSelectedImage(_ image: UIImage?)
}

class UsersPostsInformationTableViewCell: UITableViewCell {
    @IBOutlet weak var glidingCollectionPosts: GlidingCollection!
    
    var collectionView: UICollectionView!
    var mediasData = [[String:[String:String]]]()
    weak var delegate: UsersPostsInformationTableViewCellDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setupGlidingCollectionView()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    private func setupGlidingCollectionView() {
        let nib = UINib(nibName: "UsersPostsInformationImageCollectionViewCell", bundle: nil)
        self.glidingCollectionPosts.dataSource = self
        
        collectionView = self.glidingCollectionPosts.collectionView
        collectionView.register(nib, forCellWithReuseIdentifier: "UsersPostsInformationImageCollectionViewCell")
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.backgroundColor = self.glidingCollectionPosts.backgroundColor
    }
}

extension UsersPostsInformationTableViewCell: GlidingCollectionDatasource {
    func glidingCollection(_ collection: GlidingCollection, itemAtIndex index: Int) -> String {
        return "Imagem \(self.mediasData[index])"
    }
    
    func numberOfItems(in collection: GlidingCollection) -> Int {
        return mediasData.count
    }
}

extension UsersPostsInformationTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let section = self.glidingCollectionPosts.expandedItemIndex
        return mediasData[section].count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UsersPostsInformationImageCollectionViewCell", for: indexPath) as! UsersPostsInformationImageCollectionViewCell
        let section = self.glidingCollectionPosts.expandedItemIndex
                
        print(self.mediasData.count)
        
        if let imageUrl = self.mediasData[section]["medias_image\(indexPath.row)"] {
            cell.imageViewIcons.sd_setImage(with: URL(string: imageUrl["image"]!)!, completed: nil)
        }
        
        if let _ = self.mediasData[section]["medias_video\(indexPath.row)"] {
            cell.imageViewIcons.image = UIImage(named: "logo-aescare")
        }
        
        let layer = cell.layer
        let config = GlidingConfig.shared
        layer.shadowOffset = config.cardShadowOffset
        layer.shadowColor = config.cardShadowColor.cgColor
        layer.shadowOpacity = config.cardShadowOpacity
        layer.shadowRadius = config.cardShadowRadius
        
        layer.shouldRasterize = true
        layer.rasterizationScale = UIScreen.main.scale
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let section = self.glidingCollectionPosts.expandedItemIndex
        let item = indexPath.item
        let image = UIImageView()
        
        if let video = self.mediasData[section]["medias_video\(indexPath.row)"] {
            self.delegate.playSelectedVideo(video["video"]!)
        }
        else if let imageUrl = self.mediasData[section]["medias_image\(indexPath.row)"] {
            image.sd_setImage(with: URL(string: imageUrl["image"]!)!, completed: {_,_,_,_ in
                self.delegate.openSelectedImage(image.image)
            })

        }
        
        print("Selected item #\(item) in section #\(section)")
    }
}



