//
//  UsersPostsCommentsTableViewCell.swift
//  AesCare
//
//  Created by Thiago on 15/04/21.
//

import UIKit

class UsersPostsCommentsTableViewCell: UITableViewCell {
    @IBOutlet weak var labelUsername: UILabel!
    @IBOutlet weak var imageViewIcon: UIImageView!
    @IBOutlet weak var labelType: UILabel!
    @IBOutlet weak var labelPostDate: UILabel!
    @IBOutlet weak var textViewMessage: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setupViews()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    private func setupViews() {
        self.imageViewIcon.layer.cornerRadius = self.imageViewIcon.frame.height / 2
        self.imageViewIcon.layer.borderWidth = 2
        self.imageViewIcon.layer.borderColor = UIColor(red: 175/255, green: 68/255, blue: 217/255, alpha: 1.0).cgColor
        
        self.textViewMessage.setBorder(width: 2.0, borderColor: UIColor(red: 232/255, green: 233/255, blue: 235/255, alpha: 1.0), cornerRadious: 10)
    }
}
