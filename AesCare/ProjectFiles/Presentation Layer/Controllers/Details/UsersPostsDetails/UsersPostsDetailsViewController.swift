//
//  UsersPostsDetailsViewController.swift
//  AesCare
//
//  Created by Thiago on 03/04/21.
//

import Foundation
import UIKit
import Player
import Agrume

class UsersPostsDetailsViewController: UIViewController {
    @IBOutlet weak var viewGradient: UIView!
    @IBOutlet weak var closeWindow: UIButton!
    @IBOutlet weak var imageViewIcon: UIImageView!
    @IBOutlet weak var tableViewUsersPostsDetails: UITableView!
    
    var nibFiles: [UINib]?
    var postsData: PostsModel!
    var isMedic: Bool!
    var postsUrls: [String: [String:String]]!
    var videopPlayer: Player?
    var imageManager: Agrume?
    var postsMessage: UserSendMessageView!
    var isUserMessageViewHidden = false
    
    var queue: DispatchQueue!
    var workItem: DispatchWorkItem!
    var semaphore: DispatchSemaphore!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        self.postsUrls = self.extractData(self.postsData.medias!)
        
        self.setupViews()
        self.setupTableView()
    }
    
    private func setupTableView() {
        self.nibFiles = [UINib]()
        self.nibFiles?.append(UINib(nibName: "UsersPostsCommentaryTableViewCell", bundle: nil))
        self.nibFiles?.append(UINib(nibName: "UsersPostsInformationTableViewCell", bundle: nil))
        self.nibFiles?.append(UINib(nibName: "UsersPostsCommentsTableViewCell", bundle: nil))
        self.nibFiles?.append(UINib(nibName: "EmptyTableViewCell", bundle: nil))
        
        self.tableViewUsersPostsDetails.register(self.nibFiles![0], forCellReuseIdentifier: "UsersPostsCommentaryTableViewCell")
        self.tableViewUsersPostsDetails.register(self.nibFiles![1], forCellReuseIdentifier: "UsersPostsInformationTableViewCell")
        self.tableViewUsersPostsDetails.register(self.nibFiles![2], forCellReuseIdentifier: "UsersPostsCommentsTableViewCell")
        self.tableViewUsersPostsDetails.register(self.nibFiles![3], forCellReuseIdentifier: "EmptyTableViewCell")
        self.tableViewUsersPostsDetails.estimatedRowHeight = UITableView.automaticDimension
        self.tableViewUsersPostsDetails.rowHeight = 300
        self.tableViewUsersPostsDetails.dataSource = self
        self.tableViewUsersPostsDetails.delegate = self
    }
    
    private func setupViews() {
        self.closeWindow.addTarget(self, action: #selector(self.buttonCloseWindowPressed(_:)), for: .touchUpInside)
        self.closeWindow.backgroundColor = UIColor(red: 175/255, green: 68/255, blue: 217/255, alpha: 1.0)
        self.closeWindow.tintColor = .white
        self.closeWindow.layer.cornerRadius = self.closeWindow.frame.size.width / 2
        self.closeWindow.clipsToBounds = true
        
        self.imageViewIcon.layer.cornerRadius = self.imageViewIcon.frame.size.width / 2
        self.imageViewIcon.layer.masksToBounds = false
        self.imageViewIcon.clipsToBounds = true
        self.imageViewIcon.image = UIImage(named: "detailimage")
        
        self.gradientView()
        self.setupVideoPlayer()
        self.setupUserMessageView()
        //self.setupImageManager()
    }
    
    private func setupUserMessageView() {
        let buttonUserMessageView = UIButton(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        self.postsMessage = UserSendMessageView()
        
        buttonUserMessageView.setTitle("X", for: .normal)
        buttonUserMessageView.setTitleColor(.red, for: .normal)
        buttonUserMessageView.layer.cornerRadius = buttonUserMessageView.frame.height / 2
        buttonUserMessageView.layer.borderWidth = 2
        buttonUserMessageView.layer.borderColor = UIColor.red.cgColor
        buttonUserMessageView.addTarget(self, action: #selector(self.buttonHideUserSendMessagePressed(_:)), for: .touchUpInside)
        buttonUserMessageView.translatesAutoresizingMaskIntoConstraints = false
        
        self.postsMessage.buttonSendUserMessage.addTarget(self, action: #selector(self.buttonUserSendMessagePressed(_:)), for: .touchUpInside)
        self.postsMessage.buttonSendUserMessage.isEnabled = false
        self.postsMessage.textViewUserMessage.delegate = self
        self.postsMessage.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addSubview(self.postsMessage)
        self.view.addSubview(buttonUserMessageView)
        self.view.bringSubviewToFront(buttonUserMessageView)
        
        self.postsMessage.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: -10).isActive = true
        self.postsMessage.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: 10).isActive = true
        self.postsMessage.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: -10).isActive = true
        //self.resultsMessage.heightAnchor.constraint(equalTo: self.resultsMessage.heightAnchor, multiplier: 1.0).isActive = true
        //self.resultsMessage.heightAnchor.constraint(equalToConstant: 60).isActive = true
        
        buttonUserMessageView.bottomAnchor.constraint(equalTo: self.postsMessage.topAnchor, constant: -5).isActive = true
        buttonUserMessageView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        buttonUserMessageView.heightAnchor.constraint(equalToConstant: 20).isActive = true
        buttonUserMessageView.widthAnchor.constraint(equalToConstant: 20).isActive = true
    }
    
    private func gradientView() {
        let gradient = CAGradientLayer()
        gradient.frame = self.viewGradient.bounds
        gradient.colors = [UIColor(red: 168/255, green: 72/255, blue: 217/255, alpha: 1.0).cgColor, UIColor(red: 105/255, green: 108/255, blue: 220/255, alpha: 1.0).cgColor]
        gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        self.viewGradient.layer.addSublayer(gradient)
        self.viewGradient.addSubview(self.closeWindow)
        self.viewGradient.addSubview(self.imageViewIcon)
    }
    
    private func extractData(_ medias: [PostsMediasModel]) -> [String: [String:String]]{
        var dataMap = [String: [String:String]]()
        var counter = 0
        
        for urls in medias {
            if let image = urls.cover_url {
                dataMap["medias_image\(counter)"] = ["image": image]
            }
            
            if let video = urls.video_url {
                dataMap["medias_video\(counter)"] = ["video": video]
            }
            
            counter += 1
        }
        
        return dataMap
    }
    
    private func setupVideoPlayer() {
        let closeButton = UIButton(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
        closeButton.setTitle("X", for: .normal)
        closeButton.backgroundColor = .clear
        closeButton.setBorder(width: 3, borderColor: .white, cornerRadious: closeButton.frame.height / 2)
        closeButton.translatesAutoresizingMaskIntoConstraints = false
        closeButton.addTarget(self, action: #selector(self.buttonCloseVideoPlayerPressed(_:)), for: .touchUpInside)
        
        self.videopPlayer = Player()
        self.videopPlayer?.view.frame = self.view.bounds
        self.videopPlayer?.playbackDelegate = self
        self.videopPlayer?.playerDelegate = self
        self.videopPlayer?.playerView.backgroundColor = .black
        self.videopPlayer?.playerView.isHidden = true
        
        self.addChild(self.videopPlayer!)
        self.view.addSubview(self.videopPlayer!.view)
        self.videopPlayer?.playerView.addSubview(closeButton)
        
        closeButton.leadingAnchor.constraint(equalTo: (self.videopPlayer?.playerView.leadingAnchor)!, constant: 10).isActive = true
        closeButton.topAnchor.constraint(equalTo: (self.videopPlayer?.playerView.topAnchor)!, constant: 10).isActive = true
        closeButton.heightAnchor.constraint(equalToConstant: 60).isActive = true
        closeButton.widthAnchor.constraint(equalToConstant: 60).isActive = true
    }
    
    private func setupImageManager() {
        self.imageManager = Agrume(image: UIImage(named: "logo-aescare")!)
    }
    
    private func setupBackgroundResultsRequest() {
        self.queue = DispatchQueue.global(qos: .userInitiated)
        self.semaphore = DispatchSemaphore(value: 0)
//        self.workItem = DispatchWorkItem {
//            while !self.workItem.isCancelled {
//                self.requestComments(self.resultsData!.user_id! , completion: {
//                    data in
//                    if let data = data {
//                        self.resultsComments[0] = data
//
//                        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(5), execute: {
//                            self.tableViewResultsDetails.reloadSections(IndexSet(integer: 2), with: .none)
//
//                            self.semaphore.signal()
//                        })
//                    }
//                    else {
//                        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(5), execute: {
//                            print("No data")
//                            self.semaphore.signal()
//                        })
//                    }
//                })
//
//                self.semaphore.wait()
//            }
//
//            self.workItem = nil
//        }
//
//        self.queue.async(execute: self.workItem)
    }
    
}

extension UsersPostsDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
        else if section == 1{
            if self.postsData.medias!.count > 0 {
                return self.postsUrls.count
                //return self.postsData.medias!.count
            }
            else {
                return 0
            }
        }
        else if section == 2 {
            if self.postsData.comments_count! > 0 {
                return self.postsData.comments_count!
            }
            else {
                return 1
            }
        }
        else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = self.tableViewUsersPostsDetails.dequeueReusableCell(withIdentifier: "UsersPostsCommentaryTableViewCell") as! UsersPostsCommentaryTableViewCell
            
            //print(self.postsData!.post_id)
            
            if self.isMedic {
                print(self.postsData.medic?.specialties![0].name ?? "")
                cell.imageViewUserIcon.sd_setImage(with: URL(string: (self.postsData.medic?.cover_media!.image_url!)!), completed: nil)
                cell.labelTitle.text = "Especialista em \(self.postsData.medic?.specialties![0].name ?? "")"
                cell.labelUsername.text = self.postsData.medic?.name_with_title!
                cell.labelDate.text = self.postsData.created_at_formatted!
                cell.textViewMessage.text = self.postsData.body!
            }
            else {
                cell.imageViewUserIcon.sd_setImage(with: URL(string: (self.postsData.user?.image_url!)!), completed: nil)
                cell.labelTitle.text = "Usuaria"
                cell.labelUsername.text = self.postsData.user!.username!
                cell.labelDate.text = self.postsData.created_at_formatted!
                cell.textViewMessage.text = self.postsData.body!
            }
            
            return cell
        }
        else if indexPath.section == 1 {
            let cell = self.tableViewUsersPostsDetails.dequeueReusableCell(withIdentifier: "UsersPostsInformationTableViewCell") as! UsersPostsInformationTableViewCell
            cell.mediasData.append(self.postsUrls)
            cell.delegate = self
            return cell
        }
        else if indexPath.section == 2 {
            if self.postsData.comments_count! > 0 {
                let cell = self.tableViewUsersPostsDetails.dequeueReusableCell(withIdentifier: "UsersPostsCommentsTableViewCell") as! UsersPostsCommentsTableViewCell
                
                cell.imageViewIcon.sd_setImage(with: URL(string: (self.postsData.comments![indexPath.row].user?.url_image!)!), completed: nil)
                cell.labelUsername.text = self.postsData.comments![indexPath.row].user!.username!
                cell.labelPostDate.text = self.postsData.comments![indexPath.row].created_at!
                cell.textViewMessage.text = self.postsData.comments![indexPath.row].content!
                cell.labelType.text = self.postsData.comments![indexPath.row].user?.role == "user" ? "Usuario" : "Médico"
                
                return cell
            }
            else {
                let cell = self.tableViewUsersPostsDetails.dequeueReusableCell(withIdentifier: "EmptyTableViewCell") as! EmptyTableViewCell
                
                cell.labelMessage.text = "Nenhum comentario"
                
                return cell
            }
        }
        else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let viewHeader = UITableViewHeaderFooterView()
        
        if section == 0 {
            viewHeader.textLabel?.text = "Postagem"
            return viewHeader
        }
        else if section == 1 {
            viewHeader.textLabel?.text = "Informaçōes"
            return viewHeader
        }
        else if section == 2 {
            viewHeader.textLabel?.text = "Comentarios"
            return viewHeader
        }
        else {
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.rowHeight
    }
}

extension UsersPostsDetailsViewController {
    @objc func buttonCloseWindowPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func buttonCloseVideoPlayerPressed(_ sender: UIButton) {
        self.videopPlayer?.stop()
        self.videopPlayer?.playerView.isHidden = true
    }
    
    @objc func buttonUserSendMessagePressed(_ sender: UIButton) {
        print("Button Pressed!!!")

        self.postsMessage.buttonSendUserMessage.isEnabled = false
        self.postsMessage.buttonSendUserMessage.startLoading()
        self.postsMessage.buttonSendUserMessage.update(percent: 0)
        
        self.publishPostsCommentary(self.postsMessage.textViewUserMessage.text, resultId: self.postsData!.post_id!, relatedClass: "posts", response: {
            result in
            
            if result {
                self.postsMessage.buttonSendUserMessage.update(percent: 100)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2), execute: {
                    self.postsMessage.buttonSendUserMessage.stopLoading()
                    self.postsMessage.textViewUserMessage.textColor = .lightGray
                    self.postsMessage.textViewUserMessage.text = "Faça um comentario"
                    self.postsMessage.buttonSendUserMessage.isEnabled = true
                })
                
            }
            else {
                print("Error!!!")
                self.postsMessage.buttonSendUserMessage.stopLoading()
                self.postsMessage.buttonSendUserMessage.isEnabled = true
            }
        })
    }
    
    @objc func buttonHideUserSendMessagePressed(_ sender: UIButton) {
        self.isUserMessageViewHidden = !self.isUserMessageViewHidden
        sender.isEnabled = false
        
        UIView.animate(withDuration: 1.2, animations: {
            self.postsMessage.alpha = self.isUserMessageViewHidden ? 0 : 1
        }, completion: {_ in
            self.postsMessage.isHidden = self.isUserMessageViewHidden
            
            if self.isUserMessageViewHidden {
                sender.setTitle("O", for: .normal)
                sender.setTitleColor(.green, for: .normal)
                sender.layer.borderColor = UIColor.green.cgColor
            }
            else {
                sender.setTitle("X", for: .normal)
                sender.setTitleColor(.red, for: .normal)
                sender.layer.borderColor = UIColor.red.cgColor
            }
            
            sender.isEnabled = true
        })
    }
}

extension UsersPostsDetailsViewController: UsersPostsInformationTableViewCellDelegate, PlayerPlaybackDelegate, PlayerDelegate {
    
    func playSelectedVideo(_ videoUrl: String) {
        self.videopPlayer?.url = URL(string: videoUrl)!
        self.videopPlayer?.playerView.isHidden = false
        self.videopPlayer?.playFromBeginning()
    }
    
    func openSelectedImage(_ image: UIImage?) {
        self.imageManager = Agrume(image: image!, background: .blurred(.regular))
        self.imageManager?.show(from: self)
    }
    
    func playerCurrentTimeDidChange(_ player: Player) {
        
    }
    
    func playerPlaybackWillStartFromBeginning(_ player: Player) {
        
    }
    
    func playerPlaybackDidEnd(_ player: Player) {
        
    }
    
    func playerPlaybackWillLoop(_ player: Player) {
        
    }
    
    func playerPlaybackDidLoop(_ player: Player) {
        
    }
    
    func playerReady(_ player: Player) {
        print("Player ready!!!")
        
    }
    
    func playerPlaybackStateDidChange(_ player: Player) {
        
    }
    
    func playerBufferingStateDidChange(_ player: Player) {
        
    }
    
    func playerBufferTimeDidChange(_ bufferTime: Double) {
        
    }
    
    func player(_ player: Player, didFailWithError error: Error?) {
        
    }
}

extension UsersPostsDetailsViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.textColor = .black
            textView.text = ""
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.textColor = .lightGray
            textView.text = "Faça um comentario"
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if textView.text.isEmpty {
            self.postsMessage.buttonSendUserMessage.isEnabled = false
        }
        else {
            self.postsMessage.buttonSendUserMessage.isEnabled = true
        }
    }
}
