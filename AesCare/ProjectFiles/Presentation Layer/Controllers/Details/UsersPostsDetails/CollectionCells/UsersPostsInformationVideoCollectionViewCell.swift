//
//  UsersPostsInformationVideoCollectionViewCell.swift
//  AesCare
//
//  Created by Thiago on 14/04/21.
//

import UIKit

class UsersPostsInformationVideoCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageViewIcon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setupViews()
    }
    
    private func setupViews() {
        self.imageViewIcon.image = UIImage(named: "illustration-medico")
    }

}
