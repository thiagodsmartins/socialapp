//
//  UsersPostsInformationImageCollectionViewCell.swift
//  AesCare
//
//  Created by Thiago on 14/04/21.
//

import UIKit

class UsersPostsInformationImageCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageViewIcons: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setupViews()
    }

    private func setupViews() {
        self.imageViewIcons.image = UIImage(named: "illustration-chat")
    }
}
