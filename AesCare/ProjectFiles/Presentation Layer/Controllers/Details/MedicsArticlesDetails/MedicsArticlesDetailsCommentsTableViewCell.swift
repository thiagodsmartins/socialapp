//
//  MedicsArticlesDetailsCommentsTableViewCell.swift
//  AesCare
//
//  Created by Thiago on 28/03/21.
//

import UIKit

class MedicsArticlesDetailsCommentsTableViewCell: UITableViewCell {
    @IBOutlet weak var textViewMessage: UITextView!
    @IBOutlet weak var labelUsername: UILabel!
    @IBOutlet weak var imageViewIcon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    
        self.setupViews()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    private func setupViews() {
        self.textViewMessage.setBorder(width: 1.0, borderColor: .purple, cornerRadious: 10)
        self.imageViewIcon.setBorder(width: 1.0, borderColor: .purple, cornerRadious: 10)
        
        self.labelUsername.numberOfLines = 0
        self.labelUsername.lineBreakMode = .byWordWrapping
    }
}
