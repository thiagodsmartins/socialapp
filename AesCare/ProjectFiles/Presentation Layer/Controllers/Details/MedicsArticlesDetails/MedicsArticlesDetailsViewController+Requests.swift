//
//  MedicsArticlesDetailsViewController+Requests.swift
//  AesCare
//
//  Created by Thiago on 30/03/21.
//

import Foundation
import Alamofire

extension MedicsArticlesDetailsViewController {
    func publishMedicsArticlesCommentary(_ message: String, postId: Int, relatedClass: String, response: @escaping (Bool) -> Void) {
        let headers: HTTPHeaders = ["Content-Type": "multipart/form-data", "Cookie": "aescare_session_key=\(usrerModelOBJ.session_key!)"]
        let messageToPost: Parameters = ["content": message,
                                         "related_id": String(postId),
                                         "related_class": relatedClass] as Parameters
        let url = "\(BaseUrl)/comment"
        
        Alamofire.upload(multipartFormData: {
            multipartFormData in
            for (key, value) in messageToPost {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to: url, method: .post, headers: headers, encodingCompletion: {
            encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { data in
                    print(data.result.value!)
                    response(true)
                }
            case .failure(let encodingError):
                print(encodingError)
                response(false)
            }
        })
    }
    
    func requestMedicsArticlesComments(_ postKey: String, completion: @escaping (MedicsArticlesCommentsModel?) -> Void) {
            let url = "\(BaseUrl)/post?post_key=\(postKey)"
            
            Alamofire.request(url, method: .get, encoding: JSONEncoding.default).validate(statusCode: 200 ..< 299).responseJSON { AFdata in
                    do {
                        guard let jsonObject = try JSONSerialization.jsonObject(with: AFdata.data!) as? [String: Any] else {
                            print("Error: Cannot convert data to JSON object")
                            return
                        }
                        guard let prettyJsonData = try? JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted) else {
                            print("Error: Cannot convert JSON object to Pretty JSON data")
                            completion(nil)
                            return
                        }
                        guard String(data: prettyJsonData, encoding: .utf8) != nil else {
                            print("Error: Could print JSON in String")
                            completion(nil)
                            return
                        }
                        
                        let data = try! JSONDecoder().decode(MedicsArticlesCommentsResponse.self, from: AFdata.data!)
                        
                        if data.post!.comments!.isEmpty {
                            completion(nil)
                        }
                        else {
                            completion(data.post)
                        }
                    } catch {
                        print("Error: Trying to convert JSON data to string")
                        completion(nil)
                        return
                    }
                }
    }
    
    func deleteMedicsArticlesCommentary(_ commentId: Int, response: @escaping (Bool) -> Void) {
        let headers: HTTPHeaders = ["Content-Type": "multipart/form-data", "Cookie": "aescare_session_key=\(usrerModelOBJ.session_key!)"]
        let messageToPost: Parameters = ["comment_id": String(commentId)] as Parameters
        let url = "\(BaseUrl)/comment"
        
        Alamofire.upload(multipartFormData: {
            multipartFormData in
            for (key, value) in messageToPost {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to: url, method: .delete, headers: headers, encodingCompletion: {
            encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { data in
                    print(data.result.value!)
                    response(true)
                }
            case .failure(let encodingError):
                print(encodingError)
                response(false)
            }
        })
    }
    
}
