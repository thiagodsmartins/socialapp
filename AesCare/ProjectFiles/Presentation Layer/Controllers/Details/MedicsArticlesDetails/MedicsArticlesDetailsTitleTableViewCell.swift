//
//  MedicsArticlesDetailsTitleTableViewCell.swift
//  AesCare
//
//  Created by Thiago on 28/03/21.
//

import UIKit

class MedicsArticlesDetailsTitleTableViewCell: UITableViewCell {
    @IBOutlet weak var imageViewIcon: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setupViews()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    private func setupViews() {
        self.imageViewIcon.setBorder(width: 1.0, borderColor: .purple, cornerRadious: 10)
        self.labelTitle.numberOfLines = 0
        self.labelTitle.lineBreakMode = .byWordWrapping
    }
    
}
