//
//  MedicsArticlesDetailsViewController.swift
//  AesCare
//
//  Created by Thiago on 28/03/21.
//

import Foundation
import UIKit
import JJFloatingActionButton
import YoutubeKit

class MedicsArticlesDetailsViewController: UIViewController {
    @IBOutlet weak var tableViewMedicsArticlesDetails: UITableView!
    @IBOutlet weak var buttonCloseWindow: UIButton!
    @IBOutlet weak var imageViewIcon: UIImageView!
    @IBOutlet weak var viewGradient: UIView!
    
    var nibFiles: [UINib]?
    var floatButton: JJFloatingActionButton!
    var textViewMessage: UITextView!
    var medicsArticlesData: MedicsArticlesModel?
    var medicsArticlesComments: [MedicsArticlesCommentsModel?]!
    var isLoading = true
    
    var workItem: DispatchWorkItem!
    var queue: DispatchQueue!
    var semaphore: DispatchSemaphore!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.medicsArticlesComments = [MedicsArticlesCommentsModel?]()
        self.medicsArticlesComments.insert(nil, at: 0)
        
        self.setupTableView()
        self.setupViews()
                
        DispatchQueue.main.asyncAfter(deadline: .now() + 4, execute: {
            let cell = self.tableViewMedicsArticlesDetails.cellForRow(at: IndexPath(row: 0, section: 1)) as! MedicsArticlesDetailsVideoTableViewCell
            
            cell.youtubePlayer = YTSwiftyPlayer(frame: CGRect(x: 0, y: 0, width: cell.contentView.frame.size.width, height: cell.contentView.frame.size.height), playerVars: [.videoID(self.medicsArticlesData?.content?.youtube_video_id ?? "")])

            cell.youtubePlayer.autoplay = false
            cell.youtubePlayer.loadPlayer()
            cell.contentView.addSubview(cell.youtubePlayer)

            cell.youtubePlayer.translatesAutoresizingMaskIntoConstraints = false
            cell.youtubePlayer.topAnchor.constraint(equalTo: cell.contentView.topAnchor, constant: 10).isActive = true
            cell.youtubePlayer.bottomAnchor.constraint(equalTo: cell.contentView.bottomAnchor, constant: -10).isActive = true
            cell.youtubePlayer.leadingAnchor.constraint(equalTo: cell.contentView.leadingAnchor, constant: 15).isActive = true
            cell.youtubePlayer.trailingAnchor.constraint(equalTo: cell.contentView.trailingAnchor, constant: -15).isActive = true
            
            self.tableViewMedicsArticlesDetails.reloadData()
        })
        
        self.setupBackgroundCommentsRequest()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.workItem.cancel()
    }
    
    private func setupBackgroundCommentsRequest() {
        self.queue = DispatchQueue.global(qos: .userInitiated)
        self.semaphore = DispatchSemaphore(value: 0)
        self.workItem = DispatchWorkItem {
            while !self.workItem.isCancelled {
                self.requestMedicsArticlesComments(self.medicsArticlesData!.key!, completion: {
                    data in
                    if let data = data {
                        self.medicsArticlesComments.insert(data, at: 0)
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(5), execute: {
                            self.tableViewMedicsArticlesDetails.reloadData()
                            self.semaphore.signal()
                        })
                    }
                    else {
                        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(5), execute: {
                            print("No data")
                            self.semaphore.signal()
                        })
                        
                    }
                })
                
                self.semaphore.wait()
            }
            
            self.workItem = nil
        }
        
        self.queue.async(execute: self.workItem)
    }
    
    
    private func setupTableView() {
        self.nibFiles = [UINib]()
        self.nibFiles?.append(UINib(nibName: "MedicsArticlesDetailsTitleTableViewCell", bundle: nil))
        self.nibFiles?.append(UINib(nibName: "MedicsArticlesDetailsVideoTableViewCell", bundle: nil))
        self.nibFiles?.append(UINib(nibName: "MedicsArticlesDetailsCommentsTableViewCell", bundle: nil))
        self.nibFiles?.append(UINib(nibName: "EmptyTableViewCell", bundle: nil))
        
        self.tableViewMedicsArticlesDetails.register(self.nibFiles![0], forCellReuseIdentifier: "MedicsArticlesDetailsTitleTableViewCell")
        self.tableViewMedicsArticlesDetails.register(self.nibFiles![1], forCellReuseIdentifier: "MedicsArticlesDetailsVideoTableViewCell")
        self.tableViewMedicsArticlesDetails.register(self.nibFiles![2], forCellReuseIdentifier: "MedicsArticlesDetailsCommentsTableViewCell")
        self.tableViewMedicsArticlesDetails.register(self.nibFiles![3], forCellReuseIdentifier: "EmptyTableViewCell")
        self.tableViewMedicsArticlesDetails.rowHeight = UITableView.automaticDimension
        self.tableViewMedicsArticlesDetails.estimatedRowHeight = 300
        self.tableViewMedicsArticlesDetails.delegate = self
        self.tableViewMedicsArticlesDetails.dataSource = self
        
    }
    
    private func setupViews() {
        self.textViewMessage = UITextView()
        self.textViewMessage.isHidden = true
        
        self.floatButton = JJFloatingActionButton()
        self.floatButton.addItem(title: "Comentar", image: UIImage(named: "comment-image")?.withRenderingMode(.alwaysTemplate)) { item in
            self.textViewMessage.becomeFirstResponder()
        }
        
        self.floatButton.addItem(title: "Compartilhar", image: UIImage(named: "share-image")?.withRenderingMode(.alwaysTemplate)) { item in
            self.shareResult()
        }
        
        self.buttonCloseWindow.addTarget(self, action: #selector(self.buttonCloseWindowPressed(_:)), for: .touchUpInside)
        self.buttonCloseWindow.backgroundColor = UIColor(red: 175/255, green: 68/255, blue: 217/255, alpha: 1.0)
        self.buttonCloseWindow.tintColor = .white
        self.buttonCloseWindow.layer.cornerRadius = self.buttonCloseWindow.frame.size.width / 2
        self.buttonCloseWindow.clipsToBounds = true
        
        self.view.addSubview(self.floatButton)
        self.view.addSubview(self.textViewMessage)
        
        self.floatButton.translatesAutoresizingMaskIntoConstraints = false
        self.floatButton.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -16).isActive = true
        self.floatButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -16).isActive = true
        
        self.imageViewIcon.layer.cornerRadius = self.imageViewIcon.frame.size.width / 2
        self.imageViewIcon.layer.masksToBounds = false
        self.imageViewIcon.clipsToBounds = true
        self.imageViewIcon.image = UIImage(named: "detailimage")
        
        self.view.addSubview(self.textViewMessage)
        
        self.gradientView()
        self.keyboardNotifications()
        self.sendMessageKeyboardButton()
    }
    
    private func gradientView() {
        let gradient = CAGradientLayer()
        gradient.frame = self.viewGradient.bounds
        gradient.colors = [UIColor(red: 168/255, green: 72/255, blue: 217/255, alpha: 1.0).cgColor, UIColor(red: 105/255, green: 108/255, blue: 220/255, alpha: 1.0).cgColor]
        gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        self.viewGradient.layer.addSublayer(gradient)
        self.viewGradient.addSubview(self.buttonCloseWindow)
        self.viewGradient.addSubview(self.imageViewIcon)
    }
    
    private func keyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: UIResponder.keyboardDidShowNotification , object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    private func sendMessageKeyboardButton() {
        let toolBar =  UIToolbar(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 35))
        toolBar.barStyle = .default
        toolBar.sizeToFit()

        let sendButton = UIBarButtonItem(title: "Enviar", style: .plain, target: self, action: #selector(self.buttonSendMessagePressed(_:)))
        let closeButton = UIBarButtonItem(title: "Fechar", style: .plain, target: self, action: #selector(self.buttonCloseMessageSendPressed(_:)))
        toolBar.items = [sendButton, closeButton]
        toolBar.isUserInteractionEnabled = true
        self.textViewMessage.inputAccessoryView = toolBar
    }
    
    private func shareResult() {
        let text = "Test message"
        let image = UIImage(named: "illustration-medico")
        let myWebsite = NSURL(string: "https://br.aescare.com/")
        let shareAll = [text, image!, myWebsite as Any] as Any
        let activityViewController = UIActivityViewController(activityItems: shareAll as! [Any], applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
}

extension MedicsArticlesDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 || section == 1 {
            return 1
        }
        else {
            if self.medicsArticlesComments[0] == nil {
                return 1
            }
            else {
                return self.medicsArticlesComments[0]!.comments!.count
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = self.tableViewMedicsArticlesDetails.dequeueReusableCell(withIdentifier: "MedicsArticlesDetailsTitleTableViewCell") as! MedicsArticlesDetailsTitleTableViewCell
            cell.labelTitle.text = self.medicsArticlesData?.content?.title!
            return cell
        }
        else if indexPath.section == 1 {
            let cell = self.tableViewMedicsArticlesDetails.dequeueReusableCell(withIdentifier: "MedicsArticlesDetailsVideoTableViewCell") as! MedicsArticlesDetailsVideoTableViewCell
            print(self.medicsArticlesData?.content?.youtube_video_id ?? "")
            return cell
        }
        else if indexPath.section == 2 {
            if self.medicsArticlesComments[0] != nil {
                if !self.medicsArticlesComments[0]!.comments!.isEmpty {
                    let cell = self.tableViewMedicsArticlesDetails.dequeueReusableCell(withIdentifier: "MedicsArticlesDetailsCommentsTableViewCell") as! MedicsArticlesDetailsCommentsTableViewCell
                    cell.imageViewIcon.sd_setImage(with: URL(string: self.medicsArticlesComments[0]!.comments![indexPath.row].user!.url_image!), completed: nil)
                    cell.labelUsername.text = self.medicsArticlesComments[0]!.comments![indexPath.row].user?.username!
                    cell.textViewMessage.text = self.medicsArticlesComments[0]!.comments![indexPath.row].content!
                    return cell
                }
                else {
                    let cell = self.tableViewMedicsArticlesDetails.dequeueReusableCell(withIdentifier: "EmptyTableViewCell") as! EmptyTableViewCell
                    cell.labelMessage.text = "Nenhum Comentário"
                    return cell
                }
            }
            else {
                let cell = self.tableViewMedicsArticlesDetails.dequeueReusableCell(withIdentifier: "EmptyTableViewCell") as! EmptyTableViewCell
                cell.labelMessage.text = "Nenhum Comentário"
                return cell
            }
        }
        else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 100
        }
        else if indexPath.section == 1 {
            return 250
        }
        else if indexPath.section == 2{
            return tableView.rowHeight
        }
        else {
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UITableViewHeaderFooterView()
        
        if section == 0 {
            headerView.textLabel?.text = "Artigo"
            return headerView
        }
        else if section == 1 {
            headerView.textLabel?.text = "Video"
            return headerView
        }
        else if section == 2 {
            headerView.textLabel?.text = "Comentários"
            return headerView
        }
        else {
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .destructive, title:  "Apagar", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            
            self.deleteMedicsArticlesCommentary(self.medicsArticlesComments[0]!.comments![indexPath.row].comment_id!, response: {
                data in
                if data {
                    self.medicsArticlesComments![0]?.comments!.remove(at: indexPath.row)
                    tableView.deleteRows(at: [indexPath], with: .fade)
                }
            })
            
            success(true)
        })

        deleteAction.backgroundColor = .red
        
        if self.medicsArticlesComments![0]?.comments![indexPath.row].user!.user_id! == usrerModelOBJ.user?.user_id! {
            return UISwipeActionsConfiguration(actions: [deleteAction])
        }
        else {
            return nil
        }
    }
    
}

extension MedicsArticlesDetailsViewController {
    @objc func keyboardWillShow(_ notification: Notification) {
        let userInfo:NSDictionary = notification.userInfo! as NSDictionary
        let keyboardFrame:NSValue = userInfo.value(forKey: UIResponder.keyboardFrameEndUserInfoKey) as! NSValue
        let keyboardRectangle = keyboardFrame.cgRectValue
        let keyboardHeight = keyboardRectangle.height
        let keyboardWidth = keyboardRectangle.width
        
        self.textViewMessage.isHidden = false
        self.textViewMessage.setBorder(width: 1.0, borderColor: .purple, cornerRadious: 10)
        self.textViewMessage.frame = CGRect(x: 0, y: keyboardHeight + 60, width: keyboardWidth, height: 50)
    
        self.view.bringSubviewToFront(self.textViewMessage)
        self.view.layoutIfNeeded()
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        self.textViewMessage.resignFirstResponder()
        self.textViewMessage.isHidden = true
    }
    
    @objc func buttonCloseWindowPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func buttonSendMessagePressed(_ sender: UIButton) {
        if !self.textViewMessage.text.isEmpty {
            self.publishMedicsArticlesCommentary(self.textViewMessage.text, postId: self.medicsArticlesData!.post_id!, relatedClass: "posts", response: {
                data in
                if data {
                    print("Post complete")
                }
                else {
                    print("Error")
                }
                
                self.textViewMessage.resignFirstResponder()
            })
        }
    }
    
    @objc func buttonCloseMessageSendPressed(_ sender: UIButton) {
        self.textViewMessage.resignFirstResponder()
    }
}
