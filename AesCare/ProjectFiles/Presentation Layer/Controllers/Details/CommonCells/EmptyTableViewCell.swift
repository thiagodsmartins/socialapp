//
//  EmptyTableViewCell.swift
//  AesCare
//
//  Created by Thiago on 29/03/21.
//

import UIKit

class EmptyTableViewCell: UITableViewCell {
    @IBOutlet weak var labelMessage: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setupViews()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    private func setupViews() {
        self.labelMessage.textColor = UIColor(red: 220/255, green: 220/255, blue: 220/255, alpha: 1.0)
    }
    
}
