//
//  MedicsArticlesDetailsVideoTableViewCell.swift
//  AesCare
//
//  Created by Thiago on 28/03/21.
//

import UIKit
import YoutubeKit

class MedicsPostsDetailsVideoTableViewCell: UITableViewCell {
    var youtubePlayer: YTSwiftyPlayer!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
        
    func playVideo(_ videoId: String) {
        self.youtubePlayer = YTSwiftyPlayer(frame: CGRect(x: 0, y: 0, width: self.contentView.frame.size.width, height: self.contentView.frame.size.height), playerVars: [.videoID(videoId)])
        
        print("HEIGHT: \(self.contentView.frame.size.height)")
        print("WIDTH: \(self.contentView.frame.size.width)")
        
        self.youtubePlayer.autoplay = false
        self.contentView.addSubview(self.youtubePlayer)
        
        self.youtubePlayer.translatesAutoresizingMaskIntoConstraints = false
        self.youtubePlayer.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 10).isActive = true
        self.youtubePlayer.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -10).isActive = true
        self.youtubePlayer.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 15).isActive = true
        self.youtubePlayer.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -15).isActive = true
        
        self.youtubePlayer.loadPlayer()
    }
}
