//
//  MedicsArticlesDetailsCommentsTableViewCell.swift
//  AesCare
//
//  Created by Thiago on 28/03/21.
//

import UIKit

class MedicsPostsDetailsCommentsTableViewCell: UITableViewCell {
    @IBOutlet weak var textViewMessage: UITextView!
    @IBOutlet weak var labelUsername: UILabel!
    @IBOutlet weak var imageViewIcon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
