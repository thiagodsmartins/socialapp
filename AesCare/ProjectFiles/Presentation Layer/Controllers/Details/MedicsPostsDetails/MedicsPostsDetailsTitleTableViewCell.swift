//
//  MedicsArticlesDetailsTitleTableViewCell.swift
//  AesCare
//
//  Created by Thiago on 28/03/21.
//

import UIKit

class MedicsPostsDetailsTitleTableViewCell: UITableViewCell {
    @IBOutlet weak var labelMedicPostTitle: UILabel!
    @IBOutlet weak var imageViewMedicPostIcon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setupViews()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    private func setupViews() {
        self.imageViewMedicPostIcon.setBorder(width: 2, borderColor: UIColor(red: 175/255, green: 68/255, blue: 217/255, alpha: 1.0), cornerRadious: self.imageViewMedicPostIcon.frame.height / 2)
    }
    
}
