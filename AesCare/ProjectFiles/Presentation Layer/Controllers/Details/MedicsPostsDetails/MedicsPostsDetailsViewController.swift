//
//  MedicsArticlesDetailsViewController.swift
//  AesCare
//
//  Created by Thiago on 28/03/21.
//

import Foundation
import UIKit
import JJFloatingActionButton

class MedicsPostsDetailsViewController: UIViewController {    
    @IBOutlet weak var imageViewIcon: UIImageView!
    @IBOutlet weak var buttonCloseWindow: UIButton!
    @IBOutlet weak var viewGradient: UIView!
    @IBOutlet weak var tableViewMedicsPostsDetails: UITableView!
    
    var nibFiles: [UINib]?
    var floatButton: JJFloatingActionButton!
    var textViewMessage: UITextView!
    var medicPostsData: MedicsPostsModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupTableView()
        self.setupViews()
    }
    
    private func setupTableView() {
        self.nibFiles = [UINib]()
        self.nibFiles?.append(UINib(nibName: "MedicsPostsDetailsTitleTableViewCell", bundle: nil))
        self.nibFiles?.append(UINib(nibName: "MedicsPostsDetailsVideoTableViewCell", bundle: nil))
        self.nibFiles?.append(UINib(nibName: "MedicsPostsDetailsCommentsTableViewCell", bundle: nil))
        
        self.tableViewMedicsPostsDetails.register(self.nibFiles![0], forCellReuseIdentifier: "MedicsPostsDetailsTitleTableViewCell")
        self.tableViewMedicsPostsDetails.register(self.nibFiles![1], forCellReuseIdentifier: "MedicsPostsDetailsVideoTableViewCell")
        self.tableViewMedicsPostsDetails.register(self.nibFiles![2], forCellReuseIdentifier: "MedicsPostsDetailsCommentsTableViewCell")
        self.tableViewMedicsPostsDetails.rowHeight = UITableView.automaticDimension
        self.tableViewMedicsPostsDetails.estimatedRowHeight = 300
        self.tableViewMedicsPostsDetails.delegate = self
        self.tableViewMedicsPostsDetails.dataSource = self
        
    }
    
    private func setupViews() {
        self.textViewMessage = UITextView()
        
        self.floatButton = JJFloatingActionButton()
        self.floatButton.addItem(title: "Comentar", image: UIImage(named: "comment-image")?.withRenderingMode(.alwaysTemplate)) { item in
            self.textViewMessage.becomeFirstResponder()
        }
        
        self.floatButton.addItem(title: "Compartilhar", image: UIImage(named: "share-image")?.withRenderingMode(.alwaysTemplate)) { item in
            self.shareResult()
        }
        
        self.buttonCloseWindow.addTarget(self, action: #selector(self.buttonCloseWindowPressed(_:)), for: .touchUpInside)
        self.buttonCloseWindow.backgroundColor = UIColor(red: 175/255, green: 68/255, blue: 217/255, alpha: 1.0)
        self.buttonCloseWindow.tintColor = .white
        self.buttonCloseWindow.layer.cornerRadius = self.buttonCloseWindow.frame.size.width / 2
        self.buttonCloseWindow.clipsToBounds = true
        
        self.view.addSubview(self.floatButton)
        self.view.addSubview(self.textViewMessage)
        
        self.floatButton.translatesAutoresizingMaskIntoConstraints = false
        self.floatButton.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -16).isActive = true
        self.floatButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -16).isActive = true
        
        self.imageViewIcon.layer.cornerRadius = self.imageViewIcon.frame.size.width / 2
        self.imageViewIcon.layer.masksToBounds = false
        self.imageViewIcon.clipsToBounds = true
        self.imageViewIcon.image = UIImage(named: "detailimage")
        
        self.gradientView()
        self.keyboardNotifications()
        self.sendMessageKeyboardButton()
    }
    
    private func gradientView() {
        let gradient = CAGradientLayer()
        gradient.frame = self.viewGradient.bounds
        gradient.colors = [UIColor(red: 168/255, green: 72/255, blue: 217/255, alpha: 1.0).cgColor, UIColor(red: 105/255, green: 108/255, blue: 220/255, alpha: 1.0).cgColor]
        gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        self.viewGradient.layer.addSublayer(gradient)
        self.viewGradient.addSubview(self.buttonCloseWindow)
        self.viewGradient.addSubview(self.imageViewIcon)
    }
    
    private func keyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: UIResponder.keyboardDidShowNotification , object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    private func sendMessageKeyboardButton() {
        let toolBar =  UIToolbar(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 35))
        toolBar.barStyle = .default
        toolBar.sizeToFit()

        let sendButton = UIBarButtonItem(title: "Enviar", style: .plain, target: self, action: #selector(self.buttonSendMessagePressed(_:)))
        toolBar.items = [sendButton]
        toolBar.isUserInteractionEnabled = true
        self.textViewMessage.inputAccessoryView = toolBar
    }
    
    private func shareResult() {
        let text = "Test message"
        let image = UIImage(named: "illustration-medico")
        let myWebsite = NSURL(string: "https://br.aescare.com/")
        let shareAll = [text, image!, myWebsite as Any] as Any
        let activityViewController = UIActivityViewController(activityItems: shareAll as! [Any], applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
}

extension MedicsPostsDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = self.tableViewMedicsPostsDetails.dequeueReusableCell(withIdentifier: "MedicsPostsDetailsTitleTableViewCell") as! MedicsPostsDetailsTitleTableViewCell
            
            if let _ = self.medicPostsData.medic?.image_url {
                cell.imageViewMedicPostIcon.sd_setImage(with: URL(string: (self.medicPostsData.medic?.image_url!)!), completed: nil)
            }
            else {
                cell.imageViewMedicPostIcon.image = UIImage(named: "logo-aescare")
            }
            
            cell.labelMedicPostTitle.text = self.medicPostsData.content?.title!
            
            return cell
        }
        else if indexPath.section == 1 {
            let cell = self.tableViewMedicsPostsDetails.dequeueReusableCell(withIdentifier: "MedicsPostsDetailsVideoTableViewCell") as! MedicsPostsDetailsVideoTableViewCell
            
            if let video = self.medicPostsData?.content!.youtube_video_id {
                cell.playVideo(video)
            }
            else {
                cell.playVideo("")
            }
            
            return cell
        }
        else if indexPath.section == 2 {
            let cell = self.tableViewMedicsPostsDetails.dequeueReusableCell(withIdentifier: "MedicsPostsDetailsCommentsTableViewCell") as! MedicsPostsDetailsCommentsTableViewCell
            return cell
        }
        else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 100
        }
        else if indexPath.section == 1 {
            return 250
        }
        else if indexPath.section == 2{
            return tableView.rowHeight
        }
        else {
            return UITableView.automaticDimension
        }
    }
    
}

extension MedicsPostsDetailsViewController {
    @objc func keyboardWillShow(_ notification: Notification) {
        let userInfo:NSDictionary = notification.userInfo! as NSDictionary
        let keyboardFrame:NSValue = userInfo.value(forKey: UIResponder.keyboardFrameEndUserInfoKey) as! NSValue
        let keyboardRectangle = keyboardFrame.cgRectValue
        let keyboardHeight = keyboardRectangle.height
        let keyboardWidth = keyboardRectangle.width
        
        self.textViewMessage.isHidden = false
        self.textViewMessage.setBorder(width: 1.0, borderColor: .purple, cornerRadious: 10)
        self.textViewMessage.frame = CGRect(x: 0, y: keyboardHeight + 60, width: keyboardWidth, height: 50)
    
        UIApplication.shared.keyWindow?.addSubview(self.textViewMessage)
        UIApplication.shared.keyWindow?.bringSubviewToFront(self.textViewMessage)
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        self.textViewMessage.resignFirstResponder()
        self.textViewMessage.isHidden = true
    }
    
    @objc func buttonCloseWindowPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func buttonSendMessagePressed(_ sender: UIButton) {
        self.textViewMessage.resignFirstResponder()
    }
}
