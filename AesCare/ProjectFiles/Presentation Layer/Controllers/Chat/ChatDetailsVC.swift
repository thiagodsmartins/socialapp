//
//  ViewController.swift
//  ChatSample
//
//  Created by Hafiz on 20/09/2019.
//  Copyright © 2019 Nibs. All rights reserved.
//

import UIKit
import SideMenu
import IQKeyboardManagerSwift
import LinkPresentation
import Alamofire

@available(iOS 13.0, *)
class ChatDetailsVC: BaseViewController {

    @IBOutlet weak var txtViewChat: UITextView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var linkViewer: UIStackView!
    @IBOutlet weak var lblImagesToUpload: UILabel!
    
    
    @IBOutlet weak var linkPreview: UIStackView!
    @IBOutlet weak var imagePreview: UIStackView!
    @IBOutlet weak var imageToPreviewOne: UIImageView!
    @IBOutlet weak var imageToPreviewTwo: UIImageView!
    
    //var messages = [Message]()
    //var chatList = [ChatModel]()
    var chatsHistory = [Chat]()
    var modelChatHistoryDetails:Chat?
    var arrayChatMessages : [Chat1] = []
    var userName:String = ""
    var chatKey:String?
    var stopPagination = false
    var gradientLayer: CAGradientLayer!
    @IBOutlet weak var vwNavigation: UIView!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var containerSearch: designableView!
    var indicator: UIActivityIndicatorView?
    var textLabel: UILabel?
    var requestState = false
    var inputText :String = ""
    var page = 2
    var dataReady = false
    var dataCounter = 0
    var counter = 20
    var isLoading = false
    
    var imagePicker = UIImagePickerController()
    var arrayFileInfo : [MultiPartDataFormatStructure] = [MultiPartDataFormatStructure]()
    var timer = Timer()
    var lastMessageID: Int = 0
    var aesCaraLink: AESCareLinkPreview?
    var hasData = true
    var semaphore: DispatchSemaphore?
    var isEventHappening = false
    var backgroundTimer: Timer?
    var lastChatMessageId = 0
    var isFirstRequest = true
    var tempSendChatData = ""
    var previewLink: AESCareLinkPreview?
    var tapGestureRecognizer: UITapGestureRecognizer?
    var previewState = 1
    var previewImagesList: [UIImage]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.previewLink = AESCareLinkPreview()
        self.tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.imagesToUploadTapRecognizer(_:)))
        self.previewImagesList = []
        
        self.txtViewChat.delegate = self
        
        self.indicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.large)
        self.textLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 50))
        
        indicator?.color = UIColor.purple
        indicator?.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0)
        indicator?.center = view.center
        indicator?.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)

        textLabel?.textColor = UIColor.purple
        textLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
        textLabel?.numberOfLines = 0
        textLabel?.text = "Carregando"
        textLabel?.center = CGPoint(x: (indicator?.center.x)! + 50, y: (indicator?.center.y)! + 40)
        
        view.addSubview(indicator!)
        view.addSubview(textLabel!)
        
        //indicator?.bringSubviewToFront(view)
        
        self.lblUserName.text = userName
        
        self.tableView.prefetchDataSource = self
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.lblImagesToUpload.isUserInteractionEnabled = true
        self.lblImagesToUpload.addGestureRecognizer(self.tapGestureRecognizer!)
        
        setupTable()
        createGradientLayer()
        
    }
    
    @objc func imagesToUploadTapRecognizer(_ sender: UITapGestureRecognizer) {
        print("LABEL TAPPED!!!")
        
        self.linkViewer.isHidden = false
        self.imagePreview.isHidden = false
        
        self.linkPreview.isHidden = true
        
        if self.previewState != 2 {
            if arrayFileInfo.count == 1 {
                self.imageToPreviewOne.image = self.previewImagesList![0]
                self.imageToPreviewOne.isHidden = false
            }
            else if arrayFileInfo.count == 2 {
                self.imageToPreviewOne.image = self.previewImagesList![0]
                self.imageToPreviewTwo.image = self.previewImagesList![1]
                self.imageToPreviewOne.isHidden = false
                self.imageToPreviewTwo.isHidden = false
            }
            
            self.previewState += 1
        }
        else {
            self.previewState = 1
            
            self.linkPreview.isHidden = true
            self.linkViewer.isHidden = true
            self.imageToPreviewOne.isHidden = true
            self.imageToPreviewTwo.isHidden = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.requestChatData(chatKey ?? "", actionHandler: {
            result in
            if result {
                self.indicator?.stopAnimating()
                self.textLabel?.removeFromSuperview()
                self.lastChatMessageId = self.arrayChatMessages[self.arrayChatMessages.count - 1].chat_message_id!
                self.backgroundRequest(self.chatKey!)
            }
            else {
                self.textLabel?.text = "Problemas ao carregar os dados!!!"
                print("ERROR WHILE LOADING CHATLIST DATA!!!")
            }
        })
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        ///// Call Custom Tabbar
        
        gradientLayer.frame = self.vwNavigation.bounds
    }
            
    func createGradientLayer() {
        gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.vwNavigation.bounds
        gradientLayer.colors = [UIColor(named: "AppPurpleColor")!.cgColor, UIColor(named: "AppMoveColor")!.cgColor]
        self.vwNavigation.layer.addSublayer(gradientLayer)
        self.vwNavigation.bringSubviewToFront(self.btnMenu)
        self.vwNavigation.bringSubviewToFront(self.containerSearch)
    }
    
    private func menuPressed() {
        appDel.topViewControllerWithRootViewController(rootViewController: UIApplication.shared.windows.first?.rootViewController)?.view.endEditing(true)
        
        SideMenuManager.default.leftMenuNavigationController?.settings.presentationStyle = .menuSlideIn
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let menu = storyboard.instantiateViewController(withIdentifier: "SideMenuNavigationController") as! SideMenuNavigationController
        menu.leftSide = true
        
        UIApplication.shared.windows.first?.rootViewController?.present(menu, animated: true, completion: nil)
    }
    
    @IBAction func openSideMenu(_ sender: Any) {
        self.menuPressed()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if self.backgroundTimer != nil {
            self.backgroundTimer?.invalidate()
            self.backgroundTimer = nil
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("VIEW APPEAR!!!")
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        //tableView.reloadData()
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnAddImageAction(_ sender: Any) {
        self.view.endEditing(true)
        self.btnUploadPicPressed()
    }
    
    @IBAction func btnSendMessgaeAction(_ sender: Any) {
        self.view.endEditing(true)

        self.isEventHappening = true
        if !txtViewChat.text.isEmpty {
            self.postMessage(message: txtViewChat.text!, response: {
                status in
                if status {
                    print("Request Complete!!!")
                    self.txtViewChat.text = ""
                    self.isEventHappening = false
                    self.tableView.scrollToRow(at: IndexPath(row: (self.arrayChatMessages.count) - 1, section: 0), at: .bottom, animated: false)
                }
            })
        } else {
            self.isEventHappening = false
        }
    }
    
    func setupTable() {
        // config tableView
        tableView.rowHeight = UITableView.automaticDimension
        tableView.dataSource = self
        tableView.backgroundColor = UIColor.white
        tableView.tableFooterView = UIView()
        // cell setup
        tableView.register(UINib(nibName: "RightViewCell", bundle: nil), forCellReuseIdentifier: "RightViewCell")
        tableView.register(UINib(nibName: "LeftViewCell", bundle: nil), forCellReuseIdentifier: "LeftViewCell")
        tableView.register(UINib(nibName: "RightImageCell", bundle: nil), forCellReuseIdentifier: "RightImageCell")
        tableView.register(UINib(nibName: "LeftImageView", bundle: nil), forCellReuseIdentifier: "LeftImageView")
        //tableView.register(UINib(nibName: "RightImageMultiple", bundle: nil), forCellReuseIdentifier: "RightMultipleImageCell")
        //tableView.register(UINib(nibName: "LeftImageMultiple", bundle: nil), forCellReuseIdentifier: "LeftMultipleImageCell")
        
        
    }
    
    //    func fetchData() {
    //        //messages = MessageStore.getAll()
    //       // tableView.reloadData()
    //    }
    func currentTimeMillis() -> Int64 {
        return Int64(Date().timeIntervalSince1970 * 1000)
    }
    
    func getDay(value: Int) -> String {
        switch value {
        case 1:
            return "Sunday"
        case 2:
            return "Monday"
        case 3:
            return "Tuesday"
        case 4:
            return "Wednesday"
        case 5:
            return "Thursday"
        case 6:
            return "Friday"
        case 7:
            return "Saturday"
        default:
            return ""
        }
    }
    
    func getDateDayAndTime(timestamp: NSNumber) -> String {
        let date  = Date(timeIntervalSince1970: Double(truncating: timestamp)/1000)
        let calendar = Calendar.current
        
        if calendar.isDateInToday(date) {
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = NSTimeZone.local
            dateFormatter.dateFormat = "hh:mm a"
            let time = dateFormatter.string(from: date)
            return time
        }else if calendar.isDateInYesterday(date) {
            return "Yesterday"
        }
        else if calendar.isDateInWeekend(date) {
            let component = calendar.component(Calendar.Component.weekday, from: date)
            return self.getDay(value: component)
        } else {
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = NSTimeZone.local
            dateFormatter.dateFormat = "dd/MM/YY"
            let time = dateFormatter.string(from: date)
            return time
        }
    }
    func scheduledTimerWithTimeInterval(){
        // Scheduling timer to Call the function "updateCounting" with the interval of 1 seconds
        timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(self.updateCounting), userInfo: nil, repeats: true)
    }
    
    @objc func updateCounting(){
        //self.callgetMessageAPIEverySec(messgageID: lastMessageID)
    }
    
}

@available(iOS 13.0, *)
extension ChatDetailsVC: UITableViewDataSource,UITableViewDelegate, UITableViewDataSourcePrefetching {    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("CHAT DATA: \(arrayChatMessages.count)")
        dataCounter = arrayChatMessages.count
        return arrayChatMessages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var message = arrayChatMessages[indexPath.row]
        if message.is_my == false {
            //        if  indexPath.row % 2 == 0 {
            if message.files?.count == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "LeftViewCell") as! LeftViewCell
                //linkDetector(data: message.content!)
                //message.content = linkDetector(data: message.content!)
                //cell.contentView.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
                cell.configureCell(message: message)
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "LeftImageView") as! LeftImageView
                if let urlString = URL(string: message.files?[0] ?? ""){
                    cell.imgViewChat?.startAnimating()
                    cell.imgViewChat.kf.setImage(with: urlString)
                }else{
                    cell.imgViewChat.image = UIImage(named: "")
                }
                cell.lblTime.text = message.send_date ?? ""
                return cell
                // return UITableViewCell()
            }
        }
        else {
            if message.files?.count == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier: "RightViewCell") as! RightViewCell
                cell.configureCell(message: message)
                return cell
                
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "RightImageCell") as! RightImageCell
                if let urlString = URL(string: message.files?[0] ?? ""){
                    cell.imgViewChat?.startAnimating()
                    cell.imgViewChat.kf.setImage(with: urlString)
                }else{
                    cell.imgViewChat.image = UIImage(named: "")
                }
                cell.lblTime.text = message.send_date ?? ""
                
                return  cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, canPerformAction action:
    Selector, forRowAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return action == #selector(copy(_:))
    }
    
    func tableView(_ tableView: UITableView, performAction action: Selector, forRowAt indexPath: IndexPath, withSender sender: Any?) {
        if action == #selector(copy(_:)) {
            UIPasteboard.general.string = arrayChatMessages[indexPath.row].content
            print("\(UIPasteboard.general.string ?? "EMPTY")")
        }
    }
    
    func tableView(_ tableView: UITableView,
                    leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
     {
        
        let editAction = UIContextualAction(style: .normal, title:  "Editar", handler: { [self] (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            self.isEventHappening = true
        
            let cell = tableView.cellForRow(at: IndexPath(row: indexPath.row, section: 0)) as! RightViewCell
            //let tempData = cell.textMessage.text
            
            cell.textMessage.isEditable = true
            self.tempSendChatData = cell.textMessage.text
            //cell.textMessage.setBorder(width: 1, borderColor: UIColor.purple, cornerRadious: 2)
                
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
                cell.textMessage.becomeFirstResponder()
            })
            
            success(true)
        })

        let editConfirmAction = UIContextualAction(style: .normal, title:  "Confirmar", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            let cell = tableView.cellForRow(at: IndexPath(row: indexPath.row, section: 0)) as! RightViewCell
            
            cell.progressIndicator.isHidden = false
            cell.labelProgressIndicator.isHidden = false
            
            cell.progressIndicator.setProgress(0.3, animated: true)
            self.editMessage(indexPath.row, cell.textMessage.text, response: {
                data in
                if data {
                    self.tempSendChatData = ""
                    cell.progressIndicator.setProgress(1.0, animated: true)
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3), execute: {
                        cell.progressIndicator.isHidden = true
                        cell.labelProgressIndicator.isHidden = true
                    })
                }
            })
            
            success(true)
        })

        let editCancelAction = UIContextualAction(style: .normal, title:  "Cancelar", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            
            let cell = tableView.cellForRow(at: IndexPath(row: indexPath.row, section: 0)) as! RightViewCell
            cell.textMessage.text = self.tempSendChatData
            self.tempSendChatData = ""
            
            success(true)
        })
        
        editAction.backgroundColor = .blue
        editConfirmAction.backgroundColor = .green
        editCancelAction.backgroundColor = .gray
        
        if arrayChatMessages[indexPath.row].is_my! && self.tempSendChatData.isEmpty {
            return UISwipeActionsConfiguration(actions: [editAction])
        }
        else if arrayChatMessages[indexPath.row].is_my! && !self.tempSendChatData.isEmpty {
            return UISwipeActionsConfiguration(actions: [editConfirmAction, editCancelAction])
        }
        else {
            return nil
        }
     }

     func tableView(_ tableView: UITableView,
                    trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
     {
        self.isEventHappening = true
    
        let deleteAction = UIContextualAction(style: .destructive, title:  "Apagar", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
                self.deleteMessage(indexPath.row)
                self.arrayChatMessages.remove(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: .fade)
                //tableView.reloadData()
                self.isEventHappening = false
                success(true)
            })

        deleteAction.backgroundColor = .red
        
        if arrayChatMessages[indexPath.row].is_my! {
            return UISwipeActionsConfiguration(actions: [deleteAction])
        }
        else {
            return nil
        }

     }
    
    func tableView(_ tableView: UITableView, willBeginEditingRowAt indexPath: IndexPath) {
        print("BEGIN EDITING!!!")
        self.isEventHappening = true
    }
    
//    func tableView(_ tableView: UITableView, didEndEditingRowAt indexPath: IndexPath?) {
//        print("END EDITING!!!")
//        self.isEventHappening = false
//    }
    
    func tableView(_ tableView: UITableView, shouldShowMenuForRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    public func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
       // print("CARREGANDO!!!")
    }
    
    func tableView(_ tableView: UITableView, cancelPrefetchingForRowsAt indexPaths: [IndexPath]) {
       // print("CANCELANDO!!!")
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        //let indexCount = arrayChatMessages.count - counter
        
//        print("\((tableView.indexPathsForVisibleRows?.first)!)")
        
//        if (self.tableView.indexPathsForVisibleRows?.first![0])! <= self.arrayChatMessages.count - 5 {
//            self.isEventHappening = true
//        }
        
        if indexPath.row == 0 && !self.isLoading {
            self.isEventHappening = true
            self.loadChatDataOnDemand(chatKey!, response: {
                data in
                self.counter = self.arrayChatMessages.count
                self.page += 1
                self.isEventHappening = false
            })
            print("AT TOP OF TABLE!!!")
        }
    
        if indexPath.section == tableView.numberOfSections - 1 &&
            indexPath.row == tableView.numberOfRows(inSection: indexPath.section) - 1 {
            //self.page = 1
            self.requestRecentData(self.chatKey!)
            self.isEventHappening = false
            self.page = 2
            print("AT BOTTOM!!!")
        }
    
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("TABLE SELECTED!!!")
        self.isEventHappening = true
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        print("TABLE UNSELECTED!!!")
        self.isEventHappening = false
    }
        
}

@available(iOS 13.0, *)
extension ChatDetailsVC:UITextViewDelegate{
    
    func encode(_ s: String) -> String {
        let data = s.data(using: .nonLossyASCII, allowLossyConversion: true)!
        return String(data: data, encoding: .utf8)!
    }
    func decode(_ s: String) -> String? {
        let data = s.data(using: .utf8)!
        return String(data: data, encoding: .nonLossyASCII)
    }
    
    func textViewDidChange(_ textView: UITextView) {
        print("TEXTO ALTERADO!!!")
        DispatchQueue.main.async {
            self.previewLink?.retriveMetaData(&textView.text, extractLinkFromString: true, requestHandler: {
                metadata in
                if let data = metadata {
                    self.linkPreview.insertArrangedSubview(data, at: 0)
                    self.linkPreview.isHidden = false
                    self.linkViewer.isHidden = false
                    self.imagePreview.isHidden = true
                }
                else {
                    DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
                        self.linkPreview.isHidden = true
                        self.linkViewer.isHidden = true
                    }) 
                }
            })
        }
//        
//        self.txtViewChat.text = textView.text
        self.inputText = self.encode(textView.text)
    }
}

@available(iOS 13.0, *)
extension ChatDetailsVC: UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    func btnUploadPicPressed(){
        self.view.endEditing(true)
        let alert = UIAlertController(title: "Choose image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera()
    {
        if(UIImagePickerController.isSourceTypeAvailable(.camera))
        {
            imagePicker.delegate = self
            imagePicker.sourceType = .camera;
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum)
        {
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary;
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        let underlineAttribute = [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.thick.rawValue]
        //let underlineAttributedString = NSAttributedString(string: "StringWithUnderLine", attributes: underlineAttribute)
        
        
        picker.dismiss(animated: true)
        var stringBase64:String? = ""
        if let pickedimage = info[.editedImage] as? UIImage, self.arrayFileInfo.count < 2
        {
            
            self.previewImagesList?.append(pickedimage)
            //let resizeImage = self.resizeImage(image: pickedimage, newWidth: 100)!
            //imageData = resizeImage.jpegData(compressionQuality: 1)
            //arrayFileInfo.removeAll()
            let imageData = pickedimage.jpegData(compressionQuality: 1)
            //stringBase64 = imageData?.base64EncodedString(options: .lineLength64Characters)
            
                        let imageFileInfo = MultiPartDataFormatStructure.init(key: "attachments", mimeType: .image_jpeg, data: imageData, name: "chat.jpg")
                        arrayFileInfo.append(imageFileInfo)
            //self.callAddNewMessage(messageType: "image")
            
            //tableView.reloadData()
        }
        else
        {
            print("Something went wrong")
        }
        
        
        if self.arrayFileInfo.count == 1 {
            self.lblImagesToUpload.isHidden = false
            self.lblImagesToUpload.attributedText =  NSAttributedString(string: "\(arrayFileInfo.count) anexo", attributes: underlineAttribute)
        }
        else if self.arrayFileInfo.count == 2 {
            self.lblImagesToUpload.isHidden = false
            self.lblImagesToUpload.attributedText =  NSAttributedString(string: "\(arrayFileInfo.count) anexos", attributes: underlineAttribute)
        }
        
        self.dismiss(animated: true, completion: nil)
        self.view.setNeedsLayout()
        //self.callfileSendAPI()
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        self.dismiss(animated: true, completion: { () -> Void in
            
            
        })
    }
    
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage? {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage
    }
    
}

extension String {
    
    func decode() -> String {
        let data = self.data(using: .utf8)!
        return String(data: data, encoding: .nonLossyASCII) ?? self
    }
    
    func encode() -> String {
        let data = self.data(using: .nonLossyASCII, allowLossyConversion: true)!
        return String(data: data, encoding: .utf8)!
    }
}

@available(iOS 13.0, *)
extension ChatDetailsVC {
    func deleteMessage(_ index: Int) {
        let headers: HTTPHeaders = ["Content-Type": "multipart/form-data", "Cookie": "aescare_session_key=\(usrerModelOBJ.session_key!)"]
        let delField = ["chat_message_id": arrayChatMessages[index].chat_message_id!]
        let urlDelete = "\(BaseUrl)/chat/message"
        
        Alamofire.request(urlDelete, method: .delete, parameters: delField, headers: headers).validate(statusCode: 200 ..< 299).responseJSON { AFdata in
            
            switch AFdata.result {
            case let .success(value):
                print(value)
            case let .failure(error):
                print(error)
            }
            
                do {
                    guard let jsonObject = try JSONSerialization.jsonObject(with: AFdata.data!) as? [String: Any] else {
                        print("Error: Cannot convert data to JSON object")
                        return
                    }
                    guard let prettyJsonData = try? JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted) else {
                        print("Error: Cannot convert JSON object to Pretty JSON data")
                        return
                    }
                    guard let prettyPrintedJson = String(data: prettyJsonData, encoding: .utf8) else {
                        print("Error: Could print JSON in String")
                        return
                    }

                    print(prettyPrintedJson)
                } catch {
                    print("Error: Trying to convert JSON data to string")
                    return
                }
            }
    }
    
    func editMessage(_ index: Int, _ msg: String, response: @escaping (Bool) -> Void) {
        let headers: HTTPHeaders = ["Content-Type": "multipart/form-data", "Cookie": "aescare_session_key=\(usrerModelOBJ.session_key!)"]

        let editRequest: Parameters = ["chat_message_id": arrayChatMessages[index].chat_message_id!, "comment_content": msg] as Parameters
        let url = "\(BaseUrl)/chat/edit/message"
        
        Alamofire.upload(multipartFormData: {
            multipartFormData in
            for (key, value) in editRequest {
                multipartFormData.append("\(value)".data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!, withName: key)
            }
        }, to: url, method: .post, headers: headers, encodingCompletion: {
            encodingResult in
            switch encodingResult {
            case .success(_, _, _):
                //response(true)
                print("UPDATE Success")
                response(true)
            case .failure(let encodingError):
                print(encodingError)
                //response(false)
                response(false)
            }
        })
    }
    
    func postMessage(message: String, response: @escaping (Bool) -> Void) {
        let headers: HTTPHeaders = ["Content-Type": "multipart/form-data", "Cookie": "aescare_session_key=\(usrerModelOBJ.session_key!)"]
        let messageToPost: Parameters = ["chat_key": self.chatKey!, "comment_content": message] as Parameters
        let urlPostMessage = "\(BaseUrl)/chat/send_message"
        
        Alamofire.upload(multipartFormData: {
            multipartFormData in
            for (key, value) in messageToPost {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to: urlPostMessage, method: .post, headers: headers, encodingCompletion: {
            encodingResult in
            switch encodingResult {
            case .success(_, _, _):
                response(true)
            case .failure(let encodingError):
                print(encodingError)
                response(false)
            }
        })
    }
    
    func requestChatData(_ key: String, actionHandler: @escaping (Bool) -> Void) {
        let pageData = "\(BaseUrl)/get_chat_message?chat_key=\(key)&page=1"
        self.indicator?.startAnimating()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 10, execute: {
            if !self.requestState {
                actionHandler(false)
            }
        })
        
        Alamofire.request(pageData, method: .get, encoding: JSONEncoding.default).validate(statusCode: 200 ..< 299).responseJSON { [weak self] AFdata in
                do {
                    guard let jsonObject = try JSONSerialization.jsonObject(with: AFdata.data!) as? [String: Any] else {
                        print("Error: Cannot convert data to JSON object")
                        return
                    }
                    guard let prettyJsonData = try? JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted) else {
                        print("Error: Cannot convert JSON object to Pretty JSON data")
                        return
                    }
                    guard String(data: prettyJsonData, encoding: .utf8) != nil else {
                        print("Error: Could print JSON in String")
                        return
                    }
                    
                    let data = try! JSONDecoder().decode(Chat_Base.self, from: AFdata.data!)
                    self?.arrayChatMessages.append(contentsOf: data.chats ?? [])
                    
                    print("DATA: \(data.chats!)")
                    
                    DispatchQueue.main.async {
                        self?.tableView.reloadData()
                        if (self?.arrayChatMessages.count)! > 0{
                            self?.tableView.scrollToRow(at: IndexPath(row: (self?.arrayChatMessages.count)! - 1, section: 0), at: .bottom, animated: false)
                            
                            self?.indicator?.stopAnimating()
                            self?.requestState = true
                            
                            actionHandler(true)
                        }
                    }
                } catch {
                    print("Error: Trying to convert JSON data to string")
                    return
                }
            }
    }
    
    func loadChatDataOnDemand(_ key: String, response: @escaping (Bool) -> Void) {
        let pageData = "\(BaseUrl)/get_chat_message?chat_key=\(key)&page=\(self.page)"
        
        self.indicator?.startAnimating()
        
        Alamofire.request(pageData, method: .get, encoding: JSONEncoding.default).validate(statusCode: 200 ..< 299).responseJSON { [weak self] AFdata in
                do {
                    guard let jsonObject = try JSONSerialization.jsonObject(with: AFdata.data!) as? [String: Any] else {
                        print("Error: Cannot convert data to JSON object")
                        return
                    }
                    guard let prettyJsonData = try? JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted) else {
                        print("Error: Cannot convert JSON object to Pretty JSON data")
                        return
                    }
                    guard String(data: prettyJsonData, encoding: .utf8) != nil else {
                        print("Error: Could print JSON in String")
                        return
                    }
                    
                    let data = try! JSONDecoder().decode(Chat_Base.self, from: AFdata.data!)
                    
                    if data.chats!.count > 0 {
                        self?.arrayChatMessages.insert(contentsOf: data.chats!, at: 0)
                        self?.isLoading = false
                        self?.indicator?.stopAnimating()
                    }
                    else {
                        self?.indicator?.stopAnimating()
                        return
                    }
                    
                    print("DATA: \(data.chats!)")
                    
                    DispatchQueue.main.async {
                        self?.tableView.reloadData()
                        if (self?.arrayChatMessages.count)! > 0{
                            self?.tableView.scrollToRow(at: IndexPath(row: 20, section: 0), at: .top, animated: false)
                        }
                        
                        response(true)
                    }
                } catch {
                    print("Error: Trying to convert JSON data to string")
                    return
                }
            }
    }
    
    func requestLastData(_ key: String, response: @escaping (Bool) -> Void) {
        let pageData = "\(BaseUrl)/get_chat_message?chat_key=\(key)&page=1"
        self.indicator?.startAnimating()
                
        Alamofire.request(pageData, method: .get, encoding: JSONEncoding.default).validate(statusCode: 200 ..< 299).responseJSON { [weak self] AFdata in
                do {
                    guard let jsonObject = try JSONSerialization.jsonObject(with: AFdata.data!) as? [String: Any] else {
                        print("Error: Cannot convert data to JSON object")
                        return
                    }
                    guard let prettyJsonData = try? JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted) else {
                        print("Error: Cannot convert JSON object to Pretty JSON data")
                        return
                    }
                    guard String(data: prettyJsonData, encoding: .utf8) != nil else {
                        print("Error: Could print JSON in String")
                        return
                    }
                    
                    let data = try! JSONDecoder().decode(Chat_Base.self, from: AFdata.data!)
                    self?.arrayChatMessages.removeAll()
                    self?.arrayChatMessages.append(contentsOf: data.chats ?? [])
                    
                    print("DATA: \(data.chats!)")
                    self?.indicator?.stopAnimating()
                    response(true)
                } catch {
                    print("Error: Trying to convert JSON data to string")
                    return
                }
            }
    }
    
    func requestRecentData(_ key: String) {
        //let pageDataFirst = "\(BaseUrl)/get_chat_message?chat_key=\(key)&page=1"
        let pageData = "\(BaseUrl)/chat/get_message?chat_key=\(key)&last_message_id=\(self.lastChatMessageId)"
        
        if !self.isEventHappening {
            Alamofire.request(pageData, method: .get, encoding: JSONEncoding.default).validate(statusCode: 200 ..< 299).responseJSON { [weak self] AFdata in
                    do {
                        guard let jsonObject = try JSONSerialization.jsonObject(with: AFdata.data!) as? [String: Any] else {
                            print("Error: Cannot convert data to JSON object")
                            return
                        }
                        
                        guard let prettyJsonData = try? JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted) else {
                            print("Error: Cannot convert JSON object to Pretty JSON data")
                            return
                        }
                        
                        guard String(data: prettyJsonData, encoding: .utf8) != nil else {
                            print("Error: Could print JSON in String")
                            return
                        }
                        
                        let data = try! JSONDecoder().decode(Chat_Base.self, from: AFdata.data!)
                        
                        //print(data)
                        //self?.arrayChatMessages.removeAll()
                        if !data.chats!.isEmpty {
                            self?.arrayChatMessages.append(contentsOf: data.chats ?? [])
                            self?.lastChatMessageId = (self?.arrayChatMessages[self!.arrayChatMessages.count - 1].chat_message_id)!
                            self?.isFirstRequest = false
                            //print("LAST MESSAGE ID: \(self!.lastMessageID)")
            
                            DispatchQueue.main.async {
                                self?.tableView.reloadData()
    //                            if let arrayData = self?.arrayChatMessages.count, arrayData > 0 {
    //                                let indexPath = IndexPath(row: (self?.arrayChatMessages.count)! - 1, section: 0)
    //                                self!.tableView.scrollToRow(at: indexPath, at: .bottom, animated: false)
                                self?.tableView.reloadDataSavingSelections()
                            }
                        }
                        
                    } catch {
                        print("Error: Trying to convert JSON data to string")
                        return
                    }
                }
        }
    }

    func backgroundRequest(_ key: String) {
        self.backgroundTimer = Timer.scheduledTimer(withTimeInterval: 3, repeats: true) { [weak self] _ in
            self?.requestRecentData(key)
        }
    }
}

extension UITableView {
    func reloadDataSavingSelections() {
        let selectedRows = indexPathsForSelectedRows

        reloadData()

        if let selectedRow = selectedRows {
            for indexPath in selectedRow {
                selectRow(at: indexPath, animated: false, scrollPosition: .none)
            }
        }
    }
}

