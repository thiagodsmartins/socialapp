//
//  ChatListVC.swift
//  AesCare
//
//  Created by Mahesh Mahalik on 02/10/20.
//

import UIKit
import SideMenu
import Alamofire
import AMTabView

class ChatListVC: BaseViewController,SelectionDelegate, UIGestureRecognizerDelegate, TabItem {
    
    @IBOutlet weak var imgRecentChatTab: UIImageView!
    var vl: UIGestureRecognizer?
    @IBOutlet weak var heightCollection: NSLayoutConstraint!
    @IBOutlet weak var tableChat: UITableView!
    var selectedIndex:Int = 0
    var gradientLayer: CAGradientLayer!
    @IBOutlet weak var vwNavigation: UIView!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var containerSearch: designableView!
    @IBOutlet weak var txtFieldSearch: UITextField!
    @IBOutlet weak var txtFieldGroupName: TextFieldX!
    @IBOutlet weak var txtFieldUserName: TextFieldX!
    var arrayChatGroup :[Chat_groups] = []
    @IBOutlet weak var viewSearchUserResult: UIView!
    @IBOutlet weak var heightSearchResult: NSLayoutConstraint!
    @IBOutlet weak var viewTransBack: UIView!
    @IBOutlet weak var viewCreateGroup: ViewX!
    @IBOutlet weak var collectionUserList: UICollectionView!
    @IBOutlet weak var tableSearchUser: UITableView!
    var arrayGroupStrings : [String] = []
    var arrayUsers :[User1] = []
    var arrayLastMessages:[Chats] = []
    var page:Page = Page()
    var stopPagination:Bool = false
    var arrayAllMessages:[Chat1] = []
    var arraymyGroups:[Chats] = []
    var selectedUsers:[User1] = []
    var swipeGesture: UISwipeGestureRecognizer?
    var aesCareRequest: AESCareRequest?
    
    var tabImage: UIImage? {
      return UIImage(named: "chat-Active")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.aesCareRequest = AESCareRequest(sessionPrivacy: .publicSession)
        
        self.swipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(self.openSideMenuWithSwipe(_:)))
        self.swipeGesture?.direction = .right
        
        createGradientLayer()
        self.viewTransBack.isHidden = true
        viewCreateGroup.isHidden = true
        heightSearchResult.constant = 0
        heightCollection.constant = 0
        viewSearchUserResult.isHidden = true
        self.callgroupChatAPI()
        
        self.view.addGestureRecognizer(self.swipeGesture!)
        
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        ///// Call Custom Tabbar
        //tabbarUISetup()
        gradientLayer.frame = self.vwNavigation.bounds

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         //self.callLastMessagesAPI(page: 1)
        self.requestChatGroup(completion: {
            result in
            if result {
                print("Request completed")
                self.tableChat.reloadData()
            }
        })

    }
    
    func tabbarUISetup(){
        ///// Call Custom Tabbar
        customTabbar.myDelegate = self
        customTabbar.setup(activeFor:"Chat")
    }
    
    func createGradientLayer() {
        gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.vwNavigation.bounds
        gradientLayer.colors = [UIColor(named: "AppPurpleColor")!.cgColor, UIColor(named: "AppMoveColor")!.cgColor]
        self.vwNavigation.layer.addSublayer(gradientLayer)
        self.vwNavigation.bringSubviewToFront(self.btnMenu)
        self.vwNavigation.bringSubviewToFront(self.containerSearch)
    }
    
    private func menuPressed() {
        
        appDel.topViewControllerWithRootViewController(rootViewController: UIApplication.shared.windows.first?.rootViewController)?.view.endEditing(true)
        
        SideMenuManager.default.leftMenuNavigationController?.settings.presentationStyle = .menuSlideIn
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let menu = storyboard.instantiateViewController(withIdentifier: "SideMenuNavigationController") as! SideMenuNavigationController
        menu.leftSide = true
        
        UIApplication.shared.windows.first?.rootViewController?.present(menu, animated: true, completion: nil)
        
    }
    
    @objc func openSideMenuWithSwipe(_ swipeGesture: UISwipeGestureRecognizer) {
        if swipeGesture.state == .ended {
            self.menuPressed()
        }
    }
    
    @IBAction func openSideMenu(_ sender: Any) {
        self.view.endEditing(true)
        self.menuPressed()
    }
    
    @IBAction func btnSearchAction(_ sender: UIButton) {
        self.view.endEditing(true)
    }
    @IBAction func btnAddGroup(_ sender: UIButton) {
        self.view.endEditing(true)
        self.viewTransBack.isHidden = false
        viewCreateGroup.isHidden = false
        viewSearchUserResult.isHidden = false
    }
    @IBAction func btnCreateGroup(_ sender: UIButton) {
        self.view.endEditing(true)
        self.viewTransBack.isHidden = true
        viewCreateGroup.isHidden = true
        heightSearchResult.constant = 0
         heightCollection.constant = 0
        viewSearchUserResult.isHidden = false
//        txtFieldGroupName.text = ""
        if txtFieldGroupName.text?.count ?? 0 > 0{
            if selectedUsers.count > 0{
        if selectedUsers.count > 1{
            self.callJoinGroupAPI(chatTitle: txtFieldGroupName.text ?? "", userkeys: self.retriveKeys(), isGroup: true)
        }else{
            self.callJoinGroupAPI(chatTitle: txtFieldGroupName.text ?? "", userkeys: selectedUsers[0].key ?? "", isGroup: false)
        }
            }
        }
    }
    @IBAction func btnCancelGroupCreateAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.viewTransBack.isHidden = true
        viewCreateGroup.isHidden = true
        heightSearchResult.constant = 0
        heightCollection.constant = 0
        viewSearchUserResult.isHidden = true
        txtFieldGroupName.text = ""
        txtFieldUserName.text = ""
    }
    func retriveKeys() ->  String{
        var chatKeys  = ""
        
        for i in  0...self.selectedUsers.count - 1{
            if i == self.selectedUsers.count - 1{
                chatKeys = chatKeys + (self.selectedUsers[i].key ?? "")
            }else{
            chatKeys = chatKeys + (self.selectedUsers[i].key ?? "") + ","
            }
        }
        return chatKeys
    }
    fileprivate func changeColorThemeTAb(_ sender: Int) {
        selectedIndex = sender
        let indexPath = IndexPath(row: 0, section: 1)
        let cell = tableChat.cellForRow(at: indexPath) as? CategoryCell
        if let getCell = cell{
            
            if sender == 0{
                getCell.lblFirstTect.textColor = #colorLiteral(red: 0.6859999895, green: 0.2660000026, blue: 0.8500000238, alpha: 1)
                getCell.lblSecondText.textColor = #colorLiteral(red: 0.5600000024, green: 0.6069999933, blue: 0.7009999752, alpha: 1)
                getCell.lblFistTab.backgroundColor = #colorLiteral(red: 0.6859999895, green: 0.2660000026, blue: 0.8500000238, alpha: 1)
                getCell.lblSecondTab.backgroundColor = .clear
                
            }else{
                getCell.lblSecondText.textColor = #colorLiteral(red: 0.6859999895, green: 0.2660000026, blue: 0.8500000238, alpha: 1)
                getCell.lblFirstTect.textColor = #colorLiteral(red: 0.5600000024, green: 0.6069999933, blue: 0.7009999752, alpha: 1)
                getCell.lblSecondTab.backgroundColor = #colorLiteral(red: 0.6859999895, green: 0.2660000026, blue: 0.8500000238, alpha: 1)
                getCell.lblFistTab.backgroundColor = .clear
            }
        }
    }
    
    @IBAction func btnChangeTabAction(_ sender: UIButton) {
        changeColorThemeTAb(sender.tag)
        self.tableChat.reloadData()
    }
    
    @IBAction func textfieldSearchMember(_ sender: UITextField) {
        self.callChatMembersAPI(page: 1, userName: sender.text ?? "")
    }
    
}
extension ChatListVC:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == self.tableChat{
            return 3
        }else{
            return 1
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.tableChat{
            if section == 0{
                return 1
            }else if section == 1{
                return 1
            }else{
                if selectedIndex == 1{
                    return arraymyGroups.count
                }else{
                return arrayLastMessages.count
                }
            }
        }else{
            return self.arrayUsers.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.tableChat{
            switch indexPath.section {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell", for: indexPath) as! CategoryCell
                cell.arraySepared = arrayGroupStrings
                cell.groupImage = arrayChatGroup
                cell.delegate = self
                cell.collectionViewText.reloadData()
                return cell
            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell1", for: indexPath) as! CategoryCell
                self.changeColorThemeTAb(selectedIndex)
                return cell
            case 2:
                if selectedIndex == 1{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell2", for: indexPath) as! CategoryCell
                    cell.lblTitle.text = arraymyGroups[indexPath.row].title ?? ""
                    cell.arraymyGroups = arraymyGroups
                    cell.showImage(indexPath.row)
                    return cell
                }else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "DoctorTextCell", for: indexPath) as! DoctorTextCell
                    
                    if let url = URL(string: arrayLastMessages[indexPath.row].file_url_image ?? ""){
                        cell.imgviewCell2.kf.indicatorType = .activity
                        cell.imgviewCell2.kf.setImage(with: url)
                        cell.imgviewCell2.isHidden = false
                    }else{
                        cell.imgviewCell2.isHidden = true
                    }
                    cell.lblDesc.text = arrayLastMessages[indexPath.row].last_message ?? ""
                    cell.lblTitle.text = arrayLastMessages[indexPath.row].title ?? ""
                    if let url = URL(string: arrayLastMessages[indexPath.row].image_url ?? ""){
                        cell.imgViewCell.kf.indicatorType = .activity
                        cell.imgViewCell.kf.setImage(with: url)
                    }
                    return cell
                }
            default:
                return UITableViewCell()
            }
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell2", for: indexPath) as! CategoryCell
            cell.lblTitle.text = arrayUsers[indexPath.row].name ?? ""
            if let url = URL(string:  arrayUsers[indexPath.row].chatImage ?? ""){
                cell.imgviewCell.kf.indicatorType = .activity
                cell.imgviewCell.kf.setImage(with: url)
            }
            if arrayUsers[indexPath.row].isSelected == true{
                cell.imageviewCellRight.image = UIImage(named: "checked_box")
            }else{
                cell.imageviewCellRight.image = UIImage(named: "unchecked_box")
            }
            return cell
        }
    }
    func selectGroup(index: Int) {
        if #available(iOS 13.0, *) {
            
            let vc = self.storyboard?.instantiateViewController(identifier: "ChatDetailsVC") as! ChatDetailsVC
           
            vc.userName = arrayChatGroup[index].title ?? ""
            vc.chatKey = arrayChatGroup[index].key
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            let vc = ChatDetailsVC.init(nibName: "ChatDetailsVC", bundle: nil)
            //vc.arrayChatMessages = self.arrayAllMessages
            vc.userName = arrayChatGroup[index].title ?? ""
            vc.chatKey = arrayChatGroup[index].key
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == self.tableChat{
           
                if selectedIndex == 0{
                //self.callPerticularMessagesAPI(page: 1, key: arrayLastMessages[indexPath.row].key ?? "", userName: arrayLastMessages[indexPath.row].owner_username ?? "")
                    if #available(iOS 13.0, *) {
                        
                        let vc = self.storyboard?.instantiateViewController(identifier: "ChatDetailsVC") as! ChatDetailsVC
                        vc.arrayChatMessages = self.arrayAllMessages

                        if indexPath.row <= selectedUsers.count {
                            vc.userName = arrayLastMessages[indexPath.row].owner_username ?? ""
                        }

                        vc.chatKey = arrayLastMessages[indexPath.row].key
                        self.navigationController?.pushViewController(vc, animated: true)
                    } else {
                        let vc = ChatDetailsVC.init(nibName: "ChatDetailsVC", bundle: nil)
                        vc.arrayChatMessages = self.arrayAllMessages
                        vc.userName = arrayLastMessages[indexPath.row].owner_username ?? ""
                        vc.chatKey = arrayLastMessages[indexPath.row].key
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }else{
                    if #available(iOS 13.0, *) {
                        
                        let vc = self.storyboard?.instantiateViewController(identifier: "ChatDetailsVC") as! ChatDetailsVC
                        vc.arrayChatMessages = self.arrayAllMessages
                        vc.userName = arraymyGroups[indexPath.row].title ?? ""
                        vc.chatKey = arraymyGroups[indexPath.row].key ?? ""
                        self.navigationController?.pushViewController(vc, animated: true)
                    } else {
                        let vc = ChatDetailsVC.init(nibName: "ChatDetailsVC", bundle: nil)
                        vc.arrayChatMessages = self.arrayAllMessages
                        vc.userName = arraymyGroups[indexPath.row].title ?? ""
                        vc.chatKey = arraymyGroups[indexPath.row].key ?? ""
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    //self.callPerticularMessagesAPI(page: 1, key: arraymyGroups[indexPath.row].key ?? "", userName: arraymyGroups[indexPath.row].title ?? "")
                }
            
            
        }else{
            let selectedObj = arrayUsers[indexPath.row]
            if selectedUsers.count > 0{
            var identified = -1
            for i in 0...selectedUsers.count - 1{
                if selectedUsers[i].key == selectedObj.key{
                    identified = i
                }
            }
            if identified > -1{
                self.selectedUsers.remove(at: identified)
                arrayUsers[indexPath.row].isSelected = false
            }else{
            self.selectedUsers.append(arrayUsers[indexPath.row])
                arrayUsers[indexPath.row].isSelected = true
            }
            }else{
                self.selectedUsers.append(arrayUsers[indexPath.row])
                arrayUsers[indexPath.row].isSelected = true
            }
            if selectedUsers.count == 0{
                self.heightCollection.constant = 0
            }else{
                self.heightCollection.constant = 60
            }
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
           self.collectionUserList.reloadData()
            self.tableSearchUser.reloadData()
        }
    }
    
    
}

extension ChatListVC{
    func callChatMembersAPI(page:Int,userName:String) {
        
        // let getUrl :String = API.groupSuggession.getURL()?.absoluteString ?? ""
        var getUrl :String = "https://ws.aescare.com/messengers/users?page=1&limit=100&order_by=date&user_name="
        getUrl.append(userName)
        
        
        let operation = WebServiceOperation.init(getUrl, nil, .WEB_SERVICE_GET, nil)
        
        operation.completionBlock =
            {
                DispatchQueue.main.async {
                    
                    print(operation.responseData?.dictionary?.prettyprintedJSON ?? "")
                    guard let dictResponse = operation.responseData?.dictionary, dictResponse.count > 0 else {
                        
                        return
                    }
                    
                    if let responceCode = dictResponse["code"] ,responceCode as! Int == 1{
                        if let dataUserDetails = dictResponse.data, dataUserDetails.count > 0 {
                            do{
                                if let allUsersDict = dictResponse["users"] as? [Any]{
                                    if let userdetailsData = allUsersDict.data, userdetailsData.count > 0{
                                        let users = try JSONDecoder().decode([User1].self, from: userdetailsData )
                                        self.arrayUsers = users
                                    }
                                }
                                
                            }catch let e{
                                print(e.localizedDescription)
                                
                                //showAlertMessage(title: App_Title, message: "Something went wrong", vc: self)
                            }
                        }
                    }else{
                        // showAlertMessage(title: App_Title, message: "Something went wrong", vc: self)
                    }
                    self.tableSearchUser.reloadData()
                    
                    if (self.tableSearchUser.contentSize.height + 300) > 450{
                        self.heightSearchResult.constant = 300
                    }else{
                        self.heightSearchResult.constant = self.tableSearchUser.contentSize.height + 300
                    }
                    
                }
            }
        
        appDel.operationQueue_background.addOperation(operation)
        
    }
    func callgroupChatAPI() {
        
        let getUrl :String = API.groupSuggession.getURL()?.absoluteString ?? ""
        
        
        let operation = WebServiceOperation.init(getUrl, nil, .WEB_SERVICE_GET, nil)
        
        operation.completionBlock =
            {
                DispatchQueue.main.async {
                    
                    // print(operation.responseData?.dictionary?.prettyprintedJSON ?? "")
                    guard let dictResponse = operation.responseData?.dictionary, dictResponse.count > 0 else {
                        
                        return
                    }
                    if let dataUserDetails = dictResponse.data, dataUserDetails.count > 0 {
                        do{
                            
                            let chatGroup = try JSONDecoder().decode(ChatGroup_Base.self, from: dataUserDetails)
                            self.arrayChatGroup = chatGroup.chat_groups ?? []
                            self.arrayGroupStrings.removeAll()
                            for group in self.arrayChatGroup{
                                self.arrayGroupStrings.append(group.title ?? "")
                            }
                        }catch let e{
                            print(e.localizedDescription)
                            
                            //showAlertMessage(title: App_Title, message: "Something went wrong", vc: self)
                        }
                    }
                    
                    self.tableChat.reloadData()
                    
                }
            }
        
        appDel.operationQueue.addOperation(operation)
        
    }
    func callPerticularMessagesAPI(page:Int,key:String,userName:String) {
        
        var getUrl :String = API.PerticularChatMessage.getURL()?.absoluteString ?? ""
        // https://ws.aescare.com/messenger/chats?page=1
        getUrl.append("?page="+"\(page)")
        getUrl.append("&chat_key="+"\(key)")
        let operation = WebServiceOperation.init(getUrl, nil, .WEB_SERVICE_GET, nil)
        
        operation.completionBlock =
            {
                DispatchQueue.main.async {
                    
                    print(operation.responseData?.dictionary?.prettyprintedJSON ?? "")
                    guard let dictResponse = operation.responseData?.dictionary, dictResponse.count > 0 else {
                        
                        return
                    }
                    
                    
                    if let dataUserDetails = dictResponse.data, dataUserDetails.count > 0 {
                        do{
                            
                            let lastMessage = try JSONDecoder().decode(Chat_Base.self, from: dataUserDetails)
                            self.arrayAllMessages.append(contentsOf: lastMessage.chats ?? [])
                            //self.page.pageNumber = page
                            if lastMessage.chats?.count == 0{
                                self.stopPagination = true
                                if #available(iOS 13.0, *) {
                                    
                                    let vc = self.storyboard?.instantiateViewController(identifier: "ChatDetailsVC") as! ChatDetailsVC
                                    vc.arrayChatMessages = self.arrayAllMessages
                                    vc.userName = userName
                                    vc.chatKey = key
                                    self.navigationController?.pushViewController(vc, animated: true)
                                } else {
                                    let vc = ChatDetailsVC.init(nibName: "ChatDetailsVC", bundle: nil)
                                    vc.arrayChatMessages = self.arrayAllMessages
                                    vc.userName = userName
                                    vc.chatKey = key
                                    self.navigationController?.pushViewController(vc, animated: true)
                                }
                            }else{
                                self.callPerticularMessagesAPI(page: page + 1, key: key, userName: userName)
                            }
                            //
                        }catch let e{
                            print(e.localizedDescription)
                            
                            //showAlertMessage(title: App_Title, message: "Something went wrong", vc: self)
                        }
                    }
                    
                    self.tableChat.reloadData()
                    
                }
            }
        
        appDel.operationQueue.addOperation(operation)
        
    }
    func callJoinGroupAPI(chatTitle:String,userkeys:String,isGroup:Bool) {
           
        let getUrl :String = API.JoinGroup.getURL()?.absoluteString ?? ""
        var param :[String:AnyObject] = [:]
        param["chat_title"] = chatTitle as AnyObject
        if isGroup == true{
            param["user_keys"] = userkeys as AnyObject
        }else{
            param["user_key"] = userkeys as AnyObject
        }
        
       
        
           
           let operation = WebServiceOperation.init(getUrl, param, .WEB_SERVICE_MULTI_PART, nil)
           
           operation.completionBlock =
               {
                   DispatchQueue.main.async {
                       
                       print(operation.responseData?.dictionary?.prettyprintedJSON ?? "")
                       guard let dictResponse = operation.responseData?.dictionary, dictResponse.count > 0 else {
                           
                           return
                       }
                       
                       
                    if let responceCode = dictResponse["code"] ,responceCode as! Int == 1{
                        if let dataUserDetails = dictResponse.data, dataUserDetails.count > 0 {
                            do{
                                if let chatKey = dictResponse["chat_key"] as? String{
                                    if #available(iOS 13.0, *) {
                                        
                                        let vc = self.storyboard?.instantiateViewController(identifier: "ChatDetailsVC") as! ChatDetailsVC
                                        //vc.arrayChatMessages = self.arrayAllMessages
                                        vc.userName = chatTitle
                                        vc.chatKey = chatKey
                                        self.navigationController?.pushViewController(vc, animated: true)
                                    } else {
                                        let vc = ChatDetailsVC.init(nibName: "ChatDetailsVC", bundle: nil)
                                        //vc.arrayChatMessages = self.arrayAllMessages
                                        vc.userName = chatTitle
                                        vc.chatKey = chatKey
                                        self.navigationController?.pushViewController(vc, animated: true)
                                    }
                                        
                                    }
                                }
                                
                            }
                        }
                    
                    
                    
                    
                      
                       
                       self.tableChat.reloadData()
                       
                   }
               }
           
           appDel.operationQueue.addOperation(operation)
           
       }
    
    func callLastMessagesAPI(page:Int) {
        
        var getUrl :String = API.LastMessages.getURL()?.absoluteString ?? ""
        // https://ws.aescare.com/messenger/chats?page=1
        getUrl.append("?page="+"\(page)")
        
        let operation = WebServiceOperation.init(getUrl, nil, .WEB_SERVICE_GET, nil)
        
        operation.completionBlock =
            {
                DispatchQueue.main.async {
                    
                    print(operation.responseData?.dictionary?.prettyprintedJSON ?? "")
                    guard let dictResponse = operation.responseData?.dictionary, dictResponse.count > 0 else {
                        
                        return
                    }
                    
                    
                    if let dataUserDetails = dictResponse.data, dataUserDetails.count > 0 {
                        do{
                            
                            let lastMessage = try JSONDecoder().decode(LastChat_Base.self, from: dataUserDetails)
                            
                            if self.arrayLastMessages.isEmpty || self.arrayLastMessages.count < lastMessage.chats!.count {
                                self.arrayLastMessages.append(contentsOf: lastMessage.chats ?? [])
                            }

                            
                            self.page.pageNumber = page
                            if lastMessage.chats?.count == 0{
                                self.stopPagination = true
                            }
                            self.arraymyGroups = self.arrayLastMessages.filter{
                                $0.participants_count ?? 0 > 1
                            }
                            //
                        }catch let e{
                            print(e.localizedDescription)
                            
                            //showAlertMessage(title: App_Title, message: "Something went wrong", vc: self)
                        }
                    }
                    
                    self.tableChat.reloadData()
                    
                }
            }
        
        appDel.operationQueue.addOperation(operation)
        
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if(scrollView == tableChat) {
            
            let height = scrollView.frame.size.height
            let contentYoffset = scrollView.contentOffset.y
            let distanceFromBottom = scrollView.contentSize.height - contentYoffset
            if ((distanceFromBottom*100).rounded()/100 <= (height*100).rounded()/100) {
                
                self.paginate()
                // print(" you reached end of the table")
            }
        }
    }
    func paginate(){
        
        if stopPagination == false{
            self.callLastMessagesAPI(page: page.pageNumber + 1)
            page.pageNumber = (page.pageNumber + 1)
        }
        
    }
}
extension ChatListVC:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return selectedUsers.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collectionUserList{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SliderCollCell1", for: indexPath) as! SliderCollCell
            cell.labelMheaderName.text = selectedUsers[indexPath.row].name
            if let url = URL(string:  selectedUsers[indexPath.row].chatImage ?? ""){
                cell.imageviewSlider.kf.indicatorType = .activity
                cell.imageviewSlider.kf.setImage(with: url)
            }
            return cell
        }else{
            
            
            return UICollectionViewCell()
        }
    }
    
    @objc func tap(_ sender: UITapGestureRecognizer) {
        print("Pressionado!!!")
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
//        arrayUsers[indexPath.row].isSelected = true
        if selectedUsers.count > 0{
        var identified = -1
        for i in 0...arrayUsers.count - 1{
            if arrayUsers[i].key == selectedUsers[indexPath.row].key{
                identified = i
                break
            }
        }
        if identified > -1{
            
            arrayUsers[identified].isSelected = false
        }
             selectedUsers.remove(at: indexPath.row)
        }else{
            
        }
        if selectedUsers.count == 0{
            self.heightCollection.constant = 0
        }
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
        self.collectionUserList.reloadData()
        self.tableSearchUser.reloadData()
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //return CGSize(width: 90, height: 60)
        
        return  CGSize(width: 50, height: 50)
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        //         if collectionView == collectionViewText{
        //        return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        //         }else{
        return .zero
        //  }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
        
    }
    
    func recentChatImg(_ index: Int) {
        DispatchQueue.global().async {
            let data = try! Data(contentsOf: URL(string: "https://br.aescare.com/chat/photo/\(self.arrayChatGroup[index].key ?? "")")!)
            
            DispatchQueue.main.async {
                self.imgRecentChatTab.image = UIImage(data: data)
            }
        }
    }
}

extension ChatListVC {
    func requestChatGroup(completion: @escaping (Bool) -> Void) {
        let url = "\(BaseUrl)/messengers/chats?page=1"
        
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default).validate(statusCode: 200 ..< 299).responseJSON { AFdata in
            
            switch AFdata.result {
            case let .success(value):
                print(value)
            case let .failure(error):
                print(error)
            }
            
                do {
                    guard let jsonObject = try JSONSerialization.jsonObject(with: AFdata.data!) as? [String: Any] else {
                        print("Error: Cannot convert data to JSON object")
                        return
                    }
                    guard let prettyJsonData = try? JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted) else {
                        print("Error: Cannot convert JSON object to Pretty JSON data")
                        return
                    }
                    guard let prettyPrintedJson = String(data: prettyJsonData, encoding: .utf8) else {
                        print("Error: Could print JSON in String")
                        return
                    }

                    let lastMessage = try JSONDecoder().decode(LastChat_Base.self, from: AFdata.data!)
                    
                    if self.arrayLastMessages.isEmpty || self.arrayLastMessages.count < lastMessage.chats!.count {
                        self.arrayLastMessages.append(contentsOf: lastMessage.chats ?? [])
                    }
                    
                    
                    print(prettyPrintedJson)
                    completion(true)
                } catch {
                    print("Error: Trying to convert JSON data to string")
                    return
                }
            }
    }
    
    func requestEnterGroup(_ key: String) {
        let url = "\(BaseUrl)/chat/get_chat?chat_key=\(key)&invite=true"
        
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default).validate(statusCode: 200 ..< 299).responseJSON { AFdata in
            
            switch AFdata.result {
            case let .success(value):
                print(value)
            case let .failure(error):
                print(error)
            }
            
                do {
                    guard let jsonObject = try JSONSerialization.jsonObject(with: AFdata.data!) as? [String: Any] else {
                        print("Error: Cannot convert data to JSON object")
                        return
                    }
                    guard let prettyJsonData = try? JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted) else {
                        print("Error: Cannot convert JSON object to Pretty JSON data")
                        return
                    }
                    guard let prettyPrintedJson = String(data: prettyJsonData, encoding: .utf8) else {
                        print("Error: Could print JSON in String")
                        return
                    }

                    let lastMessage = try JSONDecoder().decode(LastChat_Base.self, from: AFdata.data!)
                    
                    if self.arrayLastMessages.isEmpty || self.arrayLastMessages.count < lastMessage.chats!.count {
                        self.arrayLastMessages.append(contentsOf: lastMessage.chats ?? [])
                    }
                    
                    
                    print(prettyPrintedJson)
                } catch {
                    print("Error: Trying to convert JSON data to string")
                    return
                }
            }
    }
}

