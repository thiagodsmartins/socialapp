//
//  SpeachVC.swift
//  AesCare
//
//  Created by Gali Srikanth on 08/08/20.
//

import UIKit

class SpeachVC: UIViewController {
    override func viewDidLoad() {
           super.viewDidLoad()
       }
    @IBAction func navigateNext(_ sender: UIButton) {
        
        if #available(iOS 13.0, *) {
            let vc = self.storyboard?.instantiateViewController(identifier: "LoginVC") as! LoginVC
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            // Fallback on earlier versions
            let vc = LoginVC.init(nibName: "LoginVC", bundle: nil)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
