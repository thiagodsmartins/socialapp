//
//  BemVindaVC.swift
//  AesCare
//
//  Created by Gali Srikanth on 08/08/20.
//

import UIKit

class BemVindaVC: UIViewController , UIScrollViewDelegate {
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var scrollView: UIScrollView!{
        didSet{
            scrollView.delegate = self
        }
    }
    var slides:[UIView] = [];
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        slides = createSlides()
        setupSlideScrollView(slides: slides)
        
        pageControl.numberOfPages = slides.count
        pageControl.currentPage = 0
        view.bringSubviewToFront(pageControl)
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func createSlides() -> [UIView] {
        
        let slide1 = BemVindaVW()
        slide1.frame = CGRect(x:0,y:0,width:self.view.frame.size.width,height:scrollView.frame.size.height)
        slide1.basicSetup()
        
        
        let slide2 = AgendaVW()
        slide2.frame = CGRect(x:0,y:0,width:self.view.frame.size.width,height:scrollView.frame.size.height)
        slide2.basicSetup()
        
        let slide3 = ConverseVW()
        slide3.frame =  CGRect(x:0,y:0,width:self.view.frame.size.width,height:scrollView.frame.size.height)
        slide3.basicSetup()
        
        let slide4 = PerfilVW()
        slide4.frame =  CGRect(x:0,y:0,width:self.view.frame.size.width,height:scrollView.frame.size.height)
        slide4.basicSetup()
        
        return [slide1,slide2,slide3,slide4]
    }
    func setupSlideScrollView(slides : [UIView]) {
        //scrollView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        scrollView.contentSize = CGSize(width: scrollView.frame.size.width * CGFloat(slides.count), height: scrollView.frame.size.height)
        scrollView.isPagingEnabled = true
        
        for i in 0 ..< slides.count {
            //slides[i].frame = CGRect(x: view.frame.width * CGFloat(i), y: 0, width: view.frame.width, height: view.frame.height)
            //slides[i].frame = CGRect(x:(self.view.frame.size.width *  CGFloat(i)),y:0,width:(self.view.frame.size.height * 0.8),height:self.view.frame.size.width)
            slides[i].frame = CGRect(x:(slides[i].frame.size.width *  CGFloat(i)),y:0,width:slides[i].frame.size.width,height:slides[i].frame.size.height)
            scrollView.addSubview(slides[i])
        }
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView.contentOffset.y>0 {
            scrollView.contentOffset.y = 0
        }
        
        
        let pageIndex = round(scrollView.contentOffset.x/view.frame.width)
        pageControl.currentPage = Int(pageIndex)
         /*
         let maximumHorizontalOffset: CGFloat = scrollView.contentSize.width - scrollView.frame.width
         let currentHorizontalOffset: CGFloat = scrollView.contentOffset.x
         
         // vertical
         let maximumVerticalOffset: CGFloat = scrollView.contentSize.height - scrollView.frame.height
         let currentVerticalOffset: CGFloat = scrollView.contentOffset.y
         
         let percentageHorizontalOffset: CGFloat = currentHorizontalOffset / maximumHorizontalOffset
         let percentageVerticalOffset: CGFloat = currentVerticalOffset / maximumVerticalOffset
         */
        
        /*
         * below code changes the background color of view on paging the scrollview
         */
        //        self.scrollView(scrollView, didScrollToPercentageOffset: percentageHorizontalOffset)
        
        
        /*
         * below code scales the imageview on paging the scrollview
         */
        /*
         let percentOffset: CGPoint = CGPoint(x: percentageHorizontalOffset, y: percentageVerticalOffset)
         
         if(percentOffset.x > 0 && percentOffset.x <= 0.25) {
         
         slides[0].imageView.transform = CGAffineTransform(scaleX: (0.25-percentOffset.x)/0.25, y: (0.25-percentOffset.x)/0.25)
         slides[1].imageView.transform = CGAffineTransform(scaleX: percentOffset.x/0.25, y: percentOffset.x/0.25)
         
         } else if(percentOffset.x > 0.25 && percentOffset.x <= 0.50) {
         slides[1].imageView.transform = CGAffineTransform(scaleX: (0.50-percentOffset.x)/0.25, y: (0.50-percentOffset.x)/0.25)
         slides[2].imageView.transform = CGAffineTransform(scaleX: percentOffset.x/0.50, y: percentOffset.x/0.50)
         
         } else if(percentOffset.x > 0.50 && percentOffset.x <= 0.75) {
         slides[2].imageView.transform = CGAffineTransform(scaleX: (0.75-percentOffset.x)/0.25, y: (0.75-percentOffset.x)/0.25)
         slides[3].imageView.transform = CGAffineTransform(scaleX: percentOffset.x/0.75, y: percentOffset.x/0.75)
         
         } else if(percentOffset.x > 0.75 && percentOffset.x <= 1) {
         slides[3].imageView.transform = CGAffineTransform(scaleX: (1-percentOffset.x)/0.25, y: (1-percentOffset.x)/0.25)
         slides[4].imageView.transform = CGAffineTransform(scaleX: percentOffset.x, y: percentOffset.x)
         }*/
    }
    
    
    @IBAction func nextSlidePress(_ sender: Any) {
        let nextPage = pageControl.currentPage + 1
        if(nextPage < slides.count){
            scrollView.setContentOffset(CGPoint(x: scrollView.frame.size.width * CGFloat(nextPage), y: scrollView.contentSize.height), animated: true)
            }else{
            if #available(iOS 13.0, *) {
                let vc = self.storyboard?.instantiateViewController(identifier: "SpeachVC") as! SpeachVC
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                // Fallback on earlier versions
                let vc = SpeachVC.init(nibName: "SpeachVC", bundle: nil)
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
}
    
    
  



