//
//  JornadaTutorialVC.swift
//  AesCare
//
//  Created by Gali Srikanth on 12/08/20.
//

import UIKit

class JornadaTutorialVC: BaseViewController {
    //@IBOutlet weak var veBlur: UIView!
    //@IBOutlet var customTabbar:CustomTabbarView!
    @IBOutlet weak var indicationImage: UIImageView!
    @IBOutlet weak var containerPopup: UIView!
    @IBOutlet weak var vwContainerPublication: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        makeBlurEffect(toVW: self.view)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        ///// Call Custom Tabbar
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        ///// Call Custom Tabbar
        tabbarUISetup()
    }
    func tabbarUISetup(){
        ///// Call Custom Tabbar
        customTabbar.setup(activeFor:"Home")
    }
    func makeBlurEffect(toVW:UIView){
        
        if #available(iOS 13.0, *) {
            let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.systemUltraThinMaterialLight)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView.frame = toVW.bounds
            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            //toVW.alpha = 0.5
            toVW.addSubview(blurEffectView)
            
        } else {
            // Fallback on earlier versions
        }
        
    }
    override func viewLayoutMarginsDidChange() {
        self.view.bringSubviewToFront(self.customTabbar)
        callCustomPopupForIndication()
        self.view.bringSubviewToFront(self.containerPopup)
    }
    func callCustomPopupForIndication(){
        let optionView = CustomPopupTutorial()
        
        optionView.frame = CGRect(x:0,
                                  y:0,
                                  width:(self.view.frame.size.width * 0.9),
                                  height:(self.view.frame.size.height * 0.3))
                                  
                                  
                                  //(((self.view.frame.size.width * 180) / 330) - self.indicationImage.frame.size.height))
            //((self.view.frame.size.height * 0.25) - (self.indicationImage.frame.size.height - 30)))
         
        optionView.arrOption = ["Aqui você pode enviar o relato e fotos \n do resultado do seu procedimento."]
    
        optionView.tag = 1000
        optionView.basicSetup()
        optionView.fadeIn()
        optionView.delegate = self
        self.containerPopup.bringSubviewToFront(optionView)
        self.containerPopup.addSubview(optionView)
        self.containerPopup.bringSubviewToFront(self.indicationImage)
    }
}
extension JornadaTutorialVC:PopupViewDelegate{
    func didSelectNextPressed(_ view: CustomPopupTutorial, loadFor: String) {
        if #available(iOS 13.0, *) {
            let vc = self.storyboard?.instantiateViewController(identifier: "DashBoardVC") as! DashBoardVC
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            // Fallback on earlier versions
            let vc = DashBoardVC.init(nibName: "DashBoardVC", bundle: nil)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func didSelectSkipPressed(_ view: CustomPopupTutorial, loadFor: String) {
        if #available(iOS 13.0, *) {
            let vc = self.storyboard?.instantiateViewController(identifier: "DashBoardVC") as! DashBoardVC
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            // Fallback on earlier versions
            let vc = DashBoardVC.init(nibName: "DashBoardVC", bundle: nil)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
