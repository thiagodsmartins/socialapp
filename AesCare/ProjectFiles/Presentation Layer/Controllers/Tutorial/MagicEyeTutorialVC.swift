//
//  MagicEyeTutorialVC.swift
//  AesCare
//
//  Created by Gali Srikanth on 11/08/20.
//

import UIKit

class MagicEyeTutorialVC: BaseViewController {
    //@IBOutlet weak var veBlur: UIView!
    //@IBOutlet var customTabbar:CustomTabbarView!
    @IBOutlet weak var indicationImage: UIImageView!
    @IBOutlet weak var containerPopup: UIView!
    @IBOutlet weak var vwContainerPublication: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        makeBlurEffect(toVW: self.view)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        ///// Call Custom Tabbar
        tabbarUISetup()
    }
    func tabbarUISetup(){
        ///// Call Custom Tabbar
        customTabbar.setup(activeFor:"Home")
    }
    func makeBlurEffect(toVW:UIView){
        //only apply the blur if the user hasn't disabled transparency effects
        //view.backgroundColor = .white
        if #available(iOS 13.0, *) {
            let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.systemUltraThinMaterialLight)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView.frame = toVW.bounds
            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            //toVW.alpha = 0.5
            toVW.addSubview(blurEffectView)
            
        } else {
            // Fallback on earlier versions
        }
    }
    override func viewLayoutMarginsDidChange() {
       
        self.view.bringSubviewToFront(self.vwContainerPublication)
        callCustomPopupForIndication()
        self.view.bringSubviewToFront(self.containerPopup)
    }
    func callCustomPopupForIndication(){
        let optionView = CustomPopupTutorial()
        
        optionView.frame = CGRect(x:0,
                                  y:0,
                                  width:(self.view.frame.size.width * 0.9),
                                  height:(self.view.frame.size.height * 0.3))
            
            
            //(((self.view.frame.size.width * 220) / 330) - self.indicationImage.frame.size.height))
         
        optionView.arrOption = ["Esse é o olho mágico, para conteúdos \n sensíveis.","Caso você não se sinta \n confortável com as imagens,\n você pode deixa-las com filtro."]
        optionView.tag = 1000
        optionView.basicSetup()
        optionView.fadeIn()
        optionView.delegate = self
        self.containerPopup.bringSubviewToFront(optionView)
        self.containerPopup.addSubview(optionView)
        
    }
}
extension MagicEyeTutorialVC:PopupViewDelegate{
    func didSelectNextPressed(_ view: CustomPopupTutorial, loadFor: String) {
        if #available(iOS 13.0, *) {
            let vc = self.storyboard?.instantiateViewController(identifier: "NavigationTutorialVC") as! NavigationTutorialVC
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            // Fallback on earlier versions
            let vc = NavigationTutorialVC.init(nibName: "NavigationTutorialVC", bundle: nil)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func didSelectSkipPressed(_ view: CustomPopupTutorial, loadFor: String) {
        if #available(iOS 13.0, *) {
            let vc = self.storyboard?.instantiateViewController(identifier: "DashBoardVC") as! DashBoardVC
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            // Fallback on earlier versions
            let vc = DashBoardVC.init(nibName: "DashBoardVC", bundle: nil)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
}

