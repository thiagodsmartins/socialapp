//
//  NavigationTutorialVC.swift
//  AesCare
//
//  Created by Gali Srikanth on 12/08/20.
//

import UIKit

class NavigationTutorialVC: BaseViewController {
    //@IBOutlet weak var veBlur: UIView!
    //@IBOutlet var customTabbar:CustomTabbarView!
    @IBOutlet weak var indicationImage: UIImageView!
    @IBOutlet weak var containerPopup: UIView!
    @IBOutlet weak var vwContainerPublication: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        makeBlurEffect(toVW: self.view)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        ///// Call Custom Tabbar
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        ///// Call Custom Tabbar
        tabbarUISetup()
    }
    func tabbarUISetup(){
        ///// Call Custom Tabbar
        customTabbar.setup(activeFor:"Home")
    }
    func makeBlurEffect(toVW:UIView){
        if #available(iOS 13.0, *) {
            let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.systemUltraThinMaterialLight)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView.frame = toVW.bounds
            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            //toVW.alpha = 0.5
            toVW.addSubview(blurEffectView)
            
        } else {
            // Fallback on earlier versions
        }
    }
    override func viewLayoutMarginsDidChange() {
        self.view.bringSubviewToFront(self.customTabbar)
        callCustomPopupForIndication()
        self.view.bringSubviewToFront(self.containerPopup)
    }
    func callCustomPopupForIndication(){
        let optionView = CustomPopupTutorial()
        
        optionView.frame = CGRect(x:0,
                                  y:0,
                                  width:(self.view.frame.size.width * 0.9),
                                  height:(self.view.frame.size.height * 0.5))
                                  
                                  
                                  //(self.view.frame.size.width - self.indicationImage.frame.size.height))
            //((self.view.frame.size.height * 0.5) - (self.indicationImage.frame.size.height - 40)))
         
        optionView.arrOption = ["Esse é o seu menu de navegação. \n Por ele você navega entre as \n funcionalidades do app."]
        
        var arrOption1  : [[String:String]] = [[String:String]]()
        var param:[String:String] = [String:String]()
        param["itemName"] = "Início"
        param["imageName"] = "home-Active"
        arrOption1.append(param)
        
        param["itemName"] = "Envie seu Caso"
        param["imageName"] = "jornada-Active"
        arrOption1.append(param)
        
        param["itemName"] = "Agendar Consulta"
        param["imageName"] = "agendar-Active"
        arrOption1.append(param)
        
        param["itemName"] = "Mensagens"
        param["imageName"] = "chat-Active"
        arrOption1.append(param)
        
        param["itemName"] = "Meu Perfil"
        param["imageName"] = "profile-Active"
        arrOption1.append(param)
        
        optionView.arrOption1 = arrOption1
        optionView.tag = 1000
        optionView.basicSetup()
        optionView.fadeIn()
        optionView.delegate = self
        optionView.isNavigateForCustomTabbar = true
        self.containerPopup.bringSubviewToFront(optionView)
        self.containerPopup.addSubview(optionView)
        self.containerPopup.bringSubviewToFront(self.indicationImage)
    }
}
extension NavigationTutorialVC:PopupViewDelegate{
    func didSelectNextPressed(_ view: CustomPopupTutorial, loadFor: String) {
        if #available(iOS 13.0, *) {
            let vc = self.storyboard?.instantiateViewController(identifier: "ChatTutorialVC") as! ChatTutorialVC
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            // Fallback on earlier versions
            let vc = ChatTutorialVC.init(nibName: "ChatTutorialVC", bundle: nil)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func didSelectSkipPressed(_ view: CustomPopupTutorial, loadFor: String) {
        if #available(iOS 13.0, *) {
            let vc = self.storyboard?.instantiateViewController(identifier: "DashBoardVC") as! DashBoardVC
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            // Fallback on earlier versions
            let vc = DashBoardVC.init(nibName: "DashBoardVC", bundle: nil)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
}
