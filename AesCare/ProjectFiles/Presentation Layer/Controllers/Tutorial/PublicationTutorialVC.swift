//
//  PublicationTutorialVC.swift
//  AesCare
//
//  Created by Gali Srikanth on 11/08/20.
//

import UIKit

class PublicationTutorialVC: BaseViewController {
    //@IBOutlet weak var vwBlur: UIView!
    //@IBOutlet var customTabbar:CustomTabbarView!
    @IBOutlet weak var indicationImage: UIImageView!
    @IBOutlet weak var containerPopup: UIView!
    @IBOutlet weak var vwContainerPublication: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UserDefaults.standard.set(true, forKey: "hasUserLoggedOnce")
//        if UserDefaults.standard.bool(forKey: "isAppFirstTimeLaunched") {
//            if #available(iOS 13.0, *) {
//                self.dismiss(animated: false, completion: nil)
//                let vc = self.storyboard?.instantiateViewController(identifier: "DashBoardVC") as! DashBoardVC
//                //self.present(vc, animated: true, completion: nil)
//                self.navigationController?.pushViewController(vc, animated: true)
//            } else {
//                let vc = DashBoardVC.init(nibName: "DashBoardVC", bundle: nil)
//                self.navigationController?.pushViewController(vc, animated: true)
//            }
//        }
        //vwBlur.isHidden = true
        // Do any additional setup after loading the view.
        //makeBlurEffect(toVW: self.view)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
       
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        ///// Call Custom Tabbar
        tabbarUISetup()
    }
    func tabbarUISetup(){
        ///// Call Custom Tabbar
        customTabbar.setup(activeFor:"Home")
        //customTabbar.myDelegate = self
        //makeShadowToChildView(shadowView: customTabbar)
    }
    func makeBlurEffect(toVW:UIView){
        
        /*
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.extraLight)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = toVW.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        toVW.addSubview(blurEffectView)
        */
        
        /*
       let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.extraLight)
       let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.backgroundColor = UIColor.white.withAlphaComponent(0.2)
       blurEffectView.frame = toVW.bounds
       blurEffectView.autoresizingMask = [.flexibleTopMargin, .flexibleBottomMargin, .flexibleWidth, .flexibleHeight]
       //toVW.alpha = 0.5
       toVW.addSubview(blurEffectView)
        */
        /*
       let blurDilutionView = UIView(frame: toVW.frame)
       blurDilutionView.backgroundColor = UIColor.white.withAlphaComponent(0.5)
       blurDilutionView.autoresizingMask = [.flexibleTopMargin, .flexibleBottomMargin, .flexibleWidth, .flexibleHeight] //if not using AutoLayout
        
        toVW.addSubview(blurDilutionView)
        */
       //toVW.insertSubview(blurDilutionView, at: 0)
        /*
        let backView = UIView(frame: toVW.bounds)
        backView.backgroundColor = .white
        toVW.addSubview(backView)

        let blurEffect = UIBlurEffect(style: .light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = toVW.bounds
        toVW.addSubview(blurEffectView)
        */
       
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.extraLight)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = toVW.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        //toVW.alpha = 0.6
        self.view.addSubview(blurEffectView)

    }
    override func viewLayoutMarginsDidChange() {
        self.vwContainerPublication.layer.shadowColor = UIColor(named: "AppLightPink")?.cgColor
        
        vwContainerPublication.layer.shadowOpacity = 1
        vwContainerPublication.layer.shadowOffset = .zero
        vwContainerPublication.layer.shadowRadius = 2
        
        
        self.view.bringSubviewToFront(self.vwContainerPublication)
        callCustomPopupForIndication()
        self.view.bringSubviewToFront(self.containerPopup)
        
       
        ///
    }
    func callCustomPopupForIndication(){
        let optionView = CustomPopupTutorial()
        let arrData = ["Na caixinha acima você pode escrever publicações.","Escreva sobre você, seu caso ou solicite informações aqui.","Você pode anexar fotos e vídeos na sua publicação."]
        optionView.frame = CGRect(x:0,
                                  y:(self.indicationImage.frame.size.height + self.indicationImage.frame.origin.y),
                                  width:(self.view.frame.size.width * 0.9),
                                  height:(self.view.frame.size.height * 0.3))
            //CGFloat(Float((arrData.count * 30) + 50)))
          
        //(((self.view.frame.size.width * 280) / 340) - (self.indicationImage.frame.size.height - 20)))
            
        //((self.view.frame.size.height * 0.45) - self.indicationImage.frame.size.height))
        
        optionView.arrOption = arrData
        optionView.tag = 1000
        optionView.basicSetup()
        optionView.fadeIn()
        optionView.delegate = self
        self.containerPopup.bringSubviewToFront(optionView)
        self.containerPopup.addSubview(optionView)
        self.containerPopup.bringSubviewToFront(self.indicationImage)
        
    }
   
}
extension PublicationTutorialVC:PopupViewDelegate{
    func didSelectNextPressed(_ view: CustomPopupTutorial, loadFor: String) {
        if #available(iOS 13.0, *) {
            let vc = self.storyboard?.instantiateViewController(identifier: "MagicEyeTutorialVC") as! MagicEyeTutorialVC
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            // Fallback on earlier versions
            let vc = MagicEyeTutorialVC.init(nibName: "MagicEyeTutorialVC", bundle: nil)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func didSelectSkipPressed(_ view: CustomPopupTutorial, loadFor: String) {
        if #available(iOS 13.0, *) {
            let vc = self.storyboard?.instantiateViewController(identifier: "DashBoardVC") as! DashBoardVC
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            // Fallback on earlier versions
            let vc = DashBoardVC.init(nibName: "DashBoardVC", bundle: nil)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
}
extension UIImageView
{
    func makeBlurImage(targetImageView:UIImageView?)
    {
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = targetImageView!.bounds

        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight] // for supporting device rotation
        targetImageView?.addSubview(blurEffectView)
    }
}
