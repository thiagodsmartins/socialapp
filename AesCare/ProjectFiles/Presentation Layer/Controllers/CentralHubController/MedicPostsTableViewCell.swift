//
//  MedicPostsTableViewCell.swift
//  AesCare
//
//  Created by Thiago on 15/03/21.
//

import UIKit

protocol MedicPostsTableViewCellDelegate: class {
    func didPressMedicPostsReadMore(selected index: Int)
}

class MedicPostsTableViewCell: UITableViewCell {
    @IBOutlet weak var imageViewDoctorIcon: UIImageView!
    @IBOutlet weak var labelPostTitle: UILabel!
    @IBOutlet weak var labelPostDate: UILabel!
    @IBOutlet weak var labelMedicName: UILabel!
    @IBOutlet weak var imageViewDoctorInfo: UIImageView!
    @IBOutlet weak var labelPostDescription: UILabel!
    @IBOutlet weak var buttonReadMore: UIButton!
    var index = 0
    weak var delegate: MedicPostsTableViewCellDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        

        self.setupViews()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        if selected {
            self.labelPostDescription.textColor = .purple
            self.labelPostDescription.setBorder(width: 1, borderColor: .purple, cornerRadious: 10)
        }
        else {
            self.labelPostDescription.textColor = .black
            self.labelPostDescription.setBorder(width: 0, borderColor: .white, cornerRadious: 0)
        }
    }
 
    private func setupViews() {
        self.selectionStyle = .none
        
        self.imageViewDoctorIcon.setBorder(width: 2, borderColor: UIColor(red: 175/255, green: 68/255, blue: 217/255, alpha: 1.0), cornerRadious: self.imageViewDoctorIcon.frame.height / 2)
        
        self.imageViewDoctorInfo.setBorder(width: 2, borderColor: UIColor(red: 175/255, green: 68/255, blue: 217/255, alpha: 1.0), cornerRadious: 10)
        self.imageViewDoctorInfo.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleBottomMargin, .flexibleRightMargin, .flexibleLeftMargin, .flexibleTopMargin]
        self.imageViewDoctorInfo.contentMode = .scaleToFill
        self.imageViewDoctorInfo.clipsToBounds = true
        
        self.labelPostTitle.lineBreakMode = .byWordWrapping
        self.labelPostTitle.numberOfLines = 0
        
        self.labelPostDescription.lineBreakMode = .byWordWrapping
        self.labelPostDescription.numberOfLines = 0
        self.labelPostDescription.sizeToFit()
        
        self.labelMedicName.lineBreakMode = .byWordWrapping
        self.labelMedicName.numberOfLines = 0
        self.labelMedicName.sizeToFit()
        
        self.buttonReadMore.layer.cornerRadius = 10
        self.buttonReadMore.backgroundColor = UIColor(red: 175/255, green: 68/255, blue: 217/255, alpha: 1.0)
        self.buttonReadMore.tintColor = .white
        self.buttonReadMore.addTarget(self, action: #selector(self.buttonReadMorePressed(_:)), for: .touchUpInside)
    }
}

extension MedicPostsTableViewCell {
    @objc func buttonReadMorePressed(_ sender: UIButton) {
        self.delegate.didPressMedicPostsReadMore(selected: index)
    }
}
