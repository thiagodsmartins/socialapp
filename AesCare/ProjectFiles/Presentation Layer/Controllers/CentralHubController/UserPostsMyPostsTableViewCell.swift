//
//  UserPostsMyPostsTableViewCell.swift
//  AesCare
//
//  Created by Thiago on 08/04/21.
//

import UIKit

class UserPostsMyPostsTableViewCell: UITableViewCell {
    @IBOutlet weak var labelPostMessage: UILabel!
    @IBOutlet weak var imageViewUserIcon: UIImageView!
    @IBOutlet weak var labelPostDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setupViews()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    private func setupViews() {
        self.imageViewUserIcon.layer.cornerRadius = self.imageViewUserIcon.frame.height / 2
        self.imageViewUserIcon.layer.borderWidth = 2
        self.imageViewUserIcon.layer.borderColor = UIColor(red: 175/255, green: 68/255, blue: 217/255, alpha: 1.0).cgColor
    }
    
}
