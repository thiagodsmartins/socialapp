//
//  MedicsArticlesTableViewCell.swift
//  AesCare
//
//  Created by Thiago on 21/03/21.
//

import UIKit

protocol MedicsArticlesTableViewDelegate: class {
    func didPressMedicsArticlesReadMore(selected index: Int)
}

class MedicsArticlesTableViewCell: UITableViewCell {
    @IBOutlet weak var imageViewMedicIcon: UIImageView!
    @IBOutlet weak var labelMedicName: UILabel!
    @IBOutlet weak var labelArticleDescription: UILabel!
    @IBOutlet weak var labelArticleTitle: UILabel!
    @IBOutlet weak var buttonReadMore: UIButton!
    var index = 0
    weak var delegate: MedicsArticlesTableViewDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setupViews()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    private func setupViews() {
        self.selectionStyle = .none
        self.imageViewMedicIcon.setBorder(width: 2, borderColor: .purple, cornerRadious: 10)
        self.imageViewMedicIcon.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleBottomMargin, .flexibleRightMargin, .flexibleLeftMargin, .flexibleTopMargin]
        
        self.buttonReadMore.backgroundColor = UIColor(red: 175/255, green: 68/255, blue: 217/255, alpha: 1.0)
        self.buttonReadMore.layer.cornerRadius = 10
        self.buttonReadMore.tintColor = .white
        self.buttonReadMore.addTarget(self, action: #selector(self.buttonReadMorePressed(_:)), for: .touchUpInside)
    }
    
}

extension MedicsArticlesTableViewCell {
    @objc func buttonReadMorePressed(_ sender: UIButton) {
        self.delegate.didPressMedicsArticlesReadMore(selected: index)
    }
}
