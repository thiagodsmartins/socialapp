//
//  MenuCollectionViewCell.swift
//  AesCare
//
//  Created by Thiago on 13/03/21.
//

import UIKit

class MenuCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var buttonSelection: UIButton!
    
    var cellButtonTap: (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.buttonSelection.addTarget(self, action: #selector(self.buttonPressed(_:)), for: .touchUpInside)
    }

    func cellButtonTitle(index: Int) {
        switch index {
        case 0:
            self.buttonSelection.setTitle("Resultados", for: .normal)
            self.buttonSelection.setTitleColor(.purple, for: .normal)
        case 1:
            self.buttonSelection.setTitle("Artigos\nMédicos", for: .normal)
            self.buttonSelection.setTitleColor(.purple, for: .normal)
        case 2:
            self.buttonSelection.setTitle("Posts de\nUsuárias", for: .normal)
            self.buttonSelection.setTitleColor(.purple, for: .normal)
        case 3:
            self.buttonSelection.setTitle("Posts\nMédicos", for: .normal)
            self.buttonSelection.setTitleColor(.purple, for: .normal)
        default:
            print("")
        }
    }
}

extension MenuCollectionViewCell {
    @objc func buttonPressed(_ sender: UIButton) {
        self.cellButtonTap!()
    }
}
