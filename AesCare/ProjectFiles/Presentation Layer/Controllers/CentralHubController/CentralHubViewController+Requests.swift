//
//  CentralHubViewController+Requests.swift
//  AesCare
//
//  Created by Thiago on 16/03/21.
//

import Foundation
import SDWebImage
import Alamofire
import GoogleSignIn

extension CentralHubViewController {
    func publishUserPost(_ message: String, _ status: String = "a", response: @escaping (Bool) -> Void) {
        let headers: HTTPHeaders = ["Content-Type": "multipart/form-data", "Cookie": "aescare_session_key=\(usrerModelOBJ.session_key!)"]
        let messageToPost: Parameters = ["body": message, "status": status, "type": "post"] as Parameters
        let url = "\(BaseUrl)/post"
        
        Alamofire.upload(multipartFormData: {
            multipartFormData in
            for (key, value) in messageToPost {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to: url, method: .post, headers: headers, encodingCompletion: {
            encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { data in
                    print(data.result.value!)
                    response(true)
                }
            case .failure(let encodingError):
                print(encodingError)
                response(false)
            }
        })
    }
    
    func requestResults(_ page: Int = 1, completion: @escaping (Bool) -> Void) {
        let url = "\(BaseUrl)/results?limit=9&page=\(page)"
        let headers: HTTPHeaders = ["Content-Type": "application/json",
            "Cookie": "aescare_session_key=\(usrerModelOBJ.session_key!)"]
        
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers).validate(statusCode: 200 ..< 299).responseJSON { [weak self] AFdata in
            switch AFdata.result {
            case let .success(value):
                print(value)
            case let .failure(error):
                print(error)
            }
                do {
                    guard let jsonObject = try JSONSerialization.jsonObject(with: AFdata.data!) as? [String: Any] else {
                        print("Error: Cannot convert data to JSON object")
                        return
                    }
                    guard let prettyJsonData = try? JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted) else {
                        print("Error: Cannot convert JSON object to Pretty JSON data")
                        completion(false)
                        return
                    }
                    guard String(data: prettyJsonData, encoding: .utf8) != nil else {
                        print("Error: Could print JSON in String")
                        completion(false)
                        return
                    }
                    
                    let data = try! JSONDecoder().decode(ResultsResponse.self, from: AFdata.data!)
                    
                    print(data)
                    
                    if data.results!.isEmpty {
                        completion(false)
                    }
                    else {
                        self?.resultsDataArray.append(contentsOf: data.results!)
                        
//                        for i in self!.resultsDataArray {
//                            for j in i.comments! {
//                                print(j.user?.username!)
//                            }
//                        }
//
                        completion(true)
                    }
                } catch {
                    print("Error: Trying to convert JSON data to string")
                    completion(false)
                    return
                }
            }
    }
    
    func requestFilteredResults(_ page: Int = 1, filter: [String:Any?]? = nil, completion: @escaping (ResultsResponse?) -> Void) {
        var url = ""
        var medicId = ""
        var cityId = ""
        var procedureId = ""
        var specializations = ""
        
        if let _ = filter {
            if let medic = filter?["medic"] as? Int {
                medicId = "&medic_id=\(medic)"
            }
            
            if let city = filter?["city"] as? Int {
                cityId = "&city_id=\(city)"
            }
        
            if let specs = filter?["specialization"] as? [String:Int] {
                for (_, value) in specs {
                    specializations += "&specialty_id=\(value)"
                }
            }
        
            if let procedure = filter?["procedure"] as? Int {
                procedureId = "&treatment_id=\(procedure)"
            }
            
            url = "\(BaseUrl)/results?page=\(page)&limit=9\(cityId)\(medicId)\(specializations)\(procedureId)"
        }

        Alamofire.request(url, method: .get, encoding: JSONEncoding.default).validate(statusCode: 200 ..< 299).responseJSON { AFdata in
                do {
                    guard let jsonObject = try JSONSerialization.jsonObject(with: AFdata.data!) as? [String: Any] else {
                        print("Error: Cannot convert data to JSON object")
                        return
                    }
                    guard let prettyJsonData = try? JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted) else {
                        print("Error: Cannot convert JSON object to Pretty JSON data")
                        completion(nil)
                        return
                    }
                    guard String(data: prettyJsonData, encoding: .utf8) != nil else {
                        print("Error: Could print JSON in String")
                        completion(nil)
                        return
                    }
                    
                    let data = try! JSONDecoder().decode(ResultsResponse.self, from: AFdata.data!)
                    
                    if data.results!.isEmpty {
                        self.isFilteredRequestEmpty = true
                        completion(nil)
                    }
                    else {
                        self.isFilteredRequestEmpty = false
                        completion(data)
                    }
                } catch {
                    print("Error: Trying to convert JSON data to string")
                    completion(nil)
                    return
                }
            }
    }
    
    func requestUsersPosts(_ page: Int = 1, completion: @escaping (Bool) -> Void) {
        //let url = "\(BaseUrl)/posts?page=\(page)&is_medic=false&limit=9"
        let url = "\(BaseUrl)/posts?with_treatments_name=true&order_by=date&limit=9&type=post&page=\(page)"
        
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default).validate(statusCode: 200 ..< 299).responseJSON { [weak self] AFdata in
                do {
                    guard let jsonObject = try JSONSerialization.jsonObject(with: AFdata.data!) as? [String: Any] else {
                        print("Error: Cannot convert data to JSON object")
                        return
                    }
                    guard let prettyJsonData = try? JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted) else {
                        print("Error: Cannot convert JSON object to Pretty JSON data")
                        completion(false)
                        return
                    }
                    guard String(data: prettyJsonData, encoding: .utf8) != nil else {
                        print("Error: Could print JSON in String")
                        completion(false)
                        return
                    }
                    
                    let data = try! JSONDecoder().decode(PostsResponse.self, from: AFdata.data!)
                    
                    print(data.posts)
                    
                    if data.posts.isEmpty {
                        completion(false)
                    }
                    else {
                        self?.usersPostsDataArray.append(contentsOf: data.posts)
                        completion(true)
                    }
                } catch {
                    print("Error: Trying to convert JSON data to string")
                    completion(false)
                    return
                }
            }
    }
    
    func requestMyPosts(completion: @escaping ([PostsUsersMyPostsModel]?) -> Void) {
        let headers: HTTPHeaders = ["Content-Type": "multipart/form-data", "Cookie": "aescare_session_key=\(usrerModelOBJ.session_key!)"]
        let url = "\(BaseUrl)/posts?is_my=true"
        
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers).validate(statusCode: 200 ..< 299).responseJSON { AFdata in
                do {
                    guard let jsonObject = try JSONSerialization.jsonObject(with: AFdata.data!) as? [String: Any] else {
                        print("Error: Cannot convert data to JSON object")
                        return
                    }
                    guard let prettyJsonData = try? JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted) else {
                        print("Error: Cannot convert JSON object to Pretty JSON data")
                        //completion(nil)
                        return
                    }
                    guard String(data: prettyJsonData, encoding: .utf8) != nil else {
                        print("Error: Could print JSON in String")
                        //completion(nil)
                        return
                    }
                    
                    let data = try! JSONDecoder().decode(PostsUsersMyPosts.self, from: AFdata.data!)

                    if let _ = data.posts {
                        completion(data.posts)
                    }
                    else {
                        completion(nil)
                    }
                } catch {
                    print("Error: Trying to convert JSON data to string")
                    //completion(nil)
                    return
                }
            }
    }
    
    func requestMedicsPosts(_ page: Int = 1, completion: @escaping (Bool) -> Void) {
        //let url = "\(BaseUrl)/posts?page=\(page)&is_medic=true&limit=9"
        let url = "\(BaseUrl)/posts?limit=9&type=artigo&page=\(page)"
        
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default).validate(statusCode: 200 ..< 299).responseJSON { [weak self] AFdata in
                do {
                    guard let jsonObject = try JSONSerialization.jsonObject(with: AFdata.data!) as? [String: Any] else {
                        print("Error: Cannot convert data to JSON object")
                        return
                    }
                    guard let prettyJsonData = try? JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted) else {
                        print("Error: Cannot convert JSON object to Pretty JSON data")
                        completion(false)
                        return
                    }
                    guard String(data: prettyJsonData, encoding: .utf8) != nil else {
                        print("Error: Could print JSON in String")
                        completion(false)
                        return
                    }
                    
                    let data = try! JSONDecoder().decode(MedicsPostsResponse.self, from: AFdata.data!)
                    
                    if data.posts!.isEmpty {
                        completion(false)
                    }
                    else {
                        self?.medicsPostsDataArray.append(contentsOf: data.posts!)
                        completion(true)
                    }
                } catch {
                    print("Error: Trying to convert JSON data to string")
                    completion(false)
                    return
                }
            }
    }
    
    func requestMedicsArticles(_ page: Int = 1, completion: @escaping (Bool) -> Void) {
        let url = "\(BaseUrl)/posts?limit=9&type=artigo&page=\(page)"
        
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default).validate(statusCode: 200 ..< 299).responseJSON { [weak self] AFdata in
                do {
                    guard let jsonObject = try JSONSerialization.jsonObject(with: AFdata.data!) as? [String: Any] else {
                        print("Error: Cannot convert data to JSON object")
                        return
                    }
                    guard let prettyJsonData = try? JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted) else {
                        print("Error: Cannot convert JSON object to Pretty JSON data")
                        completion(false)
                        return
                    }
                    guard String(data: prettyJsonData, encoding: .utf8) != nil else {
                        print("Error: Could print JSON in String")
                        completion(false)
                        return
                    }
                    
                    let data = try! JSONDecoder().decode(MedicsArticlesResponse.self, from: AFdata.data!)
                    
                    if data.posts!.isEmpty {
                        completion(false)
                    }
                    else {
                        self?.medicsArticlesDataArray.append(contentsOf: data.posts!)
                        completion(true)
                    }
                } catch {
                    print("Error: Trying to convert JSON data to string")
                    completion(false)
                    return
                }
            }
    }
    
    public func renewUserData(_ data: GIDGoogleUser, response: @escaping (Bool) -> Void) {
        let parameters: Parameters = ["userId": data.userID!,
                                      "email": data.profile.email!] as Parameters
        let url = "\(BaseUrl)/login/google/mobile"
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default).validate(statusCode: 200 ..< 299).responseJSON { AFdata in

            switch AFdata.result {
            case let .success(value):
                print(value)
            case let .failure(error):
                print(error)
            }
                do {
                    guard let jsonObject = try JSONSerialization.jsonObject(with: AFdata.data!) as? [String: Any] else {
                        print("Error: Cannot convert data to JSON object")
                        response(false)
                        return
                    }
                    guard let prettyJsonData = try? JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted) else {
                        print("Error: Cannot convert JSON object to Pretty JSON data")
                        response(false)
                        return
                    }
                    guard let _ = String(data: prettyJsonData, encoding: .utf8) else {
                        print("Error: Could print JSON in String")
                        response(false)
                        return
                    }

                    usrerModelOBJ = try! JSONDecoder().decode(ModelUser.self, from: prettyJsonData)

                    response(true)
                } catch {
                    print("Error: Trying to convert JSON data to string")
                    response(false)
                    return
                }
            }
    }
    
}
