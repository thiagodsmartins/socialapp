//
//  MessageCollectionViewCell.swift
//  AesCare
//
//  Created by Thiago on 14/03/21.
//

import UIKit

class MessageCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageViewUser: UIImageView!
    @IBOutlet weak var textViewSendMessage: UITextView!
    @IBOutlet weak var buttonSendMessage: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    private func setupViews() {
        self.textViewSendMessage.text = "Faça uma publicação"
        self.textViewSendMessage.textColor = .lightGray
    }

}
