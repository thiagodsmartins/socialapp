//
//  CentralHubViewController.swift
//  AesCare
//
//  Created by Thiago on 12/03/21.
//

import Foundation
import UIKit
import AMTabView
import RESegmentedControl
import SDWebImage
import SRActivityIndicator
import SideMenu
import Alamofire
import Kingfisher

class CentralHubViewController: BaseViewController, TabItem {
    @IBOutlet weak var sectionTab: RESegmentedControl!
    @IBOutlet weak var tableViewResultados: UITableView!
    @IBOutlet weak var buttonOpenMenu: UIButton!
    @IBOutlet weak var viewGradient: UIView!
    @IBOutlet weak var viewSearch: designableView!
    
    let sectionTitles = ["Resultados", "Posts", "Posts Médicos"]
    var timer: Timer?
    var sectionTabIndex = 0
    var userImage: SDWebImageManager?
    var fabButton: UIButton!
    var fabButtonFilter: UIButton!
    var imageManager: SDWebImageDownloader!
    var blurImage: SDImageBlurTransformer!
    var resizedImage = UIImage()
    let app = UIApplication.shared.windows.first {$0.isKeyWindow}
    var navigation: UINavigationController!
    
    var isResultsImagesHidden = false
    var isPageLoaded = false
    var isPublishSelected = true
    var isFilterSelected = false
    var isResultsSelected = true
    var isUsersPostsSelected = false
    var isResultsDataLoading = false
    var isResultsDataAvaliable = false
    var isUsersPostsDataLoading = false
    var isUsersPostsDataAvaliable = false
    var isMedicsArticlesDataLoading = false
    var isMedicsArticlesDataAvaliable = false
    var isMedicsPostsDataLoading = false
    var isMedicsPostsDataAvaliable = false
    var isFilteredDataLoading = false
    var isFilteredDataAvaliable = false
    var isFilteredDataSelected = false
    var isFilteredRequestEmpty = false
    
    lazy var userImagesList = [UIImage]()
    lazy var hiddenImagePlaceHolder = UIImage(named: "logo-aescare")
    lazy var resultsDataArray = [ResultsModel]()
    lazy var usersPostsDataArray = [PostsModel]()
    lazy var usersPostsMyPostsDataArray = [PostsUsersMyPostsModel]()
    lazy var medicsPostsDataArray = [MedicsPostsModel]()
    lazy var medicsArticlesDataArray = [MedicsArticlesModel]()
    lazy var filteredDataArray = [ResultsModel]()
    lazy var filteredParameters = [String:Any?]()
    
    var resultsDataPagination = 1
    var usersPostsDataPagination = 1
    var medicsArticlesDataPagination = 1
    var medicsPostsDataPagination = 1
    var filteredDataPagination = 1
    
    var resizeLabel: CGFloat = 0.0
    var indexTracker: CGFloat = 0.0
    
    var tabImage: UIImage? {
        return UIImage(named: "home-icon")
    }
    
    var segmentItems: [SegmentModel] {
        return self.sectionTitles.map({ SegmentModel(title: $0) })
    }
    
    let modifier = AnyModifier { request in
        var r = request
        // replace "Access-Token" with the field name you need, it's just an example
        r.setValue("aescare_session_key=\(usrerModelOBJ.session_key!)", forHTTPHeaderField: "Cookie")
        return r
    }
    
    var nibFile: [UINib]?
    var tabIndex: Any?

    override func viewDidLoad() {
        super.viewDidLoad()
    
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let login = storyboard.instantiateViewController(identifier: "LoginVC") as! LoginVC
        let dashboard = storyboard.instantiateViewController(withIdentifier: "DashBoardVC") as! DashBoardVC
            
        self.navigation = UINavigationController()
        self.navigation.pushViewController(login, animated: true)
        self.navigation.pushViewController(dashboard, animated: true)
//        self.app?.rootViewController?.navigationController?.viewControllers.append(login)
//        self.app?.rootViewController?.navigationController?.viewControllers.append(dashboard)
        
        //app.window?.rootViewController = app.navigationStack
        self.blurImage = SDImageBlurTransformer(radius: 30)
        
        self.imageManager = SDWebImageDownloader()
        self.imageManager.setValue("aescare_session_key=\(usrerModelOBJ.session_key ?? "")", forHTTPHeaderField: "Cookie")
        
        //self.buttonOpenMenu.addTarget(self, action: #selector(self.buttonOpenSideMenuPressed(_:)), for: .touchUpInside)
        
        self.gradientView()
        self.setupTableView()
        //28, 233, 242  228, 233, 242     175, 68, 217
        var preset = MaterialPreset(backgroundColor: .white, tintColor: UIColor(red: 175/255, green: 68/255, blue: 217/255, alpha: 1.0))
        preset.textColor = UIColor(red: 220/255, green: 220/255, blue: 220/255, alpha: 1.0)
        
        self.sectionTab.configure(segmentItems: self.segmentItems, preset: preset, selectedIndex: 0)
        
        if !UserDefaults.standard.bool(forKey: "isUserDataComplete") {
            self.completeAccountData()
        }
        else {
            self.startCentralHubData()
        }
        
        self.fabButtonConfig()
        self.fabButtonFiltersConfig()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.greetingsMessages()
        self.greetingsTimer()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.timer?.invalidate()
        self.timer = nil
    }
    
    override func viewDidLayoutSubviews() {
        self.fabButton.layer.cornerRadius = self.fabButton.bounds.size.width / 2
        self.fabButtonFilter.layer.cornerRadius = self.fabButtonFilter.bounds.size.width / 2
        
    }
    
    private func startCentralHubData() {
        self.startActivityLoader()
        self.requestResults(completion: {
            resultsResponse in
            if resultsResponse {
                self.resultsDataPagination += 1
                
                self.requestMedicsArticles(completion: {
                    medicsArticlesResponse in
                    if medicsArticlesResponse {
                        self.medicsPostsDataPagination += 1
                        
                        self.requestUsersPosts(completion: {
                            usersPostsResponse in
                            if usersPostsResponse {
                                self.usersPostsDataPagination += 1
                                
                                self.requestMedicsPosts(completion: {
                                    medicsPosts in
                                    if medicsPosts {
                                        self.medicsPostsDataPagination += 1
                                        
                                        self.tableViewResultados.reloadData()
                                        
                                        self.tableViewResultados.isHidden = false
                                        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(5), execute: {
                                            self.stopActivityLoader()
                                            self.isPageLoaded = true
                                            self.fabButton.isHidden = false
                                            UserDefaults.standard.set(try! JSONEncoder().encode(usrerModelOBJ), forKey: "userData")
                                        })
                                    }
                                    else {
                                        self.stopActivityLoader()
                                        AESCarePopupDialog.dialog(title: "Problema de Carregamento", message: "Ocorreu um problema no carregamento dos dados. Tente novamente", image: UIImage(named: "common_image1"), viewController: self, {
                                            
                                        })
                                    }
                                })
                            }
                            else {
                                self.stopActivityLoader()
                                AESCarePopupDialog.dialog(title: "Problema de Carregamento", message: "Ocorreu um problema no carregamento dos dados. Tente novamente", image: UIImage(named: "common_image1"), viewController: self, {
                                    
                                })
                            }
                        })
                    }
                    else {
                        self.stopActivityLoader()
                        AESCarePopupDialog.dialog(title: "Problema de Carregamento", message: "Ocorreu um problema no carregamento dos dados. Tente novamente", image: UIImage(named: "common_image1"), viewController: self, {
                            
                            self.app?.window?.rootViewController?.navigationController?.popViewController(animated: true)
                        })
                    }
                })
            }
            else {
                self.stopActivityLoader()
                AESCarePopupDialog.dialog(title: "Problema de Carregamento", message: "Ocorreu um problema no carregamento dos dados. Tente novamente", image: UIImage(named: "common_image1"), viewController: self) {
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let loginViewController = storyboard.instantiateViewController(identifier: "LoginVC") as! LoginVC
                    
                    self.show(loginViewController, sender: self)
                }
                
            }
        })
        
        self.requestMyPosts(completion: {
            data in
            
            if let myPosts = data {
                self.usersPostsMyPostsDataArray = myPosts
                
                print(self.usersPostsMyPostsDataArray)
            }
        })
    }
    
    private func setupTableView() {
        self.nibFile = [UINib]()
        
        self.nibFile?.append(UINib(nibName: "ResultadosTableViewCell", bundle: nil))
        self.nibFile?.append(UINib(nibName: "FilterTableViewCell", bundle: nil))
        self.nibFile?.append(UINib(nibName: "ResultsTableViewCell", bundle: nil))
        self.nibFile?.append(UINib(nibName: "MedicPostsTableViewCell", bundle: nil))
        self.nibFile?.append(UINib(nibName: "UserPostsTableViewCell", bundle: nil))
        self.nibFile?.append(UINib(nibName: "MedicsArticlesTableViewCell", bundle: nil))
        self.nibFile?.append(UINib(nibName: "UserPostsMyPostsTableViewCell", bundle: nil))
        self.tableViewResultados.separatorColor = .white
        self.tableViewResultados.register(self.nibFile![0], forCellReuseIdentifier: "ResultadosTableViewCell")
        self.tableViewResultados.register(self.nibFile![1], forCellReuseIdentifier: "FilterTableViewCell")
        self.tableViewResultados.register(self.nibFile![2], forCellReuseIdentifier: "ResultsTableViewCell")
        self.tableViewResultados.register(self.nibFile![3], forCellReuseIdentifier: "MedicPostsTableViewCell")
        self.tableViewResultados.register(self.nibFile![4], forCellReuseIdentifier: "UserPostsTableViewCell")
        self.tableViewResultados.register(self.nibFile![5], forCellReuseIdentifier: "MedicsArticlesTableViewCell")
        self.tableViewResultados.register(self.nibFile![6], forCellReuseIdentifier: "UserPostsMyPostsTableViewCell")
        self.tableViewResultados.rowHeight = UITableView.automaticDimension
        self.tableViewResultados.estimatedRowHeight = 300
        self.tableViewResultados.isHidden = true
        self.tableViewResultados.delegate = self
        self.tableViewResultados.dataSource = self
        
    }
    
    private func startActivityLoader() {
        let url = URL(string: usrerModelOBJ.user?.url_image ?? "")
        let data = try? Data(contentsOf: url ?? URL(string: "")!)
        
        let activityMessage = UILabel(frame: CGRect(x: 140, y: 280, width: 200, height: 100))
        usrerModelOBJ.user?.gender == "F" ? (activityMessage.text = "Bem-vinda \(usrerModelOBJ.user!.username!)") :
                                            (activityMessage.text = "Bem-vindo \(usrerModelOBJ.user!.username!.prefix(30))")
        activityMessage.numberOfLines = 0
        activityMessage.textColor = .white
        activityMessage.translatesAutoresizingMaskIntoConstraints = false
        
        SRActivityIndicator.innerStrokeColor = UIColor(red: 255/255, green: 105/255, blue: 180/255, alpha: 1.0)
        SRActivityIndicator.outerStrokeColor = .purple
        SRActivityIndicator.centerImageSize = 100 
        SRActivityIndicator.centerImage = UIImage(data: data ?? Data())
        SRActivityIndicator.setMaskingWith(maskType: .Blur, alpha: 5)
        SRActivityIndicator.addSubview(activityMessage)
        SRActivityIndicator.show()
        
        activityMessage.centerXAnchor.constraint(equalTo: SRActivityIndicator.centerXAnchor).isActive = true
        activityMessage.bottomAnchor.constraint(equalTo: SRActivityIndicator.topAnchor, constant: 230).isActive = true
    }
    
    private func stopActivityLoader() {
        SRActivityIndicator.dissmiss()
    }
    
    private func fabButtonConfig() {
        self.fabButton = UIButton()
        //let yPst = ((self.view.frame.size.height / 2) + 320)
        //let xPst = ((self.view.frame.size.width / 2) + 140)
        //self.fabButton.frame = CGRect(x: 0, y: 0, width: 60, height: 60)
        self.fabButton.backgroundColor = .white
        self.fabButton.setImage(UIImage(named: "olho-aberto"), for: .normal)
        self.fabButton.autoresizingMask = [.flexibleTopMargin, .flexibleRightMargin]
        self.fabButton.addTarget(self, action: #selector(buttonHideResultsImagesPressed(_:)), for:.touchUpInside)
        self.fabButton.layer.shadowRadius = 3
        self.fabButton.layer.shadowColor = UIColor.lightGray.cgColor
        self.fabButton.layer.shadowOpacity = 0.9
        self.fabButton.layer.shadowOffset = CGSize.zero
        self.fabButton.layer.zPosition = 1
        self.fabButton.isHidden = true
        self.fabButton.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addSubview(self.fabButton)
        
        self.fabButton.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -20).isActive = true
        self.fabButton.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -80).isActive = true
        self.fabButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        self.fabButton.widthAnchor.constraint(equalToConstant: 50).isActive = true
        
    }
    
    private func fabButtonFiltersConfig() {
        self.fabButtonFilter = UIButton()
        //let yPst = ((self.view.frame.size.height / 2) + 320)
        //let xPst = ((self.view.frame.size.width / 2) + 140)
        //self.fabButton.frame = CGRect(x: 0, y: 0, width: 60, height: 60)
        self.fabButtonFilter.backgroundColor = .clear
        self.fabButtonFilter.setImage(UIImage(named: "filtro-limpo"), for: .normal)
        self.fabButtonFilter.autoresizingMask = [.flexibleTopMargin, .flexibleRightMargin]
        self.fabButtonFilter.addTarget(self, action: #selector(self.buttonRemoveFilterPressed(_:)), for:.touchUpInside)
        self.fabButtonFilter.layer.borderWidth = 1.0
        self.fabButtonFilter.layer.borderColor = UIColor.purple.cgColor
        self.fabButtonFilter.layer.shadowRadius = 3
        self.fabButtonFilter.layer.shadowColor = UIColor.lightGray.cgColor
        self.fabButtonFilter.layer.shadowOpacity = 0.9
        self.fabButtonFilter.layer.shadowOffset = CGSize.zero
        self.fabButtonFilter.layer.zPosition = 1
        self.fabButtonFilter.isHidden = true
        self.fabButtonFilter.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addSubview(self.fabButtonFilter)
        
        self.fabButtonFilter.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 20).isActive = true
        self.fabButtonFilter.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -80).isActive = true
        self.fabButtonFilter.heightAnchor.constraint(equalToConstant: 50).isActive = true
        self.fabButtonFilter.widthAnchor.constraint(equalToConstant: 50).isActive = true
        
    }
    
    func greetingsTimer() {
        self.timer = Timer.scheduledTimer(timeInterval: 60, target: self, selector: #selector(self.greetingsMessages(_:)), userInfo: nil, repeats: true)
    }
        
    private func greetingsMessages() {
        let formatter = DateFormatter()
        let indexPath = IndexPath(row: 0, section: 0)
        
        if let cell = self.tableViewResultados.cellForRow(at: indexPath) as? ResultadosTableViewCell {
            formatter.dateFormat = "HH:mm:ss"
            
            let currentDateStr = formatter.string(from: Date())
            let currentTime = currentDateStr.components(separatedBy: ":")
            let currentTimeConverted = Int(currentTime[0])
            
            if currentTimeConverted! >= 0 && currentTimeConverted! <= 5 ||
                currentTimeConverted! >= 18 {
                cell.labelDayPeriodGreetings.text = "Boa noite"
            }
            else if currentTimeConverted! >= 6 && currentTimeConverted! < 12 {
                cell.labelDayPeriodGreetings.text = "Bom dia"
            }
            else if currentTimeConverted! > 12 && currentTimeConverted! < 18 {
                cell.labelDayPeriodGreetings.text = "Boa tarde"
            }
        }
    }
    
    private func gradientView() {
//        let gradient = CAGradientLayer()
//        gradient.frame = self.viewGradient.bounds
//        gradient.colors = [UIColor(red: 168/255, green: 72/255, blue: 217/255, alpha: 1.0).cgColor, UIColor(red: 105/255, green: 108/255, blue: 220/255, alpha: 1.0).cgColor]
//        gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
//        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
//        self.viewGradient.layer.addSublayer(gradient)
//        self.viewGradient.addSubview(self.viewSearch)
//        self.viewGradient.addSubview(self.buttonOpenMenu)
    }
    
    private func formatText(_ text: String, maxSize: Int) -> String {
        var msg = text
        
        if msg.count > maxSize {
            let range = msg.index(text.startIndex, offsetBy: 50)
            msg.removeSubrange(range..<msg.endIndex)
            return msg + "..." + " Ver Mais"
        }
        else {
            return text + " Ver Mais"
        }
    }
    
    func limitTextSizeBy(_ text: String, maxTextSize size: Int) -> NSMutableAttributedString {
        let formattedText = self.formatText(text, maxSize: size)

        let formattedString = NSMutableAttributedString(string: formattedText)

        let textAttribute = [
            NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue,
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16.0),
            NSAttributedString.Key.link: URL(string: "ios.app.aescare") ?? URL.self
        ] as [NSAttributedString.Key : Any]


        let textRange = (formattedText as NSString).range(of: "Ver Mais")
        formattedString.addAttributes(textAttribute, range: textRange)

        return formattedString
    }
    
    private func completeAccountData() {
        let alert = UIAlertController(title: "Primeiro Acesso", message: "Para utilizar o aplicativo de forma efetiva, é necessario que preencha todos os campos requisitados no seu perfil de usuario", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Ir para o Perfil", style: .default, handler: {
            _ in
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let perfilViewController = storyboard.instantiateViewController(identifier: "UserProfileEditVC") as! UserProfileEditVC
            perfilViewController.delegate = self
            perfilViewController.modalPresentationStyle = .formSheet
            perfilViewController.modalTransitionStyle = .crossDissolve
            perfilViewController.isModalInPresentation = true
            
            self.present(perfilViewController, animated: true, completion: nil)
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
}

// MARK: - TABLEVIEW DELEGATES IMPLEMENTATIONS
extension CentralHubViewController: UITableViewDelegate,
                                    UITableViewDataSource,
                                    UIGestureRecognizerDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
        else if section == 1 {
            if self.sectionTabIndex == 1 {
                return self.usersPostsMyPostsDataArray.count
            }
            else {
                return 1
            }
        }
        else if section == 2 {
            if isResultsSelected {
                if self.sectionTabIndex == 0 {
                    print("Number of Rows: \(self.resultsDataArray.count)")
                    if self.isFilteredDataSelected {
                        if self.filteredDataArray.isEmpty {
                            return 0
                        }
                        else {
                            return self.filteredDataArray.count
                        }
                    }
                    else {
                        return self.resultsDataArray.count
                    }
                }
//                else if self.sectionTabIndex == 1 {
//                    return self.medicsArticlesDataArray.count
//                }
                else if self.sectionTabIndex == 1 {
                    return self.usersPostsDataArray.count
                }
                else if self.sectionTabIndex == 2 {
                    return self.medicsPostsDataArray.count
                }
                else {
                    return 0
                }
            }
            else {
                tableView.separatorColor = .white
                return 0
            }
        }
        else {
            return 0
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = self.tableViewResultados.dequeueReusableCell(withIdentifier: "ResultadosTableViewCell", for: indexPath) as! ResultadosTableViewCell
            cell.backgroundColor = .white
            cell.delegate = self
            cell.imageViewUserIcon.sd_setImage(with: URL(string: usrerModelOBJ.user?.perfil_image ?? ""), completed: nil)
            cell.labelUserName.text = usrerModelOBJ.user?.username
            
            return cell
        }
        else if indexPath.section == 1 {
            if self.sectionTabIndex == 1 {
                if !self.usersPostsMyPostsDataArray.isEmpty {
                    let cell = self.tableViewResultados.dequeueReusableCell(withIdentifier: "UserPostsMyPostsTableViewCell") as! UserPostsMyPostsTableViewCell
                    
                    cell.labelPostMessage.text = self.usersPostsMyPostsDataArray[indexPath.row].body!
                    cell.labelPostDate.text = self.usersPostsMyPostsDataArray[indexPath.row].created_at_formatted!
                    cell.imageViewUserIcon.sd_setImage(with: URL(string: usrerModelOBJ.user!.url_image!), completed: nil)
                    return cell
                }
                else {
                    let cell = self.tableViewResultados.dequeueReusableCell(withIdentifier: "EmptyTableViewCell") as! EmptyTableViewCell
                    cell.labelMessage.text = "Você não tem posts"
                    return cell
                }
            }
            else {
                let cell = self.tableViewResultados.dequeueReusableCell(withIdentifier: "FilterTableViewCell", for: indexPath) as! FilterTableViewCell
                cell.backgroundColor = .white
                cell.delegate = self
                print("\(indexPath.section)")
                
                return cell
            }
        }
        else if indexPath.section == 2 {
            if self.sectionTabIndex == 0 {
                if !self.isFilteredDataSelected {
                    let cell = self.tableViewResultados.dequeueReusableCell(withIdentifier: "ResultsTableViewCell", for: indexPath) as! ResultsTableViewCell
                    cell.delegate = self
                    cell.index = indexPath.row
                    cell.backgroundColor = .white
                    cell.labelResultsTitle.text = self.resultsDataArray[indexPath.row].title!
                    cell.setTextToLabel(self.resultsDataArray[indexPath.row].city?.name ?? "", label: cell.labelLocation, imageName: "map-icon")
                    cell.setTextToLabel(self.resultsDataArray[indexPath.row].payment_type ?? "", label: cell.labelPaymentMethod, imageName: "parcelamento-icon")
                    cell.setTextToLabel(self.resultsDataArray[indexPath.row].treatment_value ?? "", label: cell.labelPaymentValue, imageName: "valor-icon")
                    cell.setTextToLabel(self.resultsDataArray[indexPath.row].medic?.name_with_title ?? "", label: cell.labelDoctorName, imageName: "medico-icon")
                    cell.starRating.rating = Double(self.resultsDataArray[indexPath.row].evaluation_result ?? 1)
                    
                    if !self.isResultsImagesHidden {
                        if let cachedImage = SDImageCache.shared.imageFromCache(forKey: self.resultsDataArray[indexPath.row].image_url!) {
                            cell.imageViewResultsUser.image = cachedImage
                        }
                        else {
                            self.imageManager.downloadImage(with: URL(string: self.resultsDataArray[indexPath.row].image_url!), options: .highPriority, progress: nil, completed: {
                                (image, error, cacheType, finished) in
                                
                                if finished {
                                    if let image = image {
                                        DispatchQueue.main.async {
                                            cell.imageViewResultsUser.image = image
                                            SDImageCache.shared.storeImage(toMemory: image, forKey: self.resultsDataArray[indexPath.row].image_url!)
                                        }
                                    }
                                    else {
                                        DispatchQueue.main.async {
                                            cell.imageViewResultsUser.image = UIImage(named: "illustration-chat")
                                        }
                                    }
                                }
                            })
                        }
                    }
                    else {
                        if let cachedImage = SDImageCache.shared.imageFromCache(forKey: self.resultsDataArray[indexPath.row].image_url!) {
                            cell.imageViewResultsUser.image = self.blurImage.transformedImage(with: cachedImage, forKey: self.resultsDataArray[indexPath.row].image_url!)
                        }
                        else {
                            self.imageManager.downloadImage(with: URL(string: self.resultsDataArray[indexPath.row].image_url!), options: .highPriority, progress: nil, completed: {
                                (image, error, cacheType, finished) in
                                
                                if finished {
                                    if let image = image {
                                        DispatchQueue.main.async {
                                            cell.imageViewResultsUser.image = self.blurImage.transformedImage(with: image, forKey: self.resultsDataArray[indexPath.row].image_url!)
                                            SDImageCache.shared.storeImage(toMemory: image, forKey: self.resultsDataArray[indexPath.row].image_url!)
                                        }
                                    }
                                    else {
                                        DispatchQueue.main.async {
                                            cell.imageViewResultsUser.image = UIImage(named: "illustration-chat")
                                        }
                                    }
                                }
                            })
                        }
                        //cell.imageViewResultsUser.image = self.hiddenImagePlaceHolder
                    }
                    
                    return cell
                }
                else {
                    let cell = self.tableViewResultados.dequeueReusableCell(withIdentifier: "ResultsTableViewCell", for: indexPath) as! ResultsTableViewCell
                    cell.delegate = self
                    cell.index = indexPath.row
                    cell.backgroundColor = .white
                    cell.labelResultsTitle.text = self.filteredDataArray[indexPath.row].title!
                    cell.setTextToLabel(self.filteredDataArray[indexPath.row].city?.name ?? "", label: cell.labelLocation, imageName: "map-icon")
                    cell.setTextToLabel(self.filteredDataArray[indexPath.row].payment_type ?? "", label: cell.labelPaymentMethod, imageName: "parcelamento-icon")
                    cell.setTextToLabel(self.filteredDataArray[indexPath.row].treatment_value ?? "", label: cell.labelPaymentValue, imageName: "valor-icon")
                    cell.setTextToLabel(self.filteredDataArray[indexPath.row].medic?.name_with_title ?? "", label: cell.labelDoctorName, imageName: "medico-icon")
                    
                    if !self.isResultsImagesHidden {
                        if let cachedImage = SDImageCache.shared.imageFromCache(forKey: self.filteredDataArray[indexPath.row].image_url!) {
                            cell.imageViewResultsUser.image = cachedImage
                        }
                        else {
                            self.imageManager.downloadImage(with: URL(string: self.filteredDataArray[indexPath.row].image_url!), options: .highPriority, progress: nil, completed: {
                                (image, error, cacheType, finished) in
                                
                                if finished {
                                    if let image = image {
                                        DispatchQueue.main.async {
                                            cell.imageViewResultsUser.image = image
                                            SDImageCache.shared.storeImage(toMemory: image, forKey: self.filteredDataArray[indexPath.row].image_url!)
                                        }
                                    }
                                    else {
                                        DispatchQueue.main.async {
                                            cell.imageViewResultsUser.image = UIImage(named: "illustration-chat")
                                        }
                                    }
                                }
                            })
                        }
                    }
                    else {
                        cell.imageViewResultsUser.image = self.hiddenImagePlaceHolder
                    }
                    
                    return cell
                }
            }
//            else if self.sectionTabIndex == 1 {
//                let cell = self.tableViewResultados.dequeueReusableCell(withIdentifier: "MedicsArticlesTableViewCell", for: indexPath) as! MedicsArticlesTableViewCell
//                cell.delegate = self
//                cell.index = indexPath.row
//                cell.backgroundColor = .white
//                cell.labelArticleTitle.text = self.medicsArticlesDataArray[indexPath.row].content?.title!
//                cell.labelArticleDescription.text = self.medicsArticlesDataArray[indexPath.row].content?.summary!
//                cell.labelMedicName.text = self.medicsArticlesDataArray[indexPath.row].medic?.name_with_title!
//                cell.imageViewMedicIcon.sd_setImage(with: URL(string: (self.medicsArticlesDataArray[indexPath.row].medic?.image_url!)!), completed: nil)
//                print("\(indexPath.section)")
//
//                return cell
//            }
            else if self.sectionTabIndex == 1 {
                let cell = self.tableViewResultados.dequeueReusableCell(withIdentifier: "UserPostsTableViewCell", for: indexPath) as! UserPostsTableViewCell
                cell.delegate = self
                cell.index = indexPath.row
                cell.backgroundColor = .white
                //cell.textViewUserPost.text = self.usersPostsDataArray[indexPath.row].body!
                cell.textViewUserPost.attributedText = self.limitTextSizeBy(self.usersPostsDataArray[indexPath.row].body!, maxTextSize: 50)
                cell.labelUserName.text = self.usersPostsDataArray[indexPath.row].user?.username!
                cell.labelUserPostDate.text = "Em \(self.usersPostsDataArray[indexPath.row].created_at_formatted!)"
                cell.imageViewLoggedUserIcon.sd_setImage(with: URL(string: (usrerModelOBJ.user?.perfil_image!)!), completed: nil)
                
                if self.usersPostsDataArray[indexPath.row].medias!.isEmpty {
                    cell.hideImage()
                }
                else {
                    cell.showImage()
                    cell.imageViewPostImage.sd_setImage(with: URL(string: self.usersPostsDataArray[indexPath.row].medias![0].cover_url!), completed: nil)
                }
                
                if let medic = self.usersPostsDataArray[indexPath.row].medic {
                    cell.labelMedicPost.isHidden = false
                    cell.imageViewUserIcon.sd_setImage(with: URL(string: medic.cover_media!.image_url!), completed: nil)
                }
                else {
                    cell.labelMedicPost.isHidden = false
                    cell.labelMedicPost.text = "Post Usuario"
                    cell.imageViewUserIcon.sd_setImage(with: URL(string: self.usersPostsDataArray[indexPath.row].user!.image_url!), completed: nil)
                }
                
                print("\(indexPath.section)")
                
                return cell
            }
            else if self.sectionTabIndex == 2 {
                let cell = self.tableViewResultados.dequeueReusableCell(withIdentifier: "MedicPostsTableViewCell", for: indexPath) as! MedicPostsTableViewCell
                cell.delegate = self
                cell.index = indexPath.row
                cell.backgroundColor = .white
                cell.labelPostTitle.text = self.medicsPostsDataArray[indexPath.row].content?.title!
                cell.labelPostDescription.text = self.medicsPostsDataArray[indexPath.row].content?.summary
                cell.imageViewDoctorInfo.sd_setImage(with: URL(string: (self.medicsPostsDataArray[indexPath.row].content?.cover_image_url ?? "")!), completed: { (image, error, cache, url) in
                    if error != nil {
                        cell.imageViewDoctorIcon.image = UIImage(named: "illustration-icon")
                    }
                })
                
                if let _ = self.medicsPostsDataArray[indexPath.row].medic?.image_url {
                    cell.imageViewDoctorIcon.sd_setImage(with: URL(string: (self.medicsPostsDataArray[indexPath.row].medic?.image_url!)!), completed: nil)
                }
                else {
                    cell.imageViewDoctorIcon.image = UIImage(named: "logo-aescare")
                }
                
                cell.labelPostDate.text = self.medicsPostsDataArray[indexPath.row].created_at_formatted!
                cell.labelMedicName.text = self.medicsPostsDataArray[indexPath.row].medic?.name_with_title!
                
                return cell
            }
            else {
                return UITableViewCell()
            }
        }
        else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            UIView.animate(withDuration: 0.3, animations: {
                tableView.cellForRow(at: indexPath)?.backgroundColor = UIColor(red: 255/255, green: 236/255, blue: 255/255, alpha: 1.0)
            })
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            tableView.cellForRow(at: indexPath)?.backgroundColor = .white
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            if self.isPublishSelected {
                return tableView.rowHeight
            }
            else {
                return 10
            }
        }
        else if indexPath.section == 1 {
            if self.isFilterSelected {
                return tableView.rowHeight
            }
            else {
//                if self.sectionTabIndex == 2 {
//                    return 0
//                }
//                else {
//                    return 30
//                }
                return 0
            }
        }
        else if indexPath.section == 2 {
            if self.isResultsSelected {
                if self.sectionTabIndex == 0 {
                    return tableView.rowHeight
                }
//                else if self.sectionTabIndex == 1 {
//                    return tableView.rowHeight
//                }
                else if self.sectionTabIndex == 1 {
                    return tableView.rowHeight
                }
                else if self.sectionTabIndex == 2 {
                    return tableView.rowHeight
                }
                else {
                    return 30
                }
            }
            else {
                return 30
            }
        }
        else {
            return 0
        }
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let viewHeader = UITableViewHeaderFooterView()
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.tappedHeader(_:)))
        // 228, 233, 242
        gesture.delegate = self
        viewHeader.textLabel?.textColor = .black
        viewHeader.contentView.backgroundColor = UIColor(red: 228/255, green: 233/255, blue: 242/255, alpha: 1.0)
        viewHeader.tag = section
        viewHeader.addGestureRecognizer(gesture)
        
        return viewHeader
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if self.isPublishSelected {
            let viewFooter = UITableViewHeaderFooterView()
            viewFooter.textLabel?.textColor = .purple
            viewFooter.contentView.backgroundColor = .white
            
            return viewFooter
        }
        else {
            let viewFooter = UITableViewHeaderFooterView()
            viewFooter.contentView.backgroundColor = .white
            
            return viewFooter
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
        guard let footer = view as? UITableViewHeaderFooterView else { return }
        footer.textLabel?.textColor = UIColor.red
        footer.textLabel?.font = UIFont.boldSystemFont(ofSize: 10)
        footer.textLabel?.frame = footer.frame
        footer.textLabel?.textAlignment = .left
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if self.sectionTabIndex == 0 {
            switch section {
            case 0:
                return "Criar Publicação"
            case 1:
                return "Filtrar"
            case 2:
                return "Resultados"
            default:
                return nil
            }
        }
//        if self.sectionTabIndex == 1 {
//            switch section {
//            case 0:
//                return "Criar Publicação"
//            case 1:
//                return "Filtrar"
//            case 2:
//                return "Artigos Médicos"
//            default:
//                return nil
//            }
//        }
        if self.sectionTabIndex == 1 {
            switch section {
            case 0:
                return "Criar Publicação"
            case 1:
                return "Minhas Postagens"
            case 2:
                return "Posts de Usuarias"
            default:
                return nil
            }
        }
        if self.sectionTabIndex == 2 {
            switch section {
            case 0:
                return "Criar Publicação"
            case 1:
                return "Filtrar"
            case 2:
                return "Posts Médicos"
            default:
                return nil
            }
        }
        else {
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        if self.sectionTabIndex == 0 {
            switch section {
            case 0:
                if self.isPublishSelected {
                    return "Sua publicação estará na aba Posts"
                }
                else {
                    return nil
                }
            default:
                return nil
            }
        }
//        if self.sectionTabIndex == 1 {
//            switch section {
//            case 0:
//                if self.isPublishSelected {
//                    return "Sua publicação estará na aba Posts de Usuarias"
//                }
//                else {
//                    return nil
//                }
//            default:
//                return nil
//            }
//        }
        if self.sectionTabIndex == 2 {
            switch section {
            case 0:
                if self.isPublishSelected {
                    return "Sua publicação estará na aba Posts de Usuarias"
                }
                else {
                    return nil
                }
            default:
                return nil
            }
        }
        else {
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.section == 2 {
            if self.sectionTabIndex == 1 {
                cell.layer.borderWidth = 8
                cell.layer.cornerRadius = 10
                cell.layer.borderColor = UIColor(red: 232/255, green: 233/255, blue: 235/255, alpha: 1.0).cgColor
            }
            else {
                tableView.separatorStyle = .singleLine
                tableView.separatorColor = UIColor(red: 175/255, green: 68/255, blue: 217/255, alpha: 1.0)
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        let currentOffset = scrollView.contentOffset.y
//        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
//        let dispatchGroup = DispatchGroup()
//
//        self.indexTracker = currentOffset
//
//        if maximumOffset - currentOffset <= 10.0 {
//            if self.sectionTabIndex == 0 && !self.isResultsDataLoading {
//                self.isResultsDataLoading = true
//                dispatchGroup.enter()
//                DispatchQueue.global(qos: .utility).async {
//                    self.requestResults(self.resultsDataPagination, completion: {
//                        result in
//                        if result {
//                            print("Results request completed")
//                            self.isResultsDataLoading = false
//                            self.isResultsDataAvaliable = true
//                            dispatchGroup.leave()
//                        }
//                        else {
//                            self.isResultsDataAvaliable = false
//                            dispatchGroup.leave()
//                        }
//                    })
//
//                    dispatchGroup.notify(queue: .main, execute: {
//                        self.resultsDataPagination += 1
//
//                        DispatchQueue.main.async {
//                            UIView.transition(with: self.tableViewResultados, duration: 1.2, options: .transitionCrossDissolve,
//                                animations: {
//                                    self.tableViewResultados.reloadData()
//                            }, completion: nil)
//                        }
//                    })
//                }
//            }
//            else if self.sectionTabIndex == 2 && !self.isUsersPostsDataLoading {
//                self.isUsersPostsDataLoading = true
//                dispatchGroup.enter()
//                DispatchQueue.global(qos: .utility).async {
//                    self.requestUsersPosts(self.usersPostsDataPagination, completion: {
//                        result in
//                        if result {
//                            print("Users Posts request completed")
//                            self.isUsersPostsDataLoading = false
//                            self.isUsersPostsDataAvaliable = true
//                            dispatchGroup.leave()
//                        }
//                        else {
//                            self.isUsersPostsDataAvaliable = false
//                            dispatchGroup.leave()
//                        }
//                    })
//
//                    dispatchGroup.notify(queue: .main, execute: {
//                            self.usersPostsDataPagination += 1
//
//                            if self.isUsersPostsDataAvaliable && !self.isUsersPostsDataLoading {
//                                UIView.transition(with: self.tableViewResultados, duration: 0.5, options: .transitionCrossDissolve,
//                                    animations: {self.tableViewResultados.reloadData()
//                                }, completion: nil)
//                            }
//
//                    })
//                }
//            }
//            else if self.sectionTabIndex == 3 && !self.isMedicsPostsDataLoading {
//                self.isMedicsPostsDataLoading = true
//                dispatchGroup.enter()
//                DispatchQueue.global(qos: .utility).async {
//                    self.requestMedicsPosts(self.medicsPostsDataPagination, completion: {
//                        result in
//                        if result {
//                            print("Medics Posts request completed")
//                            self.isMedicsPostsDataLoading = false
//                            self.isMedicsPostsDataAvaliable = true
//                            dispatchGroup.leave()
//                        }
//                        else {
//                            self.isMedicsPostsDataAvaliable = false
//                            dispatchGroup.leave()
//                        }
//                    })
//
//                    dispatchGroup.notify(queue: .main, execute: {
//                        self.medicsPostsDataPagination += 1
//                        UIView.transition(with: self.tableViewResultados, duration: 0.5, options: .transitionCrossDissolve,
//                            animations: {
//                                self.tableViewResultados.reloadData()
//                        }, completion: nil)
//                    })
//                }
//            }
//        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        let dispatchGroup = DispatchGroup()
        
        self.indexTracker = currentOffset
        
        if maximumOffset - currentOffset <= 10.0 {
            if self.sectionTabIndex == 0 && !self.isResultsDataLoading && !self.isFilteredDataSelected {
                self.isResultsDataLoading = true
                dispatchGroup.enter()
                DispatchQueue.global(qos: .utility).async {
                    self.requestResults(self.resultsDataPagination, completion: {
                        result in
                        if result {
                            print("Results request completed")
                            self.isResultsDataLoading = false
                            self.isResultsDataAvaliable = true
                            dispatchGroup.leave()
                        }
                        else {
                            self.isResultsDataAvaliable = false
                            dispatchGroup.leave()
                        }
                    })
                                        
                    dispatchGroup.notify(queue: .main, execute: {
                        self.resultsDataPagination += 1
                        
                        DispatchQueue.main.async {
                            UIView.transition(with: self.tableViewResultados, duration: 1.2, options: .transitionCrossDissolve,
                                animations: {
                                    self.tableViewResultados.reloadData()
                            }, completion: nil)
                        }
                    })
                }
            }
            if self.sectionTabIndex == 0 && !self.isFilteredDataLoading && self.isFilteredDataSelected && !self.isFilteredRequestEmpty {
                self.isFilteredDataLoading = true
                dispatchGroup.enter()
                DispatchQueue.global(qos: .utility).async {
                    self.requestFilteredResults(self.filteredDataPagination, filter: self.filteredParameters, completion: {
                        result in
                        guard let result = result else {
                            self.isFilteredDataAvaliable = false
                            self.isFilteredDataLoading = false
                            dispatchGroup.leave()
                            return
                        }
                        
                        self.isFilteredDataLoading = false
                        self.isFilteredDataAvaliable = true
                        self.filteredDataArray.append(contentsOf: result.results!)
                        dispatchGroup.leave()
                    })
                                        
                    dispatchGroup.notify(queue: .main, execute: {
                        self.filteredDataPagination += 1
                        
                        DispatchQueue.main.async {
                            UIView.transition(with: self.tableViewResultados, duration: 1.2, options: .transitionCrossDissolve,
                                animations: {
                                    self.tableViewResultados.reloadData()
                            }, completion: nil)
                        }
                    })
                }
            }
//            else if self.sectionTabIndex == 1 && !self.isMedicsArticlesDataLoading {
//                self.isMedicsArticlesDataLoading = true
//                dispatchGroup.enter()
//                DispatchQueue.global(qos: .utility).async {
//                    self.requestMedicsArticles(self.medicsArticlesDataPagination, completion: {
//                        result in
//                        if result {
//                            print("Results request completed")
//                            self.isMedicsArticlesDataLoading = false
//                            self.isMedicsArticlesDataAvaliable = true
//                            dispatchGroup.leave()
//                        }
//                        else {
//                            self.isMedicsArticlesDataAvaliable = false
//                            dispatchGroup.leave()
//                        }
//                    })
//
//                    dispatchGroup.notify(queue: .main, execute: {
//                        self.medicsArticlesDataPagination += 1
//
//                        DispatchQueue.main.async {
//                            UIView.transition(with: self.tableViewResultados, duration: 1.2, options: .transitionCrossDissolve,
//                                animations: {
//                                    self.tableViewResultados.reloadData()
//                            }, completion: nil)
//                        }
//                    })
//                }
//            }
            else if self.sectionTabIndex == 1 && !self.isUsersPostsDataLoading {
                self.isUsersPostsDataLoading = true
                dispatchGroup.enter()
                DispatchQueue.global(qos: .utility).async {
                    self.requestUsersPosts(self.usersPostsDataPagination, completion: {
                        result in
                        if result {
                            print("Users Posts request completed")
                            self.isUsersPostsDataLoading = false
                            self.isUsersPostsDataAvaliable = true
                            dispatchGroup.leave()
                        }
                        else {
                            self.isUsersPostsDataAvaliable = false
                            dispatchGroup.leave()
                        }
                    })
                                        
                    dispatchGroup.notify(queue: .main, execute: {
                            self.usersPostsDataPagination += 1
                            
                            if self.isUsersPostsDataAvaliable && !self.isUsersPostsDataLoading {
                                UIView.transition(with: self.tableViewResultados, duration: 0.5, options: .transitionCrossDissolve,
                                    animations: {self.tableViewResultados.reloadData()
                                }, completion: nil)
                            }
                        
                    })
                }
            }
            else if self.sectionTabIndex == 2 && !self.isMedicsPostsDataLoading {
                self.isMedicsPostsDataLoading = true
                dispatchGroup.enter()
                DispatchQueue.global(qos: .utility).async {
                    self.requestMedicsPosts(self.medicsPostsDataPagination, completion: {
                        result in
                        if result {
                            print("Medics Posts request completed")
                            self.isMedicsPostsDataLoading = false
                            self.isMedicsPostsDataAvaliable = true
                            dispatchGroup.leave()
                        }
                        else {
                            self.isMedicsPostsDataAvaliable = false
                            dispatchGroup.leave()
                        }
                    })
                                        
                    dispatchGroup.notify(queue: .main, execute: {
                        self.medicsPostsDataPagination += 1
                        UIView.transition(with: self.tableViewResultados, duration: 0.5, options: .transitionCrossDissolve,
                            animations: {
                                self.tableViewResultados.reloadData()
                        }, completion: nil)
                    })
                }
            }
        }
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if self.isFilteredDataSelected {
            self.fabButtonFilter.isHidden = true
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if self.isFilteredDataSelected {
            self.fabButtonFilter.isHidden = false
        }
    }
}

// MARK: - CELLS DELEGATES
extension CentralHubViewController: FilterTableViewCellDelegate,
                                    ResultadosTableViewCellDelegate,
                                    ResultsTableViewDelegate,
                                    MedicsArticlesTableViewDelegate,
                                    UserPostsTableViewCellDelegate,
                                    MedicPostsTableViewCellDelegate,
                                    UserProfileEditVCDelegate {
    func didPressPublishButton(toSend message: String) {
        let index = IndexPath(row: 0, section: 0)
        let cell = self.tableViewResultados.cellForRow(at: index) as! ResultadosTableViewCell
        
        cell.stackViewActivity.isHidden = false
        cell.activityPublish.startAnimating()
        cell.buttonPublish.isEnabled = false
        
        self.publishUserPost(message, response: {
            result in
            if result {
                DispatchQueue.main.asyncAfter(deadline: .now() + 3, execute: {
                    print("Publish completed")
                    cell.activityPublish.stopAnimating()
                    cell.stackViewActivity.isHidden = true
                    cell.buttonPublish.isEnabled = true
                })
            }
            else {
                DispatchQueue.main.asyncAfter(deadline: .now() + 3, execute: {
                    print("Error publishing message")
                    cell.activityPublish.stopAnimating()
                    cell.stackViewActivity.isHidden = true
                    cell.buttonPublish.isEnabled = true
                })
            }
        })
    }
    
    func didPressFilterButton(filtered list: [String:AnyObject?]) {
        var requestData = [String:Any?]()
        let indexPath = IndexPath(row: 0, section: 1)
        let cell = self.tableViewResultados.cellForRow(at: indexPath) as! FilterTableViewCell
        let medicData = list["medic"] as? MedicName
        let cityData = list["city"] as? CityName
        let specData = list["specialization"] as? [String:Int]
        let procedureData = list["procedure"] as? ProcedureName
        
        requestData["medic"] = medicData?.medic_id as Any
        requestData["city"] = cityData?.city_id as Any
        requestData["specialization"] = specData as Any
        requestData["procedure"] = procedureData?.treatment_id as Any
        
        if medicData == nil && cityData == nil && specData == nil && procedureData == nil {
            return
        }
        
        self.filteredDataArray.removeAll()
        self.filteredDataPagination = 1
        self.isFilteredDataSelected = true
        
        cell.stackViewFilteriActivity.isHidden = false
        cell.activityFilter.startAnimating()
        
        self.requestFilteredResults(self.filteredDataPagination, filter: requestData, completion: {
            data in
            guard let data = data else {
                cell.activityFilter.stopAnimating()
                cell.stackViewFilteriActivity.isHidden = true
                
                DispatchQueue.main.async {
                    self.tableViewResultados.reloadData()
                }
                
                return
            }
            
            self.filteredDataPagination += 1
            self.filteredParameters = requestData
            self.filteredDataArray.append(contentsOf: data.results!)
            
            DispatchQueue.main.async {
                cell.activityFilter.stopAnimating()
                cell.stackViewFilteriActivity.isHidden = true
                self.fabButtonFilter.isHidden = false
                self.tableViewResultados.reloadData()
            }
        })
    }
    
    func didPressAttachButton() {
        let picker = UIImagePickerController()
        picker.allowsEditing = true
        picker.delegate = self
        present(picker, animated: true)
    }
    
    func didPressResultsSeeMore(selected index: Int) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ResultsDetailsViewController") as! ResultsDetailsViewController
        vc.isModalInPresentation = true
        
        if self.isFilteredDataSelected {
            vc.resultsData = self.filteredDataArray[index]
        }
        else {
            vc.resultsData = self.resultsDataArray[index]
        }
        
        self.present(vc, animated: true, completion: nil)
    }
    
    func didPressMedicsArticlesReadMore(selected index: Int) {
        print(self.medicsArticlesDataArray[index].medic!.name_with_title!)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "MedicsArticlesDetailsViewController") as! MedicsArticlesDetailsViewController
        vc.isModalInPresentation = true
        vc.medicsArticlesData = self.medicsArticlesDataArray[index]
        self.present(vc, animated: true, completion: nil)
    }
    
    func didPressUserPostsReadMore(selected index: Int) {
        print(self.usersPostsDataArray[index].user!.username!)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "UsersPostsDetailsViewController") as! UsersPostsDetailsViewController
        vc.isModalInPresentation = true
        vc.postsData = self.usersPostsDataArray[index]
        self.usersPostsDataArray[index].medic != nil ? (vc.isMedic = true) : (vc.isMedic = false)
        self.present(vc, animated: true, completion: nil)
    }
    
    func didPressMedicPostsReadMore(selected index: Int) {
        //print(self.medicsPostsDataArray[index].medic!.name_with_title!)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "MedicsPostsDetailsViewController") as! MedicsPostsDetailsViewController
        vc.isModalInPresentation = true
        vc.medicPostsData = self.medicsPostsDataArray[index]
        self.present(vc, animated: true, completion: nil)
    }
    
    func didCompleteProfileEdit() {
        let alert = UIAlertController(title: "A AesCare lhe da Boas-Vindas", message: "Com seus dados completos, o aplicativo AesCare conseguirá fornecer as melhores informações com base no seu perfil.", preferredStyle: .actionSheet)
         
        alert.addAction(UIAlertAction(title: "Continuar", style: .default, handler: nil))
        
//        self.renewUserData(loginUserGmail, response: {
//            dataResponse in
//
//            if dataResponse {
//                self.startCentralHubData()
//                self.present(alert, animated: true, completion: nil)
//            }
//        })
        self.startCentralHubData()
        self.present(alert, animated: true, completion: nil)
    }
}

// MARK: - CLASS DELEGATES
extension CentralHubViewController: UIImagePickerControllerDelegate & UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let indexPath = IndexPath(row: 0, section: 0)
        let cell = self.tableViewResultados.cellForRow(at: indexPath) as! ResultadosTableViewCell

        if let image = info[.editedImage] as? UIImage {
            if cell.imageViewUser1.image == nil {
                cell.imageViewUser1.image = image
                cell.imageViewUser1.setBorder(width: 1, borderColor: .purple, cornerRadious: 5)
            }
            else if cell.imageViewUser2.image == nil {
                cell.imageViewUser2.image = image
                cell.imageViewUser2.setBorder(width: 1, borderColor: .purple, cornerRadious: 5)
            }
            else if cell.imageViewUser3.image == nil {
                cell.imageViewUser3.image = image
                cell.imageViewUser3.setBorder(width: 1, borderColor: .purple, cornerRadious: 5)
            }
        }
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
}

// MARK: - EVENT FUNCTIONS
extension CentralHubViewController {
    @objc func tappedHeader(_ gesture: UIGestureRecognizer) {
        print("Header Pressed")
        
        guard let section = gesture.view?.tag else {
            return
        }
        
        if section == 0 {
            self.isPublishSelected = !self.isPublishSelected
            self.tableViewResultados.reloadSections(IndexSet(integer: 0), with: .fade)
        }
        else if section == 1 {
            self.isFilterSelected = !self.isFilterSelected
            self.tableViewResultados.reloadSections(IndexSet(integer: 1), with: .fade)
        }
        else if section == 2 {
            self.isResultsSelected = !self.isResultsSelected
                        
            self.tableViewResultados.reloadSections(IndexSet(integer: 2), with: .fade)
            
            if self.isResultsSelected {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute: {
                    self.tableViewResultados.setContentOffset(CGPoint(x: 0, y: self.indexTracker), animated: false)
                })
            }
        }
    }
    
    @objc func buttonHideResultsImagesPressed(_ sender: UIButton) {
        self.isResultsImagesHidden = !self.isResultsImagesHidden
        
        self.fabButton.setImage(self.isResultsImagesHidden ? UIImage(named: "olho-fechado") : UIImage(named: "olho-aberto"), for: .normal)
        self.fabButton.backgroundColor = self.isResultsImagesHidden ? .gray : .white
        
        UIView.transition(with: self.tableViewResultados, duration: 0.5, options: .transitionCurlDown, animations: {
            self.tableViewResultados.reloadData()
        }, completion: nil)
    }
    
    @objc func buttonRemoveFilterPressed(_ sender: UIButton) {
        if let cell = (self.tableViewResultados.cellForRow(at: IndexPath(row: 0, section: 1)) as? FilterTableViewCell) {
            
            cell.textFieldProcedureFilter.text = ""
            cell.textFieldCityFilter.text = ""
            cell.textFieldMedicFilter.text = ""
            
            self.filteredDataArray.removeAll()
            self.isFilteredDataSelected = false
            self.fabButtonFilter.isHidden = true
            self.tableViewResultados.reloadData()
        }
        else {
            let cell = self.tableViewResultados.dataSource?.tableView(self.tableViewResultados, cellForRowAt: IndexPath(row: 0, section: 1)) as! FilterTableViewCell
            
            cell.textFieldProcedureFilter.text = ""
            cell.textFieldCityFilter.text = ""
            cell.textFieldMedicFilter.text = ""
            
            self.filteredDataArray.removeAll()
            self.isFilteredDataSelected = false
            self.fabButtonFilter.isHidden = true
            self.tableViewResultados.reloadData()
        }
        
    }
    
    @objc func greetingsMessages(_ timer: Timer) {
        let formatter = DateFormatter()
        let indexPath = IndexPath(row: 0, section: 0)
        
        if let cell = self.tableViewResultados.cellForRow(at: indexPath) as? ResultadosTableViewCell {
            formatter.dateFormat = "HH:mm:ss"
            
            print("Timer Running!!!")
            let currentDateStr = formatter.string(from: Date())
            let currentTime = currentDateStr.components(separatedBy: ":")
            let currentTimeConverted = Int(currentTime[0])
            
            if currentTimeConverted! >= 0 && currentTimeConverted! <= 5 ||
                currentTimeConverted! >= 18 {
                cell.labelDayPeriodGreetings.text = "Boa noite"
            }
            else if currentTimeConverted! >= 6 && currentTimeConverted! < 12 {
                cell.labelDayPeriodGreetings.text = "Bom dia"
            }
            else if currentTimeConverted! > 12 && currentTimeConverted! < 18 {
                cell.labelDayPeriodGreetings.text = "Boa tarde"
            }
        }
    }
    
    @objc func buttonOpenSideMenuPressed(_ sender: UIButton) {
        appDel.topViewControllerWithRootViewController(rootViewController: UIApplication.shared.windows.first?.rootViewController)?.view.endEditing(true)
        
        SideMenuManager.default.leftMenuNavigationController?.settings.presentationStyle = .menuSlideIn
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let menu = storyboard.instantiateViewController(withIdentifier: "SideMenuNavigationController") as! SideMenuNavigationController
        menu.leftSide = true
        
        UIApplication.shared.windows.first?.rootViewController?.present(menu, animated: true, completion: nil)
    }
    
    @IBAction func segmentValueChanged(_ sender: RESegmentedControl) {
        print("selected segment index: \(sender.selectedSegmentIndex)")
        
        self.sectionTabIndex = sender.selectedSegmentIndex
        
        if sectionTabIndex == 0 && self.isPageLoaded {
            self.fabButton.isHidden = false
        }
        else if sectionTabIndex != 0 {
            self.fabButton.isHidden = true
        }
        
        UIView.transition(with: self.tableViewResultados, duration: 1.2, options: .transitionCrossDissolve, animations: {
            self.tableViewResultados.reloadData()
        }, completion: nil)
    }
}








