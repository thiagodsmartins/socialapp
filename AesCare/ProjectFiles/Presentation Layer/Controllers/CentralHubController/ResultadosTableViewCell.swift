//
//  ResultadosTableViewCell.swift
//  AesCare
//
//  Created by Thiago on 14/03/21.
//

import UIKit
import SRActivityIndicator

protocol ResultadosTableViewCellDelegate: class {
    func didPressPublishButton(toSend message: String)
    func didPressAttachButton()
}

class ResultadosTableViewCell: UITableViewCell {
    @IBOutlet weak var imageViewUserIcon: UIImageView!
    @IBOutlet weak var textViewMessage: UITextView!
    @IBOutlet weak var buttonPublish: UIButton!
    @IBOutlet weak var stackViewActivity: UIStackView!
    @IBOutlet weak var activityPublish: UIActivityIndicatorView!
    @IBOutlet weak var labelUserName: UILabel!
    @IBOutlet weak var labelDayPeriodGreetings: UILabel!
    @IBOutlet weak var buttonAttachImage: UIButton!
    @IBOutlet weak var stackViewUserImages: UIStackView!
    @IBOutlet weak var imageViewUser1: UIImageView!
    @IBOutlet weak var imageViewUser2: UIImageView!
    @IBOutlet weak var imageViewUser3: UIImageView!
    
    weak var delegate: ResultadosTableViewCellDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setupViews()
        
    }
        
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    private func setupViews() {
        self.selectionStyle = .none
        
        self.textViewMessage.text! = "Faça uma publicação"
        self.textViewMessage.textColor = .lightGray
        self.textViewMessage.setBorder(width: 1, borderColor: UIColor(red: 229/255, green: 229/255, blue: 229/255, alpha: 1.0), cornerRadious: 10)
        self.textViewMessage.backgroundColor = UIColor(red: 229/255, green: 229/255, blue: 229/255, alpha: 1.0)
        self.textViewMessage.delegate = self
        
        self.imageViewUserIcon.setBorder(width: 3, borderColor: UIColor(red: 175/255, green: 68/255, blue: 217/255, alpha: 1.0), cornerRadious: 10)
        self.imageViewUserIcon.layer.cornerRadius = self.imageViewUserIcon.frame.height / 2
        self.imageViewUserIcon.clipsToBounds = true
        
        self.buttonPublish.backgroundColor = .lightGray
        self.buttonPublish.tintColor = .white
        self.buttonPublish.layer.cornerRadius = 10
        self.buttonPublish.isEnabled = false
        self.buttonPublish.addTarget(self, action: #selector(self.buttonPublishPressed(_:)), for: .touchUpInside)
        
        self.buttonAttachImage.layer.borderWidth = 1.0
        self.buttonAttachImage.layer.borderColor = UIColor(red: 175/255, green: 68/255, blue: 217/255, alpha: 1.0).cgColor
        self.buttonAttachImage.layer.cornerRadius = 10
        self.buttonAttachImage.addTarget(self, action: #selector(self.buttonAttachPressed(_:)), for: .touchUpInside)
        
        self.stackViewActivity.isHidden = true
    }
    
}

extension ResultadosTableViewCell {
    @objc func buttonPublishPressed(_ sender: UIButton) {
        self.delegate.didPressPublishButton(toSend: self.textViewMessage.text!)
    }
    
    @objc func buttonAttachPressed(_ sender: UIButton) {
        self.delegate.didPressAttachButton()
    }
}

extension ResultadosTableViewCell: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        self.textViewMessage.setBorder(width: 2, borderColor: UIColor(red: 255/255, green: 105/255, blue: 180/255, alpha: 1.0), cornerRadious: 10)
        
        textView.backgroundColor = .white
        if textView.textColor == UIColor.lightGray {
            textView.text = ""
            textView.textColor = .black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        self.textViewMessage.setBorder(width: 1, borderColor: UIColor(red: 229/255, green: 229/255, blue: 229/255, alpha: 1.0), cornerRadious: 10)
        
        if textView.text!.isEmpty {
            textView.textColor = .lightGray
            textView.text! = "Faça uma publicação"
            textView.backgroundColor = UIColor(red: 229/255, green: 229/255, blue: 229/255, alpha: 1.0)
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if textView.text.count > 0 {
            self.buttonPublish.isEnabled = true
            self.buttonPublish.backgroundColor = UIColor(red: 175/255, green: 68/255, blue: 217/255, alpha: 1.0)
        }
        else {
            self.buttonPublish.isEnabled = false
            self.buttonPublish.backgroundColor = .lightGray
        }
    }
}
