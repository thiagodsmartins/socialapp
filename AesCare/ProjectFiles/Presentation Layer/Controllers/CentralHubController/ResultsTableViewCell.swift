//
//  ResultsTableViewCell.swift
//  AesCare
//
//  Created by Thiago on 15/03/21.
//

import UIKit
import Cosmos

protocol ResultsTableViewDelegate: class {
    func didPressResultsSeeMore(selected index: Int)
}

class ResultsTableViewCell: UITableViewCell {
    @IBOutlet weak var labelResultsTitle: UILabel!
    @IBOutlet weak var imageViewResultsUser: UIImageView!
    @IBOutlet weak var buttonSeeMore: UIButton!
    @IBOutlet weak var labelLocation: UILabel!
    @IBOutlet weak var labelPaymentMethod: UILabel!
    @IBOutlet weak var labelPaymentValue: UILabel!
    @IBOutlet weak var labelDoctorName: UILabel!
    @IBOutlet weak var starRating: CosmosView!
    
    var index = 0
    weak var delegate: ResultsTableViewDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setupViewsConfiguration()
        self.setupStarRating()
        
        self.setTextToLabel("Localizacao", label: self.labelLocation, imageName: "map-icon")
        self.setTextToLabel("Pagamento", label: self.labelPaymentMethod, imageName: "parcelamento-icon")
        self.setTextToLabel("Valor", label: self.labelPaymentValue, imageName: "valor-icon")
        self.setTextToLabel("Medico", label: self.labelDoctorName, imageName: "medico-icon")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.imageViewResultsUser.image = nil
    }
    
    private func setupViewsConfiguration() {
        self.selectionStyle = .none
        
        self.imageViewResultsUser.setBorder(width: 2, borderColor: UIColor(red: 175/255, green: 68/255, blue: 217/255, alpha: 1.0), cornerRadious: 10)
        self.imageViewResultsUser.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleBottomMargin, .flexibleRightMargin, .flexibleLeftMargin, .flexibleTopMargin]
        self.imageViewResultsUser.contentMode = .scaleAspectFill
        self.imageViewResultsUser.clipsToBounds = true
        
        //175, 68, 217
        self.buttonSeeMore.backgroundColor = UIColor(red: 175/255, green: 68/255, blue: 217/255, alpha: 1.0)
        self.buttonSeeMore.layer.cornerRadius = 10
        self.buttonSeeMore.tintColor = .white
        self.buttonSeeMore.addTarget(self, action: #selector(self.buttonSeeMorePressed(_:)), for: .touchUpInside)
    }
    
    private func setupStarRating() {
        self.starRating.rating = 5
        self.starRating.settings.filledColor = UIColor(red: 175/255, green: 68/255, blue: 217/255, alpha: 1.0)
        self.starRating.settings.updateOnTouch = false
    }
    
    func setTextToLabel(_ text: String, label: UILabel, imageName: String) {
        let imageAttachment = NSTextAttachment()
        imageAttachment.image = UIImage(named: imageName)
        let imageOffsetY: CGFloat = -3.0
        imageAttachment.bounds = CGRect(x: 0, y: imageOffsetY, width: imageAttachment.image!.size.width, height: imageAttachment.image!.size.height)
        let attachmentString = NSAttributedString(attachment: imageAttachment)
        let completeText = NSMutableAttributedString(string: "")
        completeText.append(attachmentString)
        let textAfterIcon = NSAttributedString(string: text)
        completeText.append(textAfterIcon)
        label.textAlignment = .left
        label.numberOfLines = 0
        //label.adjustsFontSizeToFitWidth = true
        //label.sizeToFit()
        label.attributedText = completeText
    }

}

extension ResultsTableViewCell {
    @objc func buttonSeeMorePressed(_ sender: UIButton) {
        self.delegate.didPressResultsSeeMore(selected: index)
    }
}
