//
//  FilterTableViewCell.swift
//  AesCare
//
//  Created by Thiago on 14/03/21.
//

import UIKit
import SearchTextField
import Alamofire
import SwiftCheckboxDialog

protocol FilterTableViewCellDelegate: class {
    func didPressFilterButton(filtered list: [String:AnyObject?])
}

class FilterTableViewCell: UITableViewCell {
    @IBOutlet weak var textFieldMedicFilter: SearchTextField!
    @IBOutlet weak var textFieldCityFilter: SearchTextField!
    @IBOutlet weak var buttonApplyFilter: UIButton!
    @IBOutlet weak var buttonSpecialization: UIButton!
    @IBOutlet weak var stackViewFilteriActivity: UIStackView!
    @IBOutlet weak var labelActivityFilter: UILabel!
    @IBOutlet weak var activityFilter: UIActivityIndicatorView!
    @IBOutlet weak var textFieldProcedureFilter: SearchTextField!
    
    private var checkBoxDialog: CheckboxDialogViewController!
    private lazy var selectedSpecializations: [(name: String, translated: String)] = []
    private lazy var filteredData = [String:[Any]]()
    private lazy var filteredList = [String:AnyObject]()
    
    weak var delegate: FilterTableViewCellDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setupViewsConfiguration()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    private func setupViewsConfiguration() {
        self.selectionStyle = .none
        
        self.textFieldMedicFilter.theme.fontColor = .purple
        self.textFieldMedicFilter.maxNumberOfResults = 5
        self.textFieldMedicFilter.minCharactersNumberToStartFiltering = 3
        self.textFieldMedicFilter.placeholder = "Filtrar Médicos"
        self.textFieldMedicFilter.delegate = self
        self.textFieldMedicFilter.setBorder(width: 2, borderColor: .purple, cornerRadious: 10)
        
        self.textFieldCityFilter.theme.fontColor = .purple
        self.textFieldCityFilter.maxNumberOfResults = 5
        self.textFieldCityFilter.minCharactersNumberToStartFiltering = 3
        self.textFieldCityFilter.placeholder = "Filtrar Cidade"
        self.textFieldCityFilter.delegate = self
        self.textFieldCityFilter.setBorder(width: 2, borderColor: .purple, cornerRadious: 10)
        
        self.textFieldProcedureFilter.theme.fontColor = .purple
        self.textFieldProcedureFilter.maxNumberOfResults = 5
        self.textFieldProcedureFilter.minCharactersNumberToStartFiltering = 3
        self.textFieldProcedureFilter.placeholder = "Filtrar Procedimento"
        self.textFieldProcedureFilter.delegate = self
        self.textFieldProcedureFilter.setBorder(width: 2, borderColor: .purple, cornerRadious: 10)
        
        self.textFieldMedicFilter.itemSelectionHandler = {
            (item, itemPosition) in
            self.textFieldMedicFilter.text! = item[itemPosition].title
            
            for medic in (self.filteredData["medic"] as! [MedicName]) {
                if medic.name! == item[itemPosition].title {
                    print("Medic found: \(medic.name!)")
                    self.filteredList["medic"] = medic as AnyObject
                    break
                }
            }
        }
        
        self.textFieldCityFilter.itemSelectionHandler = {
            (item, itemPosition) in

            if let data = self.extractTextData(item[itemPosition].title) {
                self.textFieldCityFilter.text! = data["city"]!
                
                for city in (self.filteredData["city"] as! [CityName]) {
                    if city.name! == data["city"] && city.state_abbr! == data["state"] {
                        print("Data found \(city.name!) - \(city.state_abbr!)")
                        self.filteredList["city"] = city as AnyObject
                        break
                    }
                }
            }
        }
        
        self.textFieldProcedureFilter.itemSelectionHandler = {
            (item, itemPosition) in

            self.textFieldProcedureFilter.text! = item[itemPosition].title
            
            if !self.textFieldProcedureFilter.text!.isEmpty {
                for data in (self.filteredData["procedure"] as! [ProcedureName]) {
                    if self.textFieldProcedureFilter.text == data.name! {
                        self.filteredList["procedure"] = data as AnyObject
                        break
                    }
                }
            }
        }
        
        self.textFieldMedicFilter.userStoppedTypingHandler = {
            if let criteria = self.textFieldMedicFilter.text {
                if criteria.count >= 3 {
                    self.textFieldMedicFilter.showLoadingIndicator()
                    
                    self.requestMedicData(self.textFieldMedicFilter.text!, completion: {
                        data in
                        if let data = data {
                            var names = [String]()
                            
                            for medics in data {
                                names.append(medics.name!)
                            }
                            
                            self.filteredData["medic"] = data
                            self.textFieldMedicFilter.filterStrings(names)
                            self.textFieldMedicFilter.stopLoadingIndicator()
                        }
                        else {
                            self.textFieldMedicFilter.stopLoadingIndicator()
                        }
                    })
                }
            }
        }
        
        self.textFieldCityFilter.userStoppedTypingHandler = {
            if let criteria = self.textFieldCityFilter.text {
                if criteria.count >= 3 {
                    self.textFieldCityFilter.showLoadingIndicator()
                    
                    self.requestCityData(self.textFieldCityFilter.text!, completion: {
                        data in
                        if let data = data {
                            var names = [String]()
                            
                            for cities in data {
                                names.append(cities.name! + " - " + cities.state_abbr!)
                            }
                            
                            self.filteredData["city"] = data
                            self.textFieldCityFilter.filterStrings(names)
                            self.textFieldCityFilter.stopLoadingIndicator()
                        }
                        else {
                            self.textFieldCityFilter.stopLoadingIndicator()
                        }
                    })
                }
            }
        }
        
        self.textFieldProcedureFilter.userStoppedTypingHandler = {
            if let criteria = self.textFieldProcedureFilter.text {
                if criteria.count >= 3 {
                    self.textFieldProcedureFilter.showLoadingIndicator()
                    
                    self.requestProcedureData(self.textFieldProcedureFilter.text!, completion: {
                        data in
                        if let data = data {
                            var procedures = [String]()
                            
                            for procedure in data {
                                procedures.append(procedure.name!)
                            }
                            
                            self.filteredData["procedure"] = data
                            self.textFieldProcedureFilter.filterStrings(procedures)
                            self.textFieldProcedureFilter.stopLoadingIndicator()
                        }
                        else {
                            self.textFieldCityFilter.stopLoadingIndicator()
                        }
                    })
                }
            }
        }
        
        
        self.buttonApplyFilter.layer.cornerRadius = 10
        self.buttonApplyFilter.backgroundColor = UIColor(red: 175/255, green: 68/255, blue: 217/255, alpha: 1.0)
        self.buttonApplyFilter.tintColor = .white
        self.buttonApplyFilter.setTitleColor(UIColor(red: 255/255, green: 105/255, blue: 180/255, alpha: 1.0), for: .highlighted)
        self.buttonApplyFilter.addTarget(self, action: #selector(self.buttonFilterPressed(_:)), for: .touchUpInside)
        
        self.buttonSpecialization.layer.cornerRadius = 10
        self.buttonSpecialization.backgroundColor = UIColor(red: 175/255, green: 68/255, blue: 217/255, alpha: 1.0)
        self.buttonSpecialization.tintColor = .white
        self.buttonSpecialization.setTitleColor(UIColor(red: 255/255, green: 105/255, blue: 180/255, alpha: 1.0), for: .highlighted)
        self.buttonSpecialization.addTarget(self, action: #selector(self.buttonSpecializationPressed(_:)), for: .touchUpInside)
        
        self.stackViewFilteriActivity.isHidden = true
        
    }
    
    private func extractTextData(_ text: String) -> [String:String]? {
        var textData = [String:String]()
        if let range = text.range(of: "-") {
            let state = text[range.upperBound...].trimmingCharacters(in: .whitespaces)
            textData["state"] = state
        }
        else {
            return nil
        }
        
        if let index = text.range(of: "-")?.lowerBound {
            let substring = text[..<index]
            let trimmSubstring = substring.replacingOccurrences(of: "\\s+$", with: "", options: .regularExpression)
            textData["city"] = trimmSubstring
        }
        else {
            return nil
        }
        
        return textData
    }
}

extension FilterTableViewCell {
    @objc func buttonFilterPressed(_ sender: UIButton) {
        var filteredSpecialization = [String:Int]()
        
        if self.textFieldMedicFilter.text!.isEmpty {
            self.filteredList["medic"] = nil
        }
        
        if self.textFieldCityFilter.text!.isEmpty {
            self.filteredList["city"] = nil
        }
    
        if self.selectedSpecializations.isEmpty {
            self.filteredList["specialization"] = nil
        }
        else {
            for (index, value) in self.selectedSpecializations.enumerated() {
                print("\(index): \(value.name)")
                if value.name == "Cirurgia Plástica" {
                    filteredSpecialization[value.name] = 1
                }
                else if value.name == "Dermatologia" {
                    filteredSpecialization[value.name] = 2
                }
                else if value.name == "Endocrinologia" {
                    filteredSpecialization[value.name] = 3
                }
                else if value.name == "Nutrologia" {
                    filteredSpecialization[value.name] = 4
                }
                else if value.name == "Bariátrica" {
                    filteredSpecialization[value.name] = 5
                }
                else if value.name == "Odontologia" {
                    filteredSpecialization[value.name] = 6
                }
            }
            
            self.filteredList["specialization"] = filteredSpecialization as AnyObject
        }
        
        self.delegate.didPressFilterButton(filtered: self.filteredList)
    }
    
    @objc func buttonSpecializationPressed(_ sender: UIButton) {
        var topController: UIViewController = (UIApplication.shared.windows.first?.rootViewController)!
        
        let tableData :[(name: String, translated: String)] = [("Bariátrica", "Bariátrica"),
                                                               ("Cirurgia Plástica", "Cirurgia Plástica"),
                                                               ("Dermatologia", "Dermatologia"),
                                                               ("Endocrinologia", "Endocrinologia"),
                                                               ("Nutrologia", "Nutrologia"),
                                                               ("Odontologia", "Odontologia")]
        
        self.checkBoxDialog = CheckboxDialogViewController()
        self.checkBoxDialog.titleDialog = "Especialidades"
        self.checkBoxDialog.tableData = tableData
        self.checkBoxDialog.defaultValues = self.selectedSpecializations
        self.checkBoxDialog.componentName = DialogCheckboxViewEnum.countries
        self.checkBoxDialog.delegateDialogTableView = self
        self.checkBoxDialog.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        
        
        while topController.presentedViewController != nil {
            topController = topController.presentedViewController!
        }
        
        topController.present(self.checkBoxDialog, animated: false, completion: nil)
    }
}

extension FilterTableViewCell: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.textFieldMedicFilter {
            textField.setBorder(width: 2, borderColor: UIColor(red: 255/255, green: 105/255, blue: 180/255, alpha: 1.0), cornerRadious: 10)
        }
        else if textField == self.textFieldCityFilter {
            textField.setBorder(width: 2, borderColor: UIColor(red: 255/255, green: 105/255, blue: 180/255, alpha: 1.0), cornerRadious: 10)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.setBorder(width: 2, borderColor: .purple, cornerRadious: 10)
    }
}


extension FilterTableViewCell: CheckboxDialogViewDelegate {
    func onCheckboxPickerValueChange(_ component: DialogCheckboxViewEnum, values: TranslationDictionary) {
        print(component)
        
        self.selectedSpecializations = values.map({$0})
    }
}

extension FilterTableViewCell {
    func requestMedicData(_ medicName: String, completion: @escaping ([MedicName]?) -> Void) {
         let url = "\(BaseUrl)/medics/search?name=\(medicName)"
         
         Alamofire.request(url, method: .get, encoding: JSONEncoding.default).validate(statusCode: 200 ..< 299).responseJSON { AFdata in
             
            let data = try? JSONDecoder().decode([MedicName].self, from: AFdata.data!)
                     
            if let medicData = data {
                completion(medicData)
            }
            else {
                completion(nil)
            }
         }
     }
     
     func requestCityData(_ cityName: String, completion: @escaping ([CityName]?) -> Void) {
         let url = "\(BaseUrl)/location/city?name=\(cityName)"
         
         Alamofire.request(url, method: .get, encoding: JSONEncoding.default).validate(statusCode: 200 ..< 299).responseJSON { AFdata in
             let data = try? JSONDecoder().decode([CityName].self, from: AFdata.data!)
                     
             if let cityData = data {
                 completion(cityData)
             }
             else {
                completion(nil)
             }
         }
     }
    
    func requestProcedureData(_ procedureType: String, completion: @escaping ([ProcedureName]?) -> Void) {
        let url = "\(BaseUrl)/treatment?name=\(procedureType)"
        
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default).validate(statusCode: 200 ..< 299).responseJSON { AFdata in
            let data = try? JSONDecoder().decode([ProcedureName].self, from: AFdata.data!)
                    
            if let procedureData = data {
                completion(procedureData)
            }
            else {
               completion(nil)
            }
        }
        
    }
}
