//
//  UserPostsTableViewCell.swift
//  AesCare
//
//  Created by Thiago on 15/03/21.
//

import UIKit

protocol UserPostsTableViewCellDelegate: class {
    func didPressUserPostsReadMore(selected index: Int)
}

class UserPostsTableViewCell: UITableViewCell {
    @IBOutlet weak var imageViewUserIcon: UIImageView!
    @IBOutlet weak var labelUserName: UILabel!
    @IBOutlet weak var labelUserPostDate: UILabel!
    @IBOutlet weak var textViewUserPost: UITextView!
    //@IBOutlet weak var buttonReadMore: UIButton!
    @IBOutlet weak var imageViewLoggedUserIcon: UIImageView!
    @IBOutlet weak var textViewLoggedUserPost: UITextView!
    @IBOutlet weak var buttonLoggedUserSendPost: UIButton!
    @IBOutlet weak var labelMedicPost: UILabel!
    @IBOutlet weak var imageViewPostImage: UIImageView!
    @IBOutlet weak var imageViewPostImageConstraint: NSLayoutConstraint!
    
    var index = 0
    var imageSpec: CGFloat = 0.0
    weak var delegate: UserPostsTableViewCellDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.imageSpec = self.imageViewPostImage.frame.height
//        self.imageViewPostImage.translatesAutoresizingMaskIntoConstraints = false
        
        self.setupViews()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    override func prepareForReuse() {
        self.imageViewUserIcon.image = nil
        self.imageViewLoggedUserIcon.image = nil
    }
        
    private func setupViews() {
        self.selectionStyle = .none
        
        self.labelUserPostDate.textColor = .lightGray
        
        self.imageViewUserIcon.setBorder(width: 2, borderColor: UIColor(red: 175/255, green: 68/255, blue: 217/255, alpha: 1.0), cornerRadious: self.imageViewUserIcon.frame.height / 2)
        self.imageViewUserIcon.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleBottomMargin, .flexibleRightMargin, .flexibleLeftMargin, .flexibleTopMargin]
        self.imageViewUserIcon.contentMode = .scaleAspectFit
        self.imageViewUserIcon.clipsToBounds = true
        
        self.imageViewLoggedUserIcon.setBorder(width: 2, borderColor: UIColor(red: 175/255, green: 68/255, blue: 217/255, alpha: 1.0), cornerRadious: self.imageViewLoggedUserIcon.frame.height / 2)
        self.imageViewLoggedUserIcon.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleBottomMargin, .flexibleRightMargin, .flexibleLeftMargin, .flexibleTopMargin]
        self.imageViewLoggedUserIcon.contentMode = .scaleAspectFill
        self.imageViewLoggedUserIcon.clipsToBounds = true
        
        self.imageViewPostImage.contentMode = .scaleAspectFill
        self.imageViewPostImage.clipsToBounds = true
        
//        self.textViewUserPost.setBorder(width: 2, borderColor: UIColor(red: 232/255, green: 233/255, blue: 235/255, alpha: 1.0), cornerRadious: 10)
        self.textViewUserPost.layer.borderWidth = 0
        self.textViewUserPost.delegate = self
        self.textViewUserPost.textContainer.maximumNumberOfLines = 0
        self.textViewUserPost.tag = 10
        self.textViewUserPost.isUserInteractionEnabled = true
        self.textViewUserPost.isSelectable = true
        self.textViewUserPost.isEditable = false
        //self.textViewUserPost.addSubview(self.buttonReadMore)
        
        self.textViewLoggedUserPost.setBorder(width: 2, borderColor: UIColor(red: 232/255, green: 233/255, blue: 235/255, alpha: 1.0), cornerRadious: 10)
        self.textViewLoggedUserPost.text = "Responder esta publicação"
        self.textViewLoggedUserPost.textColor = .lightGray
        self.textViewLoggedUserPost.delegate = self
        
//        self.buttonReadMore.layer.cornerRadius = 10
//        self.buttonReadMore.backgroundColor = UIColor(red: 175/255, green: 68/255, blue: 217/255, alpha: 1.0)
//        self.buttonReadMore.tintColor = .white
//        self.buttonReadMore.addTarget(self, action: #selector(self.buttonReadMorePressed(_:)), for: .touchUpInside)
        
        self.buttonLoggedUserSendPost.layer.cornerRadius = self.buttonLoggedUserSendPost.frame.height / 2
        self.buttonLoggedUserSendPost.backgroundColor = .lightGray
        self.buttonLoggedUserSendPost.tintColor = .white
        self.buttonLoggedUserSendPost.isEnabled = false
        
        self.labelMedicPost.setBorder(width: 2.0, borderColor: UIColor(red: 177/255, green: 187/255, blue: 250/255, alpha: 1.0), cornerRadious: 10)
        self.labelMedicPost.textColor = UIColor(red: 177/255, green: 187/255, blue: 250/255, alpha: 1.0)
    }
    
    func hideImage() {
        self.imageViewPostImage.isHidden = true
        self.imageViewPostImageConstraint.constant = 0
    }
    
    func showImage() {
        self.imageViewPostImage.isHidden = false
        self.imageViewPostImageConstraint.constant = self.imageSpec
    }
}

extension UserPostsTableViewCell: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.setBorder(width: 2, borderColor: UIColor(red: 174/255, green: 70/255, blue: 218/255, alpha: 1.0), cornerRadious: 10)
        
        if textView.textColor == UIColor.lightGray {
            textView.text = ""
            textView.textColor = .black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        textView.setBorder(width: 2, borderColor: UIColor(red: 174/255, green: 70/255, blue: 218/255, alpha: 1.0), cornerRadious: 10)
        
        if textView.text!.isEmpty {
            textView.setBorder(width: 2, borderColor: .lightGray, cornerRadious: 10)
            textView.textColor = .lightGray
            textView.text! = "Responder esta postagem"
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if textView.text.count > 0 {
            self.buttonLoggedUserSendPost.isEnabled = true
            self.buttonLoggedUserSendPost.backgroundColor = UIColor(red: 174/255, green: 70/255, blue: 218/255, alpha: 1.0)
        }
        else {
            self.buttonLoggedUserSendPost.isEnabled = false
            self.buttonLoggedUserSendPost.backgroundColor = .lightGray
        }
    }
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        
        print(URL)
        
        if URL.absoluteString == "ios.app.aescare" {
            self.delegate.didPressUserPostsReadMore(selected: index)
        }
        
        return true
    }
}

extension UserPostsTableViewCell {
    @objc func buttonReadMorePressed(_ sender: UIButton) {
        //self.delegate.didPressUserPostsReadMore(selected: index)
    }
}

extension UIImageView {
    open override var intrinsicContentSize: CGSize {
        let frameSizeWidth = self.frame.size.width

        // image
        // ⓘ In testing on iOS 12.1.4 heights of 1.0 and 0.5 were respected, but 0.1 and 0.0 led intrinsicContentSize to be ignored.
        guard let image = self.image else
        {
            return CGSize(width: frameSizeWidth, height: 1.0)
        }

        // MAIN
        let returnHeight = ceil(image.size.height * (frameSizeWidth / image.size.width))
        return CGSize(width: frameSizeWidth, height: returnHeight)
    }
}
