//
//  RequestDataModels.swift
//  AesCare
//
//  Created by Thiago on 17/03/21.
//

import Foundation

// MARK: - Results Models
struct ResultsResponse: Codable {
    let results: [ResultsModel]?
}

struct ResultsModel: Codable {
    let title: String?
    let payment_type: String?
    let evaluation_result: Int?
    let comments_count: Int?
    let treatment_value: String?
    let image_url: String?
    let result_id: Int?
    let user_id: Int?
    let liked: Bool?
    let likes_count: Int?
    let views_count: Int?
    let treatment_date_formated: String?
    let city: ResultsCityModel?
    let medic: ResultsMedicModel?
    let comments: [ResultsComments]?
}

struct ResultsCityModel: Codable {
    let name: String?
    let state_abbr: String?
}

struct ResultsMedicModel: Codable {
    let name_with_title: String?
    let phone: String?
    let state_abbr: String?
    let city_name: String?
    let results_count: Int?
    let cover_media: ResultsMedicCoverMediaModel?
}

struct ResultsMedicCoverMediaModel: Codable {
    let video_url: String?
    let image_url: String?
    let thumbnail_url: String?
    let cover_url: String?
}

struct ResultsCommentsResponse: Codable {
    var results: [ResultsCommentsModel]?
}

struct ResultsCommentsModel: Codable {
    var comments: [ResultsComments]?
}

struct ResultsComments: Codable {
    let comment_id: Int?
    let related_id: Int?
    let content: String?
    let likes_count: Int?
    let created_at: String?
    let comments_count: Int?
    let liked: Bool?
    let user: ResultsCommentsUser?
}

struct ResultsCommentsUser: Codable {
    let username: String?
    let key: String?
    let user_id: Int?
    let url_image: String?
}

// TESTE SECTION

struct ResultsUserComments: Codable {
    let result: ResultsUserCommentsResponse?
}

struct ResultsUserCommentsResponse: Codable {
    var comments: [ResultsUserCommentsModel]?
}

struct ResultsUserCommentsModel: Codable {
    let comment_id: Int?
    let related_id: Int?
    let content: String?
    let likes_count: Int?
    let created_at: String?
    let comments_count: Int?
    let liked: Bool?
    let user: ResultsCommentsUser?
}

struct PostsResponse: Codable {
    let posts: [PostsModel]
}

struct PostsModel: Codable {
    let post_id: Int?
    let key: String?
    let status: String?
    let body: String?
    let created_at_formatted: String?
    let comments_count: Int?
    let likes_count: Int?
    let user: PostsUsersModel?
    let medic: PostsMedicModel?
    let medias: [PostsMediasModel]?
    let comments: [PostsCommentsModel]?
}

struct PostsUsersModel: Codable {
    let key: String?
    let username: String?
    let name: String? // Pode ser null
    let image_url: String?
}

struct PostsMediasModel: Codable {
    let key: String?
    let created_at: String?
    let created_at_formatted: String?
    let user_id: Int?
    let status: String?
    let post_id: Int?
    let content_id: Int?
    let video_url: String?
    let image_url: String?
    let thumbnail_url: String?
    let cover_url: String?
}

struct PostsCommentsModel: Codable {
    let comment_id: Int?
    let related_id: Int?
    let content: String?
    let likes_count: Int?
    let created_at: String?
    let comments_count: Int?
    let liked: Bool?
    let status: String?
    let user: PostsCommentsUserModel?
}

struct PostsCommentsUserModel: Codable {
    let username: String?
    let url_image: String?
    let user_id: Int?
    let role: String?
    let key: String?
}

struct PostsMedicModel: Codable {
    let medic_id: Int?
    let user_id: Int?
    let crm: String?
    let gender: String
    let count_waiting_list: Int?
    let email: String?
    let phone: String?
    let state_abbr: String?
    let city_name: String?
    let name: String?
    let key: String?
    let medic_key: String?
    let city_id: Int?
    let name_with_title: String?
    let image_url: String?
    let has_profile: Bool?
    let evaluation_average: Double?
    let evaluation_average_formatted: String?
    let testimonials_count: Int?
    let results_count: Int?
    let has_interest_scheduling: Bool?
    let has_waiting_list: Bool?
    let has_calendar: Bool?
    let has_online_service: Bool?
    let has_noshow_fee: Bool?
    let noshow_fee: Double?
    let posts_count: Int?
    let posts_instagram_count: Int?
    let count_stories: Int?
    let count_waiting_list_notification_read: Int?
    let count_waiting_list_notification_sent: Int?
    let has_instagram_sync: Bool?
    let cover_media: PostsMedicCoverMediaModel?
    let specialties: [PostsMedicSpecialtiesModel]?
}

struct PostsMedicCoverMediaModel: Codable {
    let key: String?
    let created_at: String?
    let created_at_formatted: String?
    let user_id: Int?
    let status: String?
    let type: String?
    let post_id: Int?
    let content_id: Int?
    let video_url: String?
    let image_url: String?
    let thumbnail_url: String?
    let cover_url: String?
}

struct PostsMedicSpecialtiesModel: Codable {
    let name: String?
    let specialty_id: Int?
}

// TESTE SECTION

// MARK: - Users Posts Models
struct UsersPostsResponse: Codable {
    let posts: [UsersPostsModel]?
}

struct UsersPostsModel: Codable {
    let body: String?
    let created_at_formatted: String?
    let user: PostsUserModel?
}

struct PostsUserModel: Codable {
    let key: String?
    let username: String?
    let name: String?
    let image_url: String?
}

// MARK: - Medics Posts Models
struct MedicsPostsResponse: Codable {
    let posts: [MedicsPostsModel]?
}

struct MedicsPostsModel: Codable {
    let medic: MedicsPosts?
    let content: MedicsPostsContent?
    let created_at_formatted: String?
    //let medicsContent: MedicsPostsContent?
    //let medicsSpecialities: MedicsPostsSpecialities?
}

struct MedicsPosts: Codable {
    let email: String?
    let phone: String?
    let state_abbr: String?
    let city_name: String?
    let name_with_title: String?
    let image_url: String?
}

struct MedicsPostsContent: Codable {
    let url_youtube: String?
    let youtube_video_id: String?
    let media_type: String?
    let cover_image_url: String?
    let title: String?
    let summary: String?
}

struct MedicsPostsSpecialities: Codable {
    let name: String
}

// MARK: - Medics Articles Models
struct MedicsArticlesResponse: Codable {
    let posts: [MedicsArticlesModel]?
}

struct MedicsArticlesModel: Codable {
    let medic: MedicsArticles?
    let content: MedicsArticlesContent?
    let created_at_formatted: String?
    let post_id: Int?
    let key: String?
}

struct MedicsArticles: Codable {
    let email: String?
    let phone: String?
    let state_abbr: String?
    let city_name: String?
    let name_with_title: String?
    let image_url: String?
}

struct MedicsArticlesContent: Codable {
//    let url_youtube: String?
//    let youtube_video_id: String?
//    let media_type: String?
    let cover_image_url: String?
    let title: String?
    let summary: String?
    let youtube_video_id: String?
}

struct MedicsArticlesCommentsResponse: Codable {
    let post: MedicsArticlesCommentsModel?
}

struct MedicsArticlesCommentsModel: Codable {
    var comments: [MedicsArticlesComments]?
}

struct MedicsArticlesComments: Codable {
    let comment_id: Int?
    let related_id: Int?
    let content: String?
    let likes_count: Int?
    let created_at: String?
    let comments_count: Int?
    let liked: Bool?
    let status: String?
    let user: MedicsArticlesCommentsUser?
}

struct MedicsArticlesCommentsUser: Codable {
    let username: String?
    let url_image: String?
    let user_id: Int?
    let role: String?
    let key: String?
}

struct MedicsArticlesSpecialities: Codable {
    let name: String
}

// MARK: - Medias Model
struct MediasModel: Codable {
    let medias: [MediasContent]?
}

struct MediasContent: Codable {
    let name: String?
    let image_url: String?
    let thumbnail_url: String?
    let cover_url: String?
}



//struct PostsUserComments: Codable {
//    let post: PostsUserCommentsResponse?
//}
//
//struct PostsUserCommentsResponse: Codable {
//    var comments: [PostsUserCommentsModel]?
//}
//
//struct PostsUserCommentsModel: Codable {
//    let comment_id: Int?
//    let related_id: Int?
//    let content: String?
//    let likes_count: Int?
//    let created_at: String?
//    let comments_count: Int?
//    let liked: Bool?
//    let user: PostsCommentsUser?
//}
//
//struct PostsCommentsUser: Codable {
//    let username: String?
//    let url_image: String?
//    let user_id: Int?
//    let key: String?
//}

struct PostsUsersMyPosts: Codable {
    let posts: [PostsUsersMyPostsModel]?
}

struct PostsUsersMyPostsModel: Codable {
    let post_id: Int?
    let key: String?
    let order: String?
    let body: String?
    let created_at_formatted: String?
}
