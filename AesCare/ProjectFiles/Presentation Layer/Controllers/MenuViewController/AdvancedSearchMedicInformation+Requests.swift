//
//  AdvancedSearchMedicInformations+Requests.swift
//  AesCare
//
//  Created by Thiago on 27/04/21.
//

import Foundation
import Alamofire

extension AdvancedSearchMedicInformation {
    func requestResults(_ page: Int = 1, medicId: Int, completion: @escaping (Bool) -> Void) {
        let url = "\(BaseUrl)/results?limit=9&page=\(page)&medic_id=\(medicId)"
        let headers: HTTPHeaders = ["Content-Type": "application/json",
            "Cookie": "aescare_session_key=\(usrerModelOBJ.session_key!)"]
        
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers).validate(statusCode: 200 ..< 299).responseJSON { [weak self] AFdata in
            switch AFdata.result {
            case let .success(value):
                print(value)
            case let .failure(error):
                print(error)
                completion(false)
                return
            }
            
            do {
                let data = try JSONDecoder().decode(ResultsResponse.self, from: AFdata.data!)
                    
                print(data)
                    
                self?.resultsDataArray.append(contentsOf: data.results!)
                completion(true)
            } catch {
                print(error)
                completion(false)
                return
            }
        }.session.configuration.timeoutIntervalForRequest = TimeInterval(20)
    }
    
    func requestMedicsPosts(_ page: Int = 1, medicId: Int, completion: @escaping (Bool) -> Void) {
        let url = "\(BaseUrl)/posts?limit=9&page=\(page)&type=artigo&medic_id=\(medicId)"
        
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default).validate(statusCode: 200 ..< 299).responseJSON { [weak self] AFdata in
                do {
                    guard let jsonObject = try JSONSerialization.jsonObject(with: AFdata.data!) as? [String: Any] else {
                        print("Error: Cannot convert data to JSON object")
                        return
                    }
                    guard let prettyJsonData = try? JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted) else {
                        print("Error: Cannot convert JSON object to Pretty JSON data")
                        completion(false)
                        return
                    }
                    guard String(data: prettyJsonData, encoding: .utf8) != nil else {
                        print("Error: Could print JSON in String")
                        completion(false)
                        return
                    }
                    
                    let data = try! JSONDecoder().decode(MedicsPostsResponse.self, from: AFdata.data!)
                    
                    self?.articlesDataArray.append(contentsOf: data.posts!)
                    completion(true)
                } catch {
                    print("Error: Trying to convert JSON data to string")
                    completion(false)
                    return
                }
            }.session.configuration.timeoutIntervalForRequest = TimeInterval(20)
    }
    
    func requestUsersPosts(_ page: Int = 1, medicId: Int, completion: @escaping (Bool) -> Void) {
        let url = "\(BaseUrl)/posts?limit=9&page=\(page)&type=post&medic_id=\(medicId)"
        
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default).validate(statusCode: 200 ..< 299).responseJSON { [weak self] AFdata in
                do {
                    guard let jsonObject = try JSONSerialization.jsonObject(with: AFdata.data!) as? [String: Any] else {
                        print("Error: Cannot convert data to JSON object")
                        return
                    }
                    guard let prettyJsonData = try? JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted) else {
                        print("Error: Cannot convert JSON object to Pretty JSON data")
                        completion(false)
                        return
                    }
                    guard String(data: prettyJsonData, encoding: .utf8) != nil else {
                        print("Error: Could print JSON in String")
                        completion(false)
                        return
                    }
                    
                    let data = try! JSONDecoder().decode(PostsResponse.self, from: AFdata.data!)
                    
                    print(data.posts)
                    
                    self?.postsDataArray.append(contentsOf: data.posts)
                    completion(true)
                } catch {
                    print("Error: Trying to convert JSON data to string")
                    completion(false)
                    return
                }
            }.session.configuration.timeoutIntervalForRequest = TimeInterval(20)
    }
    
    func requestMedicAvaluation(_ page: Int = 1, medicId: Int, completion: @escaping (Bool) -> Void) {
        let url = "\(BaseUrl)/medic/opinions?related_id=\(medicId)"
        
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default).validate(statusCode: 200 ..< 299).responseJSON { [weak self] AFdata in
                do {
                    guard let jsonObject = try JSONSerialization.jsonObject(with: AFdata.data!) as? [String: Any] else {
                        print("Error: Cannot convert data to JSON object")
                        return
                    }
                    guard let prettyJsonData = try? JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted) else {
                        print("Error: Cannot convert JSON object to Pretty JSON data")
                        completion(false)
                        return
                    }
                    guard String(data: prettyJsonData, encoding: .utf8) != nil else {
                        print("Error: Could print JSON in String")
                        completion(false)
                        return
                    }
                    
                    let data = try! JSONDecoder().decode(AdvancedSearchMedicAvaluation.self, from: AFdata.data!)
                    
                    print(data.opinions!)
                    
                    self?.avaluationDataArray.append(contentsOf: data.opinions!)
                    completion(true)
                } catch {
                    print("Error: Trying to convert JSON data to string")
                    completion(false)
                    return
                }
            }.session.configuration.timeoutIntervalForRequest = TimeInterval(20)
    }
    
    func requestMedicInfo(_ medicId: Int, completion: @escaping (Bool) -> Void) {
        let url = "\(BaseUrl)/medic?medic_id=\(medicId)"
        let headers: HTTPHeaders = ["Content-Type": "application/json",
            "Cookie": "aescare_session_key=\(usrerModelOBJ.session_key!)"]
        
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers).validate(statusCode: 200 ..< 299).responseJSON { [weak self] AFdata in
            switch AFdata.result {
            case let .success(value):
                print(value)
            case let .failure(error):
                print(error)
            }
                do {
                    guard let jsonObject = try JSONSerialization.jsonObject(with: AFdata.data!) as? [String: Any] else {
                        print("Error: Cannot convert data to JSON object")
                        return
                    }
                    guard let prettyJsonData = try? JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted) else {
                        print("Error: Cannot convert JSON object to Pretty JSON data")
                        completion(false)
                        return
                    }
                    guard String(data: prettyJsonData, encoding: .utf8) != nil else {
                        print("Error: Could print JSON in String")
                        completion(false)
                        return
                    }
                    
                    let data = try! JSONDecoder().decode(AdvancedSearchMedicInfo.self, from: AFdata.data!)
                    
                    print(data)
                    
                    self?.medicInfo = data
                    
                    completion(true)
                } catch {
                    print("Error: Trying to convert JSON data to string")
                    completion(false)
                    return
                }
            }.session.configuration.timeoutIntervalForRequest = TimeInterval(20)
    }
}
