//
//  AdvancedSearchMedicInformation.swift
//  AesCare
//
//  Created by Thiago on 27/04/21.
//

import Foundation
import UIKit
import RESegmentedControl
import SDWebImage
import Cosmos
import NVActivityIndicatorView

class AdvancedSearchMedicInformation: UIViewController {
    @IBOutlet weak var segmentedTab: RESegmentedControl!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imageViewLogo: UIImageView!
    @IBOutlet weak var imageViewIcon: UIImageView!
    @IBOutlet weak var labelMedicName: UILabel!
    @IBOutlet weak var tableViewMedicInformations: UITableView!
    @IBOutlet weak var labelFollowers: UILabel!
    @IBOutlet weak var viewRating: CosmosView!
    @IBOutlet weak var labelTelefone: UILabel!
    @IBOutlet weak var labelLocal: UILabel!
    @IBOutlet weak var labelSpecialty: UILabel!
    
    lazy var resultsDataArray = [ResultsModel]()
    lazy var articlesDataArray = [MedicsPostsModel]()
    lazy var postsDataArray = [PostsModel]()
    lazy var avaluationDataArray = [AdvancedSearchMedicOpinions]()
    
    var medicInfo: AdvancedSearchMedicInfo?
    var activityLoading: NVActivityIndicatorView!
    var advancedSearchMedicData: AdvancedSearch!
    var imageManager: SDWebImageDownloader!
    var nibFiles: [UINib]!
    var sectionTitles = ["Resultados", "Avaliações", "Artigos", "Posts"]
    var preset: MaterialPreset!
    var segmentIndex = 0
    var medicId = 0
    var counter: Int? = 0
    
    var segmentItemsWithValues: [SegmentModel] = [SegmentModel(title: "Resultados"),
                                                  SegmentModel(title: "Avaliações"),
                                                  SegmentModel(title: "Artigos"),
                                                  SegmentModel(title: "Posts")] {
        willSet {
            print("Value changed from \(segmentItemsWithValues) to \(newValue)")
        }
        
        didSet {
            print("Recent value is \(oldValue) and new value is \(segmentItemsWithValues)")
        }
    }
    
    var segmentedItems: [SegmentModel] {
        return self.sectionTitles.map({ SegmentModel(title: $0) })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        NotificationCenter.default.addObserver(self,selector: #selector(self.keyboardDidShow(notification:)),
        name: UIResponder.keyboardDidShowNotification, object: nil)
        
        self.activityLoading = NVActivityIndicatorView(frame: CGRect(x: self.view.bounds.origin.x, y: self.view.bounds.origin.y, width: self.view.bounds.width / 2, height: self.view.bounds.height / 2), type: .ballScaleRippleMultiple, color: .purple, padding: nil)
        self.activityLoading.translatesAutoresizingMaskIntoConstraints = false
        
        self.imageManager = SDWebImageDownloader()
        self.imageManager.setValue("aescare_session_key=\(usrerModelOBJ.session_key!)", forHTTPHeaderField: "Cookie")
        
        self.setupTableView()
        self.setupViews()
        self.view.addSubview(self.activityLoading)
        
        NSLayoutConstraint.activate([
            self.activityLoading.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            self.activityLoading.centerYAnchor.constraint(equalTo: self.view.centerYAnchor)
        ])
        
        self.activityLoading.startAnimating()
        self.requestResults(medicId: self.advancedSearchMedicData.medic_id!, completion: {
            resultsResponse in
            
            if resultsResponse {
                self.requestMedicsPosts(medicId: self.advancedSearchMedicData.medic_id!, completion: {
                    postsResponse in
                    
                    if postsResponse {
                        self.requestUsersPosts(medicId: self.advancedSearchMedicData.medic_id!, completion: {
                            medicPostsResponse in
                            
                            if medicPostsResponse {
                                self.requestMedicAvaluation(medicId: self.advancedSearchMedicData.medic_id!, completion: {
                                    medicAvaluationResponse in
                                    
                                    if medicAvaluationResponse {
                                        self.requestMedicInfo(self.advancedSearchMedicData.medic_id!, completion: {
                                            medicInfoResponse in
                                            
                                            if medicInfoResponse {
                                                self.segmentItemsWithValues[0] = SegmentModel(title: "\(self.medicInfo!.medic!.results_count!) Resultados")
                                                self.segmentItemsWithValues[1] = SegmentModel(title: "\(self.medicInfo!.medic!.evaluations_with_contents_count!) Avaliações")
                                                self.segmentItemsWithValues[2] = SegmentModel(title: "\(self.medicInfo!.medic!.medic_posts_count!) Artigos")
                                                self.segmentItemsWithValues[3] = SegmentModel(title: "\(self.medicInfo!.medic!.posts_count!) Posts")
                                                self.segmentedTab.configure(segmentItems: self.segmentItemsWithValues, preset: self.preset, selectedIndex: 0)
                                                
                                                self.activityLoading.stopAnimating()
                                                
                                                DispatchQueue.main.async {
                                                    self.imageViewIcon.sd_setImage(with: URL(string: self.advancedSearchMedicData.cover_media!.image_url!), completed: nil)
                                                    self.labelMedicName.text = self.advancedSearchMedicData.name_with_title!
                                                    self.labelTelefone.text = self.advancedSearchMedicData.phone!
                                                    self.labelLocal.text = "\(self.advancedSearchMedicData.city!.name!)/\(self.advancedSearchMedicData.city!.state_abbr!)"
                                                    self.labelFollowers.text = "Seguidores: \(self.followersCountFormatter(self.advancedSearchMedicData!.follow_count!))"
                                                    self.viewRating.rating = Double(self.advancedSearchMedicData.evaluation_average!).rounded()
                                                    self.labelSpecialty.text = self.specialty(self.advancedSearchMedicData.specialty_id ?? 0)
                                                    self.showViewsAnimation()
                                                    self.tableViewMedicInformations.reloadData()
                                                }
                                                
                                            }
                                        })
                                    }
                                    else {
                                        self.activityLoading.stopAnimating()
                                        
                                        AESCarePopupDialog.dialog(title: "Problema de Carregamento", message: "Ocorreu um problema no carregamento dos dados. Tente novamente", image: UIImage(named: "common_image1"), viewController: self, {
                                            
                                            self.dismiss(animated: true, completion: nil)
                                        })
                                    }
                                })
                            }
                            else {
                                self.activityLoading.stopAnimating()
                                
                                AESCarePopupDialog.dialog(title: "Problema de Carregamento", message: "Ocorreu um problema no carregamento dos dados. Tente novamente", image: UIImage(named: "common_image1"), viewController: self, {
                                    
                                    self.dismiss(animated: true, completion: nil)
                                })
                            }
                        })
                    }
                    else {
                        self.activityLoading.stopAnimating()
                        
                        AESCarePopupDialog.dialog(title: "Problema de Carregamento", message: "Ocorreu um problema no carregamento dos dados. Tente novamente", image: UIImage(named: "common_image1"), viewController: self, {
                            
                            self.dismiss(animated: true, completion: nil)
                        })
                    }
                })
            }
            else {
                self.activityLoading.stopAnimating()
                
                AESCarePopupDialog.dialog(title: "Problema de Carregamento", message: "Ocorreu um problema no carregamento dos dados. Tente novamente", image: UIImage(named: "common_image1"), viewController: self, {
                    
                    self.dismiss(animated: true, completion: nil)
                })
            }
        })
    }
    
    private func setupTableView() {
        self.nibFiles = [UINib]()
        self.nibFiles.append(UINib(nibName: "ResultsTableViewCell", bundle: nil))
        self.nibFiles.append(UINib(nibName: "MedicPostsTableViewCell", bundle: nil))
        self.nibFiles.append(UINib(nibName: "UserPostsTableViewCell", bundle: nil))
        self.nibFiles.append(UINib(nibName: "AdvancedSearchMedicAvaluationTableViewCell", bundle: nil))
        self.nibFiles.append(UINib(nibName: "AdvancedSearchMedicUsersOpinionsTableViewCell", bundle: nil))
        self.nibFiles.append(UINib(nibName: "EmptyTableViewCell", bundle: nil))
        
        self.tableViewMedicInformations.separatorStyle = .none
        self.tableViewMedicInformations.register(self.nibFiles[0], forCellReuseIdentifier: "ResultsTableViewCell")
        self.tableViewMedicInformations.register(self.nibFiles[1], forCellReuseIdentifier: "MedicPostsTableViewCell")
        self.tableViewMedicInformations.register(self.nibFiles[2], forCellReuseIdentifier: "UserPostsTableViewCell")
        self.tableViewMedicInformations.register(self.nibFiles[3], forCellReuseIdentifier: "AdvancedSearchMedicAvaluationTableViewCell")
        self.tableViewMedicInformations.register(self.nibFiles[4], forCellReuseIdentifier: "AdvancedSearchMedicUsersOpinionsTableViewCell")
        self.tableViewMedicInformations.register(self.nibFiles[5], forCellReuseIdentifier: "EmptyTableViewCell")
        
        self.tableViewMedicInformations.delegate = self
        self.tableViewMedicInformations.dataSource = self
    }
    
    private func setupViews() {
        self.preset = MaterialPreset(backgroundColor: .white, tintColor: UIColor(red: 175/255, green: 68/255, blue: 217/255, alpha: 1.0))
        preset.textColor = UIColor(red: 220/255, green: 220/255, blue: 220/255, alpha: 1.0)
        
        self.segmentedTab.configure(segmentItems: self.segmentItemsWithValues, preset: self.preset, selectedIndex: 0)
        
        self.imageViewIcon.setBorder(width: 2, borderColor: UIColor(red: 175/255, green: 68/255, blue: 217/255, alpha: 1.0), cornerRadious: self.imageViewIcon.frame.height / 2)
        self.imageViewIcon.clipsToBounds = true
        self.imageViewIcon.contentMode = .scaleToFill
        
        self.imageViewLogo.clipsToBounds = true
        self.imageViewLogo.contentMode = .scaleToFill
        
        self.imageViewLogo.layer.cornerRadius = 5
        
        self.viewRating.text = nil
        self.viewRating.isUserInteractionEnabled = false
        
        self.imageViewIcon.alpha = 0
        self.imageViewLogo.alpha = 0
        self.tableViewMedicInformations.alpha = 0
        self.labelMedicName.alpha = 0
        self.labelLocal.alpha = 0
        self.labelTelefone.alpha = 0
        self.labelFollowers.alpha = 0
        self.labelSpecialty.alpha = 0
        self.viewRating.alpha = 0
        self.segmentedTab.alpha = 0
    }
    
    private func showViewsAnimation() {
        UIView.animate(withDuration: 0.8, animations: {
            self.imageViewIcon.alpha = 1
        }, completion: { _ in
            UIView.animate(withDuration: 0.5, animations: {
                self.labelMedicName.alpha = 1
                self.viewRating.alpha = 1
                self.labelSpecialty.alpha = 1
            }, completion: { _ in
                UIView.animate(withDuration: 0.8, animations: {
                    self.imageViewLogo.alpha = 1
                }, completion: { _ in
                    UIView.animate(withDuration: 0.8, animations: {
                        self.segmentedTab.alpha = 1
                    }, completion: { _ in
                        UIView.animate(withDuration: 0.8, animations: {
                            self.tableViewMedicInformations.alpha = 1
                        }, completion: { _ in
                            UIView.animate(withDuration: 0.5, animations: {
                                self.labelTelefone.alpha = 1
                                self.labelLocal.alpha = 1
                            }, completion: { _ in
                                UIView.animate(withDuration: 0.5, animations: {
                                    self.labelFollowers.alpha = 1
                                }, completion: nil)
                            })
                        })
                    })
                })
            })
        })
    }
    
    private func followersCountFormatter(_ followersCount: Int) -> String {
        let formatter = String(followersCount)
        let startIndex = formatter.index(formatter.startIndex, offsetBy: 0)
        
        if followersCount > 999 && followersCount <= 9999 {
            return "\(formatter[startIndex]) mil"
        }
        else if followersCount > 9999 && followersCount <= 99999 {
            let endIndex = formatter.index(formatter.startIndex, offsetBy: 1)
        
            return "\(formatter[startIndex...endIndex]) mil"
        }
        else if followersCount > 99999 {
            let endIndex = formatter.index(formatter.startIndex, offsetBy: 2)
            
            return "\(formatter[startIndex...endIndex]) mil"
        }
        else {
            return formatter
        }
    }
    
    private func specialty(_ id: Int) -> String {
        switch id {
        case 1:
            return "Especialista em Cirurgia Plástica"
        case 2:
            return "Especialista em Dermatologia"
        case 3:
            return "Especialista em Endocrinologia"
        case 4:
            return "Especialista em Nutrologia"
        case 5:
            return "Especialista em Bariátrica"
        case 6:
            return "Especialista em Cirurgia Odontologica"
        case 7:
            return "Especialista em Ginecologia e Obstetrícia"
        default:
            return "Não Especificada"
        }
    }
    
    func limitTextSizeBy(_ text: String, maxTextSize size: Int) -> NSMutableAttributedString {
        let formattedText = self.formatText(text, maxSize: size)

        let formattedString = NSMutableAttributedString(string: formattedText)

        let textAttribute = [
            NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue,
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16.0),
            NSAttributedString.Key.link: URL(string: "ios.app.aescare") ?? URL.self
        ] as [NSAttributedString.Key : Any]


        let textRange = (formattedText as NSString).range(of: "Ver Mais")
        formattedString.addAttributes(textAttribute, range: textRange)

        return formattedString
    }
    
    private func formatText(_ text: String, maxSize: Int) -> String {
        var msg = text
        
        if msg.count > maxSize {
            let range = msg.index(text.startIndex, offsetBy: 50)
            msg.removeSubrange(range..<msg.endIndex)
            return msg + "..." + " Ver Mais"
        }
        else {
            return text + " Ver Mais"
        }
    }
}

extension AdvancedSearchMedicInformation: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.segmentIndex == 1 {
            return 2
        }
        else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.segmentIndex == 0 {
            if self.resultsDataArray.isEmpty {
                return 1
            }
            else {
                return self.resultsDataArray.count
            }
        }
        else if self.segmentIndex == 1 {
            if section == 0 {
                return 1
            }
            else {
                if self.avaluationDataArray.isEmpty {
                    return 1
                }
                else {
                    return self.avaluationDataArray.count
                }
            }
        }
        else if self.segmentIndex == 2 {
            if self.articlesDataArray.isEmpty {
                return 1
            }
            else {
                return self.articlesDataArray.count
            }
        }
        else if self.segmentIndex == 3 {
            if self.postsDataArray.isEmpty {
                return 1
            }
            else {
                return self.postsDataArray.count
            }
        }
        else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.segmentIndex == 0 {
            if !self.resultsDataArray.isEmpty {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ResultsTableViewCell") as! ResultsTableViewCell
                cell.delegate = self
                cell.labelResultsTitle.text = self.resultsDataArray[indexPath.row].title!
                cell.setTextToLabel(self.resultsDataArray[indexPath.row].city?.name ?? "", label: cell.labelLocation, imageName: "map-icon")
                cell.setTextToLabel(self.resultsDataArray[indexPath.row].payment_type ?? "", label: cell.labelPaymentMethod, imageName: "parcelamento-icon")
                cell.setTextToLabel(self.resultsDataArray[indexPath.row].treatment_value ?? "", label: cell.labelPaymentValue, imageName: "valor-icon")
                cell.setTextToLabel(self.resultsDataArray[indexPath.row].medic?.name_with_title ?? "", label: cell.labelDoctorName, imageName: "medico-icon")
                cell.starRating.rating = Double(self.resultsDataArray[indexPath.row].evaluation_result ?? 1)
                
                if let cachedImage = SDImageCache.shared.imageFromCache(forKey: self.resultsDataArray[indexPath.row].image_url!) {
                    cell.imageViewResultsUser.image = cachedImage
                }
                else {
                    self.imageManager.downloadImage(with: URL(string: self.resultsDataArray[indexPath.row].image_url!), options: .highPriority, progress: nil, completed: {
                        (image, error, cacheType, finished) in
                        
                        if finished {
                            if let image = image {
                                DispatchQueue.main.async {
                                    cell.imageViewResultsUser.image = image
                                    SDImageCache.shared.storeImage(toMemory: image, forKey: self.resultsDataArray[indexPath.row].image_url!)
                                }
                            }
                            else {
                                DispatchQueue.main.async {
                                    cell.imageViewResultsUser.image = UIImage(named: "illustration-chat")
                                }
                            }
                        }
                    })
                }
                
                return cell
            }
            else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "EmptyTableViewCell") as! EmptyTableViewCell
                
                cell.labelMessage.text = "Este médico não possui resultados até o momento"
                
                return cell
            }
        }
        else if self.segmentIndex == 1 {
            if indexPath.section == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "AdvancedSearchMedicAvaluationTableViewCell") as! AdvancedSearchMedicAvaluationTableViewCell
                
                return cell
            }
            else {
                if !self.avaluationDataArray.isEmpty {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "AdvancedSearchMedicUsersOpinionsTableViewCell") as! AdvancedSearchMedicUsersOpinionsTableViewCell
                    
                    cell.imageViewIcon.sd_setImage(with: URL(string: self.avaluationDataArray[indexPath.row].user!.url_image! ), completed: nil)
                    cell.labelUsername.text = self.avaluationDataArray[indexPath.row].user!.username!
                    cell.viewRating.rating = Double(self.avaluationDataArray[indexPath.row].evaluation!)
                    cell.textViewMessage.text = self.avaluationDataArray[indexPath.row].opinion!
                    
                    return cell
                }
                else {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "EmptyTableViewCell") as! EmptyTableViewCell
                    
                    cell.labelMessage.text = "Este médico não possui nenhuma avaliação até o momento"
                    
                    return cell
                }
            }
        }
        else if self.segmentIndex == 2 {
            if !self.articlesDataArray.isEmpty {
                let cell = tableView.dequeueReusableCell(withIdentifier: "MedicPostsTableViewCell", for: indexPath) as! MedicPostsTableViewCell
                cell.delegate = self
                cell.index = indexPath.row
                cell.backgroundColor = .white
                cell.labelPostTitle.text = self.articlesDataArray[indexPath.row].content?.title!
                cell.labelPostDescription.text = self.articlesDataArray[indexPath.row].content?.summary
                cell.imageViewDoctorInfo.sd_setImage(with: URL(string: (self.articlesDataArray[indexPath.row].content?.cover_image_url ?? "")!), completed: { (image, error, cache, url) in
                    if error != nil {
                        cell.imageViewDoctorIcon.image = UIImage(named: "illustration-icon")
                    }
                })
                
                if let _ = self.articlesDataArray[indexPath.row].medic?.image_url {
                    cell.imageViewDoctorIcon.sd_setImage(with: URL(string: (self.articlesDataArray[indexPath.row].medic?.image_url!)!), completed: nil)
                }
                else {
                    cell.imageViewDoctorIcon.image = UIImage(named: "logo-aescare")
                }
                
                cell.labelPostDate.text = self.articlesDataArray[indexPath.row].created_at_formatted!
                cell.labelMedicName.text = self.articlesDataArray[indexPath.row].medic?.name_with_title!
                
                return cell
            }
            else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "EmptyTableViewCell") as! EmptyTableViewCell
                
                cell.labelMessage.text = "Este médico não possui nenhum artigo até o momento"
                
                return cell
            }
        }
        else if self.segmentIndex == 3 {
            if !self.postsDataArray.isEmpty {
                let cell = tableView.dequeueReusableCell(withIdentifier: "UserPostsTableViewCell", for: indexPath) as! UserPostsTableViewCell
                cell.delegate = self
                cell.index = indexPath.row
                cell.backgroundColor = .white
                cell.textViewUserPost.attributedText = self.limitTextSizeBy(self.postsDataArray[indexPath.row].body!, maxTextSize: 50)
                cell.labelUserName.text = self.postsDataArray[indexPath.row].user?.username!
                cell.labelUserPostDate.text = "Em \(self.postsDataArray[indexPath.row].created_at_formatted!)"
                cell.imageViewLoggedUserIcon.sd_setImage(with: URL(string: (usrerModelOBJ.user?.perfil_image!)!), completed: nil)
                
                if self.postsDataArray[indexPath.row].medias!.isEmpty {
                    cell.hideImage()
                }
                else {
                    cell.showImage()
                    cell.imageViewPostImage.sd_setImage(with: URL(string: self.postsDataArray[indexPath.row].medias![0].cover_url!), completed: nil)
                }
                
                if let medic = self.postsDataArray[indexPath.row].medic {
                    cell.labelMedicPost.isHidden = false
                    cell.imageViewUserIcon.sd_setImage(with: URL(string: medic.cover_media!.image_url!), completed: nil)
                }
                else {
                    cell.labelMedicPost.isHidden = false
                    cell.labelMedicPost.text = "Post Usuario"
                    cell.imageViewUserIcon.sd_setImage(with: URL(string: self.postsDataArray[indexPath.row].user!.image_url!), completed: nil)
                }
                
                print("\(indexPath.section)")
                
                return cell
            }
            else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "EmptyTableViewCell") as! EmptyTableViewCell
                
                cell.labelMessage.text = "Este médico não possui nenhum post até o momento"
                
                return cell
            }
        }
        else {
            return UITableViewCell()
        }
    }
}

extension AdvancedSearchMedicInformation: ResultsTableViewDelegate,
                                          MedicPostsTableViewCellDelegate,
                                          UserPostsTableViewCellDelegate {
    func didPressResultsSeeMore(selected index: Int) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ResultsDetailsViewController") as! ResultsDetailsViewController
        vc.isModalInPresentation = true
        vc.resultsData = self.resultsDataArray[index]
        self.present(vc, animated: true, completion: nil)
    }
    
    func didPressMedicPostsReadMore(selected index: Int) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "MedicsPostsDetailsViewController") as! MedicsPostsDetailsViewController
        vc.isModalInPresentation = true
        vc.medicPostsData = self.articlesDataArray[index]
        self.present(vc, animated: true, completion: nil)
    }
    
    func didPressUserPostsReadMore(selected index: Int) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "UsersPostsDetailsViewController") as! UsersPostsDetailsViewController
        vc.isModalInPresentation = true
        vc.postsData = self.postsDataArray[index]
        self.postsDataArray[index].medic != nil ? (vc.isMedic = true) : (vc.isMedic = false)
        self.present(vc, animated: true, completion: nil)
    }
}


extension AdvancedSearchMedicInformation {
    @IBAction func segmentValueChanged(_ sender: RESegmentedControl) {
        print("selected segment index: \(sender.selectedSegmentIndex)")
        
        self.segmentIndex = sender.selectedSegmentIndex
        print(self.segmentIndex)
        
        UIView.transition(with: self.tableViewMedicInformations, duration: 1.2, options: .transitionCrossDissolve, animations: {
            self.tableViewMedicInformations.reloadData()
        }, completion: nil)
    }
    
    @objc func keyboardDidShow(notification: NSNotification) {
        guard let info = notification.userInfo,
              let keyboardFrameRect = info[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue
               else { return }

        let keyboardRect = keyboardFrameRect.cgRectValue
        let contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardRect.height, right: 0)
        tableViewMedicInformations.contentInset = contentInset
        tableViewMedicInformations.scrollIndicatorInsets = contentInset
    }
}
