
import UIKit
import SideMenu
import FBSDKLoginKit
import GoogleSignIn
import Kingfisher

class MenuViewController: BaseViewController {
    @IBOutlet weak var menuTable: UITableView!
    
    var menuName = ["Editar Perfil", "Meus Resultados", "Quem Somos", "Sair"]
    var app: AppDelegate {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        return appDelegate
    }
    
    let imageArray = ["icones menu_user", "icones menu_heartbeat", "icones menu_calendario","icones menu_clock","icones menu_map","icones menu_headset-for-call-service","icones menu_idea","icones menu_phone","icones menu_exit"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        menuTable.rowHeight =  UITableView.automaticDimension
        menuTable.estimatedRowHeight = 100
        menuTable.reloadData()
    }
    
    func presentTestView()  {
        
    }
    
}

//Mark: - TableView Extention
extension MenuViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
        else {
            return menuName.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell: MenuTableViewCell = self.menuTable.dequeueReusableCell(withIdentifier: "cellMenu1") as! MenuTableViewCell
            
            if let url = URL.init(string: (usrerModelOBJ.user?.perfil_image)!) {
                let resource = ImageResource(downloadURL: url)
                
                cell.menuImage.kf.setImage(with: resource)
            }
            
            return cell
        }
        else {
            let cell: MenuTableViewCell = self.menuTable.dequeueReusableCell(withIdentifier: "cellMenu") as! MenuTableViewCell
            cell.populateMenuTable(name: menuName[indexPath.row],imageName:imageArray[indexPath.row])
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            self.performSegue(withIdentifier: "EditarPerfil", sender: self)
        case 1:
            self.naviGateToResultTudos()
        case 2:
            let whoWeAreView = WhoWeAreView()
            whoWeAreView.frame = CGRect(x: UIScreen.main.bounds.origin.x, y: UIScreen.main.bounds.origin.y, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
            UIApplication.shared.windows.first(where: { $0.isKeyWindow })?.addSubview(whoWeAreView)
        case 3:
            self.systemAlertForLogoutConfermation(header: App_Title, message: "Deseja realmente sair?", gallerybtnTitle: "Sim")
        default:
            print("default")
        }
        
    }
    
    func systemAlertForLogoutConfermation(header:String , message:String, gallerybtnTitle:String) {
        let alertController:UIAlertController = UIAlertController(title: header, message: message, preferredStyle: .alert)
        
        let actionLogOut = UIAlertAction(title: gallerybtnTitle, style: .default, handler: { (action) -> Void in
             self.logOut()
        })
        
        let cancelAction = UIAlertAction(title: "Nāo", style: .destructive, handler: { (action) -> Void in
            
        })
    
        alertController.addAction(actionLogOut)
        alertController.addAction(cancelAction)

        self.present(alertController, animated: true, completion: nil)
    }
    
}

extension MenuViewController: SideMenuNavigationControllerDelegate {
    
    func sideMenuWillAppear(menu: SideMenuNavigationController, animated: Bool) {
        print("SideMenu Appearing! (animated: \(animated))")
    }
    
    func sideMenuDidAppear(menu: SideMenuNavigationController, animated: Bool) {
        print("SideMenu Appeared! (animated: \(animated))")
    }
    
    func sideMenuWillDisappear(menu: SideMenuNavigationController, animated: Bool) {
        print("SideMenu Disappearing! (animated: \(animated))")
    }
     func sideMenuDidDisappear(menu: SideMenuNavigationController, animated: Bool) {
        print("SideMenu Disappeared! (animated: \(animated))")
    }
}

extension MenuViewController{
    func logOut() {
        switch UserDefaults.standard.string(forKey: "signInMethod") {
        case "gmail":
            AESCareAuth.signOutFromGmail()
            UserDefaults.standard.removeObject(forKey: "signInMethod")
            UserDefaults.standard.removeObject(forKey: "userData")
            UserDefaults.standard.removeObject(forKey: "autologin")
            self.navigateToLogin()
        case "facebook":
            AESCareAuth.signOutFromFacebook()
            UserDefaults.standard.removeObject(forKey: "signInMethod")
            UserDefaults.standard.removeObject(forKey: "userData")
            UserDefaults.standard.removeObject(forKey: "autologin")
            self.navigateToLogin()
        default:
            print("")
        }
    }
    
    func navigateToLogin(){
        if #available(iOS 13.0, *) {
            let vc = self.storyboard?.instantiateViewController(identifier: "LoginVC") as! LoginVC
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            // Fallback on earlier versions
            let vc = LoginVC.init(nibName: "LoginVC", bundle: nil)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func functioNavigateUserProfile(){
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: UserProfileEditVC.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }else{
                if #available(iOS 13.0, *) {
                    let vc = self.storyboard?.instantiateViewController(identifier: "UserProfileEditVC") as! UserProfileEditVC
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    // Fallback on earlier versions
                    let vc = UserProfileEditVC.init(nibName: "UserProfileEditVC", bundle: nil)
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                break
            }
        }
    }
    
    func naviGateToResultTudos(){
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: PublishResult.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }else{
                if #available(iOS 13.0, *) {
                    let vc = self.storyboard?.instantiateViewController(identifier: "PublishResult") as! PublishResult
                    vc.isFromSideMenu = true
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    // Fallback on earlier versions
                    let vc = PublishResult.init(nibName: "PublishResult", bundle: nil)
                    vc.isFromSideMenu = true
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                break
            }
        }
    }
    
    func naviGateToPresisadeAjuda(){
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: MinhasConsultasVC.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }else{
                if #available(iOS 13.0, *) {
                    let vc = self.storyboard?.instantiateViewController(identifier: "MinhasConsultasVC") as! MinhasConsultasVC
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    // Fallback on earlier versions
                    let vc = MinhasConsultasVC.init(nibName: "MinhasConsultasVC", bundle: nil)
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                break
            }
        }
    }
    
    func naviGateToMinhasConsultas(){
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: ConsultaDetailVC.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            } else {
                if #available(iOS 13.0, *) {
                    let vc = self.storyboard?.instantiateViewController(identifier: "ConsultaDetailVC") as! ConsultaDetailVC
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    // Fallback on earlier versions
                    let vc = ConsultaDetailVC.init(nibName: "ConsultaDetailVC", bundle: nil)
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                
                break
            }
        }
    }
}
