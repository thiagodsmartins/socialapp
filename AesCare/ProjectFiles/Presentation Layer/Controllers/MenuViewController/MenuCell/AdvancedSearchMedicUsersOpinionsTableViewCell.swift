//
//  AdvancedSearchMedicUsersOpinionsTableViewCell.swift
//  AesCare
//
//  Created by Thiago on 28/04/21.
//

import UIKit
import Cosmos

class AdvancedSearchMedicUsersOpinionsTableViewCell: UITableViewCell {
    @IBOutlet weak var imageViewIcon: UIImageView!
    @IBOutlet weak var labelUsername: UILabel!
    @IBOutlet weak var viewRating: CosmosView!
    @IBOutlet weak var textViewMessage: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setupViews()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
        
    private func setupViews() {
        self.selectionStyle = .none
        
        self.imageViewIcon.setBorder(width: 2, borderColor: .purple, cornerRadious: self.imageViewIcon.frame.height / 2)
        
        self.viewRating.settings.updateOnTouch = false
    }
}
