//
//  AdvancedSearchMedicAvaluationTableViewCell.swift
//  AesCare
//
//  Created by Thiago on 28/04/21.
//

import UIKit
import Cosmos

class AdvancedSearchMedicAvaluationTableViewCell: UITableViewCell {
    @IBOutlet weak var labelMessage: UILabel!
    @IBOutlet weak var labelRatingMessage: UILabel!
    @IBOutlet weak var viewRating: CosmosView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setupViews()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    private func setupViews() {
        self.selectionStyle = .none
        
        self.labelMessage.text = """
                                 Já passou por avaliação ou fez algum procedimento com este especialista? Escreva aqui seu comentário
                                 """
        
        self.labelRatingMessage.text = "De 1 a 5 estrelas, como você avalia o especialista?"
        self.labelRatingMessage.textColor = UIColor(red: 175/255, green: 68/255, blue: 217/255, alpha: 1.0)
        
        self.viewRating.text = "Qualidade"
        self.viewRating.settings.textColor = UIColor(red: 220/255, green: 220/255, blue: 220/255, alpha: 1.0)
        self.viewRating.didFinishTouchingCosmos = {
            rating in
            
            switch rating {
            case 1:
                self.viewRating.text = "Ruim"
                self.viewRating.settings.textColor = .red
            case 2:
                self.viewRating.text = "Bom"
                self.viewRating.settings.textColor = .blue
            case 3:
                self.viewRating.text = "Muito Bom"
                self.viewRating.settings.textColor = .green
            case 4:
                self.viewRating.text = "Ótimo"
                self.viewRating.settings.textColor = UIColor(red: 255, green: 102/255, blue: 0, alpha: 1.0)
            case 5:
                self.viewRating.text = "Excelente"
                self.viewRating.settings.textColor = UIColor(red: 255, green: 215/255, blue: 0, alpha: 1.0)
            default:
                print("Not necessary")
            }
        }
    }
    
}
