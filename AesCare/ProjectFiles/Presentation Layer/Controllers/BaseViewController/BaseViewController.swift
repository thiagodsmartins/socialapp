

import UIKit
import CoreData
import CoreLocation
import SideMenu
class BaseViewController: UIViewController,UITextFieldDelegate{
    @IBOutlet var customTabbar:CustomTabbarView!
    @IBOutlet var vwHeaderNoAction:CustomNavigationView!
    //var appDel = (UIApplication.shared.delegate as! AppDelegate)
    override func viewDidLoad() {
        super.viewDidLoad()
        
         LocalizationSystem.sharedInstance.setLanguage(languageCode: "pt_BR")
        
        
        /*
         let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
         statusBarView.backgroundColor = postsUIColor(named: "AppPurpleColor")
         view.addSubview(statusBarView)
         */
        // Do any additional setup after loading the view.
    }
    /*
     override var preferredStatusBarStyle: UIStatusBarStyle{
     return .lightContent
     }*/
    func backNavigation() {
        appDel.topViewControllerWithRootViewController(rootViewController: UIApplication.shared.windows.first?.rootViewController)?.view.endEditing(true)
        appDel.topViewControllerWithRootViewController(rootViewController: UIApplication.shared.windows.first?.rootViewController)?.navigationController?.popViewController(animated: true)
    }
    func backByToast(message:String) {
        
        backNavigation()
        if (message.count != 0) {
            UtilityClass.tosta(message: message, duration: 2.0, vc: appDel.topViewControllerWithRootViewController(rootViewController: UIApplication.shared.windows.first?.rootViewController) ?? self)
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
        
    }
}
///Mark: SearchViewDelegate Work
extension BaseViewController:PopupDoctorSelectionDelegateFromNavigation{
    func didSelectDoctorPressedForSearch(_ selectedDoctor: Medics) {
        if #available(iOS 13.0, *) {
            let vc = self.storyboard?.instantiateViewController(identifier: "DoctorDetailsVC") as! DoctorDetailsVC
            vc.medicModel = selectedDoctor
            vc.medicId = selectedDoctor.medic_id
            vc.cityID = selectedDoctor.city_id ?? 0
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            let vc = DoctorDetailsVC.init(nibName: "DoctorDetailsVC", bundle: nil)
            vc.medicModel = selectedDoctor
            vc.medicId = selectedDoctor.medic_id
            vc.cityID = selectedDoctor.city_id ?? 0
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func searchByDoctorPressedFromNavigation(_ sender: Any) {
        let optionView = SearchByDoctorNamePopUp()
        
        optionView.frame = self.view.frame
        
        optionView.tag = 1000
        optionView.basicSetup()
        optionView.fadeIn()
        optionView.delegateNavDocSearch = self
      
        if #available(iOS 13.0, *) {
            let keyWindow = UIApplication.shared.connectedScenes
                .filter({$0.activationState == .foregroundActive})
                .map({$0 as? UIWindowScene})
                .compactMap({$0})
                .first?.windows
                .filter({$0.isKeyWindow}).first
            keyWindow?.addSubview(optionView)
            keyWindow?.bringSubviewToFront(optionView)
        } else {
            UIApplication.shared.keyWindow?.addSubview(optionView)
            UIApplication.shared.keyWindow?.bringSubviewToFront(optionView)
        }
        
    }
}

extension BaseViewController:CustomTabbarDelegate{
    func GetSelectedIndex(Index: Int) {
        print(Index)
        if(Index == 1){
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: DashBoardVC.self) {
                    self.navigationController!.popToViewController(controller, animated: true)
                    break
                }else{
                    if #available(iOS 13.0, *) {
                        let vc = self.storyboard?.instantiateViewController(identifier: "DashBoardVC") as! DashBoardVC
                        self.navigationController?.pushViewController(vc, animated: true)
                    } else {
                        // Fallback on earlier versions
                        let vc = DashBoardVC.init(nibName: "DashBoardVC", bundle: nil)
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    break
                }
            }
        }
        else if(Index == 2){
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: PublishResult.self) {
                    self.navigationController!.popToViewController(controller, animated: true)
                    break
                }else{
                    if #available(iOS 13.0, *) {
                        let vc = self.storyboard?.instantiateViewController(identifier: "PublishResult") as! PublishResult
                        self.navigationController?.pushViewController(vc, animated: true)
                    } else {
                        // Fallback on earlier versions
                        let vc = PublishResult.init(nibName: "PublishResult", bundle: nil)
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    break
                }
            }
        }else if(Index == 3){
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: SearchCityList.self) {
                    self.navigationController!.popToViewController(controller, animated: true)
                    break
                }else{
                    if #available(iOS 13.0, *) {
                        let vc = self.storyboard?.instantiateViewController(identifier: "SearchCityList") as! SearchCityList
                        self.navigationController?.pushViewController(vc, animated: true)
                    } else {
                        // Fallback on earlier versions
                        let vc = SearchCityList.init(nibName: "SearchCityList", bundle: nil)
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    break
                }
            }
        }else if(Index == 4){
            for controller in self.navigationController!.viewControllers as Array {
                            if controller.isKind(of: ChatListVC.self) {
                                self.navigationController!.popToViewController(controller, animated: true)
                                break
                            }else{
                                if #available(iOS 13.0, *) {
                                    let vc = self.storyboard?.instantiateViewController(identifier: "ChatListVC") as! ChatListVC
                                    self.navigationController?.pushViewController(vc, animated: true)
                                } else {
                                    // Fallback on earlier versions
                                    let vc = ChatListVC.init(nibName: "ChatListVC", bundle: nil)
                                    self.navigationController?.pushViewController(vc, animated: true)
                                }
                                break
                            }
                        }
        }
        else if(Index == 5){
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: UserProfileVC.self) {
                    self.navigationController!.popToViewController(controller, animated: true)
                    break
                }else{
                    if #available(iOS 13.0, *) {
                        let vc = self.storyboard?.instantiateViewController(identifier: "UserProfileVC") as! UserProfileVC
                        self.navigationController?.pushViewController(vc, animated: true)
                    } else {
                        // Fallback on earlier versions
                        let vc = UserProfileVC.init(nibName: "UserProfileVC", bundle: nil)
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    break
                }
            }
        }
    }
    func navigateToHome(){
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: DashBoardVC.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }else{
                if #available(iOS 13.0, *) {
                    let vc = self.storyboard?.instantiateViewController(identifier: "DashBoardVC") as! DashBoardVC
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    // Fallback on earlier versions
                    let vc = DashBoardVC.init(nibName: "DashBoardVC", bundle: nil)
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                break
            }
        }
    }
}
extension BaseViewController{
    @IBAction func openSideMenuFromParent(_ sender: Any) {
        openSidePanel()
    }
    func openSidePanel(){
        appDel.topViewControllerWithRootViewController(rootViewController: UIApplication.shared.windows.first?.rootViewController)?.view.endEditing(true)
        
        SideMenuManager.default.leftMenuNavigationController?.settings.presentationStyle = .menuSlideIn
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let menu = storyboard.instantiateViewController(withIdentifier: "SideMenuNavigationController") as! SideMenuNavigationController
        menu.leftSide = true
        
        UIApplication.shared.windows.first?.rootViewController?.present(menu, animated: true, completion: nil)
    }
}
