//
//  StartingPageWelcomeViewController.swift
//  AesCare
//
//  Created by Thiago on 13/04/21.
//

import Foundation
import UIKit

class StartingPageWelcomeViewController: UIViewController {
    var imageViewLogo: UIImageView!
    var labelMessage: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupViews()
        
    }
    
    override func viewDidLayoutSubviews() {
        self.labelMessage.font.withSize(40.0)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.animateView()
    }
    
    private func setupViews() {
        self.imageViewLogo = UIImageView()
        self.labelMessage = UILabel()
        
        self.imageViewLogo.image = UIImage(named: "aescarewelcome-logo")
        self.imageViewLogo.contentMode = .scaleAspectFill
        self.imageViewLogo.alpha = 0
        self.imageViewLogo.translatesAutoresizingMaskIntoConstraints = false
        self.imageViewLogo.isHidden = true
        
        self.labelMessage.text = "Bem-Vindo(a)"
        self.labelMessage.numberOfLines = 0
        self.labelMessage.textColor = .white
        self.labelMessage.textAlignment = .center
        self.labelMessage.adjustsFontSizeToFitWidth = true
        self.labelMessage.alpha = 0
        self.labelMessage.translatesAutoresizingMaskIntoConstraints = false
        self.labelMessage.isHidden = true
        
        self.view.addSubview(self.imageViewLogo)
        self.view.addSubview(self.labelMessage)
        
        self.setupContraints()
    }
    
    private func setupContraints() {
        self.imageViewLogo.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        self.imageViewLogo.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
        self.imageViewLogo.heightAnchor.constraint(equalToConstant: (self.view.frame.height / 2)).isActive = true
        self.imageViewLogo.widthAnchor.constraint(equalToConstant: (self.view.frame.width / 2)).isActive = true
        
        self.labelMessage.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        self.labelMessage.widthAnchor.constraint(equalToConstant: self.view.frame.width / 2).isActive = true
        self.labelMessage.heightAnchor.constraint(equalToConstant: 50).isActive = true
        self.labelMessage.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -40).isActive = true
    }
    
    private func gradientEffect() {
        let gradient = CAGradientLayer()
        gradient.frame = self.view.bounds
        gradient.colors = [UIColor(red: 168/255, green: 72/255, blue: 217/255, alpha: 1.0).cgColor, UIColor(red: 105/255, green: 108/255, blue: 220/255, alpha: 1.0).cgColor]
        gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        
        self.view.layer.addSublayer(gradient)
    }
    
    private func animateView() {
        UIView.animate(withDuration: 3.0, delay: 1.0, options: .curveEaseOut, animations: {
            self.gradientEffect()
        }, completion: nil)
        
        UIView.animate(withDuration: 3.0, delay: 1.0, options: .curveEaseOut, animations: {
            self.view.addSubview(self.imageViewLogo)
            self.imageViewLogo.alpha = 1
            self.imageViewLogo.isHidden = false
        }, completion: nil)
        
        UIView.animate(withDuration: 3.0, delay: 1.0, options: .curveEaseOut, animations: {
            self.view.addSubview(self.labelMessage)
            self.labelMessage.alpha = 1
            self.labelMessage.isHidden = false
        }, completion: nil)
    }
}
