//
//  RecoverPasswordVC.swift
//  AesCare
//
//  Created by Gali Srikanth on 09/08/20.
//

import UIKit

class RecoverPasswordVC: BaseViewController {
    
    @IBOutlet weak var txtConfPwd: UITextField!
    @IBOutlet weak var txtPwd: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        vwHeaderNoAction.lableHeader.text = "Esqueceu sua senha?"
        vwHeaderNoAction.lableSubHeader.text = "Crie sua nova senha"
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func navigateNext(_ sender: UIButton) {
        //if(isValidAllData()){
            navigateToNewLoginByPwdVerification()
        //}
    }
    func navigateToNewLoginByPwdVerification(){
        
        if #available(iOS 13.0, *) {
            let vc = self.storyboard?.instantiateViewController(identifier: "LoginByNewPwdVC") as! LoginByNewPwdVC
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            // Fallback on earlier versions
            let vc = LoginByNewPwdVC.init(nibName: "LoginByNewPwdVC", bundle: nil)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}
///
///Mark: validation
///
extension RecoverPasswordVC {
    func isValidAllData() -> Bool {
        guard !(txtPwd.text?.isEmpty ?? true) else {
            showAlertMessage(title: "Ooops", message: ALERT_MESSAGE_PASSWORD_BLANK, vc: self)
            return false
        }
        guard txtPwd.text ==  txtConfPwd.text else {
            showAlertMessage(title: "Ooops", message: ALERT_MESSAGE_PASS_NOT_MATCH, vc: self)
            return false
        }
         return true
    }
    //
}
