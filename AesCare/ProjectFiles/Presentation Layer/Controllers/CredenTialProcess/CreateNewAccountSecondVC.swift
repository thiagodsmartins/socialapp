//
//  CreateNewAccountSecondVC.swift
//  AesCare
//
//  Created by Gali Srikanth on 10/08/20.
//

import UIKit
import FBSDKLoginKit
import FBSDKCoreKit
import GoogleSignIn
class CreateNewAccountSecondVC: BaseViewController {
    @IBOutlet weak var tblSignUp: UITableView!
    var arrSignUpInfo = [String]()
    var arrMyCityList : CityListModel!
    var selectedCityId = 0
    var cellIcon = ["nome-icon","user-icon","gender-icon","email-icon","email-icon","map-icon","senha-icon","senha-icon"]
    var arrGender = ["Feminino","Masculino","Outro"]
    var isSelectTerms = false
    var isSelectedGender = false
    var selectedRoleType:CellContent_SignupWith =  .NormalSignUp
    
   
    public enum CellContent_SignupWith:Int {
        
        case NormalSignUp = 0
        case FaceBookSignUp = 1
        case GoogleSignup = 2
        
        func serverPostValue() -> String {
            return "\(self.rawValue)"
        }
        public static func getRole_ID(_ str:String?) -> ROLE_ID? {
            guard let str = str, !(str.isEmpty) else { return nil }
            guard let val = Int(str) else { return nil }
            return ROLE_ID.init(rawValue: val)
        }
    }
    enum CellContent_SignupInfo:Int{
        case UserName = 0
        case YourUserCredentialName
        case Gender
        case GenderExtra
        case Email
        case YourCity
        case CreatePassword
        case ConfirmPassword
        case total
        func getPlaceholder () -> String {
            switch self {
            case .UserName:
                return "Seu nome"
            case .YourUserCredentialName:
                return "Seu nome de usuário"
            case .Gender:
                return "Seu gênero"
            case .GenderExtra:
                return ""
            case .Email:
                return "Seu e-mail"
            case .YourCity:
                return "Digite sua cidade"
            case .CreatePassword:
                return "Crie uma senha"
            case .ConfirmPassword:
                return "Confirme sua senha"
            default:
                return ""
            }
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //////set up default SignUP Process
        selectedRoleType =  .NormalSignUp
        
        vwHeaderNoAction.lableHeader.text = "Crie sua conta"
        vwHeaderNoAction.lableSubHeader.text = "Digite seus dados pessoais para criar sua conta"
        
        for _ in 0 ..< CellContent_SignupInfo.total.rawValue {
            arrSignUpInfo.append("")
        }
        arrSignUpInfo[CellContent_SignupInfo.GenderExtra.rawValue] = "0"
        
        //////get ciy list
        callfetchMyLocationsApi()
    }
    @IBAction func navigatePrevious(_ sender :UIButton){
        
        backNavigation()
        
    }
    //
    @IBAction func openTermsAndCondition(_ sender: UIButton) {
        //https://br.aescare.com/termos-de-uso
        guard let url = URL(string: "https://br.aescare.com/politica-de-privacidade") else {
            return
        }
        if(UIApplication.shared.canOpenURL(url as URL)){
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    @IBAction func createAccount(_ sender: Any) {
        if(isValidAllData()){
            
            if(selectedRoleType == .NormalSignUp){
                let url = (API.registartion.getURL()?.absoluteString ?? "")
                var param:[String:AnyObject] = [String:AnyObject]()
                param["contact"] = arrSignUpInfo[CellContent_SignupInfo.YourUserCredentialName.rawValue] as AnyObject
                param["username"] = arrSignUpInfo[CellContent_SignupInfo.UserName.rawValue] as AnyObject
                param["password"] = arrSignUpInfo[CellContent_SignupInfo.CreatePassword.rawValue] as AnyObject
                param["city_id"] = selectedCityId as AnyObject
                param["is_medic"] = is_medic as AnyObject
                param["city_text"] = arrSignUpInfo[CellContent_SignupInfo.YourCity.rawValue] as AnyObject
                var gender = ""
                gender =  arrSignUpInfo[CellContent_SignupInfo.GenderExtra.rawValue] == "0" ? "F" : (arrSignUpInfo[CellContent_SignupInfo.GenderExtra.rawValue] == "1" ? "M" : "")
                
                
                param["gender"] = gender as AnyObject
                
                
                callRegistrationApi(url: url, param: param)
                
            }
         }
    }
}
extension CreateNewAccountSecondVC : UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        3
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? ((isSelectedGender) ? CellContent_SignupInfo.total.rawValue : (CellContent_SignupInfo.total.rawValue - 1)) : 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height = 150
        if(indexPath.section == 0){
            if(indexPath.row == CellContent_SignupInfo.GenderExtra.rawValue && !isSelectedGender){
                height = 0
            }else{
                height = (UIDevice.current.iPad ?  100 :70)
            }
        }else if(indexPath.section == 1){
            height = (UIDevice.current.iPad ?  100 :70)
        }
        return CGFloat(height)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section == 0){
            if (indexPath.row == CellContent_SignupInfo.GenderExtra.rawValue && isSelectedGender){
                let cell: CommonCellTableViewCell = self.tblSignUp.dequeueReusableCell(withIdentifier: "CommonCellTableViewCell4") as! CommonCellTableViewCell
            
                cell.btnInstance.setImage(UIImage(named: "unselectCircle"), for: .normal)
                cell.btnInstance2.setImage(UIImage(named: "unselectCircle"), for: .normal)
                cell.btnInstance3.setImage(UIImage(named: "unselectCircle"), for: .normal)
                if let index = Int(arrSignUpInfo[indexPath.row]){
                 
                    if(index == 0){
                    cell.btnInstance.setImage(UIImage(named: "selectCircle"), for: .normal)
                    }else if(index == 1){
                        cell.btnInstance2.setImage(UIImage(named: "selectCircle"), for: .normal)
                    }else{
                       cell.btnInstance3.setImage(UIImage(named: "selectCircle"), for: .normal)
                    }
                }
                
                cell.btnInstance.tag = 0
                cell.btnInstance2.tag = 1
                cell.btnInstance3.tag = 2
                
                cell.btnInstance.addTarget(self, action: #selector(self.selectGenderOption(_:)), for: .touchUpInside)
                cell.btnInstance2.addTarget(self, action: #selector(self.selectGenderOption(_:)), for: .touchUpInside)
                cell.btnInstance3.addTarget(self, action: #selector(self.selectGenderOption(_:)), for: .touchUpInside)
                
                 cell.selectionStyle = .none
                 cell.backgroundColor = UIColor.clear
                 cell.contentView.backgroundColor = UIColor.clear
                 
                 return cell
            }else{
                let cell: CommonCellTableViewCell = self.tblSignUp.dequeueReusableCell(withIdentifier: "CommonCellTableViewCell1") as! CommonCellTableViewCell
                 
                 let type = CellContent_SignupInfo.init(rawValue: indexPath.row)
                 cell.cellSelectionText.placeholder = type?.getPlaceholder()
                 cell.cellSelectionText.text = arrSignUpInfo [indexPath.row]
                 cell.cellSelectionText.tag = indexPath.row
                 cell.cellSelectionText.delegate = self
                 
                 cell.cellSelectionText.isSecureTextEntry = false
                 cell.cellSelectionText.isUserInteractionEnabled = true
                
               
                 if(indexPath.row == CellContent_SignupInfo.CreatePassword.rawValue || indexPath.row == CellContent_SignupInfo.ConfirmPassword.rawValue ){
                     cell.cellSelectionText.isSecureTextEntry = true
                 }
                 
                 cell.cellSelectionText.addTarget(self, action: #selector(self.textFieldValueChange(_:)), for: .editingChanged)
                 
                 cell.imgContent.image = UIImage(named:cellIcon[indexPath.row])
                
                cell.btnInstance.isHidden = true
                
                if(indexPath.row == CellContent_SignupInfo.Gender.rawValue ||
                    indexPath.row == CellContent_SignupInfo.YourCity.rawValue){
                    cell.cellSelectionText.isUserInteractionEnabled = false
                    cell.btnInstance.tag = indexPath.row
                    cell.btnInstance.isHidden = false
                    cell.btnInstance.addTarget(self, action: #selector(self.selectGender(_:)), for: .touchUpInside)
                }
                 cell.selectionStyle = .none
                 cell.backgroundColor = UIColor.clear
                 cell.contentView.backgroundColor = UIColor.clear
                 
                 return cell
            }
            
        }
        else if(indexPath.section == 1){
            let cell: CommonCellTableViewCell = self.tblSignUp.dequeueReusableCell(withIdentifier: "CommonCellTableViewCell2") as! CommonCellTableViewCell
            
            let image = isSelectTerms ? UIImage(named: "checked_box") : UIImage(named: "unchecked_box")
            cell.btnInstance.setImage(image, for: .normal)
            cell.btnInstance.addTarget(self, action: #selector(btnTermsAndConditionPressed(_:)), for: .touchUpInside)
            /**Eu aceito os Termos de Uso e Privacidade do AesCare*/
            ////set color to a specific string in terms
            
            let attributedString1 = NSMutableAttributedString(string:"Eu aceito os ")
            let attributedString2 = NSMutableAttributedString(string:"Termos de Uso e")
            attributedString2.addAttribute(.link, value: termsAndConditionLink, range: NSRange(location: 0, length: attributedString2.length))
             attributedString2.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(named: "AppPurpleColor")! , range:NSRange(location: 0, length: attributedString2.length))
            let attributedString3 = NSMutableAttributedString(string:"  ")
            let attributedString4 = NSMutableAttributedString(string:"Privacidade do")
                       attributedString4.addAttribute(.link, value: privacyPolicyLink, range: NSRange(location: 0, length: attributedString4.length))
             attributedString4.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(named: "AppPurpleColor")! , range:NSRange(location: 0, length: attributedString4.length))
            let attributedString5 = NSMutableAttributedString(string:" AesCare")
       let mutableAttributedString = NSMutableAttributedString()
            mutableAttributedString.append(attributedString1)
            mutableAttributedString.append(attributedString2)
            mutableAttributedString.append(attributedString3)
            mutableAttributedString.append(attributedString4)
            mutableAttributedString.append(attributedString5)
            
            
           
            cell.txtVWInstance1.attributedText = mutableAttributedString
            
          
           //lblTermsAndCondition.isUserInteractionEnabled = true
            
            /*
            let main_string = "Eu aceito os Termos de Uso e Privacidade do AesCare"
            let string_to_color = "Termos de Uso e Privacidade"

            let range = (main_string as NSString).range(of: string_to_color)
            let attributedString = NSMutableAttributedString(string:main_string)
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(named: "AppPurpleColor")! , range: range)
            cell.lblInfo.attributedText = attributedString
            */
            cell.selectionStyle = .none
            cell.backgroundColor = UIColor.clear
            cell.contentView.backgroundColor = UIColor.clear
            
            return cell
        }else{
            let cell: CommonCellTableViewCell = self.tblSignUp.dequeueReusableCell(withIdentifier: "CommonCellTableViewCell3") as! CommonCellTableViewCell
            
            cell.selectionStyle = .none
            cell.backgroundColor = UIColor.clear
            cell.contentView.backgroundColor = UIColor.clear
            return cell
        }
    }
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        UIApplication.shared.open(URL)
        return false
    }
 }

extension CreateNewAccountSecondVC {
    
    @objc func btnTermsAndConditionPressed(_ sender:UIButton){
        isSelectTerms = !isSelectTerms
        self.tblSignUp.reloadData()
    }
    @objc func selectGenderOption(_ sender:UIButton){
        arrSignUpInfo[CellContent_SignupInfo.GenderExtra.rawValue] = "\(sender.tag)"
        arrSignUpInfo[CellContent_SignupInfo.Gender.rawValue] = arrGender[sender.tag]
        
        self.tblSignUp.reloadData()
    }
    @objc func textFieldValueChange(_ txt: UITextField)  {
        if let str = txt.text, str.count > 0 {
            self.arrSignUpInfo[txt.tag] = str
        }else{
            self.arrSignUpInfo[txt.tag] = ""
        }
    }
}
///
///Mark: validation
///
extension CreateNewAccountSecondVC {
    func isValidAllData() -> Bool {
        guard !arrSignUpInfo[CellContent_SignupInfo.UserName.rawValue].isEmpty else {
            showAlertMessage(title: "Ooops", message: ALERT_MESSAGE_NAME_BLANK, vc: self)
            return false
        }
        guard !arrSignUpInfo[CellContent_SignupInfo.YourUserCredentialName.rawValue].isEmpty else {
            showAlertMessage(title: "Ooops", message: ALERT_MESSAGE_USERNAME_BLANK, vc: self)
            return false
        }
        if(!isSelectedGender){
            showAlertMessage(title: "Ooops", message: ALERT_MESSAGE_GENDER_BLANK, vc: self)
            return false
        }
        guard !arrSignUpInfo[CellContent_SignupInfo.Email.rawValue].isEmpty else {
            showAlertMessage(title: "Ooops", message: ALERT_MESSAGE_EMAIL_BLANK, vc: self)
            return false
        }
        guard MyBasics.validateEmail(enteredEmail: arrSignUpInfo[CellContent_SignupInfo.Email.rawValue]) else {
            showAlertMessage(title: "Ooops", message: ALERT_MESSAGE_EMAIL_INVALID, vc: self)
            return false
        }
        
        guard !arrSignUpInfo[CellContent_SignupInfo.YourCity.rawValue].isEmpty else {
            showAlertMessage(title: "Ooops", message: ALERT_MESSAGE_CITY_BLANK, vc: self)
            return false
        }
        guard !arrSignUpInfo[CellContent_SignupInfo.YourCity.rawValue].isEmpty else {
            showAlertMessage(title: "Ooops", message: ALERT_MESSAGE_CITY_BLANK, vc: self)
            return false
        }
        guard !arrSignUpInfo[CellContent_SignupInfo.CreatePassword.rawValue].isEmpty else {
            showAlertMessage(title: "Ooops", message: ALERT_MESSAGE_PASSWORD_BLANK, vc: self)
            return false
        }
        guard arrSignUpInfo[CellContent_SignupInfo.CreatePassword.rawValue] ==  arrSignUpInfo[CellContent_SignupInfo.ConfirmPassword.rawValue] else {
            showAlertMessage(title: "Ooops", message: ALERT_MESSAGE_PASS_NOT_MATCH, vc: self)
            return false
        }
        if(!isSelectTerms){
            showAlertMessage(title: "Ooops", message: ALERT_MESSAGE_NOSELECT_TERMS, vc: self)
                       return false
        }
        
        return true
    }
}

//Mark: Facebook Work

extension CreateNewAccountSecondVC {
    @IBAction func signUpWithFaceBookAccount(_ sender :UIButton){
        
        let loginManager = LoginManager()
        loginManager.logIn(
            permissions: [.publicProfile,.email],
            viewController: self
        ) { result in
            self.loginManagerDidComplete(result)
        }
    }
    func loginManagerDidComplete(_ result: LoginResult) {
        
        switch result {
        case .cancelled:
            showAlertMessage(title: "Ooops", message: "User cancelled login.", vc: self)
            
        case .failed(let error):
            showAlertMessage(title: "Ooops", message: "Login failed with error \(error)", vc: self)
            
        case .success( _, _, _):
            //print(token.userID)
            self.getFBUserData()
            //  PROJECT_CONSTANT.displayAlert(Header: "Login Success", MessageBody: "Login succeeded with granted permissions: \(grantedPermissions)", AllActions: ["OK":nil], Style: .alert)
        }
        
    }
    
    func getFBUserData(){
        if((AccessToken.current) != nil){
            
            var data_access_expiration_time = 0.0
            if let dataTemp = AccessToken.current?.dataAccessExpirationDate{//expirationDate{
                data_access_expiration_time = dataTemp.timeIntervalSince1970
            }
            
            var accessToken_str = ""
            if let token = AccessToken.current?.tokenString{
                accessToken_str = token
            }
            var graphDomain = ""
            if let dataTemp = AccessToken.current?.graphDomain{
                graphDomain = dataTemp
            }
            
            var expiresIn:AnyObject = 0 as AnyObject
            var userId = ""
            if let id = AccessToken.current?.userID{
                userId = id
            }
            
            let connection = GraphRequestConnection()
            let requestAuth = GraphRequest.init(graphPath: "oauth/access_token", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email",                                             "client_id": "303341034428408","grant_type" : "fb_exchange_token","fb_exchange_token":"\(accessToken_str)","client_secret" : "24614aa278a9ce2f72da03f67529b56d"])
            connection.add(requestAuth, completionHandler: { connectionData, result, error in
                //TODO: process me information
                if (error == nil){
                    let dict_data = result as! NSDictionary
                    print("ResultAuth:\(dict_data)")
                    if let dataTemp = dict_data["expires_in"]{
                        expiresIn = dataTemp as AnyObject
                    }
                    
                    self.setUpDictionary(accessToken: accessToken_str,
                                         userID: userId,
                                         expiresIn: expiresIn,
                                         signedRequest: "",
                                         graphDomain: graphDomain,
                                         data_access_expiration_time: data_access_expiration_time,
                                         status: "connected")
                }
            })
            connection.start()
            print(expiresIn)
        }
    }
    func setUpDictionary(accessToken:String,userID:String,expiresIn:AnyObject,signedRequest:String,graphDomain:String,data_access_expiration_time:Double,status:String) {
         
        var dict_outer:[String:AnyObject] = [String:AnyObject]()
        var dict_inner:[String:AnyObject] = [String:AnyObject]()
        
        dict_inner["userID"] = userID as AnyObject
        dict_inner["accessToken"] = accessToken as AnyObject
        dict_inner["graphDomain"] = graphDomain as AnyObject
        dict_inner["data_access_expiration_time"] = data_access_expiration_time  as AnyObject
        dict_inner["signedRequest"] = signedRequest as AnyObject
        dict_inner["expiresIn"] = expiresIn  as AnyObject
        
        dict_outer["authResponse"] = dict_inner as AnyObject
        dict_outer["status"] = "connected" as AnyObject
        
        print(dict_outer)
        
        let url = (API.userFacebookLogIn.getURL()?.absoluteString ?? "")
        self.callLoginByFacebookApi(url: url, param: dict_outer)
    }
    /*
    func getFBUserDataold(){
        
        var fbId : String = ""
        var fbEmail : String = ""
        var fbName : String = ""
        var fbPickUrl : String = ""
        
        if((AccessToken.current) != nil){
            GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    //everything works print the user data
                    print("Result111:\(String(describing: result)) "as Any)
                }
                let dict = result as! NSDictionary
                print("FB Email1st:\(dict)")
                
                fbId = dict["id"] as! String
                fbName = dict["name"] as! String
              
                fbEmail = dict["email"] as! String
                //get user picture url from dictionary
                fbPickUrl = (((dict["picture"] as? [String: Any])?["data"] as? [String:Any])?["url"] as? String)!
                
                //print("FB ID: \(fbId)\n FB Email:\(fbEmail) \n FbFName:\(fbName) \n FBProfileUrl:\(fbPickUrl)\n")
                
                
                /*
                ////CALL API BY LOGIN WITH FB
                self.selectedRoleType = .FaceBookSignUp
                
                self.arrSignUpInfo[CellContent_SignupInfo.UserName.rawValue] = "\(fbName)"
                self.arrSignUpInfo[CellContent_SignupInfo.YourUserCredentialName.rawValue] = "\(fbEmail)"
                self.arrSignUpInfo[CellContent_SignupInfo.Email.rawValue] = "\(fbEmail)"
               // self.selectedFbId = "\(fbId)"
                self.tblSignUp.reloadData()
                */
                /////Api Call For login
                let url = (API.userFacebookLogIn.getURL()?.absoluteString ?? "")
                var param:[String:AnyObject] = [String:AnyObject]()
                param["userId"] = fbId as AnyObject
                param["email"] = fbEmail as AnyObject
                self.callLoginByFacebookApi(url: url, param: param)
            })
        }
        
    }*/
}
///
//Mark : SignUp With Google
///
extension CreateNewAccountSecondVC:GIDSignInDelegate{
    @IBAction func signUpWithGoogleAccount(_ sender :UIButton){
        ////INITIAL SETUP
        GIDSignIn.sharedInstance()?.presentingViewController = self
        // Automatically sign in the user.
        //GIDSignIn.sharedInstance()?.restorePreviousSignIn()
        
        ////CALL FOR SIGN IN
        GIDSignIn.sharedInstance()?.delegate = self
        GIDSignIn.sharedInstance()?.signIn()
    }
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if let error = error {
            if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
                print("The user has not signed in before or they have since signed out.")
            } else {
                print("\(error.localizedDescription)")
            }
            return
        }
        // Perform any operations on signed in user here.
        let Google_userId = user.userID                  // For client-side use only!
        //let idToken = user.authentication.idToken // Safe to send to the server
        //let fullName = user.profile.name
        //let givenName = user.profile.givenName
        //let familyName = user.profile.familyName
        let email = user.profile.email
        
        
        
        /////Api Call For login
        let url = (API.userGoogleLogIn.getURL()?.absoluteString ?? "")
        var param:[String:AnyObject] = [String:AnyObject]()
        param["userId"] = Google_userId as AnyObject
        param["email"] = email as AnyObject
        callLoginByGoogleApi(url: url, param: param)
        /*
         self.arrSignUpInfo[CellContent_SignupInfo.UserName.rawValue] = fullName ?? ""
         self.arrSignUpInfo[CellContent_SignupInfo.YourUserCredentialName.rawValue] = email ?? ""
         self.arrSignUpInfo[CellContent_SignupInfo.Email.rawValue] = email ?? ""
         self.selectedRoleType = .GoogleSignup
         self.tblSignUp.reloadData()
         */
    }
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
      // Perform any operations when the user disconnects from app here.
      // ...
        showAlertMessage(title: "Ooops", message: "Login failed with error \(String(describing: error))", vc: self)
    }
}

//
///Mark:gender and City WORK
////
extension CreateNewAccountSecondVC: CustomListDelegate{
    func GetSelectedPickerItemIndex(Index: Int) {
        arrSignUpInfo[CellContent_SignupInfo.YourCity.rawValue] = self.arrMyCityList.city_list?[Index].name ?? ""
        selectedCityId = self.arrMyCityList.city_list?[Index].city_id ?? 0
         self.tblSignUp.reloadData()
    }
    
    
    @objc func selectGender(_ sender:UIButton){
        if(sender.tag == CellContent_SignupInfo.YourCity.rawValue){
            
            let optionView = CountrySearchPopUp()
                    
                    optionView.frame = self.view.frame
                   
                    optionView.tag = 1000
                    optionView.basicSetup()
                    optionView.fadeIn()
                    optionView.delegate = self
                    optionView.isSearchforCountry = false
                    self.view.bringSubviewToFront(optionView)
                    self.view.addSubview(optionView)
            /*
            if let _ = self.arrMyCityList,let cityList = self.arrMyCityList.city_list,cityList.count != 0{
                
                var arrCity = [String]()
                for (_,obj) in cityList.enumerated(){
                    arrCity.append(obj.name!)
                }
                MyBasics.showListDropDown(Items: arrCity, ParentViewC: self)
            }  else{
                UtilityClass.tosta(message: "No city downloaded yet", duration: 2.0, vc: self)
            }*/
            
        }else{
            isSelectedGender = !isSelectedGender
        }
         self.tblSignUp.reloadData()
    }
}
extension CreateNewAccountSecondVC : PopupCountrySelectionDelegate{
    func didSelectCountryPressed(_ selectedCountry: ModelCountryList) {
        ///
    }
    func didSelectCityPressed(_ selectedCity: ModelSearchedCity){
        arrSignUpInfo[CellContent_SignupInfo.YourCity.rawValue] = selectedCity.name!
        selectedCityId = selectedCity.city_id!
        tblSignUp.reloadData()
    }
   
}
////
///Mark : Api
////
extension CreateNewAccountSecondVC {
     
    func callfetchMyLocationsApi() {
        
        let url = (API.fetchCityList.getURL()?.absoluteString ?? "")
        let operation =  WebServiceOperation.init(url,nil, .WEB_SERVICE_GET)
        
        operation.completionBlock =
            {
                DispatchQueue.main.async {
                    
                    if let dataUserDetails = operation.responseData, dataUserDetails.count > 0 {
                        do{
                            
                            self.arrMyCityList = try JSONDecoder().decode(CityListModel.self, from: dataUserDetails)
                             
                            self.tblSignUp.reloadData()
                        }catch _ {
                            showAlertMessage(title: App_Title, message: "Something went wrong", vc: self)
                        }
                    }
                    else{
                        showAlertMessage(title: App_Title, message: "Something went wrong", vc: self)
                    }
                }
        }
        
        appDel.operationQueue.addOperation(operation)
    }
    func callRegistrationApi(url:String,param:[String:AnyObject]) {
        print(url)
        let operation = WebServiceOperation.init(url, param, .WEB_SERVICE_MULTI_PART, nil)
        
        //DownLoadedProjectModel
        operation.completionBlock =
            {
                DispatchQueue.main.async {
                    
                    //print(operation.responseData?.dictionary?.prettyprintedJSON ?? "")
                    guard let dictResponse = operation.responseData?.dictionary, dictResponse.count > 0 else {
                        UtilityClass.tosta(message: "Something went wrong", duration: 2.0, vc: self)
                        return
                    }
                    
                    if let responceCode = dictResponse["code"] ,responceCode as! Int == 1 , let msg = dictResponse["message"] as? String{
                        if let dataUserDetails = dictResponse.data, dataUserDetails.count > 0 {
                            do{
                                let userModel = try JSONDecoder().decode(ModelUser.self, from: dataUserDetails)
                                let encoder = JSONEncoder()
                                if let encoded = try? encoder.encode(userModel) {
                                    let defaults = UserDefaults.standard
                                    defaults.set(encoded, forKey: "LOGGED_IN_USER_DATA")
                                }
                                
                                AESCareAnalytics.event(.Signup)
                                
                                self.navigateToDashBoard(msg: msg)
                                
                            }catch _ {
                                showAlertMessage(title: App_Title, message: "Something went wrong", vc: self)
                            }
                        }
                    }else if let msg = dictResponse["message"] as? String{
                        UtilityClass.tosta(message: msg, duration: 2.0, vc: self)
                        //showAlertMessage(title: App_Title, message: msg, vc: self)
                    }else{
                        showAlertMessage(title: App_Title, message: "Something went wrong", vc: self)
                    }
                    
                    DispatchQueue.main.async {
                        /////
                    }
                }
        }
        
        appDel.operationQueue.addOperation(operation)
        
    }
    
    func callLoginByGoogleApi(url:String,param:[String:AnyObject]) {
        
        let operation = WebServiceOperation.init(url, param, .WEB_SERVICE_POST, nil)
        
        operation.completionBlock =
            {
                DispatchQueue.main.async {
                    
                    //print(operation.responseData?.dictionary?.prettyprintedJSON ?? "")
                    guard let dictResponse = operation.responseData?.dictionary, dictResponse.count > 0 else {
                        UtilityClass.tosta(message: "Something went wrong", duration: 2.0, vc: self)
                        return
                    }
                    
                    if let responceCode = dictResponse["code"] ,responceCode as! Int == 1 , let msg = dictResponse["message"] as? String{
                        if let dataUserDetails = dictResponse.data, dataUserDetails.count > 0 {
                            do{
                                let userModel = try JSONDecoder().decode(ModelUser.self, from: dataUserDetails)
                                let encoder = JSONEncoder()
                                if let encoded = try? encoder.encode(userModel) {
                                    let defaults = UserDefaults.standard
                                    defaults.set(encoded, forKey: "LOGGED_IN_USER_DATA")
                                }
                                
                                self.navigateToDashBoard(msg: msg)
                                
                            }catch let e{
                                print(e.localizedDescription)
                                showAlertMessage(title: App_Title, message: "Something went wrong", vc: self)
                            }
                        }
                    }else if let msg = dictResponse["message"] as? String{
                        UtilityClass.tosta(message: msg, duration: 2.0, vc: self)
                        //showAlertMessage(title: App_Title, message: msg, vc: self)
                    }else{
                        showAlertMessage(title: App_Title, message: "Something went wrong", vc: self)
                    }
                    
                }
        }
        
        appDel.operationQueue.addOperation(operation)
        
    }
    func navigateToDashBoard(msg:String){
        if #available(iOS 13.0, *) {
            let vc = self.storyboard?.instantiateViewController(identifier: "PublicationTutorialVC") as! PublicationTutorialVC
            self.navigationController?.pushViewController(vc, animated: true)
            UtilityClass.tosta(message: msg, duration: 2.0, vc: vc)
        } else {
            // Fallback on earlier versions
            let vc = PublicationTutorialVC.init(nibName: "PublicationTutorialVC", bundle: nil)
            self.navigationController?.pushViewController(vc, animated: true)
            UtilityClass.tosta(message: msg, duration: 2.0, vc: vc)
        }
    }
    
    func callLoginByFacebookApi(url:String,param:[String:AnyObject]) {
        
        let operation = WebServiceOperation.init(url, param, .WEB_SERVICE_POST, nil)
        
        operation.completionBlock =
            {
                DispatchQueue.main.async {
                    
                    //print(operation.responseData?.dictionary?.prettyprintedJSON ?? "")
                    guard let dictResponse = operation.responseData?.dictionary, dictResponse.count > 0 else {
                        UtilityClass.tosta(message: "Something went wrong", duration: 2.0, vc: self)
                        return
                    }
                    
                    if let responceCode = dictResponse["code"] ,responceCode as! Int == 1 , let msg = dictResponse["message"] as? String{
                        if let dataUserDetails = dictResponse.data, dataUserDetails.count > 0 {
                            do{
                                
                                 let userModel = try JSONDecoder().decode(ModelUser.self, from: dataUserDetails)
                                 let encodedData = try NSKeyedArchiver.archivedData(withRootObject: userModel, requiringSecureCoding: false)
                                 
                                 UserDefaults.standard.set(encodedData, forKey: "LOGGED_IN_USER_DATA")
                                 self.navigateToDashBoard(msg: msg)
                                
                            }catch let e{
                                print(e.localizedDescription)
                                showAlertMessage(title: App_Title, message: "Something went wrong", vc: self)
                            }
                        }
                    }else if let msg = dictResponse["message"] as? String{
                        UtilityClass.tosta(message: msg, duration: 2.0, vc: self)
                        //showAlertMessage(title: App_Title, message: msg, vc: self)
                    }else{
                        showAlertMessage(title: App_Title, message: "Something went wrong", vc: self)
                    }
                    
                    DispatchQueue.main.async {
                        /////
                    }
                }
        }
        
        appDel.operationQueue.addOperation(operation)
        
    }
}
