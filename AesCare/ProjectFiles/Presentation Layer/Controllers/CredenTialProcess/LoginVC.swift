//
//  LoginVC.swift
//  AesCare
//
//  Created by Gali Srikanth on 08/08/20.
//

import UIKit
import FBSDKLoginKit
import FBSDKCoreKit
import GoogleSignIn
import CommonCrypto

class LoginVC: BaseViewController {

    @IBOutlet weak var switchAutoLogin: UISwitch!
    @IBOutlet weak var txtPwd: UITextField!
    @IBOutlet weak var txtUserName: UITextField!
    var GoogleId = ""
    var FaceBookId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        vwHeaderNoAction.lableHeader.text = "Login"
        vwHeaderNoAction.lableSubHeader.text = "Digite seus dados de acesso para entrar no app"
        
        self.switchAutoLogin.addTarget(self, action: #selector(isAutoLoginEnable(autoLogin:)), for: UIControl.Event.valueChanged)
        
        UserDefaults.standard.set(false, forKey: "autoLogin")
        UserDefaults.standard.set(true, forKey: "appLaunchedOnce")
    }

    @objc func isAutoLoginEnable(autoLogin: UISwitch) {
        if autoLogin.isOn {
            print("AUTO LOGIN ENABLE")
            UserDefaults.standard.set(true, forKey: "autoLogin")
        }
        else {
            print("AUTO LOGIN DISABLE")
            UserDefaults.standard.set(false, forKey: "autoLogin")
        }
    }
    
    @IBAction func navigateToForgetPassword(_ sender: UIButton) {
        
        if #available(iOS 13.0, *) {
            let vc = self.storyboard?.instantiateViewController(identifier: "ForgetPasswordVC") as! ForgetPasswordVC
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            // Fallback on earlier versions
            let vc = ForgetPasswordVC.init(nibName: "ForgetPasswordVC", bundle: nil)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func navigateToSignUp(_ sender: UIButton) {
        
        if #available(iOS 13.0, *) {
            let vc = self.storyboard?.instantiateViewController(identifier: "CreateNewAccountSecondVC") as! CreateNewAccountSecondVC
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            // Fallback on earlier versions
            let vc = CreateNewAccountSecondVC.init(nibName: "CreateNewAccountSecondVC", bundle: nil)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func navigateToDashBoard(_ sender: UIButton) {
        //navigateToDashBoard(msg: "")
        
        if(isValidAllData()){
            
            let num = Int(txtUserName.text!)
            if num != nil {
                //////login by phone number
                
            }
            else {
                ///// Login By email
                let url = (API.userNativeLoginByEmail.getURL()?.absoluteString ?? "")
                var param:[String:AnyObject] = [String:AnyObject]()
                param["username"] = txtUserName.text as AnyObject
                param["password"] = txtPwd.text as AnyObject
                
                callLoginByEmailApi(url: url, param: param)
            }
        }
    }
    
    func navigateToDashBoard(msg:String){
        if UserDefaults.standard.bool(forKey: "hasUserLoggedOnce") {
            if #available(iOS 13.0, *) {
                let vc = self.storyboard?.instantiateViewController(identifier: "DashBoardVC") as! DashBoardVC
                //self.present(vc, animated: true, completion: nil)
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                let vc = DashBoardVC.init(nibName: "PublicationTutorialVC", bundle: nil)
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        else {
            if #available(iOS 13.0, *) {
                let vc = self.storyboard?.instantiateViewController(identifier: "PublicationTutorialVC") as! PublicationTutorialVC
                //self.present(vc, animated: true, completion: nil)
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                let vc = DashBoardVC.init(nibName: "PublicationTutorialVC", bundle: nil)
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
}

extension LoginVC {
    func isValidAllData() -> Bool {
        guard !(txtUserName.text?.isEmpty ?? true) else {
            showAlertMessage(title: "Ooops", message: ALERT_MESSAGE_ALL_FIELD, vc: self)
            return false
        }
       guard !(txtPwd.text?.isEmpty ?? true) else {
           showAlertMessage(title: "Ooops", message: ALERT_MESSAGE_ALL_FIELD, vc: self)
           return false
       }
        return true
    }
}


extension LoginVC {
    @IBAction func loginWithGoogleAccount(_ sender :UIButton){
        AESCareAuth.signInWithGmail(self)
        
        AESCareAnalytics.event(.Login)
    }
    
    @IBAction func loginWithFaceBookAccount(_ sender :UIButton) {
        AESCareAuth.signInWithFacebook(self)
//        let app = UIApplication.shared
//        let appScheme = "fb://"
//        if app.canOpenURL(URL(string: appScheme)!) {
//            AESCareAuth.signInWithFacebook(self)
//        } else {
//            let alert = UIAlertController(title: "Alerta", message: "É necessario ter o app do facebook instalado para logar-se. Deseja ir para Appstore e instala-lo?", preferredStyle: UIAlertController.Style.alert)
//
//            alert.addAction(UIAlertAction(title: "Sim", style: UIAlertAction.Style.default, handler: {
//                action in
//
//                switch action.style {
//                case .default:
//                    if let url = URL(string: AESCareConstants.facebookAppstoreURL),
//                       UIApplication.shared.canOpenURL(url) {
//                        if #available(iOS 10.0, *) {
//                            UIApplication.shared.open(url, options: [:], completionHandler: nil)
//                        } else {
//                            UIApplication.shared.openURL(url)
//                        }
//                    }
//                case .cancel:
//                    print("")
//                case .destructive:
//                    print("")
//                @unknown default:
//                    print("")
//                }
//            }))
//
//            alert.addAction(UIAlertAction(title: "Nāo", style: UIAlertAction.Style.cancel, handler: nil))
//
//            self.present(alert, animated: true, completion: nil)
//        }
    }
    
    @IBAction func loginWithApple() {
        AESCareAuth.signInWithApple(self)
    }
}

extension LoginVC{
    func callLoginByEmailApi(url:String,param:[String:AnyObject]) {
        
        let operation = WebServiceOperation.init(url, param, .WEB_SERVICE_MULTI_PART, nil)
        
        operation.completionBlock =
            {
                DispatchQueue.main.async {
                    
                    //print(operation.responseData?.dictionary?.prettyprintedJSON ?? "")
                    guard let dictResponse = operation.responseData?.dictionary, dictResponse.count > 0 else {
                        UtilityClass.tosta(message: "Something went wrong", duration: 2.0, vc: self)
                        return
                    }
                    
                    if let responceCode = dictResponse["code"] ,responceCode as! Int == 1 , let msg = dictResponse["message"] as? String{
                        if let dataUserDetails = dictResponse.data, dataUserDetails.count > 0 {
                            do{
                                
                                let userModel = try JSONDecoder().decode(ModelUser.self, from: dataUserDetails)
                                let encoder = JSONEncoder()
                                if let encoded = try? encoder.encode(userModel) {
                                    let defaults = UserDefaults.standard
                                    defaults.set(encoded, forKey: "LOGGED_IN_USER_DATA")
                                }
                                
                                self.navigateToDashBoard(msg: msg)
                                
                            }catch let e{
                                print(e.localizedDescription)
                                showAlertMessage(title: App_Title, message: "Something went wrong", vc: self)
                            }
                        }
                    }else if let msg = dictResponse["message"] as? String{
                        UtilityClass.tosta(message: msg, duration: 2.0, vc: self)
                        //showAlertMessage(title: App_Title, message: msg, vc: self)
                    }else{
                        showAlertMessage(title: App_Title, message: "Something went wrong", vc: self)
                    }
                    
                    DispatchQueue.main.async {
                        /////
                    }
                }
        }
        
        appDel.operationQueue.addOperation(operation)
    }
}

