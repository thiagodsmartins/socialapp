//
//  ForgetPasswordVC.swift
//  AesCare
//
//  Created by Gali Srikanth on 09/08/20.
//

import UIKit

class ForgetPasswordVC: BaseViewController {

    @IBOutlet weak var txtUserName: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        vwHeaderNoAction.lableHeader.text = "Esqueceu sua senha?"
        vwHeaderNoAction.lableSubHeader.text = "Digite seu e-mail de acesso para recuperar sua senha"
        // Do any additional setup after loading the view.
    }
    

    
    @IBAction func navigateNext(_ sender: UIButton) {
        if(isValidAllData()){
            let url = (API.forgetPassword.getURL()?.absoluteString ?? "")
            var param:[String:AnyObject] = [String:AnyObject]()
            param["email"] = txtUserName.text as AnyObject
            
            callForgetPasswordApi(url: url, param: param)
            //navigateToPasswordNavigation()
        }
    }
    
}
///
///Mark: validation
///
extension ForgetPasswordVC {
    func isValidAllData() -> Bool {
        guard !(txtUserName.text?.isEmpty ?? true) else {
            showAlertMessage(title: "Ooops", message: ALERT_MESSAGE_EMAIL_BLANK, vc: self)
            return false
        }
      
        return true
    }
    //Código recebido
}
////
///Mark : Api
////
extension ForgetPasswordVC{
    
    func callForgetPasswordApi(url:String,param:[String:AnyObject]) {
        
        let operation = WebServiceOperation.init(url, param, .WEB_SERVICE_MULTI_PART, nil)
        
        operation.completionBlock =
            {
                DispatchQueue.main.async {
                    
                    //print(operation.responseData?.dictionary?.prettyprintedJSON ?? "")
                    guard let dictResponse = operation.responseData?.dictionary, dictResponse.count > 0 else {
                        UtilityClass.tosta(message: "Something went wrong", duration: 2.0, vc: self)
                        return
                    }
                    
                    
                    if let msg = dictResponse["message"] as? String{
                        self.backByToast(message:msg)
                    }else{
                        showAlertMessage(title: App_Title, message: "Something went wrong", vc: self)
                    }
                    
                }
        }
        
        appDel.operationQueue.addOperation(operation)
        
    }
    
}
