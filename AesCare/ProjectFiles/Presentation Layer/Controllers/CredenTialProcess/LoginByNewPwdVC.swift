//
//  LoginByNewPwdVC.swift
//  AesCare
//
//  Created by Gali Srikanth on 09/08/20.
//

import UIKit

class LoginByNewPwdVC: BaseViewController {
    @IBOutlet weak var txtPwd: UITextField!
    @IBOutlet weak var txtUserName: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        vwHeaderNoAction.lableHeader.text = "Nova senha criada!"
        vwHeaderNoAction.lableSubHeader.text = "Você já pode acessar o aplicativo"
        // Do any additional setup after loading the view.
    }
    
    
    
    @IBAction func navigateToDashBoard(_ sender: UIButton) {
        // if(isValidAllData()){
        if #available(iOS 13.0, *) {
            let vc = self.storyboard?.instantiateViewController(identifier: "DashBoardVC") as! DashBoardVC
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            // Fallback on earlier versions
            let vc = DashBoardVC.init(nibName: "DashBoardVC", bundle: nil)
            self.navigationController?.pushViewController(vc, animated: true)
        }
        // }
    }
}
///
///Mark: validation
///
extension LoginByNewPwdVC {
    func isValidAllData() -> Bool {
        guard !(txtUserName.text?.isEmpty ?? true) else {
            showAlertMessage(title: "Ooops", message: ALERT_MESSAGE_ALL_FIELD, vc: self)
            return false
        }
        guard !(txtPwd.text?.isEmpty ?? true) else {
            showAlertMessage(title: "Ooops", message: ALERT_MESSAGE_ALL_FIELD, vc: self)
            return false
        }
        return true
    }
}


