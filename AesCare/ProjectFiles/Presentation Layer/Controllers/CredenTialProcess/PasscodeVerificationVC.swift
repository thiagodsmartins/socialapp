//
//  PasscodeVerificationVC.swift
//  AesCare
//
//  Created by Gali Srikanth on 09/08/20.
//

import UIKit

class PasscodeVerificationVC: BaseViewController {
    
    @IBOutlet weak var txtOTP: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        vwHeaderNoAction.lableHeader.text = "Esqueceu sua senha?"
        vwHeaderNoAction.lableSubHeader.text = "Digite o código que você recebeu por e-mail"
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func navigateNext(_ sender: UIButton) {
        //if(isValidAllData()){
            navigateToRecoverPassword()
       // }
    }
    func navigateToRecoverPassword(){
        
        if #available(iOS 13.0, *) {
            let vc = self.storyboard?.instantiateViewController(identifier: "RecoverPasswordVC") as! RecoverPasswordVC
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            // Fallback on earlier versions
            let vc = RecoverPasswordVC.init(nibName: "RecoverPasswordVC", bundle: nil)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

///
///Mark: validation
///
extension PasscodeVerificationVC {
    func isValidAllData() -> Bool {
        guard !(txtOTP.text?.isEmpty ?? true) else {
            showAlertMessage(title: "Ooops", message: ALERT_OTP_BLANK, vc: self)
            return false
        }
         return true
    }
    //
}
