//
//  DoctorSearchCell.swift
//  AesCare
//
//  Created by Mahesh Mahalik on 24/09/20.
//

import UIKit

class DoctorSearchCell: UITableViewCell {

    @IBOutlet weak var lblCityName: UILabel!
    @IBOutlet weak var lblRate: UILabel!
    @IBOutlet weak var imgviewCell: designableImage!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
