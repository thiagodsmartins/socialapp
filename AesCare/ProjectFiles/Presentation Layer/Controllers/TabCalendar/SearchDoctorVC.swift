//
//  SearchDoctorVC.swift
//  AesCare
//
//  Created by Mahesh Mahalik on 24/09/20.
//

import UIKit

class SearchDoctorVC: UIViewController {
    @IBOutlet weak var lblFilter: UILabel!
    
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var heightLabelStatic: NSLayoutConstraint!
    @IBOutlet var lblTabText: [UILabel]!
    @IBOutlet var lblMarker: [UILabel]!
    @IBOutlet weak var textFieldSearch: UITextField!
    @IBOutlet weak var tableDoctor: UITableView!
    @IBOutlet weak var tableSearch: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnSearchAction(_ sender: UIButton) {
    }
    @IBAction func tabFavAction(_ sender: UIButton) {
        self.changeTabColor(index: sender.tag)
    }
    func changeTabColor(index:Int){
        for label in lblTabText{
            if label.tag == index{
                label.textColor = #colorLiteral(red: 0.6859999895, green: 0.2660000026, blue: 0.8500000238, alpha: 1)
            }else{
                 label.textColor = #colorLiteral(red: 0.5600000024, green: 0.6069999933, blue: 0.7009999752, alpha: 1)
            }
        }
        for label in lblMarker{
            if label.tag == index{
                label.backgroundColor = #colorLiteral(red: 0.6859999895, green: 0.2660000026, blue: 0.8500000238, alpha: 1)
            }else{
                label.backgroundColor = .clear
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
