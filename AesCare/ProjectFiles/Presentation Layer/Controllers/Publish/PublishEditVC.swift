//
//  PublishEditVC.swift
//  AesCare
//
//  Created by Gali Srikanth on 16/10/20.
//

import UIKit

class PublishEditVC: BaseViewController {
    @IBOutlet weak var vwNavigation: UIView!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var containerSearch: designableView!
    @IBOutlet weak var tblPubliscation: UITableView!
    var imagePicker = UIImagePickerController()
    var inputText = ""
    var myPost:Posts_Media!
    var gradientLayer: CAGradientLayer!
    var publication_multipartData : [MultiPartDataFormatStructure] = [MultiPartDataFormatStructure]()
    var imgArr = [UIImage]()
    override func viewDidLoad() {
        super.viewDidLoad()
        createGradientLayer()
        
        
        var body = " "
        if let mdObj = myPost , let bodyTemp = mdObj.body{
            body = bodyTemp
        }
        inputText = body
        
        
        if let mediaArray = myPost.medias{
            for(_,object) in mediaArray.enumerated(){
                do{
                    let imgData =  try Data(contentsOf: URL(string: object.image_url!)! as URL)
                    //if(imgData != nil){
                    /*
                        let timeInterval = NSDate().timeIntervalSince1970
                        let theFileName : String = "\(timeInterval).jpeg"
                        let multipartData = MultiPartDataFormatStructure.init(key: "attachments", mimeType: MultiPartDataFormatStructure.MIME_TYPE.image_jpeg, data: imgData, name: theFileName)
                        publication_multipartData.append(multipartData)
                        */
                        imgArr.append(UIImage(data: imgData)!)
                   // }
                }catch _{
                    
                }
            }
        }
       
        self.tblPubliscation.reloadData()
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        ///// Call Custom Tabbar
        tabbarUISetup()
        gradientLayer.frame = self.vwNavigation.bounds
    }
    func tabbarUISetup(){
        ///// Call Custom Tabbar
        customTabbar.myDelegate = self
        customTabbar.setup(activeFor:"Profile")
    }
    func createGradientLayer() {
        gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.vwNavigation.bounds
        gradientLayer.colors = [UIColor(named: "AppPurpleColor")!.cgColor, UIColor(named: "AppMoveColor")!.cgColor]
        self.vwNavigation.layer.addSublayer(gradientLayer)
        self.vwNavigation.bringSubviewToFront(self.btnMenu)
        self.vwNavigation.bringSubviewToFront(self.containerSearch)
    }
    @IBAction func backNavigationPressed(_ sender: Any) {
        backNavigation()
    }
}
//
//Mark: table work
//
extension PublishEditVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.row == 0){
            let cell: PublishMediaCell = self.tblPubliscation.dequeueReusableCell(withIdentifier: "PublishMediaCell") as! PublishMediaCell
            if let mdObj = myPost ,let url = URL(string:mdObj.publisher?.image_url ?? ""){
                cell.imgContent.kf.indicatorType = .activity
                cell.imgContent.kf.setImage(with: url)
                cell.imgContent.contentMode = .scaleAspectFill
            }
            
            cell.txtVWInstance1.text  = inputText
            cell.imgArr = imgArr
            cell.colVWImageAdd.reloadData()
            return cell
        } else {
            let cell: CommonCellTableViewCell = self.tblPubliscation.dequeueReusableCell(withIdentifier: "CommonCellTableViewCell1") as! CommonCellTableViewCell
            
            cell.btnInstance.tag = indexPath.row
            cell.btnInstance.addTarget(self, action: #selector(attachMedia(_:)), for: .touchUpInside)
            
            cell.btnInstance2.tag = indexPath.row
            cell.btnInstance2.addTarget(self, action: #selector(savePost(_:)), for: .touchUpInside)
            cell.btnInstance3.tag = indexPath.row
            cell.btnInstance3.addTarget(self, action: #selector(deletePost(_:)), for: .touchUpInside)
            return cell
        }
    }
    @objc func attachMedia(_ sender:UIButton){
       self.systemAlertofAttachmentForPublication(header: App_Title, message: "Escolha a sua foto")
    }
    @objc func savePost(_ sender:UIButton){
        
        let selectedPostKey = myPost.key
        var param:[String:AnyObject] = [String:AnyObject]()
        param["key"] = selectedPostKey as AnyObject
        param["status"] = "a" as AnyObject
        param["body"] = inputText as AnyObject
        
        self.callEditBodyForPublish(param: param, multipartData: publication_multipartData)
    }
    @objc func deletePost(_ sender:UIButton){
        let selectedPostKey = myPost.key
        var param:[String:String] = [String:String]()
        param["key"] = selectedPostKey 
        param["status"] = "d" as String
        self.deletePostApi(param: param)
    }
}

///
///Mark : Publish Save
///
extension PublishEditVC{
    func callEditBodyForPublish(param:[String:AnyObject],multipartData:[MultiPartDataFormatStructure]) {
        let getUrl :String = BaseUrl + "/post"
        
        let operation = WebServiceOperation.init(getUrl, param, .WEB_SERVICE_MULTI_PART, nil)
        
        operation.completionBlock =
            {
                DispatchQueue.main.async {
                    
                    //print(operation.responseData?.dictionary?.prettyprintedJSON ?? "")
                    guard let dictResponse = operation.responseData?.dictionary, dictResponse.count > 0 else {
                        //UtilityClass.tosta(message: "Something went wrong", duration: 2.0, vc: self)
                        return
                    }
                    
                    
                    if let responceCode = dictResponse["code"] ,responceCode as! Int == 1 , let post =  dictResponse["post"] as? [String : AnyObject] , let postkey = post["key"],let msg = dictResponse["message"] as? String {
                        
                        if(self.publication_multipartData.count == 0){
                            self.backByToast(message: msg)
                        }else{
                            var param1:[String:AnyObject] = [String:AnyObject]()
                            param1["post_key"] = postkey as AnyObject
                            
                            self.callPublishPhotoEdit(param: param1, multipartData: multipartData)
                        }
                    }else if let msg = dictResponse["message"] as? String{
                        UtilityClass.tosta(message: msg, duration: 2.0, vc: self)
                        
                    }else{
                        showAlertMessage(title: App_Title, message: "Something went wrong", vc: self)
                    }
                }
        }
        
        appDel.operationQueue.addOperation(operation)
        
    }
    
    func callPublishPhotoEdit(param:[String:AnyObject],multipartData:[MultiPartDataFormatStructure]) {
        let getUrl :String = BaseUrl + "/post/add-media"
        let operation = WebServiceOperation.init(getUrl, param, .WEB_SERVICE_MULTI_PART, multipartData)
        
        operation.completionBlock =
            {
                DispatchQueue.main.async {
                    
                    //print(operation.responseData?.dictionary?.prettyprintedJSON ?? "")
                    guard let dictResponse = operation.responseData?.dictionary, dictResponse.count > 0 else {
                        //UtilityClass.tosta(message: "Something went wrong", duration: 2.0, vc: self)
                        return
                    }
                    
                    if let responceCode = dictResponse["code"] ,responceCode as! Int == 1 , let msg = dictResponse["message"] as? String{
                        self.backByToast(message: msg)
                        
                    }else if let msg = dictResponse["message"] as? String{
                        UtilityClass.tosta(message: msg, duration: 2.0, vc: self)
                    }else{
                        showAlertMessage(title: App_Title, message: "Something went wrong", vc: self)
                    }
                    
                }
        }
        appDel.operationQueue.addOperation(operation)
    }
    
    func deletePostApi(param:[String:String]) {
        let url = (API.deletePost.getURL()?.absoluteString ?? "")
        let operation = WebServiceOperation.init(url, param as Dictionary<String, AnyObject>, .WEB_SERVICE_MULTI_PART, nil)
        operation.completionBlock =
            {
                DispatchQueue.main.async {
                    
                    print(operation.responseData?.dictionary?.prettyprintedJSON ?? "")
                    guard let dictResponse = operation.responseData?.dictionary, dictResponse.count > 0 else {
                        UtilityClass.tosta(message: "Something went wrong", duration: 2.0, vc: self)
                        return
                    }
                    if let responceCode = dictResponse["code"] ,responceCode as! Int == 1 , let msg = dictResponse["message"] as? String{
                        
                        UtilityClass.tosta(message: msg, duration: 2.0, vc: self)
                        self.backByToast(message: msg)
                    }else{
                        UtilityClass.tosta(message: "Something wentwrong", duration: 2.0, vc: self)
                    }
                    
                }
        }
        appDel.operationQueue.addOperation(operation)
    }
}
extension PublishEditVC:UITextViewDelegate{
    
    func encode(_ s: String) -> String {
        let data = s.data(using: .nonLossyASCII, allowLossyConversion: true)!
        return String(data: data, encoding: .utf8)!
    }
    func decode(_ s: String) -> String? {
        let data = s.data(using: .utf8)!
        return String(data: data, encoding: .nonLossyASCII)
    }
    func textViewDidChange(_ textView: UITextView) {
        
        inputText = self.encode(textView.text)
    }
}
///
/////mark: Upload publication content
////
extension PublishEditVC:UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
   
    ////alert to represent attachment option
    func systemAlertofAttachmentForPublication(header:String , message:String) {
        
        let alertController:UIAlertController = UIAlertController(title: header, message: message, preferredStyle: .alert)
        
        
        let actionVideoGallery = UIAlertAction(title: "Torar foto", style: .default, handler: { (action) -> Void in
            // Get TextFields text
            self.openCamera()
            
        })
        let actionImageGallery = UIAlertAction(title: "Escolher foto", style: .default, handler: { (action) -> Void in
            // Get TextFields text
            self.openPhotoGallary()
            
        })
        let cancelAction = UIAlertAction(title: "Cancelher foto", style: .destructive, handler: { (action) -> Void in
            
        })
        
        alertController.addAction(actionVideoGallery)
        alertController.addAction(actionImageGallery)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(.camera)){
            self.imagePicker.delegate = self
            self.imagePicker.sourceType = .camera
            self.imagePicker.allowsEditing = true
            UIApplication.shared.keyWindow?.rootViewController?.present(imagePicker, animated: true, completion: nil)
        }else{
            showAlertMessage(title: "Ooops", message: "unable to open Camera", vc: self)
        }
    }
    func openPhotoGallary(){
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            self.imagePicker.delegate = self
            self.imagePicker.allowsEditing = true
            self.imagePicker.sourceType = .savedPhotosAlbum//.photoLibrary
            self.imagePicker.mediaTypes = ["public.image"]
            
            UIApplication.shared.keyWindow?.rootViewController?.present(self.imagePicker, animated: true, completion: nil)
        } else {
            showAlertMessage(title: "Ooops", message: "unable to open PhotoAlbum", vc: self)
        }
        
    }
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)
        if let pickedimage = info[.editedImage] as? UIImage {
            let data = pickedimage.jpegData(compressionQuality: 1)
            let timeInterval = NSDate().timeIntervalSince1970
            let theFileName : String = "\(timeInterval).jpeg"
            let imageFileInfo = MultiPartDataFormatStructure.init(key: "attachments", mimeType: MultiPartDataFormatStructure.MIME_TYPE.image_jpeg, data: data, name: theFileName)
            self.publication_multipartData.append(imageFileInfo)
            self.imgArr.append(pickedimage)
            self.view.setNeedsLayout()
            self.tblPubliscation.reloadData()
            
        }else{
            showAlertMessage(title: "Ooops", message: "unable to select image", vc: self)
        }
        
        self.dismiss(animated: true, completion: nil)
        self.view.setNeedsLayout()
    }
    
}
