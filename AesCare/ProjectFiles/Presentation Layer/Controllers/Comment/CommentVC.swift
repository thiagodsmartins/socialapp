//
//  CommentVC.swift
//  AesCare
//
//  Created by Mahesh Mahalik on 21/10/20.
//

import UIKit
import SideMenu

class CommentVC: BaseViewController,UITableViewDataSource,UITableViewDelegate {
     @IBOutlet weak var txtViewChat: UITextView!
    var gradientLayer: CAGradientLayer!
       @IBOutlet weak var vwNavigation: UIView!
       @IBOutlet weak var btnMenu: UIButton!
       @IBOutlet weak var containerSearch: designableView!
       @IBOutlet weak var tableHomeDetails: UITableView!
    var review:Comments?
    var arraycomments:[Comments] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        createGradientLayer()

        self.callCommentPostAPI(relatedID: review?.comment_id ?? 0)
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        tableHomeDetails.dataSource = self
        tableHomeDetails.delegate  = self
        
        gradientLayer.frame = self.vwNavigation.bounds
    }
    func createGradientLayer() {
        gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.vwNavigation.bounds
        gradientLayer.colors = [UIColor(named: "AppPurpleColor")!.cgColor, UIColor(named: "AppMoveColor")!.cgColor]
        self.vwNavigation.layer.addSublayer(gradientLayer)
        self.vwNavigation.bringSubviewToFront(self.btnMenu)
        self.vwNavigation.bringSubviewToFront(self.containerSearch)
    }
    private func menuPressed() {
        
        appDel.topViewControllerWithRootViewController(rootViewController: UIApplication.shared.windows.first?.rootViewController)?.view.endEditing(true)
        
        SideMenuManager.default.leftMenuNavigationController?.settings.presentationStyle = .menuSlideIn
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let menu = storyboard.instantiateViewController(withIdentifier: "SideMenuNavigationController") as! SideMenuNavigationController
        menu.leftSide = true
        
        UIApplication.shared.windows.first?.rootViewController?.present(menu, animated: true, completion: nil)
        
    }
    @IBAction func openSideMenu(_ sender: Any) {
        self.view.endEditing(true)
        self.menuPressed()
    }
    @IBAction func btnBackAction(_ sender: UIButton) {
           self.navigationController?.popViewController(animated: true)
       }

    @IBAction func btnSendMessgaeAction(_ sender: Any) {
        self.view.endEditing(true)
        //self.addObserverForFireBase()
        if txtViewChat.text.count > 0{
            self.callPostAPI(relatedID: review?.comment_id ?? 0, content: txtViewChat.text)
        }
        
        
    }
    func callPostAPI(relatedID:Int,content:String) {
        
     let getUrl :String = API.commentOfcomment.getURL()?.absoluteString ?? ""
     var param :[String:AnyObject] = [:]
     param["comment_id"] = relatedID as AnyObject
        if content != ""{
             param["comment_content"] = content as AnyObject
        }
        let operation = WebServiceOperation.init(getUrl, param, .WEB_SERVICE_MULTI_PART, nil)
        
        operation.completionBlock =
            {
                DispatchQueue.main.async {
                    
                    print(operation.responseData?.dictionary?.prettyprintedJSON ?? "")
                    guard let dictResponse = operation.responseData?.dictionary, dictResponse.count > 0 else {
                        
                        return
                    }
                    
                    
                
                     if let dataUserDetails = dictResponse.data, dataUserDetails.count > 0 {
                         do{
                            if let result = dictResponse["comment"] as? [String:Any]{
                                let comment = try JSONDecoder().decode(Comments.self, from: result.data!)
                           
                                 self.arraycomments.append(comment)
                                self.txtViewChat.text = ""
                            
                            }
                             
                             //
                         }catch let e{
                             print(e.localizedDescription)
                             
                             //showAlertMessage(title: App_Title, message: "Something went wrong", vc: self)
                         }
                     }
                     
                    self.tableHomeDetails.reloadSections([1], with: .automatic)
                         
                     
                    
                }
            }
        
        appDel.operationQueue.addOperation(operation)
        
    }
    
    
    
    func callCommentPostAPI(relatedID:Int) {
        if #available(iOS 13.0, *) {
            let keyWindow = UIApplication.shared.connectedScenes
                .filter({$0.activationState == .foregroundActive})
                .map({$0 as? UIWindowScene})
                .compactMap({$0})
                .first?.windows
                .filter({$0.isKeyWindow}).first
            CustomActivityIndicator.sharedInstance.display(onView: keyWindow, done: {
                
            })
        } else {
            
            UIApplication.shared.keyWindow?.rootViewController?.view.endEditing(true)
            CustomActivityIndicator.sharedInstance.display(onView: UIApplication.shared.keyWindow, done: {
                
            })
        }
        
        
         let getUrl :String = BaseUrl + "/comments?related_id="+"\(relatedID)"+"&related_class=comments"
        if let urlString  = URL(string: getUrl){
            var request = URLRequest(url: urlString)
            //request.addValue("U4LsBbfYdKlYTB3kJYKpCKiG", forHTTPHeaderField: "aescare_session_key")
            request.httpMethod = "GET"
            URLSession.shared.dataTask(with: request) { data, response, error in
                //print(String(data: data!, encoding: .utf8)!)
                DispatchQueue.main.async {
                    CustomActivityIndicator.sharedInstance.hide {
                    }
                }
                if let dataTemp = data, dataTemp.count > 0 {
                    
                    do {
                        
                        self.arraycomments = try JSONDecoder().decode([Comments].self, from: dataTemp)
                       
                       
                        
                    } catch let error  {
                        print("Parsing Failed \(error.localizedDescription)")
                    }
                     DispatchQueue.main.async {
                    self.tableHomeDetails.reloadSections([1], with: .automatic)
                    }
                }
            }.resume()
        }
    }
    
    func callCommentPostAPI2(relatedID:Int) {
            
         let getUrl :String = BaseUrl + "/comments?related_id="+"\(relatedID)"+"&related_class=comments"
         //var param :[String:AnyObject] = [:]
         //param["related_id"] = relatedID as AnyObject
         
            // param["related_class"] = "posts" as AnyObject
        
            let operation = WebServiceOperation.init(getUrl, nil, .WEB_SERVICE_GET, nil)
            
            operation.completionBlock =
                {
                    DispatchQueue.main.async {
                        
                        print(operation.responseData?.dictionary?.prettyprintedJSON ?? "")
                        guard let dictResponse = operation.responseData?.dictionary, dictResponse.count > 0 else {
                            
                            return
                        }
                        
                        
                    
                         if let dataUserDetails = dictResponse.data, dataUserDetails.count > 0 {
                             do{
                               
                                self.arraycomments = try JSONDecoder().decode([Comments].self, from: dataUserDetails)
                                
                                
                                 
                                 //
                             }catch let e{
                                 print(e.localizedDescription)
                                 
                                 //showAlertMessage(title: App_Title, message: "Something went wrong", vc: self)
                             }
                         }
                         
                        self.tableHomeDetails.reloadSections([1], with: .automatic)
                             
                         
                        
                    }
                }
            
            appDel.operationQueue.addOperation(operation)
            
        }

}
extension CommentVC{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           if section == 0{
           return 1
           }else{
            return arraycomments.count
           }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell", for: indexPath) as! CategoryCell
            cell.lblTitle.text = review?.content ?? ""
            return cell
        }
    else {
       
       
            let comment = arraycomments[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewCell", for: indexPath) as! ReviewCell
            cell.lblTitle.text = comment.user?.username
            cell.lblComment.text = comment.content ?? ""
            if let url = URL(string: comment.user?.url_image ?? ""){
                cell.imgviewCell.kf.indicatorType = .activity
                cell.imgviewCell.kf.setImage(with: url)
            }
            cell.lblReplyCount.text = "\(comment.comments_count ?? 0) Responder"
            cell.lblCount.text = "\(comment.likes_count ?? 0) Curtir"
            cell.lblDatePost.text = comment.created_at
            cell.btnLike.tag = indexPath.row
            cell.btnReply.tag = indexPath.row
            return cell
        
        
    }
}
}
