//
//  DoctorDetailsVC.swift
//  AesCare
//
//  Created by Mahesh Mahalik on 25/09/20.
//

import UIKit

class DoctorDetailsVC: BaseViewController {
    
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblRate: UILabel!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var imgviewDoctor: UIImageView!
    @IBOutlet weak var lblNumberResultados: UILabel!
    @IBOutlet weak var lblNumberFacebook: UILabel!
    @IBOutlet weak var lblNumberAvailables: UILabel!
    @IBOutlet weak var lblNumberPost: UILabel!
    @IBOutlet var selectedArrayMarkerLabel: [UILabel]!
    var arrayListFirstSegment :[ResultsFirst] = []
    @IBOutlet weak var tableDoc: UITableView!
    @IBOutlet var viewTransArray: [UIView]!
    var medicId:Int?
    var selectedindex:Int = 0
    var cityID:Int = 0
    var medicModel:Medics?
    var arrayListFourthSegment :[Posts] = []
    var arrayListSecondSegment :[Opinions] = []
    var arrayListThirdSegment :[FacebookPost] = []
    var page:Page = Page()
    
    ///Navigation Work
    @IBOutlet weak var vwNavigation: UIView!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var containerSearch: designableView!
    var gradientLayer: CAGradientLayer!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.colorchangeSelectedTab(selectedIndex: selectedindex)
        self.prelOadData()
        self.callFirstSegmentApi(orderBy: "relevance", pageNumber: 1, near: cityID, limit: 2)
        self.callSecondSegmentApi()
        self.callThirdSegmentApi()
        self.callAllApiWithPage()
        createGradientLayer()
        // Do any additional setup after loading the view.
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        ///// Call Custom Tabbar
        tabbarUISetup()
        gradientLayer.frame = self.vwNavigation.bounds
    }
    func tabbarUISetup(){
        ///// Call Custom Tabbar
        customTabbar.myDelegate = self
        customTabbar.setup(activeFor:"Agendar")
    }
    func createGradientLayer() {
        gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.vwNavigation.bounds
        gradientLayer.colors = [UIColor(named: "AppPurpleColor")!.cgColor, UIColor(named: "AppMoveColor")!.cgColor]
        self.vwNavigation.layer.addSublayer(gradientLayer)
        self.vwNavigation.bringSubviewToFront(self.btnMenu)
        self.vwNavigation.bringSubviewToFront(self.containerSearch)
    }
    func prelOadData() {
        let inKm = (medicModel?.distance ?? 0) / 1000
        //print(medicModel?.distance ?? 0)
        lblDistance.text = "\(inKm) KM"
        lblRate.text = "\(medicModel?.evaluation_average ?? 0 )"
        lblNumberFacebook.text = "\(medicModel?.evaluations_facebook_count ?? 0 )"
        lblNumberResultados.text = "\(medicModel?.results_count ?? 0 )"
        lblNumberAvailables.text = "\(medicModel?.results_count ?? 0 )"
        lblNumberPost.text = "\(medicModel?.posts_count ?? 0 )"
        lblName.text = medicModel?.name ?? ""
        imgviewDoctor.kf.indicatorType = .activity
        if let url  = URL(string:medicModel?.cover_media?.cover_url ?? ""){
            imgviewDoctor.kf.setImage(with: url)
        }
        imgviewDoctor.layer.cornerRadius = self.imgviewDoctor.frame.size.height / 2
        imgviewDoctor.clipsToBounds = true
        
    }
    
    fileprivate func colorchangeSelectedTab(selectedIndex: Int) {
        for i in 0...selectedArrayMarkerLabel.count - 1{
            
            if selectedArrayMarkerLabel[i].tag == selectedIndex{
                selectedArrayMarkerLabel[i].textColor = #colorLiteral(red: 0.6859999895, green: 0.2660000026, blue: 0.8500000238, alpha: 1)
            }else{
                selectedArrayMarkerLabel[i].textColor = .clear
            }
        }
        
        for i in 0...viewTransArray.count - 1{
            
            if viewTransArray[i].tag == selectedIndex{
                viewTransArray[i].isHidden = true
            }else{
                viewTransArray[i].isHidden = false
            }
        }
    }
    
    @IBAction func btnSelectedtabAction(_ sender: UIButton) {
        colorchangeSelectedTab(selectedIndex:sender.tag)
        selectedindex = sender.tag
        self.tableDoc.reloadData()
    }
    @IBAction func btnNavigateForAppointmentAction(_ sender: UIButton) {
        
        if #available(iOS 13.0, *) {
            let vc = self.storyboard?.instantiateViewController(identifier: "DoctorCalendarVC") as! DoctorCalendarVC
            vc.medicModel = medicModel
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            // Fallback on earlier versions
            let vc = DoctorCalendarVC.init(nibName: "DoctorCalendarVC", bundle: nil)
            vc.medicModel = medicModel
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
///
///Mark : Table View work
///
extension DoctorDetailsVC:UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        if selectedindex == 0{
            return 2
        }else{
            return 1
        }
    }//arrayListSecondSegment
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if selectedindex == 0{
            if section == 0{
                if arrayListFirstSegment.count > 1{
                    return 2
                }else{
                    return 0
                }
            }else{
                return 1
            }
        }else if selectedindex == 3{
            return arrayListFourthSegment.count
        }
        else if selectedindex == 1{
            return arrayListSecondSegment.count
        }
        else if selectedindex == 2{
            return arrayListThirdSegment.count
        }
        else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if selectedindex == 0{
            if indexPath.section == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier: "DoctorListCell", for: indexPath) as! DoctorListCell
                cell.updateCell(obj: arrayListFirstSegment[indexPath.row])
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell", for: indexPath) as! CategoryCell
                cell.arraySepared = medicModel?.arrayString ?? []
                cell.collectionViewText.reloadData()
                return cell
            }
        }else if selectedindex == 3{
            let cell = tableView.dequeueReusableCell(withIdentifier: "DoctorImageCell1", for: indexPath) as! DoctorImageCell
                //cell.isMedic = true
            //cell.updateCell(info:arrayListFourthSegment[indexPath.row])
            cell.lblName.text = arrayListFourthSegment[indexPath.row].medic?.name ?? ""
            cell.lblDesc.text = arrayListFourthSegment[indexPath.row].content?.summary ?? ""
            cell.lblPostDate.text = "0 d"
            if let cretaeDate = arrayListFourthSegment[indexPath.row].created_at_formatted{
                let formated = DateFormatter()
                formated.dateFormat = "dd/MM/yyyy HH:mm"
                
                if let dateCreated = formated.date(from: cretaeDate){
                    let today = Date()
                    let components = Calendar.current.dateComponents([.day], from: dateCreated, to: today)
                    
                    cell.lblPostDate.text = "\(components.day ?? 0) d"
                    
                }
            }
            return cell
        }else if selectedindex == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "DoctorImageCell", for: indexPath) as! DoctorImageCell
            cell.lblName.text = arrayListThirdSegment[indexPath.row].facebook_user ?? ""
            cell.lblDesc.text = arrayListThirdSegment[indexPath.row].content ?? ""
            cell.viewRate.rating = Double((arrayListThirdSegment[indexPath.row].evaluation ?? 0))
            
            return cell
        }else if selectedindex == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "DoctorImageCell", for: indexPath) as! DoctorImageCell
            cell.lblName.text = arrayListSecondSegment[indexPath.row].user?.username ?? ""
            cell.lblDesc.text = arrayListSecondSegment[indexPath.row].opinion ?? ""
            cell.viewRate.rating = Double((arrayListSecondSegment[indexPath.row].evaluation ?? 0))
            return cell
        }else{
            return UITableViewCell()
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if selectedindex == 0{
            if indexPath.section == 1{
                return 280
            }
        }
        return UITableView.automaticDimension
    }
    
    
}
///
///Mark : Api call
////
extension DoctorDetailsVC{
    func callFirstSegmentApi(orderBy:String,pageNumber:Int,near:Int,limit:Int) {
        //results?page=1&near=2863&order_by=relevance&limit=10
        var getUrl :String = BaseUrl + "/results?page=" + "\(pageNumber)" + "&near=" + "\(near)" + "&order_by=" + orderBy + "&limit=" + "\(limit)" + "&medic_id="
        let idString = String(medicId ?? 0)
        getUrl.append(idString )
        print("URL===%@",getUrl)
        //        if sortingID != 0{
        //                   getUrl.append("&specialty_id=\(sortingID)")
        //               }
        let operation = WebServiceOperation.init(getUrl, nil, .WEB_SERVICE_GET, nil)
        
        operation.completionBlock =
            {
                DispatchQueue.main.async {
                    
                    print(operation.responseData?.dictionary?.prettyprintedJSON ?? "")
                    guard let dictResponse = operation.responseData?.dictionary, dictResponse.count > 0 else {
                        
                        return
                    }
                    
                    if let responceCode = dictResponse["code"] ,responceCode as! Int == 1{
                        if let dataUserDetails = dictResponse.data, dataUserDetails.count > 0 {
                            do{
                                
                                let getParseData = try JSONDecoder().decode(Resultados_Base.self, from: dataUserDetails)
                                print(getParseData)
                                self.arrayListFirstSegment.append(contentsOf: getParseData.results ?? [])
                                
                                
                                
                                
                                
                            }catch let e{
                                print(e.localizedDescription)
                                
                                //showAlertMessage(title: App_Title, message: "Something went wrong", vc: self)
                            }
                        }
                    }else{
                        // showAlertMessage(title: App_Title, message: "Something went wrong", vc: self)
                    }
                    self.tableDoc.reloadData()
                    
                }
        }
        
        appDel.operationQueue.addOperation(operation)
        
    }
    
    func callSecondSegmentApi() {
        
        var getUrl :String = BaseUrl + "/medic/opinions?related_id="
        let idString = String(medicId ?? 0)
        getUrl.append(idString )
        let operation = WebServiceOperation.init(getUrl, nil, .WEB_SERVICE_GET, nil)
        
        operation.completionBlock =
            {
                DispatchQueue.main.async {
                    
                    print(operation.responseData?.dictionary?.prettyprintedJSON ?? "")
                    guard let dictResponse = operation.responseData?.dictionary, dictResponse.count > 0 else {
                        
                        return
                    }
                    
                    
                    if let dataUserDetails = dictResponse.data, dataUserDetails.count > 0 {
                        do{
                            
                            let getParseData = try JSONDecoder().decode(Opinions_Base.self, from: dataUserDetails)
                            //self.page.pageNumber = pageNumber
                            self.arrayListSecondSegment.append(contentsOf: getParseData.opinions ?? [])
                            
                            
                        }catch let e{
                            print(e.localizedDescription)
                            //  showAlertMessage(title: App_Title, message: "Something went wrong", vc: self)
                        }
                        
                    }else{
                        // showAlertMessage(title: App_Title, message: "Something went wrong", vc: self)
                    }
                    
                    
                    self.tableDoc.reloadData()
                    self.lblNumberAvailables.text = "\(self.arrayListSecondSegment.count)"
                }
        }
        
        appDel.operationQueue.addOperation(operation)
        
    }
    
    func callThirdSegmentApi() {
        
        var getUrl :String = BaseUrl + "/facebook-evaluations?medic_id="
        let idString = String(medicId ?? 0)
        getUrl.append(idString )
        let operation = WebServiceOperation.init(getUrl, nil, .WEB_SERVICE_GET, nil)
        
        operation.completionBlock =
            {
                DispatchQueue.main.async {
                    
                    print(operation.responseData?.dictionary?.prettyprintedJSON ?? "")
                    guard let dictResponse = operation.responseData?.dictionary, dictResponse.count > 0 else {
                        
                        return
                    }
                    
                    
                    if let dataUserDetails = dictResponse.data, dataUserDetails.count > 0 {
                        do{
                            
                            let getParseData = try JSONDecoder().decode([FacebookPost].self, from: dataUserDetails)
                            //self.page.pageNumber = pageNumber
                            self.arrayListThirdSegment.append(contentsOf: getParseData)
                            
                            
                        }catch let e{
                            print(e.localizedDescription)
                            //  showAlertMessage(title: App_Title, message: "Something went wrong", vc: self)
                        }
                        
                    }else{
                        // showAlertMessage(title: App_Title, message: "Something went wrong", vc: self)
                    }
                    
                    
                    self.tableDoc.reloadData()
                    self.lblNumberFacebook.text = "\(self.arrayListThirdSegment.count)"
                }
        }
        
        appDel.operationQueue.addOperation(operation)
        
    }
    
    
    
    
    func callFourthSegmentApi() {
        //&is_medic=true
        var getUrl :String = BaseUrl + "/posts?" + "order_by=date&medic_id="
        let idString = String(medicId ?? 0)
        getUrl.append(idString )
        let operation = WebServiceOperation.init(getUrl, nil, .WEB_SERVICE_GET, nil)
        
        operation.completionBlock =
            {
                DispatchQueue.main.async {
                    
                    print(operation.responseData?.dictionary?.prettyprintedJSON ?? "")
                    guard let dictResponse = operation.responseData?.dictionary, dictResponse.count > 0 else {
                        
                        return
                    }
                    
                    
                    if let dataUserDetails = dictResponse.data, dataUserDetails.count > 0 {
                        do{
                            
                            let getParseData = try JSONDecoder().decode(Doctor_Post_Base.self, from: dataUserDetails)
                            //self.page.pageNumber = pageNumber
                            self.arrayListFourthSegment.append(contentsOf: getParseData.posts ?? [])
                            
                            
                        }catch let e{
                            print(e.localizedDescription)
                            //  showAlertMessage(title: App_Title, message: "Something went wrong", vc: self)
                        }
                        
                    }else{
                        // showAlertMessage(title: App_Title, message: "Something went wrong", vc: self)
                    }
                    
                    
                    self.tableDoc.reloadData()
                    self.lblNumberPost.text = "\(self.arrayListFourthSegment.count)"
                    
                }
        }
        
        appDel.operationQueue.addOperation(operation)
        
    }
    
    func callFirstSegmentApi() {
        
        var getUrl :String = API.DoctorDetails.getURL()?.absoluteString ?? ""
        let idString = String(medicId ?? 0)
        getUrl.append(idString )
        
        let operation = WebServiceOperation.init(getUrl, nil, .WEB_SERVICE_GET, nil)
        
        operation.completionBlock =
            {
                DispatchQueue.main.async {
                    
                    print(operation.responseData?.dictionary?.prettyprintedJSON ?? "")
                    guard let dictResponse = operation.responseData?.dictionary, dictResponse.count > 0 else {
                        
                        return
                    }
                    
                    if let responceCode = dictResponse["code"] ,responceCode as! Int == 1{
                        if let dataUserDetails = dictResponse.data, dataUserDetails.count > 0 {
                            do{
                                
                                self.medicModel = try JSONDecoder().decode(Medics.self, from: dataUserDetails)
                                
                                
                                
                                
                                
                                
                                
                            }catch let e{
                                print(e.localizedDescription)
                                
                                //showAlertMessage(title: App_Title, message: "Something went wrong", vc: self)
                            }
                        }
                    }else{
                        // showAlertMessage(title: App_Title, message: "Something went wrong", vc: self)
                    }
                    self.tableDoc.reloadData()
                    
                }
        }
        
        appDel.operationQueue.addOperation(operation)
        
    }
//    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//        if(scrollView == tableDoc) {
//
//            let height = scrollView.frame.size.height
//            let contentYoffset = scrollView.contentOffset.y
//            let distanceFromBottom = scrollView.contentSize.height - contentYoffset
//            if ((distanceFromBottom*100).rounded()/100 <= (height*100).rounded()/100) {
//
//                self.paginate()
//                // print(" you reached end of the table")
//            }
//        }
//    }
//
//    func paginate(){
//        if selectedindex == 3{
//            self.callAllApiWithPage(page: (page.pageNumber)+1)
//            page.pageNumber = (page.pageNumber)+1
//        }
//    }
    fileprivate func callAllApiWithPage() {
        self.callFourthSegmentApi()
        
    }
}
