//
//  Home_ExtensionAPI.swift
//  AesCare
//
//  Created by Mahesh Mahalik on 25/08/20.
//

import Foundation
import UIKit

extension DashBoardVC{
    
    
//    func callFourthSegmentApi(pageNumber:Int) {
//        
//        let getUrl :String = BaseUrl + "/posts?page=" + "\(pageNumber)" + "&with_treatments_name=true&order_by=relevance&limit=20&type=artigo"
//        print(getUrl)
//        let operation = WebServiceOperation.init(getUrl, nil, .WEB_SERVICE_GET, nil)
//        
//        operation.completionBlock =
//            {
//                DispatchQueue.main.async {
//                    self.allApiCount = self.allApiCount - 1
//                    //print(operation.responseData?.dictionary?.prettyprintedJSON ?? "")
//                    guard let dictResponse = operation.responseData?.dictionary, dictResponse.count > 0 else {
//                        if self.allApiCount <= 0{
//                            self.arrayAllObject = self.callFirstIndex()
//                            self.allApiCount = 4
//                            self.tableHome.reloadData()
//                        }
//                        return
//                    }
//                    
//                    
//                    if let dataUserDetails = dictResponse.data, dataUserDetails.count > 0 {
//                        do{
//                            
//                            let getParseData = try JSONDecoder().decode(Doctor_Post_Base.self, from: dataUserDetails)
//                            self.page.pageNumber = pageNumber
//                            self.arrayListFourthSegment.append(contentsOf: getParseData.posts ?? [])
//                            
//                            
//                        }catch let e{
//                            print(e.localizedDescription)
//                            //  showAlertMessage(title: App_Title, message: "Something went wrong", vc: self)
//                        }
//                        
//                    }else{
//                        // showAlertMessage(title: App_Title, message: "Something went wrong", vc: self)
//                    }
//                    
//                    if self.allApiCount <= 0{
//                        self.arrayAllObject = self.callFirstIndex()
//                        self.tableHome.reloadData()
//                        self.allApiCount = 4
//                    }
//                }
//        }
//        
//        appDel.operationQueue.addOperation(operation)
//        
//    }
//    
//    func callThirdSegmentApi(pageNumber:Int) {
//        
//        let getUrl :String = BaseUrl + "/posts?page=" + "\(pageNumber)" +
//            "&with_treatments_name=true&order_by=relevance&limit=20&type=post"
//        
//        let operation = WebServiceOperation.init(getUrl, nil, .WEB_SERVICE_GET, nil)
//        
//        operation.completionBlock =
//            {
//                DispatchQueue.main.async {
//                    self.allApiCount = self.allApiCount - 1
//                    print(operation.responseData?.dictionary?.prettyprintedJSON ?? "")
//                    guard let dictResponse = operation.responseData?.dictionary, dictResponse.count > 0 else {
//                        if self.allApiCount <= 0{
//                            self.arrayAllObject = self.callFirstIndex()
//                            self.allApiCount = 4
//                            self.tableHome.reloadData()
//                        }
//                        return
//                    }
//                    
//                    
//                    if let dataUserDetails = dictResponse.data, dataUserDetails.count > 0 {
//                        do{
//                            
//                            let getParseData = try JSONDecoder().decode(Doctor_Post_Base.self, from: dataUserDetails)
//                            self.page.pageNumber = pageNumber
//                            self.arrayListThirdSegment.append(contentsOf: getParseData.posts ?? [])
//                            
//                            
//                            
//                        }catch let e{
//                            print(e.localizedDescription)
//                            //  showAlertMessage(title: App_Title, message: "Something went wrong", vc: self)
//                        }
//                    }
//                    
//                    if self.allApiCount <= 0{
//                        self.arrayAllObject = self.callFirstIndex()
//                        self.tableHome.reloadData()
//                        self.allApiCount = 4
//                    }
//                }
//        }
//        
//        appDel.operationQueue.addOperation(operation)
//        
//    }
//    
//    
//    
//    func callSecondSegmentApi(pageNumber:Int) {
//        
//        let getUrl :String = BaseUrl + "/posts?is_from_instagram=false&with_treatments_name=true&page=" + "\(pageNumber)" + "&order_by=date&is_medic=true"
//        
//        let operation = WebServiceOperation.init(getUrl, nil, .WEB_SERVICE_GET, nil)
//        
//        operation.completionBlock =
//            {
//                DispatchQueue.main.async {
//                    self.allApiCount = self.allApiCount - 1
//                    print(operation.responseData?.dictionary?.prettyprintedJSON ?? "")
//                    guard let dictResponse = operation.responseData?.dictionary, dictResponse.count > 0 else {
//                        if self.allApiCount <= 0{
//                            self.arrayAllObject = self.callFirstIndex()
//                            self.allApiCount = 4
//                            self.tableHome.reloadData()
//                        }
//                        return
//                    }
//                    
//                    
//                    if let dataUserDetails = dictResponse.data, dataUserDetails.count > 0 {
//                        do{
//                            
//                            
//                            
//                            let getParseData = try JSONDecoder().decode(Doctor_Post_Base.self, from: dataUserDetails)
//                            self.page.pageNumber = pageNumber
//                            self.arrayListSecondSegment.append(contentsOf: getParseData.posts ?? [])
//                            
//                            
//                        }catch let e{
//                            print(e.localizedDescription)
//                            
//                            // showAlertMessage(title: App_Title, message: "Something went wrong", vc: self)
//                        }
//                    }
//                    if self.allApiCount <= 0{
//                        self.arrayAllObject = self.callFirstIndex()
//                        self.tableHome.reloadData()
//                        self.allApiCount = 4
//                    }
//                    
//                    
//                    
//                }
//        }
//        
//        appDel.operationQueue.addOperation(operation)
//        
//    }
//    
//    
//    func callFirstSegmentApi(orderBy:String,pageNumber:Int,near:Int,limit:Int) {
//        //results?page=1&near=2863&order_by=relevance&limit=10
//        let getUrl :String = BaseUrl + "/results?page=" + "\(pageNumber)" + "&near=" + "\(near)" + "&order_by=" + orderBy + "&limit=" + "\(limit)"
//        print("URL===%@",getUrl)
//        //        if sortingID != 0{
//        //                   getUrl.append("&specialty_id=\(sortingID)")
//        //               }
//        let operation = WebServiceOperation.init(getUrl, nil, .WEB_SERVICE_GET, nil)
//        
//        operation.completionBlock =
//            {
//                DispatchQueue.main.async {
//                    self.allApiCount = self.allApiCount - 1
//                    // print(operation.responseData?.dictionary?.prettyprintedJSON ?? "")
//                    guard let dictResponse = operation.responseData?.dictionary, dictResponse.count > 0 else {
//                        if self.allApiCount <= 0{
//                            self.arrayAllObject = self.callFirstIndex()
//                            self.allApiCount = 4
//                            self.tableHome.reloadData()
//                        }
//                        return
//                    }
//                    
//                    if let responceCode = dictResponse["code"] ,responceCode as! Int == 1{
//                        if let dataUserDetails = dictResponse.data, dataUserDetails.count > 0 {
//                            do{
//                                
//                                let getParseData = try JSONDecoder().decode(Resultados_Base.self, from: dataUserDetails)
//                                print(getParseData)
//                                self.arrayListFirstSegment.append(contentsOf: getParseData.results ?? [])
//                                self.page.pageNumber = pageNumber
//                                
//                                
//                                
//                                
//                            }catch let e{
//                                print(e.localizedDescription)
//                                
//                                //showAlertMessage(title: App_Title, message: "Something went wrong", vc: self)
//                            }
//                        }
//                    }else{
//                        // showAlertMessage(title: App_Title, message: "Something went wrong", vc: self)
//                    }
//                    if self.allApiCount <= 0{
//                        self.arrayAllObject = self.callFirstIndex()
//                        self.tableHome.reloadData()
//                        self.allApiCount = 4
//                    }
//                    
//                }
//        }
//        
//        appDel.operationQueue.addOperation(operation)
//        
//    }
//    func callDoctorsApi(orderBy:String,pageNumber:Int,near:Int,limit:Int) {
//        //results?page=1&near=2863&order_by=relevance&limit=10
//        var getUrl :String = BaseUrl + "/doctors?page=" + "\(pageNumber)" + "&near=" + "\(near)" + "&order_by=" + orderBy + "&limit=" + "\(limit)" + "&get_calendar=true"
//        if sortingID != 0{
//            getUrl.append("&specialty_id=\(sortingID)")
//        }
//        //getUrl.append("&medic_id=25621")
//        
//        //https://ws.aescare.com/doctors?limit=10&page=1&near=2869&order_by=relevance&specialty_id=1&get_calendar=true
//        let operation = WebServiceOperation.init(getUrl, nil, .WEB_SERVICE_GET, nil)
//        
//        operation.completionBlock =
//            {
//                DispatchQueue.main.async {
//                    
//                    print(operation.responseData?.dictionary?.prettyprintedJSON ?? "")
//                    guard let dictResponse = operation.responseData?.dictionary, dictResponse.count > 0 else {
//                        
//                        return
//                    }
//                    
//                    if let responceCode = dictResponse["code"] ,responceCode as? Int == 1 {
//                        if let dataUserDetails = dictResponse.data, dataUserDetails.count > 0 {
//                            do{
//                                let getParseData = try JSONDecoder().decode(Model_Doctor.self, from: dataUserDetails)
//                                self.arrayListFifthSegment.append(contentsOf: getParseData.medics ?? [])
//                                
//                                self.page.pageDoctor = pageNumber
//                                self.tableHome.reloadData()
//                            }catch let e{
//                                print(e.localizedDescription)
//                                //showAlertMessage(title: App_Title, message: "Something went wrong", vc: self)
//                            }
//                        }
//                    }else{
//                        // showAlertMessage(title: App_Title, message: "Something went wrong", vc: self)
//                    }
//                    
//                    DispatchQueue.main.async {
//                        /////
//                    }
//                }
//        }
//        
//        appDel.operationQueue.addOperation(operation)
//        
//    }
//    
//    func callNextSlotDateApi(medic_id:String,currentDate:String,near:Int,limit:Int) {
//        //https://ws.aescare.com/medic/calendar/slots/next?medic_id=4123&date=2020-08-28
//        let getUrl :String = BaseUrl + "medic/calendar/slots/next?medic_id=" + "\(medic_id)" + "&date=" + "\(currentDate)"
//        
//        let operation = WebServiceOperation.init(getUrl, nil, .WEB_SERVICE_GET, nil)
//        
//        operation.completionBlock =
//            {
//                DispatchQueue.main.async {
//                    
//                    //print(operation.responseData?.dictionary?.prettyprintedJSON ?? "")
//                    guard let dictResponse = operation.responseData?.dictionary, dictResponse.count > 0 else {
//                        //UtilityClass.tosta(message: "Something went wrong", duration: 2.0, vc: self)
//                        return
//                    }
//                    
//                    if let responceCode = dictResponse["code"] ,responceCode as! Int == 1 , let msg = dictResponse["message"] as? String{
//                        if let dataUserDetails = dictResponse.data, dataUserDetails.count > 0 {
//                            do{
//                                
//                                let userModel = try JSONDecoder().decode(Resultados_Base.self, from: dataUserDetails)
//                                
//                                
//                                
//                                
//                                
//                            }catch let e{
//                                print(e.localizedDescription)
//                                //showAlertMessage(title: App_Title, message: "Something went wrong", vc: self)
//                            }
//                        }
//                    }else if let msg = dictResponse["message"] as? String{
//                        UtilityClass.tosta(message: msg, duration: 2.0, vc: self)
//                        //showAlertMessage(title: App_Title, message: msg, vc: self)
//                    }else{
//                        //showAlertMessage(title: App_Title, message: "Something went wrong", vc: self)
//                    }
//                    
//                    DispatchQueue.main.async {
//                        /////
//                    }
//                }
//        }
//        
//        appDel.operationQueue.addOperation(operation)
//        
//    }
//    func callFirstIndex() -> ZeroIndexObject{
//        let zeroIndex = ZeroIndexObject()
//        zeroIndex.total = self.arrayListFirstSegment.count + self.arrayListSecondSegment.count + self.arrayListThirdSegment.count + self.arrayListFourthSegment.count
//        for i in zeroIndex.arrayOrderSet.count...zeroIndex.total{
//            if self.arrayListFirstSegment[safe: i] != nil{
//                zeroIndex.arrayOrderSet.append(self.arrayListFirstSegment[i] as AnyObject)
//            }
//            if self.arrayListSecondSegment[safe: i] != nil{
//                self.arrayListSecondSegment[i].postType = 2
//                zeroIndex.arrayOrderSet.append(self.arrayListSecondSegment[i] as AnyObject)
//            }
//            if self.arrayListThirdSegment[safe: i] != nil{
//                self.arrayListThirdSegment[i].postType = 3
//                zeroIndex.arrayOrderSet.append(self.arrayListThirdSegment[i] as AnyObject)
//            }
//            if self.arrayListFourthSegment[safe: i] != nil{
//                self.arrayListFourthSegment[i].postType = 4
//                zeroIndex.arrayOrderSet.append(self.arrayListFourthSegment[i] as AnyObject)
//            }
//            
//            
//            
//            
//        }
//        return zeroIndex
//    }
//    //
//    //Mark:Publish a post
//    ///
//    func callAddBodyForPublish(param:[String:AnyObject],multipartData:[MultiPartDataFormatStructure]) {
//        let getUrl :String = BaseUrl + "/post"
//        
//        let operation = WebServiceOperation.init(getUrl, param, .WEB_SERVICE_MULTI_PART, nil)
//        
//        operation.completionBlock =
//            {
//                DispatchQueue.main.async {
//                    
//                    //print(operation.responseData?.dictionary?.prettyprintedJSON ?? "")
//                    guard let dictResponse = operation.responseData?.dictionary, dictResponse.count > 0 else {
//                        //UtilityClass.tosta(message: "Something went wrong", duration: 2.0, vc: self)
//                        return
//                    }
//                    
//                    
//                    if let responceCode = dictResponse["code"] ,responceCode as! Int == 1 , let post =  dictResponse["post"] as? [String : AnyObject] , let postkey = post["key"],let msg = dictResponse["message"] as? String {
//                        
//                        if(multipartData.count == 0){
//                            let indexPath = IndexPath.init(row: 0, section: 0)
//                            if let cell = self.tableHome.cellForRow(at: indexPath) as? PublishCell{
//                                   
//                            cell.txtVWStatus.text = ""
//                            }
//                            UtilityClass.tosta(message: msg, duration: 2.0, vc: self)
//                        }else{
//                            var param1:[String:AnyObject] = [String:AnyObject]()
//                            param1["post_key"] = postkey as AnyObject
//                            
//                            self.callPublish(param: param1, multipartData: multipartData)
//                        }
//                    }else if let msg = dictResponse["message"] as? String{
//                        UtilityClass.tosta(message: msg, duration: 2.0, vc: self)
//                        
//                    }else{
//                        showAlertMessage(title: App_Title, message: "Something went wrong", vc: self)
//                    }
//                    
//                    DispatchQueue.main.async {
//                        /////
//                    }
//                }
//        }
//        
//        appDel.operationQueue.addOperation(operation)
//        
//    }
//    
//    func callPublish(param:[String:AnyObject],multipartData:[MultiPartDataFormatStructure]) {
//        let getUrl :String = BaseUrl + "/post/add-media"
//        let operation = WebServiceOperation.init(getUrl, param, .WEB_SERVICE_MULTI_PART, multipartData)
//        
//        operation.completionBlock =
//            {
//                DispatchQueue.main.async {
//                    
//                    //print(operation.responseData?.dictionary?.prettyprintedJSON ?? "")
//                    guard let dictResponse = operation.responseData?.dictionary, dictResponse.count > 0 else {
//                        //UtilityClass.tosta(message: "Something went wrong", duration: 2.0, vc: self)
//                        return
//                    }
//                    let indexPath = IndexPath.init(row: 0, section: 0)
//                    if let cell = self.tableHome.cellForRow(at: indexPath) as? PublishCell{
//                        
//                        cell.txtVWStatus.text  = ""
//                    }
//                    
//                    if let responceCode = dictResponse["code"] ,responceCode as! Int == 1 , let msg = dictResponse["message"] as? String{
//                        UtilityClass.tosta(message: msg, duration: 2.0, vc: self)
//                        
//                    }else if let msg = dictResponse["message"] as? String{
//                        UtilityClass.tosta(message: msg, duration: 2.0, vc: self)
//                        self.publication_multipartData.removeAll()
//                    }else{
//                        showAlertMessage(title: App_Title, message: "Something went wrong", vc: self)
//                    }
//                    
//                    DispatchQueue.main.async {
//                        /////
//                    }
//                }
//        }
//        
//        appDel.operationQueue.addOperation(operation)
//        
//    }
//    
//}
//extension String {
//    
//    
//    var html2AttributedString: NSAttributedString? {
//        guard
//            let data = data(using: String.Encoding.utf8)
//            else { return nil }
//        do {
//            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html,.characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
//        } catch let error as NSError {
//            print(error.localizedDescription)
//            return  nil
//        }
//    }
//    var html2String: String {
//        return html2AttributedString?.string ?? ""
//    }
    
}
