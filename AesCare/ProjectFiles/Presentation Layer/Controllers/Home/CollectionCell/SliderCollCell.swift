//
//  ManageAccCollCell.swift
//  CAL
//
//  Created by DIPIKA GHOSH on 10/08/20.
//  Copyright © 2020 met. All rights reserved.
//

import UIKit

class SliderCollCell: UICollectionViewCell {
    
    @IBOutlet weak var imgViewGroup: UIImageView!
    @IBOutlet weak var labelUnderLine: UILabel!
    @IBOutlet weak var labelMheaderName: UILabel!
    
    @IBOutlet weak var imageviewSlider: UIImageView!
    @IBOutlet weak var btnSlider: UIButton!
    
    func show(_ groupData: Chat_groups) {
        let url = URL(string: "https://br.aescare.com/chat/photo/\(groupData.key ?? "")")
        self.imgViewGroup.layer.borderWidth = 1
        self.imgViewGroup.layer.masksToBounds = false
        self.imgViewGroup.layer.borderColor = UIColor.black.cgColor
        self.imgViewGroup.layer.cornerRadius = self.imgViewGroup.frame.height / 2
        self.imgViewGroup.clipsToBounds = true
        
        DispatchQueue.global().async {
            let data = try! Data(contentsOf: url!)
            
            DispatchQueue.main.async {
                self.imgViewGroup.image = UIImage(data: data)
            }
        }
    }
}
