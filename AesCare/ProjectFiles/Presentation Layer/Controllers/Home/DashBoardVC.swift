//
//  DashBoardVC.swift
//  AesCare
//
//  Created by Gali Srikanth on 10/08/20.
//

import UIKit
import SideMenu
import AMTabView

class DashBoardVC: AMTabsViewController {
    var notification = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        self.setupStoryboards()
        self.selectedTabIndex = 0
        
        self.notification.aesCareNotifications?.sendToken(self.notification.aesCareNotifications?.token ?? "empty", response: {
            data in
            if data {
                print("Request Completed successfully")
                //print("\(usrerModelOBJ!.user!.name!)")
            }
            else {
                print("Error!")
            }
        })
    }
    
    func setupStoryboards() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let chatListStoryboard = storyboard.instantiateViewController(identifier: "ChatListVC")
        let publishResultsStoryboard = storyboard.instantiateViewController(identifier: "PublishResult")
        let centralHubStoryboard = storyboard.instantiateViewController(identifier: "CentralHubStoryboard")
        let scheduleAppointmentStoryboard = storyboard.instantiateViewController(identifier: "ScheduleConsultationVC")
        let userProfileStoryboard = storyboard.instantiateViewController(identifier: "UserProfileVC")
        
        AMTabView.settings.ballColor = UIColor(red: 255/255, green: 105/255, blue: 180/255, alpha: 1.0)
        AMTabView.settings.selectedTabTintColor = .purple
        AMTabView.settings.unSelectedTabTintColor = .black
        AMTabView.settings.tabColor = UIColor(red: 228/255, green: 233/255, blue: 242/255, alpha: 1.0)
        
        self.viewControllers = [
            centralHubStoryboard,
            publishResultsStoryboard,
            scheduleAppointmentStoryboard,
            chatListStoryboard,
            userProfileStoryboard
        ]
    }
}
