//
//  SearchVC.swift
//  AesCare
//
//  Created by Mahesh Mahalik on 17/09/20.
//

import UIKit

class SearchVC: BaseViewController {
    var searchString:String?
    var page:Page = Page()
    var arraySearch:[Answers] = []
    
    @IBOutlet weak var txtFieldSearch: UITextField!
    @IBOutlet weak var tableSearch: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.txtFieldSearch.text = searchString
        

        self.callSearchApi(pageNumber: 1, search: searchString ?? "")
    }
    
    @IBAction func btnSearchAction(_ sender: UIButton) {
        guard txtFieldSearch.text?.count ?? 0 > 0 else {
            return
        }
        self.callSearchApi(pageNumber: 1, search: txtFieldSearch.text ?? "")
    }
    @IBAction func btnBackNavigation(_ sender: UIButton) {
        backNavigation()
    }
    

}
extension SearchVC:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arraySearch.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let obj = arraySearch[safe: indexPath.row]{
            if obj.card_type == "result"{
                let cell = tableView.dequeueReusableCell(withIdentifier: "DoctorListCell", for: indexPath) as! DoctorListCell
                cell.btnEye.tag = indexPath.row
                cell.updateResultCell(result: obj)
                
                return cell
            }else if obj.card_type == "content"{
                let cell = tableView.dequeueReusableCell(withIdentifier: "DoctorTextCell", for: indexPath) as! DoctorTextCell
                cell.updateResultCell(post: obj)
                return cell
            }else if obj.card_type == "medic"{
                let cell = tableView.dequeueReusableCell(withIdentifier: "DoctorImageCell1", for: indexPath) as! DoctorImageCell
                 cell.updateResultCell(result: obj)
                return cell
            }else{
                return UITableViewCell()
            }
        }else{
            return UITableViewCell()
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}
extension SearchVC{
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if(scrollView == tableSearch) {
            
            let height = scrollView.frame.size.height
            let contentYoffset = scrollView.contentOffset.y
            let distanceFromBottom = scrollView.contentSize.height - contentYoffset
            if ((distanceFromBottom*100).rounded()/100 <= (height*100).rounded()/100) {
                
                self.paginate()
                // print(" you reached end of the table")
            }
        }
    }
    
    func paginate(){
        
        self.callSearchApi(pageNumber: (page.pageNumber)+1, search: searchString ?? "")
            page.pageNumber = (page.pageNumber)+1
    }
    func callSearchApi(pageNumber:Int,search:String) {
        
        let getUrl :String = BaseUrl + "/assistant/mobile/question?q=\(search)&page=\(pageNumber)"
        
        let operation = WebServiceOperation.init(getUrl, nil, .WEB_SERVICE_GET, nil)
        
        operation.completionBlock =
            {
                DispatchQueue.main.async {
                    
                    print(operation.responseData?.dictionary?.prettyprintedJSON ?? "")
                    guard let dictResponse = operation.responseData?.dictionary, dictResponse.count > 0 else {
                        
                        return
                    }
                    
                    
                    if let dataUserDetails = dictResponse.data, dataUserDetails.count > 0 {
                        do{
                            
                            
                            
                            let getParseData = try JSONDecoder().decode(SearchResult.self, from: dataUserDetails)
                            self.page.pageNumber = pageNumber
                            self.arraySearch.append(contentsOf: getParseData.answers ?? [])
                           
                            
                        }catch let e{
                            print(e.localizedDescription)
                            
                            // showAlertMessage(title: App_Title, message: "Something went wrong", vc: self)
                        }
                    }
                    
                        
                        self.tableSearch.reloadData()
                        
                    }
                    
                    
                    
                }
        
        
        appDel.operationQueue.addOperation(operation)
        
    }
}
