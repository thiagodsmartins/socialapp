//
//  ReviewCell.swift
//  AesCare
//
//  Created by Mahesh Mahalik on 19/10/20.
//

import UIKit

class ReviewCell: UITableViewCell {

    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var btnReply: UIButton!
    @IBOutlet weak var imgviewCell: designableImage!
    @IBOutlet weak var lblCount: UILabel!
    @IBOutlet weak var lblReplyCount: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDatePost: UILabel!
    @IBOutlet weak var lblComment: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
