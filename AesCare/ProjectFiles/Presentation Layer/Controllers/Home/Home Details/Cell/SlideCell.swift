//
//  SliderTableCell.swift
//  Alkindi
//
//  Created by Mahesh Mahalik on 30/01/20.
//  Copyright © 2020 Alkindi. All rights reserved.
//

import UIKit
import Kingfisher

protocol SliderCellDelegate:class {
    func didSelectImageToPreview(didSelectedImage image: UIImage)
}

class SlideCell: UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var sliderImgCollVw: UICollectionView!
    @IBOutlet weak var heightCell: NSLayoutConstraint!
    @IBOutlet weak var pageController: UIPageControl!
    @IBOutlet weak var pageC: UIPageControl!
    
    weak var delegate : SliderCellDelegate?
    var arrBannerList :[Medias] = []
    var isResultos:Bool = false
    var arrayMedias:[MediasResult] = []
    var x = 0
    
    var imageViewer: UIView!
    var imagePreview: UIImageView!
    var pinchGesture: UIPinchGestureRecognizer!
    var imageDataArray: [UIImage]!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.imageViewer = UIView()
        self.imagePreview = UIImageView(frame: CGRect(x: 50, y: 50, width: 100, height: 100))
        self.pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector(self.zoomImage(_:)))
        self.imageDataArray = [UIImage]()
        
        if sliderImgCollVw != nil{
            sliderImgCollVw.delegate = self
            sliderImgCollVw.dataSource = self
        }
    }

    @objc func zoomImage(_ pinchImage: UIPinchGestureRecognizer) {
        let scaleResult = pinchImage.view?.transform.scaledBy(x: pinchImage.scale, y: pinchImage.scale)
        guard let scale = scaleResult, scale.a > 1, scale.d > 1 else { return }
        pinchImage.view?.transform = scale
        pinchImage.scale = 1
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isResultos == false{
            return arrBannerList.count
        } else {
            return arrayMedias.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Item selected \(indexPath.row)")
        self.delegate?.didSelectImageToPreview(didSelectedImage: self.imageDataArray[indexPath.row])
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.pageController.currentPage = indexPath.row
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SliderCollCell", for: indexPath) as! SliderCollCell
        if isResultos == false {
            if let url = URL.init(string: arrBannerList[indexPath.row].cover_url ?? ""){
                cell.imageviewSlider.kf.indicatorType = .activity
                cell.imageviewSlider.kf.setImage(with: url)
            }
        } else {
            if let url = URL.init(string: arrayMedias[indexPath.row].image_url ?? "") {
                cell.imageviewSlider.kf.indicatorType = .activity
                var value =  ""
                
                if usrerModelOBJ != nil {
                    if let valueTemp =  usrerModelOBJ.session_key {
                        value = "aescare_session_key=" + valueTemp
                    }
                }
                                  
                let request = NSMutableURLRequest(url: url)
                request.setValue(value, forHTTPHeaderField: "Cookie")
                request.httpMethod = "GET"
                
                cell.imageviewSlider.startAnimating()
                
                let dataTask = URLSession.shared.dataTask(with: url) {[weak self] (data, _, _) in
                    if let data = data {
                        DispatchQueue.main.async {
                            cell.imageviewSlider.kf.indicatorType = .activity
                            cell.imageviewSlider.stopAnimating()
                            cell.imageviewSlider.image = UIImage(data: data)
                            self?.imageDataArray.append(UIImage(data: data)!)
                        }
                    }
                }
                                  
                dataTask.resume()
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return  CGSize.init(width: (sliderImgCollVw.bounds.size.width), height: (sliderImgCollVw.bounds.size.height))
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        targetContentOffset.pointee = scrollView.contentOffset
        let pageWidth:CGFloat = CGFloat(self.sliderImgCollVw.bounds.width)
        let minSpace:CGFloat = 10.0
        var cellToSwipe:Double = Double(Float((scrollView.contentOffset.x))/Float((pageWidth+minSpace))) + Double(0.5)
        
        if cellToSwipe < 0 {
            cellToSwipe = 0
        } else if cellToSwipe >= Double(3) {
            cellToSwipe = Double(3) - Double(1)
        }
        
        let indexPath:IndexPath = IndexPath(row: Int(cellToSwipe), section:0)
        
        //let currentPage:CGFloat = floor((scrollView.contentOffset.x-pageWidth/2)/pageWidth)+1
        
        //self.pageController.currentPage = Int(currentPage);
        if isResultos == false {
            if self.x < arrBannerList.count {
                self.pageC.currentPage = indexPath.row
                self.x = indexPath.row
                self.sliderImgCollVw.scrollToItem(at:indexPath, at: UICollectionView.ScrollPosition.left, animated: true)
                self.x = self.x + 1
            } else {
                self.x = 0
                self.pageC.currentPage = 0
                self.sliderImgCollVw.scrollToItem(at: IndexPath(item: 0, section: 0), at: .centeredHorizontally, animated: true)
            }
        } else {
            if self.x < arrayMedias.count {
                self.pageC.currentPage = indexPath.row
                self.x = indexPath.row
                self.sliderImgCollVw.scrollToItem(at:indexPath, at: UICollectionView.ScrollPosition.left, animated: true)
                self.x = self.x + 1
            } else {
                self.x = 0
                self.pageC.currentPage = 0
                self.sliderImgCollVw.scrollToItem(at: IndexPath(item: 0, section: 0), at: .centeredHorizontally, animated: true)
            }
        }
    }
}

