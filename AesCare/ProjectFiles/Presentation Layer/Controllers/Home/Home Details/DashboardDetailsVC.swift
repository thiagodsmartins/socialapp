//
//  HomeDetailsVC.swift
//  AesCare
//
//  Created by Mahesh Mahalik on 17/10/20.
//

import UIKit
import SideMenu
import WebKit
class DashboardDetailsVC: BaseViewController,UITableViewDataSource,UITableViewDelegate, WKNavigationDelegate {
    
    
    var gradientLayer: CAGradientLayer!
    @IBOutlet weak var vwNavigation: UIView!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var containerSearch: designableView!
    @IBOutlet weak var tableHomeDetails: UITableView!
    var detailsPostObj :Posts?
    var detailsResultObj:ResultsFirst?
    let textView = UITextView(frame: CGRect.zero)
    var arrayMediasResult : [MediasResult]?
    var imagePreviewOverlay: UIView!
    var imagePreview: UIImageView!
    var scrollView: UIScrollView!
    var tapGesture: UITapGestureRecognizer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapToCloseImagePreview(_:)))
        self.tapGesture.numberOfTapsRequired = 2
        
        self.scrollView = UIScrollView()
        self.imagePreview = UIImageView()
        
        self.scrollView.delegate = self
        self.scrollView.isHidden = true
        self.scrollView.frame = CGRect(x: 0, y: 150, width: self.view.frame.width, height: self.view.frame.height - 230)
        self.scrollView.minimumZoomScale = 1
        self.scrollView.maximumZoomScale = 10
        self.scrollView.bounces = false
        
        self.imagePreview.frame = CGRect(x: 0, y: 0, width: self.scrollView.frame.width, height: self.scrollView.frame.height)
        self.imagePreview.backgroundColor = .black
        self.imagePreview.contentMode = .scaleAspectFit
        
        createGradientLayer()
        
        self.scrollView.addGestureRecognizer(self.tapGesture)
        self.scrollView.addSubview(self.imagePreview)
        
        self.view.addSubview(self.scrollView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if detailsResultObj != nil{
            self.callImagesDetailsResultadosAPI(key: detailsResultObj?.result_id ?? 0)
            self.callCommentPostAPI(relatedID: detailsResultObj?.result_id ?? 0)
        }else{
             self.callCommentPostAPI2(relatedID: detailsPostObj?.post_id ?? 0)
        }
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        tableHomeDetails.dataSource = self
        tableHomeDetails.delegate  = self
        tabbarUISetup()
        gradientLayer.frame = self.vwNavigation.bounds
    }
    func tabbarUISetup(){
        ///// Call Custom Tabbar
        customTabbar.myDelegate = self
        customTabbar.setup(activeFor:"Home")
    }
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    fileprivate func showTextAlert(isReply:Bool) {
        let alert = UIAlertController(title: "Resposta", message: "Escreva sua resposta", preferredStyle: .alert)
        let textView = UITextView()
        textView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        let controller = UIViewController()
        
        textView.frame = controller.view.frame
        controller.view.addSubview(textView)
        
        alert.setValue(controller, forKey: "contentViewController")
        
        let height: NSLayoutConstraint = NSLayoutConstraint(item: alert.view!, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: view.frame.height * 0.4)
        alert.view.addConstraint(height)
        
        let saveAction = UIAlertAction(title: "OK", style: .default) { (action) in
            if textView.text != ""{
                if self.detailsPostObj != nil{
                    self.callPostAPI(relatedID: self.detailsPostObj?.post_id ?? 0, content: textView.text)
                }else{
                    self.callPostAPI(relatedID: self.detailsResultObj?.result_id ?? 0, content: textView.text)
                }
            
            }
        }
        
        //saveAction.isEnabled = false
        
        let cancelAction = UIAlertAction.init(title: "Cancel", style: .default) { (action) in
          
        }
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    @IBAction func btnReplayAction(_ sender: UIButton) {
        if #available(iOS 13.0, *) {
            
            let vc = self.storyboard?.instantiateViewController(identifier: "CommentVC") as! CommentVC
           
            if detailsResultObj != nil {
                vc.review = detailsResultObj?.comments?[sender.tag]
            }else{
                vc.review = detailsPostObj?.comments?[sender.tag]
            }
            
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            let vc = CommentVC.init(nibName: "CommentVC", bundle: nil)
            if detailsResultObj != nil {
                vc.review = detailsResultObj?.comments?[sender.tag]
            }else{
                vc.review = detailsPostObj?.comments?[sender.tag]
            }
            
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func btnCommentAction(_ sender: UIButton) {
        self.showTextAlert(isReply: false)
       
    }
    
    func showAlertView() {
         let alertController = UIAlertController(title: "Resposta", message: "Escreva sua resposta", preferredStyle: .alert)

               textView.textContainerInset = UIEdgeInsets.init(top: 8, left: 5, bottom: 8, right: 5)

               let saveAction = UIAlertAction(title: "OK", style: .default) { (action) in
                  // self.label.text = textView.text
                   alertController.view.removeObserver(self, forKeyPath: "bounds")
               }

               saveAction.isEnabled = false

               let cancelAction = UIAlertAction.init(title: "Cancel", style: .default) { (action) in
                   alertController.view.removeObserver(self, forKeyPath: "bounds")
               }

               alertController.view.addObserver(self, forKeyPath: "bounds", options: NSKeyValueObservingOptions.new, context: nil)

               NotificationCenter.default.addObserver(forName: UITextView.textDidChangeNotification, object: textView, queue: OperationQueue.main) { (notification) in
                   saveAction.isEnabled = self.textView.text != ""
               }

               textView.backgroundColor = UIColor.white
               alertController.view.addSubview(textView)

               alertController.addAction(saveAction)
               alertController.addAction(cancelAction)

               self.present(alertController, animated: true, completion: nil)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "bounds"{
            if let rect = (change?[NSKeyValueChangeKey.newKey] as? NSValue)?.cgRectValue {
                let margin:CGFloat = 8.0
                textView.frame = CGRect.init(x: rect.origin.x + margin, y: rect.origin.y + margin, width: rect.width - 2*margin, height: rect.height / 2)
                textView.bounds = CGRect.init(x: rect.origin.x + margin, y: rect.origin.y + margin, width: rect.width - 2*margin, height: rect.height / 2)
            }
        }
    }
    func createGradientLayer() {
        gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.vwNavigation.bounds
        gradientLayer.colors = [UIColor(named: "AppPurpleColor")!.cgColor, UIColor(named: "AppMoveColor")!.cgColor]
        self.vwNavigation.layer.addSublayer(gradientLayer)
        self.vwNavigation.bringSubviewToFront(self.btnMenu)
        self.vwNavigation.bringSubviewToFront(self.containerSearch)
    }
    private func menuPressed() {
        
        appDel.topViewControllerWithRootViewController(rootViewController: UIApplication.shared.windows.first?.rootViewController)?.view.endEditing(true)
        
        SideMenuManager.default.leftMenuNavigationController?.settings.presentationStyle = .menuSlideIn
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let menu = storyboard.instantiateViewController(withIdentifier: "SideMenuNavigationController") as! SideMenuNavigationController
        menu.leftSide = true
        
        UIApplication.shared.windows.first?.rootViewController?.present(menu, animated: true, completion: nil)
        
    }
    @IBAction func openSideMenu(_ sender: Any) {
        self.view.endEditing(true)
        self.menuPressed()
    }
    
    
}
extension DashboardDetailsVC{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
        return 4
        }else{
            if detailsResultObj != nil{
            return detailsResultObj?.comments?.count ?? 0
            }else{
                return detailsPostObj?.comments?.count ?? 0
            }
        }
        //        if detailsResultObj != nil{
        //            return 4
        //        }else{
        //            if  let object =  detailsPostObj{
        //                if object.postType == 2{
        //
        //
        //                }else if object.postType == 3{
        //
        //                }
        //                else if object.postType == 4{
        //
        //                }
        //
        //            }
        //        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
        if detailsResultObj != nil{
            
            switch indexPath.row {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell", for: indexPath) as! CategoryCell
                cell.lblTitle.text = "Resultado"
                return cell
                
            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier: "SlideCell", for: indexPath) as! SlideCell
                cell.isResultos = true
                cell.delegate = self
                if arrayMediasResult?.count ?? 0 > 0{
                     cell.pageController.numberOfPages = arrayMediasResult?.count ?? 0
                    cell.pageController.isHidden = false
                    cell.heightCell.constant = 200
                }else{
                    cell.pageController.isHidden = true
                    cell.heightCell.constant = 0
                }
                cell.arrayMedias = self.arrayMediasResult ?? []
                cell.sliderImgCollVw.reloadData()
               //
                cell.setNeedsLayout()
                return cell
            case 2:
                let cell = tableView.dequeueReusableCell(withIdentifier: "DoctorListCell", for: indexPath) as! DoctorListCell
                cell.btnEye.tag = indexPath.row
                cell.updateCell(obj: detailsResultObj!)
                return cell
                
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell2", for: indexPath) as! CategoryCell
                return cell
                
            }
        }else{
            if  let object =  detailsPostObj{
                if object.postType == 2{
                    switch indexPath.row {
                    case 0:
                        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell", for: indexPath) as! CategoryCell
                        cell.lblTitle.text = detailsPostObj?.content?.title ?? ""
                        return cell
                        
                    case 1:
                        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell1", for: indexPath) as! CategoryCell
                        cell.lblTitle.text = detailsPostObj?.content?.summary ?? ""
                                               cell.lblSecondText.text = detailsPostObj?.created_at_formatted ?? ""
                              
                        if let url = URL(string: detailsPostObj?.medic?.image_url ?? ""){
                                   cell.imgviewCell.kf.indicatorType = .activity
                                   cell.imgviewCell.kf.setImage(with: url)
                                   
                               }else{
                                   cell.imgviewCell.image = UIImage(named: "")
                                  
                               }
                        return cell
                        case 2:
                            let cell = tableView.dequeueReusableCell(withIdentifier: "VideoCell", for: indexPath) as! VideoCell
                            if let youtube = detailsPostObj?.content?.youtube_video_id{
                            //https://www.youtube.com/watch?v=WBl10Z6aGfE
                                if let youtubeUrl = URL(string:"https://www.youtube.com/embed/" + "\(youtube)"){

                            
                        
                            
                            let urlRequest = URLRequest(url: youtubeUrl)
                            cell.heightView.constant = 250
                            cell.viewWeb.load(urlRequest)
                            cell.viewWeb.navigationDelegate = self
                            
                                
                                }
                            
                           }else{
                            cell.heightView.constant = 0
                            }
                                                   
                            return cell
                        
                    default:
                        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell2", for: indexPath) as! CategoryCell
                        return cell
                    }
                }
                else if object.postType == 3{
                    switch indexPath.row {
                    case 0:
                        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell", for: indexPath) as! CategoryCell
                        cell.lblTitle.text = "Publicação de \(detailsPostObj?.user?.username ?? "")"
                        return cell
                        
                    case 1:
                        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell1", for: indexPath) as! CategoryCell
                        cell.lblTitle.text = ""
                        cell.lblSecondText.text = detailsPostObj?.created_at_formatted ?? ""
                        if let url = URL(string: detailsPostObj?.user?.image_url ?? ""){
                                        cell.imgviewCell.kf.indicatorType = .activity
                                       cell.imgviewCell.kf.setImage(with: url)
                                   }else{
                                       cell.imgviewCell.image = UIImage(named: "")
                                   }
                        return cell
                    case 2:
                        let cell = tableView.dequeueReusableCell(withIdentifier: "SlideCell", for: indexPath) as! SlideCell
                        cell.arrBannerList = detailsPostObj?.medias ?? []
                        cell.pageController.numberOfPages = detailsPostObj?.medias?.count ?? 0
                        if detailsPostObj?.medias?.count ?? 0 > 0{
                            cell.heightCell.constant = 200
                        }else{
                            cell.heightCell.constant = 0
                        }
                        cell.sliderImgCollVw.reloadData()
                        cell.setNeedsLayout()
                        return cell
                    default:
                        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell2", for: indexPath) as! CategoryCell
                        return cell
                    }
                }
                else if object.postType == 4{
                    switch indexPath.row {
                    case 0:
                        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell", for: indexPath) as! CategoryCell
                        cell.lblTitle.text = detailsPostObj?.content?.title ?? ""
                        return cell
                        
                    case 1:
                        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell1", for: indexPath) as! CategoryCell
                        cell.lblTitle.text = detailsPostObj?.content?.summary ?? ""
                        cell.lblSecondText.text = detailsPostObj?.created_at_formatted ?? ""
                        if let cover = URL(string :detailsPostObj?.medic?.cover_media?.cover_url ?? ""){
                            cell.imgviewCell.kf.indicatorType = .activity
                            cell.imgviewCell.kf.setImage(with: cover)
                        }
                        return cell
                    case 2:
                        let cell = tableView.dequeueReusableCell(withIdentifier: "VideoCell", for: indexPath) as! VideoCell
                        if let youtube = detailsPostObj?.content?.youtube_video_id{
                        //https://www.youtube.com/watch?v=WBl10Z6aGfE
                            if let youtubeUrl = URL(string:"https://www.youtube.com/embed/" + "\(youtube)"){

                        
                    
                        
                        let urlRequest = URLRequest(url: youtubeUrl)
                        cell.heightView.constant = 250
                        cell.viewWeb.load(urlRequest)
                        cell.viewWeb.navigationDelegate = self
                        
                            
                            }
                        
                       }else{
                        cell.heightView.constant = 0
                        }
                                               
                        return cell
                    default:
                        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell2", for: indexPath) as! CategoryCell
                        return cell
                    }
                }
                
            }
        }
        }else if indexPath.section == 1{
            var object : Any?
            if let object1 = detailsResultObj{
                let comment = object1.comments?[indexPath.row]
                let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewCell", for: indexPath) as! ReviewCell
                cell.lblTitle.text = comment?.user?.username
                cell.lblComment.text = comment?.content ?? ""
                if let url = URL(string: comment?.user?.url_image ?? ""){
                    cell.imgviewCell.kf.indicatorType = .activity
                    cell.imgviewCell.kf.setImage(with: url)
                }
                cell.lblReplyCount.text = "\(comment?.comments_count ?? 0) Responder"
                cell.lblCount.text = "\(comment?.likes_count ?? 0) Curtir"
                cell.lblDatePost.text = comment?.created_at
                cell.btnLike.tag = indexPath.row
                cell.btnReply.tag = indexPath.row
                return cell
            }else if let object2 = detailsPostObj{
                let comments = object2.comments?[indexPath.row]
                let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewCell", for: indexPath) as! ReviewCell
                cell.lblTitle.text = comments?.user?.username
                cell.lblComment.text = comments?.content ?? ""
                if let url = URL(string: comments?.user?.url_image ?? ""){
                    cell.imgviewCell.kf.indicatorType = .activity
                    cell.imgviewCell.kf.setImage(with: url)
                }
                cell.lblReplyCount.text = "\(comments?.comments_count ?? 0) Responder"
                cell.lblCount.text = "\(comments?.likes_count ?? 0) Curtir"
                cell.lblDatePost.text = comments?.created_at
                cell.btnLike.tag = indexPath.row
                cell.btnReply.tag = indexPath.row
                return cell
            }
            
        }
        return UITableViewCell()
    }
}
extension DashboardDetailsVC{
    func callImagesDetailsResultadosAPI(key:Int) {
        
        var getUrl :String = API.ResultaosImages.getURL()?.absoluteString ?? ""
       
        getUrl.append("\(key)")
        
        let operation = WebServiceOperation.init(getUrl, nil, .WEB_SERVICE_GET, nil)
        
        operation.completionBlock =
            {
                DispatchQueue.main.async {
                    
                    print(operation.responseData?.dictionary?.prettyprintedJSON ?? "")
                    guard let dictResponse = operation.responseData?.dictionary, dictResponse.count > 0 else {
                        
                        return
                    }
                    
                    
                    if let dataUserDetails = dictResponse.data, dataUserDetails.count > 0 {
                        do{
                            
                            let mediaBase = try JSONDecoder().decode(Media_Base.self, from: dataUserDetails)
                            self.arrayMediasResult = mediaBase.medias
                            //
                        }catch let e{
                            print(e.localizedDescription)
                            
                            //showAlertMessage(title: App_Title, message: "Something went wrong", vc: self)
                        }
                    }
                    DispatchQueue.main.async {
                        self.tableHomeDetails.reloadData()
                        
                    }
                    
                }
        }
        
        appDel.operationQueue.addOperation(operation)
        
    }
    func callPostAPI(relatedID:Int,content:String) {
        
     let getUrl :String = API.Comments.getURL()?.absoluteString ?? ""
     var param :[String:AnyObject] = [:]
     param["related_id"] = relatedID as AnyObject
     
         param["related_class"] = "posts" as AnyObject
    
         param["content"] = content as AnyObject
     
     
    
     
        
        let operation = WebServiceOperation.init(getUrl, param, .WEB_SERVICE_MULTI_PART, nil)
        
        operation.completionBlock =
            {
                DispatchQueue.main.async {
                    
                    print(operation.responseData?.dictionary?.prettyprintedJSON ?? "")
                    guard let dictResponse = operation.responseData?.dictionary, dictResponse.count > 0 else {
                        
                        return
                    }
                    
                    
                
                     if let dataUserDetails = dictResponse.data, dataUserDetails.count > 0 {
                         do{
                            if let result = dictResponse["comment"] as? [String:Any]{
                                let comment = try JSONDecoder().decode(Comments.self, from: result.data!)
                            if self.detailsResultObj != nil{
                                if let getComments = self.detailsResultObj?.comments{
                                self.detailsResultObj?.comments?.append(comment)
                                }else{
                                    self.detailsResultObj?.comments = []
                                    self.detailsResultObj?.comments?.append(comment)
                                }
                            }else{
                                if let getComments = self.detailsPostObj?.comments{
                                    self.detailsPostObj?.comments?.append(comment)
                                }else{
                                     self.detailsPostObj?.comments = []
                                     self.detailsPostObj?.comments?.append(comment)
                                }
                                 
                            }
                            }
                             
                             //
                         }catch let e{
                             print(e.localizedDescription)
                             
                             //showAlertMessage(title: App_Title, message: "Something went wrong", vc: self)
                         }
                     }
                     
                    self.tableHomeDetails.reloadSections([1], with: .automatic)
                         
                     
                    
                }
            }
        
        appDel.operationQueue.addOperation(operation)
        
    }
    func callCommentPostRegardingAPI(relatedID:String) {
        if #available(iOS 13.0, *) {
            let keyWindow = UIApplication.shared.connectedScenes
                .filter({$0.activationState == .foregroundActive})
                .map({$0 as? UIWindowScene})
                .compactMap({$0})
                .first?.windows
                .filter({$0.isKeyWindow}).first
            CustomActivityIndicator.sharedInstance.display(onView: keyWindow, done: {
                
            })
        } else {
            
            UIApplication.shared.keyWindow?.rootViewController?.view.endEditing(true)
            CustomActivityIndicator.sharedInstance.display(onView: UIApplication.shared.keyWindow, done: {
                
            })
        }
        
        
         let getUrl :String = BaseUrl + "/posts?post_key="+"\(relatedID)"
        if let urlString  = URL(string: getUrl){
            var request = URLRequest(url: urlString)
            //request.addValue("U4LsBbfYdKlYTB3kJYKpCKiG", forHTTPHeaderField: "aescare_session_key")
            request.httpMethod = "GET"
            URLSession.shared.dataTask(with: request) { data, response, error in
                //print(String(data: data!, encoding: .utf8)!)
                DispatchQueue.main.async {
                    CustomActivityIndicator.sharedInstance.hide {
                    }
                }
                if let dataTemp = data, dataTemp.count > 0 {
                    
                    do {
                         let comment = try JSONDecoder().decode([Comments].self, from: dataTemp)
                                                    if self.detailsResultObj != nil{
                        //                                if let getComments = self.detailsResultObj?.comments{
                        //                                self.detailsResultObj?.comments?.append(comment)
                        //                                }else{
                                                            self.detailsResultObj?.comments = []
                                                        self.detailsResultObj?.comments = comment
                                                    }else{
                        //                                if let getComments = self.detailsPostObj?.comments{
                        //                                    self.detailsPostObj?.comments?.append(comment)
                        //                                }else{
                                                             self.detailsPostObj?.comments = []
                                                             self.detailsPostObj?.comments = comment
                                                        //}
                                                         
                                                    }
                        
                        //self.arraycomments = try JSONDecoder().decode([Comments].self, from: dataTemp)
                       
                       
                        
                    } catch let error  {
                        print("Parsing Failed \(error.localizedDescription)")
                    }
                     DispatchQueue.main.async {
                    self.tableHomeDetails.reloadSections([1], with: .automatic)
                    }
                }
            }.resume()
        }
    }
    func callCommentPostAPI(relatedID:Int) {
        if #available(iOS 13.0, *) {
            let keyWindow = UIApplication.shared.connectedScenes
                .filter({$0.activationState == .foregroundActive})
                .map({$0 as? UIWindowScene})
                .compactMap({$0})
                .first?.windows
                .filter({$0.isKeyWindow}).first
            CustomActivityIndicator.sharedInstance.display(onView: keyWindow, done: {
                
            })
        } else {
            
            UIApplication.shared.keyWindow?.rootViewController?.view.endEditing(true)
            CustomActivityIndicator.sharedInstance.display(onView: UIApplication.shared.keyWindow, done: {
                
            })
        }
        
        
         let getUrl :String = BaseUrl + "/comments?related_id="+"\(relatedID)"+"&related_class=comments"
        if let urlString  = URL(string: getUrl){
            var request = URLRequest(url: urlString)
            //request.addValue("U4LsBbfYdKlYTB3kJYKpCKiG", forHTTPHeaderField: "aescare_session_key")
            request.httpMethod = "GET"
            URLSession.shared.dataTask(with: request) { data, response, error in
                //print(String(data: data!, encoding: .utf8)!)
                DispatchQueue.main.async {
                    CustomActivityIndicator.sharedInstance.hide {
                    }
                }
                if let dataTemp = data, dataTemp.count > 0 {
                    
                    do {
                         let comment = try JSONDecoder().decode([Comments].self, from: dataTemp)
                                                    if self.detailsResultObj != nil{
                        //                                if let getComments = self.detailsResultObj?.comments{
                        //                                self.detailsResultObj?.comments?.append(comment)
                        //                                }else{
                                                            self.detailsResultObj?.comments = []
                                                        self.detailsResultObj?.comments = comment
                                                    }else{
                        //                                if let getComments = self.detailsPostObj?.comments{
                        //                                    self.detailsPostObj?.comments?.append(comment)
                        //                                }else{
                                                             self.detailsPostObj?.comments = []
                                                             self.detailsPostObj?.comments = comment
                                                        //}
                                                         
                                                    }
                        
                        //self.arraycomments = try JSONDecoder().decode([Comments].self, from: dataTemp)
                       
                       
                        
                    } catch let error  {
                        print("Parsing Failed \(error.localizedDescription)")
                    }
                     DispatchQueue.main.async {
                    self.tableHomeDetails.reloadSections([1], with: .automatic)
                    }
                }
            }.resume()
        }
    }
    func callCommentPostAPI2(relatedID:Int) {
        
     let getUrl :String = BaseUrl + "/comments?related_id="+"\(relatedID)"+"&related_class=comments"
     //var param :[String:AnyObject] = [:]
     //param["related_id"] = relatedID as AnyObject
     
        // param["related_class"] = "posts" as AnyObject
    
        let operation = WebServiceOperation.init(getUrl, nil, .WEB_SERVICE_GET, nil)
        
        operation.completionBlock =
            {
                DispatchQueue.main.async {
                    
                    print(operation.responseData?.dictionary?.prettyprintedJSON ?? "")
                    guard let dictResponse = operation.responseData?.dictionary, dictResponse.count > 0 else {
                        
                        return
                    }
                    
                    
                
                     if let dataUserDetails = dictResponse.data, dataUserDetails.count > 0 {
                         do{
                           
                                let comment = try JSONDecoder().decode([Comments].self, from: dataUserDetails)
                            if self.detailsResultObj != nil{
//                                if let getComments = self.detailsResultObj?.comments{
//                                self.detailsResultObj?.comments?.append(comment)
//                                }else{
                                    self.detailsResultObj?.comments = []
                                self.detailsResultObj?.comments = comment
                            }else{
//                                if let getComments = self.detailsPostObj?.comments{
//                                    self.detailsPostObj?.comments?.append(comment)
//                                }else{
                                     self.detailsPostObj?.comments = []
                                     self.detailsPostObj?.comments = comment
                                //}
                                 
                            }
                            
                             
                             //
                         }catch let e{
                             print(e.localizedDescription)
                             
                             //showAlertMessage(title: App_Title, message: "Something went wrong", vc: self)
                         }
                     }
                     
                    self.tableHomeDetails.reloadSections([1], with: .automatic)
                         
                     
                    
                }
            }
        
        appDel.operationQueue.addOperation(operation)
        
    }
}

extension DashboardDetailsVC {
    @objc func tapToCloseImagePreview(_ tap: UITapGestureRecognizer) {
        self.scrollView.isHidden = true
    }
}

extension DashboardDetailsVC: SliderCellDelegate, UIScrollViewDelegate {
    func didSelectImageToPreview(didSelectedImage image: UIImage) {
        DispatchQueue.main.async {
            self.imagePreview.image = image
            self.scrollView.isHidden = false
        }
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.imagePreview
    }
}
