//
//  DoctorImageCell.swift
//  AesCare
//
//  Created by Mahesh Mahalik on 23/08/20.
//

import UIKit
import Cosmos
class DoctorImageCell: UITableViewCell {
    
    @IBOutlet weak var lblPostDate: UILabel!
    @IBOutlet weak var viewRate: CosmosView!
    @IBOutlet weak var heightImage: NSLayoutConstraint!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgViewCell: designableImage!
    @IBOutlet weak var lblDesc: UILabel!
    
    @IBOutlet weak var lblRate: UILabel!
    @IBOutlet weak var ViewFav: UIStackView!
    @IBOutlet weak var imgViewProf: UIImageView!
    @IBOutlet weak var imgViewPost: UIImageView!
    @IBOutlet weak var imgViewFav: UIImageView!
    var isMedic :Bool = false
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func updateCell(info:Posts){
        if isMedic == false{
            
            self.imgViewProf.isHidden = false
            self.ViewFav.isHidden = true
            self.lblName.text = info.user?.username ?? ""
            self.lblDesc.text = ""
            self.lblDesc.numberOfLines = 4
            self.imgViewPost.startAnimating()
            if let mediasTemp = info.medias , mediasTemp.count > 0, let url = URL(string: mediasTemp[0].image_url ?? ""){
                 self.imgViewCell.kf.indicatorType = .activity
                self.imgViewPost.kf.setImage(with: url)
                self.heightImage.constant = 140
            }else{
                self.imgViewPost.image = UIImage(named: "")
                self.heightImage.constant = 0
            }
            
            if let url = URL(string: info.user?.image_url ?? ""){
                 self.imgViewCell.kf.indicatorType = .activity
                self.imgViewCell.kf.setImage(with: url)
            }else{
                self.imgViewCell.image = UIImage(named: "")
            }
        }else{
            self.imgViewProf.isHidden = false
            
            self.ViewFav.isHidden = false
            self.lblName.text = info.medic?.name ?? ""
            self.lblDesc.text = info.content?.summary ?? ""
            self.imgViewPost.startAnimating()
            if let url = URL(string: info.content?.cover_image_url ?? ""){
                                 self.imgViewPost.kf.setImage(with: url)
                                  self.heightImage.constant = 140
                             }else{
              self.imgViewPost.image = UIImage(named: "")
            self.heightImage.constant = 0
             }
            
            if let url = URL(string: info.medic?.image_url ?? ""){
                 self.imgViewCell.kf.indicatorType = .activity
                self.imgViewCell.kf.setImage(with: url)
            }else{
                self.imgViewCell.image = UIImage(named: "")
            }
        }
        
    }
    func updateResultCell(result:Answers){
        //self.imgViewProf.isHidden = false
        //self.ViewFav.isHidden = false
        self.lblName.text = result.name ?? ""
        self.lblDesc.text = "Cirurgia Plastica"
        self.lblRate.text = "\(result.evaluation_result ?? 0)"
        self.lblPostDate.text = result.city_name ?? ""
       // self.imgViewCell.startAnimating()
        //            if let url = URL(string: info.medic.cover_media?.image_url ?? ""){
        //                     self.imgViewPost.kf.setImage(with: url)
        //                      self.heightImage.constant = 140
        //                 }else{
        //  self.imgViewPost.image = UIImage(named: "")
        //self.heightImage.constant = 0
        // }

        if let url = URL(string: result.cover_media?.image_url ?? ""){
             self.imgViewCell.kf.indicatorType = .activity
            self.imgViewCell.kf.setImage(with: url)
        }else{
            self.imgViewCell.image = UIImage(named: "")
        }
   }
    
}
