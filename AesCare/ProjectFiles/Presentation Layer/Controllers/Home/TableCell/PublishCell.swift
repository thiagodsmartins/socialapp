//
//  PublishCell.swift
//  AesCare
//
//  Created by Mahesh Mahalik on 23/08/20.
//

import UIKit

class PublishCell: UITableViewCell,UITextViewDelegate {
    @IBOutlet weak var imgUser: designableImage!
    @IBOutlet weak var txtVWStatus: UITextView!
    @IBOutlet weak var lblTextMessage: UILabel!
    
    var isTextTyped = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        txtVWStatus.textColor = UIColor.lightGray
        txtVWStatus.delegate = self
        if txtVWStatus.text == "" {
            txtVWStatus.placeholder = "Digite sua mensagem"
        }
        
        self.lblTextMessage.isHidden = true
        
        DispatchQueue.global().async {
            let data = try! Data(contentsOf: URL(string: usrerModelOBJ.user!.perfil_image ?? "")!)
        
            DispatchQueue.main.async {
                self.imgUser.image = UIImage(data: data)
            }
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            self.isTextTyped = true
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if(validate(textView)){
            print("======valid====")
        } else {
            textView.placeholder = "Digite sua mensagem"
            textView.textColor = UIColor.lightGray
        }
        /*
        if txt.isEmpty {
            textView.text = "No que você está pensando?"
            textView.textColor = UIColor.lightGray
        }*/
    }

    func validate(_ textView: UITextView) -> Bool {
        guard let text = textView.text,
            !text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).isEmpty else {
            // this will be reached if the text is nil (unlikely)
            // or if the text only contains white spaces
            // or no text at all
            return false
        }

        return true
    }
}
