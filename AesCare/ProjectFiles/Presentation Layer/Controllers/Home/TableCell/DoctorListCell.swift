//
//  DoctorListCell.swift
//  AesCare
//
//  Created by Mahesh Mahalik on 23/08/20.
//

import UIKit
import Kingfisher
import Cosmos
class DoctorListCell: UITableViewCell {
    
    @IBOutlet weak var viewRating: CosmosView!
    @IBOutlet weak var viewTrans: UIView!
    @IBOutlet weak var lblFourth: UILabel!
    @IBOutlet weak var lblThird: UILabel!
    @IBOutlet weak var lblSecond: UILabel!
    @IBOutlet weak var lblFirst: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imageCell: UIImageView!
    
    @IBOutlet weak var btnEye: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func updateCell(obj: ResultsFirst)  {
        
        self.lblTitle.text = obj.title ?? ""
        self.lblFourth.text = obj.medic_name ?? " "
        self.imageCell.startAnimating()
        self.viewRating.rating = Double(obj.evaluation_result ?? 0)
        if obj.isClickedEye == false{
            if let url = URL(string: obj.image_url ?? ""){
                
                var value =  ""
                if usrerModelOBJ != nil {
                    if let valueTemp =  usrerModelOBJ.session_key {
                        value = "aescare_session_key=" + valueTemp
                    }
                }
                
                
                let request = NSMutableURLRequest(url: url)
                request.setValue(value, forHTTPHeaderField: "Cookie")
                
                request.httpMethod = "GET"
                
                let dataTask = URLSession.shared.dataTask(with: url) { [weak self] (data, _, _) in
                    if let data = data {
                        // Create Image and Update Image View
                        DispatchQueue.main.async {
                            self?.imageCell.image = UIImage(data: data)
                        }
                     }
                }
                
                dataTask.resume()
               
                //self.imageCell.kf.setImage(with: url, options: [.requestModifier(TokenPlugin(cookie:value))])
                 
                
                //self.imageCell.kf.setImage(with: url)
            }else{
                self.imageCell.image = UIImage(named: "")
            }
        }else{
            if let url = URL(string: obj.image_url ?? ""){
                
                var value =  ""
                if let valueTemp =  usrerModelOBJ.session_key {
                    value = "aescare_session_key=" + valueTemp
                }
                 self.imageCell.kf.indicatorType = .activity
                self.imageCell.kf.setImage(with: url, options: [.requestModifier(TokenPlugin(cookie:value))])
                //self.imageCell.kf.setImage(with: url)
            }else{
                self.imageCell.image = UIImage(named: "")
            }
        }
        self.lblFirst.text =  "outro"
        self.lblThird.text = obj.payment_type ?? " "
        self.lblSecond.text = obj.treatment_value ?? " "
        
    }
    func updateResultCell(result:Answers){
        self.lblTitle.text = result.name_with_title ?? ""
               self.lblFourth.text = result.medic_name ?? " "
               self.imageCell.startAnimating()
               self.viewRating.rating = Double(result.evaluation_result ?? 0)
               if result.isClickedEye == false{
                   if let url = URL(string: result.image_url ?? ""){
                       
                       var value =  ""
                       if usrerModelOBJ != nil {
                           if let valueTemp =  usrerModelOBJ.session_key {
                               value = "aescare_session_key=" + valueTemp
                           }
                       }
                       
                       
                       let request = NSMutableURLRequest(url: url)
                       request.setValue(value, forHTTPHeaderField: "Cookie")
                       
                       request.httpMethod = "GET"
                       
                       let dataTask = URLSession.shared.dataTask(with: url) { [weak self] (data, _, _) in
                           if let data = data {
                               // Create Image and Update Image View
                               DispatchQueue.main.async {
                                   self?.imageCell.image = UIImage(data: data)
                               }
                            }
                       }
                       
                       dataTask.resume()
                      
                       //self.imageCell.kf.setImage(with: url, options: [.requestModifier(TokenPlugin(cookie:value))])
                        
                       
                       //self.imageCell.kf.setImage(with: url)
                   }else{
                       self.imageCell.image = UIImage(named: "")
                   }
               }else{
                   if let url = URL(string: result.image_url ?? ""){
                       
                       var value =  ""
                       if let valueTemp =  usrerModelOBJ.session_key {
                           value = "aescare_session_key=" + valueTemp
                       }
                    //var value = ""
                           if let valueTemp =  usrerModelOBJ.session_key {
                               value = "aescare_session_key=" + valueTemp
                           }
                           KingfisherManager.shared.defaultOptions = [.requestModifier(TokenPlugin(cookie:value))]
                        self.imageCell.kf.indicatorType = .activity
                       self.imageCell.kf.setImage(with: url, options: [.requestModifier(TokenPlugin(cookie:value))])
                       //self.imageCell.kf.setImage(with: url)
                   }else{
                       self.imageCell.image = UIImage(named: "")
                   }
               }
               self.lblFirst.text =  " "
               self.lblThird.text = result.payment_type ?? " "
               self.lblSecond.text = result.treatment_value ?? " "
    }
}
class TokenPlugin: ImageDownloadRequestModifier {
    let cookie:String

    init(cookie:String) {
        print("=======Cookie=====")
        print(cookie)
        self.cookie = cookie
    }

    func modified(for request: URLRequest) -> URLRequest? {
        var request = request
        request.addValue(cookie, forHTTPHeaderField: "Cookie")
        return request
    }
}
