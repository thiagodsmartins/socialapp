//
//  VideoCell.swift
//  AesCare
//
//  Created by Mahesh Mahalik on 17/10/20.
//

import UIKit
import WebKit

class VideoCell: UITableViewCell {
    @IBOutlet weak var heightView: NSLayoutConstraint!
    @IBOutlet weak var viewWeb: WKWebView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
