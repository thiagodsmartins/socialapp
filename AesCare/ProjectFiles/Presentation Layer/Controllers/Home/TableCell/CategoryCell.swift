//
//  CategoryCell.swift
//  AesCare
//
//  Created by Mahesh Mahalik on 30/08/20.
//

import UIKit
protocol SelectionDelegate:class {
    func selectGroup(index:Int)
}
class CategoryCell: UITableViewCell {
    @IBOutlet weak var imgRecentChat: UIImageView!
    @IBOutlet weak var lblFistTab: UILabel!
    weak var delegate:SelectionDelegate?
    @IBOutlet weak var lblSecondTab: UILabel!
    @IBOutlet weak var lblSecondText: UILabel!
    @IBOutlet weak var lblNoSlot: UILabel!
    @IBOutlet weak var lblFirstTect: UILabel!
    @IBOutlet weak var collectionViewText: UICollectionView!
    @IBOutlet weak var imageviewCellRight: UIImageView!
    @IBOutlet weak var heightCollection: NSLayoutConstraint!
    @IBOutlet weak var collectionCell: UICollectionView!
    @IBOutlet weak var imgviewCell: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    var arraySepared :[String] = []
    var doctorSlots : [ModelCalender]!
    var groupImage: [Chat_groups] = []
    var arraymyGroups:[Chats] = []
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        if let _ = collectionCell{
            collectionCell.delegate = self
            collectionCell.dataSource = self
            
            
        }
        if let _ = collectionViewText{
                        collectionViewText.delegate = self
                        collectionViewText.dataSource = self
                    }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    func updateCell(info:Medics){
        if info.calendar?.count ?? 0 > 0 {
            self.collectionCell.isHidden = false
            self.lblNoSlot.isHidden = true
        }else{
            self.collectionCell.isHidden = true
            self.lblNoSlot.isHidden = false
        }
        
        self.lblTitle.text = info.name ?? ""
        self.doctorSlots = info.calendar
        if let url = URL(string:info.cover_media?.image_url ?? ""){
            self.imgviewCell.kf.indicatorType = .activity
            self.imgviewCell.kf.setImage(with: url)
        }else{
            self.imgviewCell.image = UIImage(named: "")
        }
    }
    func updateDoctorDetailsCell(info:Medics){
        //self.lblTitle.text = info.name ?? ""
        //self.doctorSlots = info.calendar
        if let url = URL(string:info.cover_media?.image_url ?? ""){
            self.imgviewCell.kf.indicatorType = .activity
            self.imgviewCell.kf.setImage(with: url)
        }else{
            self.imgviewCell.image = UIImage(named: "")
        }
    }
    
    func showImage(_ index: Int) {
        self.imgRecentChat.layer.borderWidth = 1
        self.imgRecentChat.layer.masksToBounds = false
        self.imgRecentChat.layer.borderColor = UIColor.black.cgColor
        self.imgRecentChat.layer.cornerRadius = self.imgRecentChat.frame.height / 2
        self.imgRecentChat.clipsToBounds = true
        
        DispatchQueue.global().async {
            let data = try! Data(contentsOf: URL(string: "https://br.aescare.com/chat/photo/\(self.arraymyGroups[index].key ?? "")")!)
            
            DispatchQueue.main.async {
                self.imgRecentChat.image = UIImage(data: data)
            }
        }
    }
}



extension CategoryCell:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionViewText{
            return arraySepared.count
        }else{
            var cellRow = 0
            if let _ = doctorSlots{
                cellRow = doctorSlots.count
            }
            return cellRow
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let groupImg = groupImage[indexPath.row]
        if collectionView == collectionViewText{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SliderCollCell1", for: indexPath) as! SliderCollCell
            cell.labelMheaderName.text = arraySepared[indexPath.row]
            cell.show(groupImg)
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SliderCollCell", for: indexPath) as! SliderCollCell
            
            
            let date = UtilityClass.dateFormatterFlexibleLocal(strDate: doctorSlots[indexPath.row].date_start ?? "00/00", fromDateFromat: "yyyy-MM-dd'T'HH:mm:ssZ", toDateFormat: "dd/MM")
            let hour = UtilityClass.dateFormatterFlexibleLocal(strDate: doctorSlots[indexPath.row].date_start ?? "00:00", fromDateFromat: "yyyy-MM-dd'T'HH:mm:ssZ", toDateFormat: "HH:mm")
            cell.labelUnderLine.text = date
            cell.labelMheaderName.text = hour + "h"
            cell.layoutIfNeeded()
            
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.delegate?.selectGroup(index: indexPath.row)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //return CGSize(width: 90, height: 60)
        if collectionView == collectionViewText{
            return  CGSize(width: 150, height: 50)
        }else{
            return CGSize(width: ((self.collectionCell.frame.size.width / 2) - 5), height: 50)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        //         if collectionView == collectionViewText{
        //        return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        //         }else{
        return .zero
        //  }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == collectionViewText{
            return 0
        }else{
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == collectionViewText{
            return 0
        }else{
            return 0
        }
    }
}
