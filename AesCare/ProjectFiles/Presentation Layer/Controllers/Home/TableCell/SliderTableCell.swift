//
//  SliderTableCell.swift
//  AesCare
//
//  Created by Mahesh Mahalik on 16/08/20.
//

import UIKit

class SliderTableCell: UITableViewCell {

    @IBOutlet weak var collectionSlider: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionSlider.delegate = self
        collectionSlider.dataSource = self
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
      // Configure the view for the selected state
    }

}
extension SliderTableCell:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SliderCollCell", for: indexPath) as! SliderCollCell
        cell.imageviewSlider.layer.cornerRadius = cell.imageviewSlider.frame.height / 2
        cell.imageviewSlider.clipsToBounds = true
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 60, height: 60)
    }
}
