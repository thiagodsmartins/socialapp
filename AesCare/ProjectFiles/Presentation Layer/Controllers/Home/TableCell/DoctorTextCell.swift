//
//  DoctorTextCell.swift
//  AesCare
//
//  Created by Mahesh Mahalik on 23/08/20.
//

import UIKit

class DoctorTextCell: UITableViewCell {

    @IBOutlet weak var imgviewCell2: designableImage!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var imgViewCell: designableImage!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func updateCellInfo(post:Posts){
        self.lblTitle.text = post.content?.title ?? ""
        self.lblDesc.text = post.content?.summary ?? ""
        self.lblDate.text = "0 d"
        if let cretaeDate = post.created_at_formatted{
            let formated = DateFormatter()
            formated.dateFormat = "dd/MM/yyyy HH:mm"
            
            if let dateCreated = formated.date(from: cretaeDate){
                let today = Date()
                let components = Calendar.current.dateComponents([.day], from: dateCreated, to: today)

                self.lblDate.text = "\(components.day ?? 0) d"
                
            }
        }
        if let url = URL(string: post.medic?.image_url ?? ""){
            self.imgViewCell.kf.indicatorType = .activity
            self.imgViewCell.kf.setImage(with: url)
            
        }else{
            self.imgViewCell.image = UIImage(named: "")
           
        }
    }
    
    
    func updateResultCell(post:Answers){
        self.lblTitle.text = post.name ?? ""
            self.lblDesc.text = post.city_name ?? ""
               self.lblDate.text = "0 d"
               if let cretaeDate = post.created_at_formatted{
                   let formated = DateFormatter()
                   formated.dateFormat = "dd/MM/yyyy HH:mm"
                   
                   if let dateCreated = formated.date(from: cretaeDate){
                       let today = Date()
                       let components = Calendar.current.dateComponents([.day], from: dateCreated, to: today)

                       self.lblDate.text = "\(components.day ?? 0) d"
                       
                   }
               }
               if let url = URL(string: post.medic?.image_url ?? ""){
                   self.imgViewCell.kf.indicatorType = .activity
                   self.imgViewCell.kf.setImage(with: url)
                   
               }else{
                   self.imgViewCell.image = UIImage(named: "")
                  
               }
    }

}
