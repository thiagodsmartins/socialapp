//
//  ShowRateVC.swift
//  AesCare
//
//  Created by Gali Srikanth on 29/09/20.
//

import UIKit

class ShowRateVC: BaseViewController {
    var gradientLayer: CAGradientLayer!
    @IBOutlet weak var vwNavigation: UIView!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var containerSearch: designableView!
    
    
    @IBOutlet weak var tblSchedule: UITableView!
    
    var booking_key = ""
    var amount = ""
    var arrScheduleInfo = [String]()
    var arrCardInfo = [String]()
    var cvv = ""
    enum CellContent_Schedule:Int{
        case CPF = 0
        case CEP
        case Endereço
        case Número
        case Cidade
        case Estado
        case total
        func getPlaceholder () -> String {
            switch self {
            case .CPF:
                return "CPF (Somente Números)"
            case .CEP:
                return "CEP"
            case .Endereço:
                return "Endereço"
            case .Número:
                return "Número"
            case .Cidade:
                return "Cidade"
            case .Estado:
                return "Estado"
            default:
                return ""
            }
            
        }
    }
    enum CellContent_BankInfo:Int{
        case CardNumber = 0
        case ValidationDate
        case NameofCard
        case total
        func getPlaceholder () -> String {
            switch self {
            case .CardNumber:
                return "Número do Cartão"
            case .ValidationDate:
                return "Data de Validade (MM/AA)"
            case .NameofCard:
                return "Nome no Cartão"
            default:
                return ""
            }
            
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        for _ in 0 ..< CellContent_Schedule.total.rawValue {
            arrScheduleInfo.append("")
        }
        for _ in 0 ..< CellContent_BankInfo.total.rawValue {
            arrCardInfo.append("")
        }
        
        createGradientLayer()
        // Do any additional setup after loading the view.
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        ///// Call Custom Tabbar

        gradientLayer.frame = self.vwNavigation.bounds
    }

    func createGradientLayer() {
        gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.vwNavigation.bounds
        gradientLayer.colors = [UIColor(named: "AppPurpleColor")!.cgColor, UIColor(named: "AppMoveColor")!.cgColor]
        self.vwNavigation.layer.addSublayer(gradientLayer)
        self.vwNavigation.bringSubviewToFront(self.btnMenu)
        self.vwNavigation.bringSubviewToFront(self.containerSearch)
    }
    
    @IBAction func backNavigationPressed(_ sender: Any) {
        backNavigation()
    }
    @IBAction func navigationNext(_ sender: Any) {
        if(isValidAllData()){
            
            
            if let savedUser = UserDefaults.standard.object(forKey: "LOGGED_IN_USER_DATA") as? Data {
                let decoder = JSONDecoder()
                if let replaceobj = try? decoder.decode(ModelUser.self, from: savedUser) {
                    
                    var param:[String:AnyObject] = [String:AnyObject]()
                    param["action"] = "validate" as AnyObject
                    param["key"] = booking_key as AnyObject
                    param["name"] = replaceobj.user?.firstname as AnyObject
                    param["user_id"] = replaceobj.user?.user_id as AnyObject
                    param["cpf"] = arrScheduleInfo[CellContent_Schedule.CPF.rawValue] as AnyObject
                    param["amount_total"] = amount as AnyObject
                    
                    param["address"] = arrScheduleInfo[CellContent_Schedule.Endereço.rawValue] as AnyObject
                    
                    param["street_number"] = arrScheduleInfo[CellContent_Schedule.Número.rawValue] as AnyObject
                    param["city"] = arrScheduleInfo[CellContent_Schedule.Cidade.rawValue] as AnyObject
                    
                    param["zipcode"] = arrScheduleInfo[CellContent_Schedule.CEP.rawValue] as AnyObject
                    param["state"] = arrScheduleInfo[CellContent_Schedule.Estado.rawValue] as AnyObject as AnyObject
                    
                    
                    
                    param["creditcard_card_number"] = arrCardInfo[CellContent_BankInfo.CardNumber.rawValue] as AnyObject
                    param["creditcard_card_name"] = arrCardInfo[CellContent_BankInfo.NameofCard.rawValue] as AnyObject
                    param["creditcard_card_due_date"] = arrCardInfo[CellContent_BankInfo.ValidationDate.rawValue] as AnyObject
                    param["creditcard_card_cvv"] = cvv as AnyObject
                    
                    
                    print(param)
                    validatePayment(param: param)
                }
            }
        }
        
    }
    
    func isValidAllData() -> Bool {
        guard !arrScheduleInfo[CellContent_Schedule.CPF.rawValue].isEmpty else {
            showAlertMessage(title: "Ooops", message: ALERT_MESSAGE_MANDETORY, vc: self)
            return false
        }
        guard !arrScheduleInfo[CellContent_Schedule.CEP.rawValue].isEmpty else {
            showAlertMessage(title: "Ooops", message: ALERT_MESSAGE_MANDETORY, vc: self)
            return false
        }
        guard !arrScheduleInfo[CellContent_Schedule.Endereço.rawValue].isEmpty else {
            showAlertMessage(title: "Ooops", message: ALERT_MESSAGE_MANDETORY, vc: self)
            return false
        }
        guard !arrScheduleInfo[CellContent_Schedule.Número.rawValue].isEmpty else {
            showAlertMessage(title: "Ooops", message: ALERT_MESSAGE_MANDETORY, vc: self)
            return false
        }
        
        guard !arrScheduleInfo[CellContent_Schedule.Cidade.rawValue].isEmpty else {
            showAlertMessage(title: "Ooops", message: ALERT_MESSAGE_MANDETORY, vc: self)
            return false
        }
        guard !arrScheduleInfo[CellContent_Schedule.Estado.rawValue].isEmpty else {
            showAlertMessage(title: "Ooops", message: ALERT_MESSAGE_MANDETORY, vc: self)
            return false
        }
        guard !cvv.isEmpty else {
            showAlertMessage(title: "Ooops", message: ALERT_MESSAGE_MANDETORY, vc: self)
            return false
        }
        guard !arrCardInfo[CellContent_BankInfo.CardNumber.rawValue].isEmpty else {
            showAlertMessage(title: "Ooops", message: ALERT_MESSAGE_MANDETORY, vc: self)
            return false
        }
        guard !arrCardInfo[CellContent_BankInfo.ValidationDate.rawValue].isEmpty else {
            showAlertMessage(title: "Ooops", message: ALERT_MESSAGE_MANDETORY, vc: self)
            return false
        }
        
        guard !arrCardInfo[CellContent_BankInfo.NameofCard.rawValue].isEmpty else {
            showAlertMessage(title: "Ooops", message: ALERT_MESSAGE_MANDETORY, vc: self)
            return false
        }
        
        
        return true
    }
}
extension ShowRateVC : UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return section == 1 ? 90 : 0
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if(section == 1){
            let cell = tableView.dequeueReusableCell(withIdentifier: "CommonCellTableViewCell2") as! CommonCellTableViewCell
            return cell
        }
        return nil
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? CellContent_Schedule.total.rawValue : CellContent_BankInfo.total.rawValue
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section == 0){
            let cell: CommonCellTableViewCell = self.tblSchedule.dequeueReusableCell(withIdentifier: "CommonCellTableViewCell") as! CommonCellTableViewCell
            let type = CellContent_Schedule.init(rawValue: indexPath.row)
            cell.lblHeader.text = type?.getPlaceholder()
            
            cell.cellSelectionText.tag = indexPath.row
            cell.cellSelectionText.text = arrScheduleInfo[indexPath.row]
            cell.cellSelectionText.placeholder = type?.getPlaceholder()
            cell.cellSelectionText.removeTarget(nil,
                                                action: nil,
                                                for: .allEvents)
            cell.cellSelectionText.addTarget(self, action: #selector(self.textFieldValueChange(_:)), for: .editingChanged)
            cell.cellSelectionText.keyboardType = .default
            if(indexPath.row == CellContent_Schedule.CPF.rawValue || indexPath.row == CellContent_Schedule.CEP.rawValue){
                cell.cellSelectionText.keyboardType = .decimalPad
            }
            return cell
        }else{
            if(indexPath.row == CellContent_BankInfo.ValidationDate.rawValue){
                let cell: CommonCellTableViewCell = self.tblSchedule.dequeueReusableCell(withIdentifier: "CommonCellTableViewCell1") as! CommonCellTableViewCell
                let type = CellContent_BankInfo.init(rawValue: indexPath.row)
                cell.lblHeader.text = type?.getPlaceholder()
                
                cell.cellSelectionText.tag = indexPath.row
                cell.cellSelectionText2.tag = indexPath.row
                
                cell.cellSelectionText.text = cvv
                cell.cellSelectionText2.text = arrCardInfo[CellContent_BankInfo.ValidationDate.rawValue]
                
                cell.cellSelectionText.placeholder = "CVV"
                cell.cellSelectionText2.placeholder = type?.getPlaceholder()
                
                cell.btnInstance.addTarget(self,
                action: #selector(self.openValidationDate(_:)),
                for: .touchUpInside)
                cell.cellSelectionText.keyboardType = .default
                cell.cellSelectionText.addTarget(self, action: #selector(self.textFieldCVVValueChange), for: .editingChanged)
                return cell
            }else{
                let cell: CommonCellTableViewCell = self.tblSchedule.dequeueReusableCell(withIdentifier: "CommonCellTableViewCell") as! CommonCellTableViewCell
                let type = CellContent_BankInfo.init(rawValue: indexPath.row)
                cell.lblHeader.text = type?.getPlaceholder()
                
                cell.cellSelectionText.text = ""
                cell.cellSelectionText.text = arrCardInfo[indexPath.row]
                cell.cellSelectionText.placeholder = type?.getPlaceholder()
                cell.cellSelectionText.removeTarget(nil,
                                                    action: nil,
                                                    for: .allEvents)
                cell.cellSelectionText.addTarget(self, action: #selector(self.textFieldCardInfoValueChange(_:)), for: .editingChanged)
                
                cell.cellSelectionText.tag = indexPath.row
                cell.cellSelectionText.keyboardType = .default
                if(indexPath.row == CellContent_BankInfo.CardNumber.rawValue){
                    cell.cellSelectionText.keyboardType = .decimalPad
                }
                return cell
            }
        }
    }
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        UIApplication.shared.open(URL)
        return false
    }
    
}
extension ShowRateVC{
    @objc func textFieldValueChange(_ txt: UITextField)  {
        if let str = txt.text, str.count > 0 {
            self.arrScheduleInfo[txt.tag] = str
        }else{
            self.arrScheduleInfo[txt.tag] = ""
        }
    }
    @objc func textFieldCVVValueChange(_ txt: UITextField)  {
        if let str = txt.text, str.count > 0 {
            cvv = str
        }else{
            cvv = ""
        }
    }
    @objc func textFieldCardInfoValueChange(_ txt: UITextField)  {
        if let str = txt.text, str.count > 0 {
            self.arrCardInfo[txt.tag] = str
        }else{
            self.arrCardInfo[txt.tag] = ""
        }
    }
    
    @objc func openValidationDate(_ sender:UIButton){
        MyBasics.showDatePickerDropDown(PickerType: .date, ParentViewC: self, setRestriction: "")
    }
   
}
extension ShowRateVC: DatePickerDelegate{
    func selectedDate(selectedDate: Date) {
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/YY"
        arrCardInfo[CellContent_BankInfo.ValidationDate.rawValue] = formatter.string(from: selectedDate)
        tblSchedule.reloadData()
    }
    
}
extension ShowRateVC{
    func validatePayment(param : [String:AnyObject]) {
        
        let url = (API.validatePayment.getURL()?.absoluteString ?? "")
        let operation =  WebServiceOperation.init(url,param, .WEB_SERVICE_MULTI_PART,nil)
        
        operation.completionBlock =
            {
                
                DispatchQueue.main.async {
                    
                    print(operation.responseData?.dictionary?.prettyprintedJSON ?? "")
                    guard let dictResponse = operation.responseData?.dictionary, dictResponse.count > 0 else {
                        
                        return
                    }
                    
                    if let responceCode = dictResponse["code"] ,responceCode as! Int == 1 ,let msg = dictResponse["message"] as? String{
                        self.navigateAfterPayment(msg: msg)
                        
                    } else if let msg = dictResponse["message"] as? String{
                        UtilityClass.tosta(message: msg, duration: 2.0, vc: self)
                    }
                    else{
                        UtilityClass.tosta(message: "Something wentwrong", duration: 2.0, vc: self)
                    }
                }
        }
        
        appDel.operationQueue.addOperation(operation)
        
    }
    
    func navigateAfterPayment(msg:String)  {
        if #available(iOS 13.0, *) {
            let vc = self.storyboard?.instantiateViewController(identifier: "PaymentConfirmationVC") as! PaymentConfirmationVC
            UtilityClass.tosta(message: msg, duration: 2.0, vc: vc)
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            let vc = PaymentConfirmationVC.init(nibName: "PaymentConfirmationVC", bundle: nil)
            UtilityClass.tosta(message: msg, duration: 2.0, vc: vc)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
