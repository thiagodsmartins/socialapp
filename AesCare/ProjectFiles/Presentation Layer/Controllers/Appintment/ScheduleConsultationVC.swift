//
//  ScheduleConsultationVC.swift
//  AesCare
//
//  Created by Gali Srikanth on 27/09/20.
//

import UIKit
import AMTabView

class ScheduleConsultationVC: BaseViewController, TabItem {
    var gradientLayer: CAGradientLayer!
    @IBOutlet weak var vwNavigation: UIView!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var containerSearch: designableView!
    
    
    @IBOutlet weak var tblSchedule: UITableView!
    var arrScheduleInfo = [String]()
    var countryCode = ""
    var medico = ""
    var medic_id = 0
    var selectedSlot : Slots!
    var medicModel:Medics?
    enum CellContent_Schedule:Int{
        case Doctor = 0
        case YourName
        case Celular
        case ConfirmationCode
        case Email
        case YourInterests
        case total
        func getPlaceholder () -> String {
            switch self {
            case .Doctor:
                return "Médico"
            case .YourName:
                return "Seu Nome"
            case .Celular:
                return "Seu Celular (WhatsApp)"
            case .ConfirmationCode:
                return "Código de Confirmação"
            case .Email:
                return "Seu e-mail"
            case .YourInterests:
                return "Seus interesses"
            default:
                return ""
            }
            
        }
    }
    
    var tabImage: UIImage? {
        return UIImage(named: "agendar-Active")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        for _ in 0 ..< CellContent_Schedule.total.rawValue {
            arrScheduleInfo.append("")
        }
        arrScheduleInfo[0] = medico
        //createGradientLayer()
        // Do any additional setup after loading the view.
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        ///// Call Custom Tabbar
        //gradientLayer.frame = self.vwNavigation.bounds
    }

    func createGradientLayer() {
        gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.vwNavigation.bounds
        gradientLayer.colors = [UIColor(named: "AppPurpleColor")!.cgColor, UIColor(named: "AppMoveColor")!.cgColor]
        self.vwNavigation.layer.addSublayer(gradientLayer)
        self.vwNavigation.bringSubviewToFront(self.btnMenu)
        self.vwNavigation.bringSubviewToFront(self.containerSearch)
    }
    
    @IBAction func openSideMenu(_ sender: Any) {
        openSidePanel()
    }
    @IBAction func backNavigationPressed(_ sender: Any) {
        backNavigation()
    }
    func isValidAllData() -> Bool {
        guard !arrScheduleInfo[CellContent_Schedule.YourName.rawValue].isEmpty else {
            showAlertMessage(title: "Ooops", message: ALERT_MESSAGE_MANDETORY, vc: self)
            return false
        }
        guard !arrScheduleInfo[CellContent_Schedule.Celular.rawValue].isEmpty else {
            showAlertMessage(title: "Ooops", message: ALERT_MESSAGE_MANDETORY, vc: self)
            return false
        }
        guard !arrScheduleInfo[CellContent_Schedule.Email.rawValue].isEmpty else {
            showAlertMessage(title: "Ooops", message: ALERT_MESSAGE_MANDETORY, vc: self)
            return false
        }
        guard !arrScheduleInfo[CellContent_Schedule.YourInterests.rawValue].isEmpty else {
            showAlertMessage(title: "Ooops", message: ALERT_MESSAGE_MANDETORY, vc: self)
            return false
        }
        
        return true
    }
    @IBAction func navigationNext(_ sender: Any) {
        if(isValidAllData()){
            var param:[String:AnyObject] = [String:AnyObject]()
            param["medic_id"] = medic_id as AnyObject
            param["name"] = arrScheduleInfo[CellContent_Schedule.YourName.rawValue] as AnyObject
            param["phone"] = (countryCode + arrScheduleInfo[CellContent_Schedule.Celular.rawValue]) as AnyObject
            param["email"] = arrScheduleInfo[CellContent_Schedule.Email.rawValue] as AnyObject
            param["subject"] = arrScheduleInfo[CellContent_Schedule.YourInterests.rawValue] as AnyObject
            param["selected_slot"] = selectedSlot.medic_calendar_slot_id as AnyObject
            
            if #available(iOS 13.0, *) {
                let vc = self.storyboard?.instantiateViewController(identifier: "ConsultationDataVC") as! ConsultationDataVC
                vc.selectedSlot = selectedSlot
                vc.paramToSend = param
                vc.medicModel = medicModel
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                let vc = ConsultationDataVC.init(nibName: "ConsultationDataVC", bundle: nil)
                vc.selectedSlot = selectedSlot
                vc.paramToSend = param
                vc.medicModel = medicModel
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
}
extension ScheduleConsultationVC : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 90
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommonCellTableViewCell2") as! CommonCellTableViewCell
        return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return CellContent_Schedule.total.rawValue
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.row == CellContent_Schedule.Celular.rawValue){
            let cell: CommonCellTableViewCell = self.tblSchedule.dequeueReusableCell(withIdentifier: "CommonCellTableViewCell1") as! CommonCellTableViewCell
            let type = CellContent_Schedule.init(rawValue: indexPath.row)
            cell.lblHeader.text = type?.getPlaceholder()
            
            cell.cellSelectionText.tag = indexPath.row
            cell.cellSelectionText.text = countryCode
            cell.cellSelectionText.placeholder = "código de telefone do país"
            cell.cellSelectionText2.tag = indexPath.row
            cell.cellSelectionText2.text = arrScheduleInfo[CellContent_Schedule.Celular.rawValue]
            cell.cellSelectionText2.placeholder = type?.getPlaceholder()
            cell.cellSelectionText2.removeTarget(nil,
                                                 action: nil,
                                                 for: .allEvents)
            cell.cellSelectionText2.addTarget(self, action: #selector(self.textFieldValueChange(_:)), for: .editingChanged)
            
            cell.cellSelectionText.addTarget(self, action: #selector(self.textFieldCountryValueChange), for: .editingChanged)
            return cell
        }else{
            let cell: CommonCellTableViewCell = self.tblSchedule.dequeueReusableCell(withIdentifier: "CommonCellTableViewCell") as! CommonCellTableViewCell
            let type = CellContent_Schedule.init(rawValue: indexPath.row)
            cell.lblHeader.text = type?.getPlaceholder()
            
            cell.cellSelectionText.tag = indexPath.row
            cell.cellSelectionText.text = arrScheduleInfo[indexPath.row]
            cell.cellSelectionText.placeholder = type?.getPlaceholder()
            cell.cellSelectionText.removeTarget(nil,
                                                action: nil,
                                                for: .allEvents)
            cell.cellSelectionText.addTarget(self, action: #selector(self.textFieldValueChange(_:)), for: .editingChanged)
            if(indexPath.row == CellContent_Schedule.Doctor.rawValue){
                cell.cellSelectionText.isUserInteractionEnabled = false
            }else{
                cell.cellSelectionText.isUserInteractionEnabled = true
            }
            return cell
        }
    }
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        UIApplication.shared.open(URL)
        return false
    }
    
}
extension ScheduleConsultationVC{
    @objc func textFieldValueChange(_ txt: UITextField)  {
           if let str = txt.text, str.count > 0 {
               self.arrScheduleInfo[txt.tag] = str
           }else{
               self.arrScheduleInfo[txt.tag] = ""
           }
       }
    @objc func textFieldCountryValueChange(_ txt: UITextField)  {
        if let str = txt.text, str.count > 0 {
            countryCode = str
        }else{
            countryCode = ""
        }
    }
}
