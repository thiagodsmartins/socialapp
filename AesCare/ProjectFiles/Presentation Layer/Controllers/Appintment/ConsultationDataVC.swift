//
//  ConsultationDataVC.swift
//  AesCare
//
//  Created by Gali Srikanth on 28/09/20.
//

import UIKit

class ConsultationDataVC: BaseViewController {
    var gradientLayer: CAGradientLayer!
    @IBOutlet weak var vwNavigation: UIView!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var containerSearch: designableView!
    
    var medicModel:Medics?
    @IBOutlet weak var tblSchedule: UITableView!
    var arrScheduleInfo = [String]()
    var countryCode = ""
    var selectedSlot : Slots!
    var arrNotification = [false,false,false,false,false]
    var paramToSend:[String:AnyObject] = [String:AnyObject]()
    enum CellContent_Schedule:Int{
        case ConsultationDate = 0
        case Local
        case Mensagem
        case FormOfPayment
        case RefundConfirmation
        case CreditConfirmation
        case AcceptanceConfirmation
        case total
        func getPlaceholder () -> String {
            switch self {
            case .ConsultationDate:
                return "Data da Consulta"
            case .Local:
                return "Local"
            case .Mensagem:
                return "Message"
            case .FormOfPayment:
                return "Quais as formas de pagamento que você deseja utilizar?"
            case .RefundConfirmation:
                return "Esse médico tem taxa de não comparecimento. Será feito o congelamento de R$50,00 de seu cartão de crédito. O valor será 100% devolvido após a consulta. Esse valor só será cobrado caso você não compareça à consulta e não tenha desmarcado com até 24h de antecedência pelo AesCare."
            case .CreditConfirmation:
                return "Seu horário será cancelado caso não haja saldo em seu cartão de crédito até 24h antes da consulta."
            case .AcceptanceConfirmation:
                return "Eu aceito os Termos de Uso e a Política de Privacidade do AesCare."
            default:
                return ""
            }
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        for _ in 0 ..< CellContent_Schedule.total.rawValue {
            arrScheduleInfo.append("")
        }
        
        
        
        if let date = selectedSlot.date_start_formatted{
            let date1 = UtilityClass.dateFormatterFlexibleLocal(strDate: date, fromDateFromat: "dd/MM/yyyy HH:mm", toDateFormat: "dd/MM/yyyy")
            
            let hour = UtilityClass.dateFormatterFlexibleLocal(strDate: date, fromDateFromat: "dd/MM/yyyy HH:mm", toDateFormat: "HH:mm")
          
             arrScheduleInfo[CellContent_Schedule.ConsultationDate.rawValue] = date1 + " às " + hour + "h"
        }
        
        
        
        if let address = medicModel?.address{
            arrScheduleInfo[CellContent_Schedule.Local.rawValue] = address
        }
        
       
        
        arrScheduleInfo[CellContent_Schedule.RefundConfirmation.rawValue] = "0"
        arrScheduleInfo[CellContent_Schedule.CreditConfirmation.rawValue] = "0"
        arrScheduleInfo[CellContent_Schedule.AcceptanceConfirmation.rawValue] = "0"
        
        //createGradientLayer()
        // Do any additional setup after loading the view.
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        ///// Call Custom Tabbar
        //gradientLayer.frame = self.vwNavigation.bounds
    }

    func createGradientLayer() {
        gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.vwNavigation.bounds
        gradientLayer.colors = [UIColor(named: "AppPurpleColor")!.cgColor, UIColor(named: "AppMoveColor")!.cgColor]
        self.vwNavigation.layer.addSublayer(gradientLayer)
        self.vwNavigation.bringSubviewToFront(self.btnMenu)
        self.vwNavigation.bringSubviewToFront(self.containerSearch)
    }
    @IBAction func openSideMenu(_ sender: Any) {
        openSidePanel()
    }
    @IBAction func backNavigationPressed(_ sender: Any) {
        backNavigation()
    }
    @IBAction func navigationNext(_ sender: Any) {
        guard !arrScheduleInfo[CellContent_Schedule.Mensagem.rawValue].isEmpty else {
            showAlertMessage(title: "Ooops", message: ALERT_MESSAGE_MANDETORY, vc: self)
            return
        }
        
        var selectedPayment = false
        var selectedPaymentIndex = 0
        for (index,object) in arrNotification.enumerated(){
            if(object){
                selectedPayment = true
                selectedPaymentIndex = index
                break
            }
        }
        
        if(!selectedPayment){
            showAlertMessage(title: "Ooops", message: ALERT_MESSAGE_MANDETORY, vc: self)
            return
        }
        let payementType = ["Dinheiro","Cheque","Cartão de Crédito","Cartão de Débito","Convênio"]
         
        paramToSend["description"] = arrScheduleInfo[CellContent_Schedule.Mensagem.rawValue] as AnyObject
        paramToSend["payment_types"] =  payementType[ selectedPaymentIndex] as AnyObject
        print(paramToSend)
        
        self.scheduleAppointment(param: paramToSend)
      
    }
    func navigateAfterAppointment(msg:String,booking_key:String,amt:String)  {
        if #available(iOS 13.0, *) {
            let vc = self.storyboard?.instantiateViewController(identifier: "ShowRateVC") as! ShowRateVC
            vc.booking_key = booking_key
            vc.amount = amt
            UtilityClass.tosta(message: msg, duration: 2.0, vc: vc)
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            let vc = ShowRateVC.init(nibName: "ShowRateVC", bundle: nil)
            UtilityClass.tosta(message: msg, duration: 2.0, vc: vc)
            vc.booking_key = booking_key
            vc.amount = amt
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
extension ConsultationDataVC : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 90
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommonCellTableViewCell2") as! CommonCellTableViewCell
        return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return CellContent_Schedule.total.rawValue
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.row == CellContent_Schedule.ConsultationDate.rawValue){
            let cell: CommonCellTableViewCell = self.tblSchedule.dequeueReusableCell(withIdentifier: "CommonCellTableViewCell1") as! CommonCellTableViewCell
            
            cell.btnInstance.removeTarget(nil,action: nil,for: .allEvents)
            
            let type = CellContent_Schedule.init(rawValue: indexPath.row)
            cell.lblHeader.text = type?.getPlaceholder()
            
            cell.cellSelectionText.placeholder = type?.getPlaceholder()
            cell.cellSelectionText.text = arrScheduleInfo[indexPath.row]
            
            
            if(indexPath.row == CellContent_Schedule.ConsultationDate.rawValue){
                cell.cellSelectionText.isUserInteractionEnabled = false
            }else{
                cell.cellSelectionText.isUserInteractionEnabled = true
            }
            /*
             cell.btnInstance.addTarget(self,
             action: #selector(self.openConsultationDtae(_:)),
             for: .touchUpInside)
             */
            return cell
        } else if(indexPath.row == CellContent_Schedule.FormOfPayment.rawValue) {
            let cell: CommonCellTableViewCell = self.tblSchedule.dequeueReusableCell(withIdentifier: "CommonCellTableViewCell3") as! CommonCellTableViewCell
            
            let type = CellContent_Schedule.init(rawValue: indexPath.row)
            cell.lblHeader.text = type?.getPlaceholder()
            cell.btnInstance.setImage(UIImage(named: "unchecked_box"), for: .normal)
            cell.btnInstance2.setImage(UIImage(named: "unchecked_box"), for: .normal)
            cell.btnInstance3.setImage(UIImage(named: "unchecked_box"), for: .normal)
            cell.btnInstance4.setImage(UIImage(named: "unchecked_box"), for: .normal)
            cell.btnInstance5.setImage(UIImage(named: "unchecked_box"), for: .normal)
            if(arrNotification[0]){
                cell.btnInstance.setImage(UIImage(named: "checked_box"), for: .normal)
            }
            if(arrNotification[1]){
                cell.btnInstance2.setImage(UIImage(named: "checked_box"), for: .normal)
            }
            if(arrNotification[2]){
                cell.btnInstance3.setImage(UIImage(named: "checked_box"), for: .normal)
            }
            
            if(arrNotification[3]){
                cell.btnInstance4.setImage(UIImage(named: "checked_box"), for: .normal)
            }
            if(arrNotification[4]){
                cell.btnInstance5.setImage(UIImage(named: "checked_box"), for: .normal)
            }
            cell.btnInstance.tag = 0
            cell.btnInstance2.tag = 1
            cell.btnInstance3.tag = 2
            cell.btnInstance4.tag = 3
            cell.btnInstance5.tag = 4
            
            cell.btnInstance.addTarget(self, action: #selector(self.selectNotification(_:)), for: .touchUpInside)
            cell.btnInstance2.addTarget(self, action: #selector(self.selectNotification(_:)), for: .touchUpInside)
            cell.btnInstance3.addTarget(self, action: #selector(self.selectNotification(_:)), for: .touchUpInside)
            cell.btnInstance4.addTarget(self, action: #selector(self.selectNotification(_:)), for: .touchUpInside)
            cell.btnInstance5.addTarget(self, action: #selector(self.selectNotification(_:)), for: .touchUpInside)
            
            return cell
        } else if(indexPath.row == CellContent_Schedule.RefundConfirmation.rawValue || indexPath.row == CellContent_Schedule.CreditConfirmation.rawValue) {
            let cell: CommonCellTableViewCell = self.tblSchedule.dequeueReusableCell(withIdentifier: "CommonCellTableViewCell4") as! CommonCellTableViewCell
            let type = CellContent_Schedule.init(rawValue: indexPath.row)
            cell.lblInfo.text = type?.getPlaceholder()
            
            
            cell.btnInstance.setImage(UIImage(named: "unchecked_box"), for: .normal)
            cell.btnInstance.tag = indexPath.row
            cell.btnInstance.addTarget(self, action: #selector(self.selectConfirmation(_:)), for: .touchUpInside)
            
            if let index = Int(arrScheduleInfo[indexPath.row]){
                
                if(index == 1){
                    cell.btnInstance.setImage(UIImage(named: "checked_box"), for: .normal)
                }
            }
             
            return cell
        } else if(indexPath.row == CellContent_Schedule.AcceptanceConfirmation.rawValue) {
            let cell: CommonCellTableViewCell = self.tblSchedule.dequeueReusableCell(withIdentifier: "CommonCellTableViewCell5") as! CommonCellTableViewCell
            
           
            cell.btnInstance.setImage(UIImage(named: "unchecked_box"), for: .normal)
                       cell.btnInstance.tag = indexPath.row
                       cell.btnInstance.addTarget(self, action: #selector(self.selectConfirmation(_:)), for: .touchUpInside)
                       
                       if let index = Int(arrScheduleInfo[indexPath.row]){
                           
                           if(index == 1){
                               cell.btnInstance.setImage(UIImage(named: "checked_box"), for: .normal)
                           }
                       }
            
            
            
            /**Eu aceito os Termos de Uso e Privacidade do AesCare*/
            ////set color to a specific string in terms
            
            let attributedString1 = NSMutableAttributedString(string:"Eu aceito os ")
            let attributedString2 = NSMutableAttributedString(string:"Termos de Uso e")
            attributedString2.addAttribute(.link, value: termsAndConditionLink, range: NSRange(location: 0, length: attributedString2.length))
            attributedString2.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(named: "AppPurpleColor")! , range:NSRange(location: 0, length: attributedString2.length))
            let attributedString3 = NSMutableAttributedString(string:"  ")
            let attributedString4 = NSMutableAttributedString(string:"Privacidade do")
            attributedString4.addAttribute(.link, value: privacyPolicyLink, range: NSRange(location: 0, length: attributedString4.length))
            attributedString4.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(named: "AppPurpleColor")! , range:NSRange(location: 0, length: attributedString4.length))
            let attributedString5 = NSMutableAttributedString(string:" AesCare")
            let mutableAttributedString = NSMutableAttributedString()
            mutableAttributedString.append(attributedString1)
            mutableAttributedString.append(attributedString2)
            mutableAttributedString.append(attributedString3)
            mutableAttributedString.append(attributedString4)
            mutableAttributedString.append(attributedString5)
            
            
            
            cell.txtVWInstance1.attributedText = mutableAttributedString
            
            cell.selectionStyle = .none
            cell.backgroundColor = UIColor.clear
            cell.contentView.backgroundColor = UIColor.clear
            
            return cell
        } else {
            let cell: CommonCellTableViewCell = self.tblSchedule.dequeueReusableCell(withIdentifier: "CommonCellTableViewCell") as! CommonCellTableViewCell
            let type = CellContent_Schedule.init(rawValue: indexPath.row)
            cell.lblHeader.text = type?.getPlaceholder()
            cell.cellSelectionText.tag = indexPath.row
            cell.cellSelectionText.text = ""
            cell.cellSelectionText.text = arrScheduleInfo[indexPath.row]
            cell.cellSelectionText.placeholder = type?.getPlaceholder()
            cell.cellSelectionText.removeTarget(nil,
                                                action: nil,
                                                for: .allEvents)
            cell.cellSelectionText.addTarget(self, action: #selector(self.textFieldValueChange(_:)), for: .editingChanged)
            
            cell.cellSelectionText.isUserInteractionEnabled = true
            
            return cell
        }
    }
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        UIApplication.shared.open(URL)
        return false
    }
    
}
extension ConsultationDataVC{
    @objc func selectConfirmation(_ sender:UIButton){
        arrScheduleInfo[sender.tag] = (arrScheduleInfo[sender.tag] == "1") ? "0": "1"
        tblSchedule.reloadData()
    }
     
    @objc func openConsultationDtae(_ sender:UIButton){
        MyBasics.showDatePickerDropDown(PickerType: .date, ParentViewC: self, setRestriction: "")
    }
    @objc func selectNotification(_ sender:UIButton){
        arrNotification = [false,false,false,false,false]
        arrNotification[sender.tag] = !arrNotification[sender.tag]
        self.tblSchedule.reloadData()
    }
}
extension ConsultationDataVC: DatePickerDelegate{
    func selectedDate(selectedDate: Date) {
        let formatter = DateFormatter()
        formatter.dateFormat = "YYYY-MM-dd"
        arrScheduleInfo[CellContent_Schedule.ConsultationDate.rawValue] = formatter.string(from: selectedDate)
        tblSchedule.reloadData()
    }
    
}
extension ConsultationDataVC{
    @objc func textFieldValueChange(_ txt: UITextField)  {
        if let str = txt.text, str.count > 0 {
            self.arrScheduleInfo[txt.tag] = str
        }else{
            self.arrScheduleInfo[txt.tag] = ""
        }
    }
   
}
extension ConsultationDataVC{
    func scheduleAppointment(param : [String:AnyObject]) {
        
        let url = (API.scheduleAnAppointment.getURL()?.absoluteString ?? "")
        let operation =  WebServiceOperation.init(url,param, .WEB_SERVICE_MULTI_PART,nil)
        
        operation.completionBlock =
            {
                
                DispatchQueue.main.async {
                    
                    print(operation.responseData?.dictionary?.prettyprintedJSON ?? "")
                    guard let dictResponse = operation.responseData?.dictionary, dictResponse.count > 0 else {
                        
                        return
                    }
                    
                    if let responceCode = dictResponse["code"] ,responceCode as! Int == 1 {
                        
                        if let booking = dictResponse["booking"] as? Dictionary<String, Any> , let key = booking["key"] as? String{
                            var param:[String:AnyObject] = [String:AnyObject]()
                            param["booking_key"] = key as AnyObject
                            self.paymentForAppointment(param: param, booking_key: key)
                        }
                    } else if let msg = dictResponse["message"] as? String{
                        UtilityClass.tosta(message: msg, duration: 2.0, vc: self)
                    }
                    else{
                        UtilityClass.tosta(message: "Something wentwrong", duration: 2.0, vc: self)
                    }
                }
        }
        
        appDel.operationQueue.addOperation(operation)
        
    }
    
    func paymentForAppointment(param : [String:AnyObject],booking_key:String) {
        
        let url = (API.createPayment.getURL()?.absoluteString ?? "")
        let operation =  WebServiceOperation.init(url,param, .WEB_SERVICE_MULTI_PART,nil)
        
        operation.completionBlock =
            {
                
                DispatchQueue.main.async {
                    
                    guard let dictResponse = operation.responseData?.dictionary, dictResponse.count > 0 else {
                        
                        return
                    }
                    
                    if let responceCode = dictResponse["code"] ,responceCode as! Int == 1 {
                         
                        if let payment = dictResponse["payment"] as? Dictionary<String, Any> , let amount_total = payment["amount_total"] as? String,let pay_key = payment["key"] as? String{
                            
                            self.navigateAfterAppointment(msg: "Compromisso agendado com sucesso",booking_key:pay_key,amt:amount_total)
                        }
                        
                    }else{
                        UtilityClass.tosta(message: "Something wentwrong", duration: 2.0, vc: self)
                    }
                    
                }
        }
        
        appDel.operationQueue.addOperation(operation)
        
    }
}

