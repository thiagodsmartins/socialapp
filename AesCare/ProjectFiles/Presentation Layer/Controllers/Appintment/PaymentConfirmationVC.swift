//
//  PaymentConfirmationVC.swift
//  AesCare
//
//  Created by Gali Srikanth on 08/10/20.
//

import UIKit

class PaymentConfirmationVC: BaseViewController {
    var gradientLayer: CAGradientLayer!
    @IBOutlet weak var vwNavigation: UIView!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var containerSearch: designableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        createGradientLayer()
        // Do any additional setup after loading the view.
    }
    func createGradientLayer() {
        gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.vwNavigation.bounds
        gradientLayer.colors = [UIColor(named: "AppPurpleColor")!.cgColor, UIColor(named: "AppMoveColor")!.cgColor]
        self.vwNavigation.layer.addSublayer(gradientLayer)
        self.vwNavigation.bringSubviewToFront(self.btnMenu)
        self.vwNavigation.bringSubviewToFront(self.containerSearch)
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        ///// Call Custom Tabbar
        gradientLayer.frame = self.vwNavigation.bounds
    }

    @IBAction func navigateToHome(_ sender: Any) {
        navigateToHome()
    }
    
    
}
