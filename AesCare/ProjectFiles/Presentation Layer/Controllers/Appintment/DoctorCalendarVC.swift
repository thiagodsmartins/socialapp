//
//  DoctorCalendarVC.swift
//  AesCare
//
//  Created by Gali Srikanth on 30/09/20.
//

import UIKit

class DoctorCalendarVC: BaseViewController {
    var medicModel:Medics?
    
    
    @IBOutlet weak var vwNavigation: UIView!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var containerSearch: designableView!
    
    @IBOutlet weak var calVW: CustomCalendar!
    
    var gradientLayer: CAGradientLayer!
    var medic : [Medics]!
    var doctorSlots : DoctorSlot!
    var medic_id = 25621
    @IBOutlet weak var colViewCalendarSlot: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.calVW.delegate = self
        createGradientLayer()
        // Do any additional setup after loading the view.
        LocalizationSystem.sharedInstance.setLanguage(languageCode: "pt_BR")
        medic_id = medicModel?.medic_id ?? 0
        //print(medicModel)
        callfetchCalendarApi()
        callfetchSlotApi()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        ///// Call Custom Tabbar
        gradientLayer.frame = self.vwNavigation.bounds
    }

    func createGradientLayer() {
        gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.vwNavigation.bounds
        gradientLayer.colors = [UIColor(named: "AppPurpleColor")!.cgColor, UIColor(named: "AppMoveColor")!.cgColor]
        self.vwNavigation.layer.addSublayer(gradientLayer)
        self.vwNavigation.bringSubviewToFront(self.btnMenu)
        self.vwNavigation.bringSubviewToFront(self.containerSearch)
    }
   
    @IBAction func backNavigationPressed(_ sender: Any) {
        backNavigation()
    }
    
}
extension DoctorCalendarVC :selectedDateDelegate{
    func didSelectDate(_ selectedDate: Date?) {
        guard let date =  selectedDate else { return }
        
        let dateFormatter =  DateFormatter()
        dateFormatter.dateFormat = genericDateFormatter
        let strDate = dateFormatter.string(from: date)
        
        print(strDate)
    }
}

extension DoctorCalendarVC:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        var cellRow = 0
        if let doc = doctorSlots , let slots = doc.slots{
            cellRow = slots.count
        }
        return cellRow
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SliderCollCell", for: indexPath) as! SliderCollCell
        
        if let doc = doctorSlots , let slots = doc.slots, let date = slots[indexPath.row].date_start_formatted{
            
        let date1 = UtilityClass.dateFormatterFlexibleLocal(strDate: date, fromDateFromat: "dd/MM/yyyy HH:mm", toDateFormat: "dd/MM")
            
            let hour = UtilityClass.dateFormatterFlexibleLocal(strDate: date, fromDateFromat: "dd/MM/yyyy HH:mm", toDateFormat: "HH:mm")
            
            cell.labelUnderLine.text = date1
            cell.labelMheaderName.text = hour + "h"
        }
        
        cell.btnSlider.addTarget(self, action: #selector(btnSelectSlot(_:)), for: .touchUpInside)
         
        //cell.labelUnderLine.text = "06/07"
        //cell.labelMheaderName.text = "09:15h"
        cell.layoutIfNeeded()
        
        return cell
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: ((self.colViewCalendarSlot.frame.size.width / 2) - 5), height: 60)
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        //         if collectionView == collectionViewText{
        //return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        //         }else{
        return .zero
        //  }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    @objc func btnSelectSlot(_ sender:UIButton){
        
        if #available(iOS 13.0, *) {
            let vc = self.storyboard?.instantiateViewController(identifier: "ScheduleConsultationVC") as! ScheduleConsultationVC
            vc.medic_id = medic_id;
            vc.medico = medicModel?.name ?? " "
            vc.selectedSlot = doctorSlots.slots![sender.tag]
            vc.medicModel = medicModel
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            let vc = ScheduleConsultationVC.init(nibName: "ScheduleConsultationVC", bundle: nil)
            vc.medic_id = medic_id
            vc.medico = medicModel?.name ?? " "
            vc.selectedSlot = doctorSlots.slots![sender.tag]
            vc.medicModel = medicModel
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        ///
        if #available(iOS 13.0, *) {
            let vc = self.storyboard?.instantiateViewController(identifier: "ScheduleConsultationVC") as! ScheduleConsultationVC
             vc.medicModel = medicModel
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            let vc = ScheduleConsultationVC.init(nibName: "ScheduleConsultationVC", bundle: nil)
             vc.medicModel = medicModel
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
//
///Mark:Api
//
extension DoctorCalendarVC{
    func callfetchCalendarApi() {
        //http://ws.aescare.com/doctors?medic_id=640&get_calendar=true&calendar_limit=0
       
        
        let url :String = BaseUrl + "/doctors?medic_id=" + "\(medic_id)" + "&get_calendar=true" + "&calendar_limit=0"
        let operation =  WebServiceOperation.init(url,nil, .WEB_SERVICE_GET)
        
        operation.completionBlock =
            {
                DispatchQueue.main.async {
                    
                    if let dataUserDetails = operation.responseData, dataUserDetails.count > 0 {
                        do{
                            
                            let slotArr  = try JSONDecoder().decode(Model_Doctor.self, from: dataUserDetails)
                            //print(slotArr)
                            
                            self.calVW.arrMedics = slotArr.medics
                            self.calVW.cal.reloadData()
                            
                        }catch _ {
                            showAlertMessage(title: App_Title, message: "Something went wrong", vc: self)
                        }
                    }
                    else{
                        showAlertMessage(title: App_Title, message: "Something went wrong", vc: self)
                    }
                }
        }
        
        appDel.operationQueue.addOperation(operation)
    }
    
    func callfetchSlotApi() {
        
       //https://ws.aescare.com/medic/calendar/slots?medic_id=4123&is_available=true
        
        let url :String = BaseUrl + "/medic/calendar/slots?medic_id=" + "\(medic_id)" + "&is_available=true"
        let operation =  WebServiceOperation.init(url,nil, .WEB_SERVICE_GET)
        
        operation.completionBlock =
            {
                DispatchQueue.main.async {
                    
                    if let dataUserDetails = operation.responseData, dataUserDetails.count > 0 {
                        do{
                            
                            self.doctorSlots  = try JSONDecoder().decode(DoctorSlot.self, from: dataUserDetails)
                           // print(self.slot as DoctorSlot)
                            
                            self.colViewCalendarSlot.reloadData()
                            
                        }catch _ {
                            showAlertMessage(title: App_Title, message: "Something went wrong", vc: self)
                        }
                    }
                    else{
                        showAlertMessage(title: App_Title, message: "Something went wrong", vc: self)
                    }
                }
        }
        
        appDel.operationQueue.addOperation(operation)
    }
    
}

