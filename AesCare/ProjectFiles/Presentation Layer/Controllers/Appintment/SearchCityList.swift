//
//  SearchCityList.swift
//  AesCare
//
//  Created by Gali Srikanth on 22/09/20.
//

import UIKit
import SideMenu

class SearchCityList: BaseViewController {
    var gradientLayer: CAGradientLayer!
    @IBOutlet weak var vwNavigation: UIView!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var containerSearch: designableView!
    @IBOutlet weak var lblFirstMark: UILabel!
    @IBOutlet weak var lblSecondMark: UILabel!
    //var mdObjCity : CityListModel!
    //@IBOutlet var lblMark: [UILabel]!
    var arrSearchCityList : [ModelSearchedCity]!
    @IBOutlet weak var tblCityList: UITableView!
    @IBOutlet weak var txtSearch : UITextField!
    @IBOutlet weak var tableDoctor: UITableView!
    @IBOutlet var lblTabText: [UILabel]!
    // @IBOutlet var lblMarker: [UILabel]!
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var heightLabelStatic: NSLayoutConstraint!
    @IBOutlet weak var viewSort: UIView!
    var arrayDoctors :[Medics] = []
    var sortingID = 0
    var cityID:Int?
    var page:Page = Page()
    var arraySortTitle1 = ["Cirurgia plástica","Dermatologia","Endocrinologia","Nutrologia","Bariátrica"]
    @IBOutlet weak var tableSort: UITableView!
    var swipeGesture: UISwipeGestureRecognizer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        createGradientLayer()
        self.swipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(self.openSideMenuWithSwipe(_:)))
        
        self.viewSort.isHidden = true
        self.tblCityList.isHidden = true
        self.tableDoctor.isHidden = true
        self.changeTabColor(index: 0)
        
        self.view.addGestureRecognizer(self.swipeGesture!)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        ///// Call Custom Tabbar
        gradientLayer.frame = self.vwNavigation.bounds
    }
        
    func createGradientLayer() {
        gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.vwNavigation.bounds
        gradientLayer.colors = [UIColor(named: "AppPurpleColor")!.cgColor, UIColor(named: "AppMoveColor")!.cgColor]
        self.vwNavigation.layer.addSublayer(gradientLayer)
        self.vwNavigation.bringSubviewToFront(self.btnMenu)
        self.vwNavigation.bringSubviewToFront(self.containerSearch)
    }
        
    @objc func openSideMenuWithSwipe(_ swipeGesture: UISwipeGestureRecognizer) {
        if swipeGesture.state == .ended {
            self.menuPressed()
        }
    }
    
    @IBAction func tabFavAction(_ sender: UIButton) {
        self.changeTabColor(index: sender.tag)
    }
    
    @IBAction func searchCity(){
        guard !(txtSearch.text!.isEmpty) else {
            showAlertMessage(title: "Ooops", message: ALERT_MESSAGE_SEARCH_BLANK, vc: self)
            return
        }
        self.tableDoctor.isHidden = true
        self.viewSort.isHidden = true
        callfetchMyLocationsApi(searchText: txtSearch.text!)
    }
    
    @IBAction func btnFilterAction(_ sender: UIButton) {
        self.viewSort.isHidden = false
    }
    
    @IBAction func btnCloseFilter(_ sender: UIButton) {
        self.viewSort.isHidden = true
    }
    
    @IBAction func btnFilterApply(_ sender: UIButton) {
        self.viewSort.isHidden = true
        //self.callDoctorsApi(orderBy: "relevance", pageNumber: 1, near: 2863, limit: 10)
    }
    
    @IBAction func openSideMenu(_ sender: Any) {
        self.menuPressed()
    }
}
extension SearchCityList{
    
    
    
    private func menuPressed() {
        
        appDel.topViewControllerWithRootViewController(rootViewController: UIApplication.shared.windows.first?.rootViewController)?.view.endEditing(true)
        
        SideMenuManager.default.leftMenuNavigationController?.settings.presentationStyle = .menuSlideIn
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let menu = storyboard.instantiateViewController(withIdentifier: "SideMenuNavigationController") as! SideMenuNavigationController
        menu.leftSide = true
        
        UIApplication.shared.windows.first?.rootViewController?.present(menu, animated: true, completion: nil)
        
    }
    
    func changeTabColor(index:Int){
        for i in 0...lblTabText.count - 1{
            
            if lblTabText[i].tag == index{
                lblTabText[i].textColor = #colorLiteral(red: 0.6859999895, green: 0.2660000026, blue: 0.8500000238, alpha: 1)
            }else{
                lblTabText[i].textColor = #colorLiteral(red: 0.5600000024, green: 0.6069999933, blue: 0.7009999752, alpha: 1)
            }
        }
        if index == 0{
            self.lblFirstMark.backgroundColor = #colorLiteral(red: 0.6859999895, green: 0.2660000026, blue: 0.8500000238, alpha: 1)
            self.lblSecondMark.backgroundColor = .clear
        }else{
            self.lblSecondMark.backgroundColor = #colorLiteral(red: 0.6859999895, green: 0.2660000026, blue: 0.8500000238, alpha: 1)
            self.lblFirstMark.backgroundColor = .clear
        }
        //        for i in 0...lblMark.count - 1{
        //
        //            if lblMark[i].tag == index{
        //                lblMark[i].backgroundColor = #colorLiteral(red: 0.6859999895, green: 0.2660000026, blue: 0.8500000238, alpha: 1)
        //            }else{
        //                lblMark[i].backgroundColor = .green
        //            }
        //        }
    }
}
//
//Mark: table work
//
extension SearchCityList:UITableViewDelegate,UITableViewDataSource
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tableSort{
            return arraySortTitle1.count
        }else if tableView == tblCityList{
            if let arr = arrSearchCityList {
                return arr.count
            }
            return 0
        }else{
            return self.arrayDoctors.count
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if tableView == self.tableSort{
            let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell1", for: indexPath) as! CategoryCell
            cell.lblTitle.text = arraySortTitle1[indexPath.row]
            if sortingID == indexPath.row + 1{
                cell.imageviewCellRight.image = UIImage(named: "checked_box")
            }else{
                cell.imageviewCellRight.image = UIImage(named: "unchecked_box")
            }
            return cell
        }else if tableView == tblCityList{
            let cell = tableView.dequeueReusableCell(withIdentifier: "DoctorSearchCell", for: indexPath) as? DoctorSearchCell
            if let arr = arrSearchCityList,arr.count > 0{
                let mdOBJTemp = arr[indexPath.row]
                cell?.lblCityName?.text = mdOBJTemp.name
            }
            
            return cell!
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "DoctorSearchCell", for: indexPath) as? DoctorSearchCell
            cell?.lblName.text = arrayDoctors[indexPath.row].name ?? ""
            cell?.lblDescription.text = arrayDoctors[indexPath.row].sbcp_categoria ?? ""
            cell?.lblDistance.text = "\(arrayDoctors[indexPath.row].distance ?? 0.0) KM"
            cell?.lblRate.text = "\(arrayDoctors[indexPath.row].evaluation_average ?? 0.0)"
            if let url = URL(string:arrayDoctors[indexPath.row].cover_media?.image_url ?? ""){
                cell?.imgviewCell.kf.indicatorType = .activity
                cell?.imgviewCell.kf.setImage(with: url)
            }else{
                cell?.imgviewCell.image = UIImage(named: "")
            }
            return cell ?? UITableViewCell()
            // return UITableViewCell()
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == self.tblCityList {
            self.tblCityList.isHidden = true
            if let arr = arrSearchCityList,arr.count > 0{
                let mdOBJTemp = arr[indexPath.row]
                print(mdOBJTemp)
                self.cityID = mdOBJTemp.city_id
                heightLabelStatic.constant = 0
                UIView.animate(withDuration: 0.5) {
                    self.view.layoutIfNeeded()
                }
                self.callDoctorsApi(orderBy: "relevance", pageNumber: 1, near: self.cityID ?? 0, limit: 10)
            }
        }
        else if tableView == self.tableSort {
            sortingID = indexPath.row + 1
            self.tableSort.reloadData()
        }else{
            if #available(iOS 13.0, *) {
                let vc = self.storyboard?.instantiateViewController(identifier: "DoctorDetailsVC") as! DoctorDetailsVC
                vc.medicModel = arrayDoctors[indexPath.row]
                vc.medicId = arrayDoctors[indexPath.row].medic_id
                vc.cityID = self.cityID ?? 0
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                let vc = DoctorDetailsVC.init(nibName: "DoctorDetailsVC", bundle: nil)
                vc.medicModel = arrayDoctors[indexPath.row]
                vc.medicId = arrayDoctors[indexPath.row].medic_id
                vc.cityID = self.cityID ?? 0
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
}

//
///Mark:Api
//
extension SearchCityList{
    func callfetchMyLocationsApi(searchText : String) {
        let searchTextTemp = searchText.replacingOccurrences(of: " ", with: "%20")
        let url = (API.searchByCityName.getURL()?.absoluteString ?? "") + searchTextTemp
        let operation =  WebServiceOperation.init(url,nil, .WEB_SERVICE_GET)
        
        operation.completionBlock =
            {
                DispatchQueue.main.async {
                    print(operation.responseData?.dictionary?.prettyprintedJSON ?? "")
                    if let dataUserDetails = operation.responseData, dataUserDetails.count > 0 {
                        do{
                            
                            self.arrSearchCityList  = try JSONDecoder().decode([ModelSearchedCity].self, from: dataUserDetails)
                            self.tblCityList.isHidden = false
                            self.tblCityList.reloadData()
                        }catch _ {
                            showAlertMessage(title: App_Title, message: "Something went wrong", vc: self)
                        }
                    }
                    else{
                        showAlertMessage(title: App_Title, message: "Something went wrong", vc: self)
                    }
                }
        }
        
        appDel.operationQueue.addOperation(operation)
    }
    
    func callDoctorsApi(orderBy:String,pageNumber:Int,near:Int,limit:Int) {
        //results?page=1&near=2863&order_by=relevance&limit=10
        var getUrl :String = BaseUrl + "/doctors?page=" + "\(pageNumber)" + "&near=" + "\(near)" + "&order_by=" + orderBy + "&limit=" + "\(limit)" + "&get_calendar=true"
        if sortingID != 0{
            getUrl.append("&specialty_id=\(sortingID)")
        }
        //getUrl.append("&medic_id=25621")
        
        //https://ws.aescare.com/doctors?limit=10&page=1&near=2869&order_by=relevance&specialty_id=1&get_calendar=true
        let operation = WebServiceOperation.init(getUrl, nil, .WEB_SERVICE_GET, nil)
        
        operation.completionBlock =
            {
                DispatchQueue.main.async {
                    
                    print(operation.responseData?.dictionary?.prettyprintedJSON ?? "")
                    guard let dictResponse = operation.responseData?.dictionary, dictResponse.count > 0 else {
                        
                        return
                    }
                    
                    if let responceCode = dictResponse["code"] ,responceCode as? Int == 1 {
                        if let dataUserDetails = dictResponse.data, dataUserDetails.count > 0 {
                            do{
                                let getParseData = try JSONDecoder().decode(Model_Doctor.self, from: dataUserDetails)
                                self.arrayDoctors.append(contentsOf: getParseData.medics ?? [])
                                
                                self.page.pageDoctor = pageNumber
                                self.tableDoctor.isHidden = false
                                self.tableDoctor.reloadData()
                            }catch let e{
                                print(e.localizedDescription)
                                //showAlertMessage(title: App_Title, message: "Something went wrong", vc: self)
                            }
                        }
                    }else{
                        // showAlertMessage(title: App_Title, message: "Something went wrong", vc: self)
                    }
                    
                    DispatchQueue.main.async {
                        /////
                    }
                }
        }
        
        appDel.operationQueue.addOperation(operation)
        
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if(scrollView == tableDoctor) {
            
            let height = scrollView.frame.size.height
            let contentYoffset = scrollView.contentOffset.y
            let distanceFromBottom = scrollView.contentSize.height - contentYoffset
            if ((distanceFromBottom*100).rounded()/100 <= (height*100).rounded()/100) {
                
                self.paginate()
                // print(" you reached end of the table")
            }
        }
    }
    func paginate(){
        // shouldAllowPagination = true
        
        //        let rem = (self.page.totalData)! % (self.page.perPage)!
        //        var pp = (self.page.totalData)! / (self.page.perPage)!
        //        if rem != 0 {
        //            pp += 1
        //        }
        
        //if pp >= (page.currentPage)! {
        
        self.callDoctorsApi(orderBy: "relevance", pageNumber: (page.pageDoctor)+1, near: cityID ?? 0, limit: 10)
        page.pageDoctor = (page.pageDoctor + 1)
        
        
        //}
        
    }
}
