//
//  ConsultaDetailVC.swift
//  AesCare
//
//  Created by Gali Srikanth on 19/10/20.
//

import UIKit
import Alamofire
class ConsultaDetailVC: BaseViewController, URLSessionDelegate {
    @IBOutlet weak var vwNavigation: UIView!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var containerSearch: designableView!
    @IBOutlet weak var tblConsultas: UITableView!
    @IBOutlet weak var imgSearch: UIImageView!
    var gradientLayer: CAGradientLayer!
    //var myAppointment:ModelAppintement!
    var arrLeads : [Leads]!
    override func viewDidLoad() {
        super.viewDidLoad()
        createGradientLayer()
        //
        getBookings()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        ///// Call Custom Tabbar
        gradientLayer.frame = self.vwNavigation.bounds
    }
    
    func createGradientLayer() {
        gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.vwNavigation.bounds
        gradientLayer.colors = [UIColor(named: "AppPurpleColor")!.cgColor, UIColor(named: "AppMoveColor")!.cgColor]
        self.vwNavigation.layer.addSublayer(gradientLayer)
        self.vwNavigation.bringSubviewToFront(self.btnMenu)
        self.vwNavigation.bringSubviewToFront(self.containerSearch)
    }
    @IBAction func backNavigationPressed(_ sender: Any) {
        backNavigation()
    }
}
extension ConsultaDetailVC : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let arr = self.arrLeads{
            return arr.count
        }
        return 0
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DoctorListCell", for: indexPath) as! DoctorListCell
        
        var lbl1 = "       "
        var lbl2 = "       "
        var lbl3 = "       "
        var lbl4 = "       "
        if let arr = self.arrLeads {
            let appo = arr[indexPath.row]
            lbl1 = appo.date_slot ?? lbl1
            lbl2 = appo.hours_slot ?? lbl2
            lbl3 = appo.status ?? lbl3
            lbl4 = appo.phone ?? lbl4
        }
        
        cell.lblFirst.text = lbl1
        cell.lblSecond.text = lbl2
        cell.lblThird.text = lbl3
        cell.lblFourth.text = lbl4
       
        
        cell.btnEye.tag = indexPath.row
        cell.btnEye.addTarget(self, action: #selector(deleteAppointement(_:)), for: .touchUpInside)
        
        cell.selectionStyle = .none
        return cell
    }
    @objc func deleteAppointement(_ sender: UIButton!){
        deleteBookingOld(selectedIndex: sender.tag)
    }
}
///
///Mark : Api call
///
extension ConsultaDetailVC{
    

    func deleteMethod(selectedIndex : Int) {
        let bookkey = self.arrLeads[selectedIndex].key
               
               let parameters = ["key": bookkey as Any] as [String : Any]
        
            guard let url = URL(string: "https://ws.aescare.com/booking") else {
                print("Error: cannot create URL")
                return
            }
            // Create the request
            var request = URLRequest(url: url)
            request.httpMethod = "DELETE"
        var value = ""
        if usrerModelOBJ != nil{
            if let valueTemp =  usrerModelOBJ.session_key {
                value = "aescare_session_key=" + valueTemp
            }
        }
        request.addValue(value, forHTTPHeaderField: "Cookie")
        guard let httpBody = try? JSONSerialization.data(withJSONObject:parameters, options: []) else { return }
        request.httpBody = httpBody
            URLSession.shared.dataTask(with: request) { data, response, error in
                guard error == nil else {
                    print("Error: error calling DELETE")
                    print(error!)
                    return
                }
                guard let data = data else {
                    print("Error: Did not receive data")
                    return
                }
                guard let response = response as? HTTPURLResponse, (200 ..< 299) ~= response.statusCode else {
                    print("Error: HTTP request failed")
                    return
                }
                do {
                    guard let jsonObject = try JSONSerialization.jsonObject(with: data) as? [String: Any] else {
                        print("Error: Cannot convert data to JSON")
                        return
                    }
                    guard let prettyJsonData = try? JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted) else {
                        print("Error: Cannot convert JSON object to Pretty JSON data")
                        return
                    }
                    guard let prettyPrintedJson = String(data: prettyJsonData, encoding: .utf8) else {
                        print("Error: Could print JSON in String")
                        return
                    }
                    
                    print(prettyPrintedJson)
                } catch {
                    print("Error: Trying to convert JSON data to string")
                    return
                }
            }.resume()
        }
    
    
    
    func deleteBookingTest(selectedIndex : Int) {
        
        /*
        let bookkey = self.arrLeads[selectedIndex].key
        
        let parameters = ["key": bookkey as Any] as [String : Any]
        guard let url = URL(string: "https://ws.aescare.com/booking") else { return }
        var request = URLRequest(url: url)
        request.httpMethod = "DELETE"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        var value =  ""
        if usrerModelOBJ != nil{
            if let valueTemp =  usrerModelOBJ.session_key {
                value = "aescare_session_key=" + valueTemp
            }
        }
        request.addValue(value, forHTTPHeaderField: "Cookie")
        guard let httpBody = try? JSONSerialization.data(withJSONObject:parameters, options: []) else { return }
        request.httpBody = httpBody
        
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            //print(String(data: data!, encoding: .utf8)!)
            DispatchQueue.main.async {
                CustomActivityIndicator.sharedInstance.hide {
                }
                
                if let dataTemp = data, dataTemp.count > 0 {
                    
                    do {
                        let dictResponse = try (JSONSerialization.jsonObject(with: dataTemp, options: []) as? [String : Any])
                        guard let dictResponseTemp = dictResponse, dictResponseTemp.count > 0 else {
                            UtilityClass.tosta(message: "Something wentwrong", duration: 2.0, vc: self)
                            return
                        }
                        if let responceCode = dictResponseTemp["code"] ,responceCode as! Int == 1 {
                            self.arrLeads.remove(at: selectedIndex)
                            self.tblConsultas.reloadData()
                        } else if let msg = dictResponseTemp["message"] as? String{
                            UtilityClass.tosta(message: msg, duration: 2.0, vc: self)
                        }
                        else{
                            UtilityClass.tosta(message: "Something wentwrong", duration: 2.0, vc: self)
                        }
                        
                    } catch let error  {
                        print("Parsing Failed \(error.localizedDescription)")
                    }
                }
            }
        }.resume()
        
        */
        
        if #available(iOS 13.0, *) {
            let keyWindow = UIApplication.shared.connectedScenes
                .filter({$0.activationState == .foregroundActive})
                .map({$0 as? UIWindowScene})
                .compactMap({$0})
                .first?.windows
                .filter({$0.isKeyWindow}).first
            CustomActivityIndicator.sharedInstance.display(onView: keyWindow, done: {
                
            })
        } else {
            
            UIApplication.shared.keyWindow?.rootViewController?.view.endEditing(true)
            CustomActivityIndicator.sharedInstance.display(onView: UIApplication.shared.keyWindow, done: {
                
            })
        }
        
        let bookkey = self.arrLeads[selectedIndex].key
        var param:[String:Any] = [String:Any]()
        param["key"] = bookkey!
        
        let str = "{\"key\": \"\(bookkey!)\"}"
        
        
        
        guard let url = URL(string: "https://ws.aescare.com/booking"),let payload = str.data(using: .utf8)  else{
            return
        }

        let baseCommand = "curl \"\(url.absoluteString)\""
        var request = URLRequest(url:url)
        request.httpMethod = "DELETE"
        var value =  ""
        if usrerModelOBJ != nil{
            if let valueTemp =  usrerModelOBJ.session_key {
                value = "aescare_session_key=" + valueTemp
            }
        }
        request.addValue(value, forHTTPHeaderField: "Cookie")
        //request.addValue(bookkey!, forHTTPHeaderField: "key")
        //request.addValue("multipart/form-data", forHTTPHeaderField: "Content-Type")
        request.httpBody = payload
      
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration, delegate: self, delegateQueue:OperationQueue.main)
        let task = session.dataTask(with: request, completionHandler: {data, response, error -> Void in
            guard let data = data else { print("Empty data"); return }
             DispatchQueue.main.async {
                CustomActivityIndicator.sharedInstance.hide {
                }
                
                do {
                    let dictResponse = try (JSONSerialization.jsonObject(with: data, options: []) as? [String : Any])
                    guard let dictResponseTemp = dictResponse, dictResponseTemp.count > 0 else {
                        UtilityClass.tosta(message: "Something wentwrong", duration: 2.0, vc: self)
                        return
                    }
                    if let responceCode = dictResponseTemp["code"] ,responceCode as! Int == 1 {
                        self.arrLeads.remove(at: selectedIndex)
                        self.tblConsultas.reloadData()
                    } else if let msg = dictResponseTemp["message"] as? String{
                        UtilityClass.tosta(message: msg, duration: 2.0, vc: self)
                    }
                    else{
                        UtilityClass.tosta(message: "Something wentwrong", duration: 2.0, vc: self)
                    }
                    
                } catch let error  {
                    print("Parsing Failed \(error.localizedDescription)")
                }
                
            }
        })
        task.resume()
        /*
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard error == nil else { print(error!.localizedDescription); return }
            guard let data = data else { print("Empty data"); return }
                print(str)
            DispatchQueue.main.async {
                CustomActivityIndicator.sharedInstance.hide {
                }
                
                do {
                    let dictResponse = try (JSONSerialization.jsonObject(with: data, options: []) as? [String : Any])
                    guard let dictResponseTemp = dictResponse, dictResponseTemp.count > 0 else {
                        UtilityClass.tosta(message: "Something wentwrong", duration: 2.0, vc: self)
                        return
                    }
                    if let responceCode = dictResponseTemp["code"] ,responceCode as! Int == 1 {
                        self.arrLeads.remove(at: selectedIndex)
                        self.tblConsultas.reloadData()
                    } else if let msg = dictResponseTemp["message"] as? String{
                        UtilityClass.tosta(message: msg, duration: 2.0, vc: self)
                    }
                    else{
                        UtilityClass.tosta(message: "Something wentwrong", duration: 2.0, vc: self)
                    }
                    
                } catch let error  {
                    print("Parsing Failed \(error.localizedDescription)")
                }
                
            }
            
        }.resume()
        */
        /*
        let bookkey = self.arrLeads[selectedIndex].key
        var param:[String:Any] = [String:Any]()
        param["key"] = bookkey!
               
        
        let getUrl :String = "https://ws.aescare.com/booking"
        if let urlString  = URL(string: getUrl){
            var request = URLRequest(url: urlString)
            request.httpMethod = "DELETE"
            guard let httpBody = try? JSONSerialization.data(withJSONObject:param, options: .prettyPrinted) else { return }
            request.httpBody = httpBody
            
            var value =  ""
            if usrerModelOBJ != nil{
                if let valueTemp =  usrerModelOBJ.session_key {
                    value = "aescare_session_key=" + valueTemp
                }
            }
            request.addValue(value, forHTTPHeaderField: "Cookie")
            request.addValue("multipart/form-data", forHTTPHeaderField: "Content-Type")
            
            URLSession.shared.dataTask(with: request) { data, response, error in
                print(String(data: data!, encoding: .utf8)!)
                DispatchQueue.main.async {
                    CustomActivityIndicator.sharedInstance.hide {
                    }
                    
                    if let dataTemp = data, dataTemp.count > 0 {
                        
                        do {
                            let dictResponse = try (JSONSerialization.jsonObject(with: dataTemp, options: []) as? [String : Any])
                            guard let dictResponseTemp = dictResponse, dictResponseTemp.count > 0 else {
                                UtilityClass.tosta(message: "Something wentwrong", duration: 2.0, vc: self)
                                return
                            }
                            if let responceCode = dictResponseTemp["code"] ,responceCode as! Int == 1 {
                                self.arrLeads.remove(at: selectedIndex)
                                self.tblConsultas.reloadData()
                            } else if let msg = dictResponseTemp["message"] as? String{
                                UtilityClass.tosta(message: msg, duration: 2.0, vc: self)
                            }
                            else{
                                UtilityClass.tosta(message: "Something wentwrong", duration: 2.0, vc: self)
                            }
                            
                        } catch let error  {
                            print("Parsing Failed \(error.localizedDescription)")
                        }
                    }
                }
            }.resume()
        }*/
    }
    func deleteBooking(selectedIndex : Int) {
        let bookkey = self.arrLeads[selectedIndex].key
        //schedule?.key
        var param:[String:String] = [String:String]()
        param["key"] = bookkey!
        
        let url = "https://ws.aescare.com/booking"
        var value =  ""
        if usrerModelOBJ != nil{
            if let valueTemp =  usrerModelOBJ.session_key {
                value = "aescare_session_key=" + valueTemp
            }
        }
        let headers: HTTPHeaders = [
            /* "Authorization": "your_access_token",  in case you need authorization header */
            "Cookie": value
        ]
        
        
        Alamofire.request(url, method: .delete, parameters: param, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            guard let data = response.data, data.count > 0 else{
                UtilityClass.tosta(message: "Something wentwrong", duration: 2.0, vc: self)
                return
            }
            do{
                let dictResponse = try (JSONSerialization.jsonObject(with: data, options: []) as? [String : Any])
                
                guard let dictResponseTemp = dictResponse, dictResponseTemp.count > 0 else {
                    UtilityClass.tosta(message: "Something wentwrong", duration: 2.0, vc: self)
                    return
                }
                if let responceCode = dictResponseTemp["code"] ,responceCode as! Int == 1 {
                    self.arrLeads.remove(at: selectedIndex)
                    self.tblConsultas.reloadData()
                } else if let msg = dictResponseTemp["message"] as? String{
                    UtilityClass.tosta(message: msg, duration: 2.0, vc: self)
                }
                else{
                    UtilityClass.tosta(message: "Something wentwrong", duration: 2.0, vc: self)
                }
                
            }catch let err {
                UtilityClass.tosta(message: "Something wentwrong", duration: 2.0, vc: self)
                print(err.localizedDescription)
            }
        }
    }
   
    func deleteBookingOld(selectedIndex : Int) {
        let bookkey = self.arrLeads[selectedIndex].key
        //schedule?.key
        var param:[String:AnyObject] = [String:AnyObject]()
        param["key"] = bookkey as AnyObject
        
        let url = "https://ws.aescare.com/booking"
        //(API.SendResult.getURL()?.absoluteString ?? "")
        let operation =  WebServiceOperation.init(url,param, .WEB_SERVICE_DELETE,nil)
        
        operation.completionBlock =
            {
                
                DispatchQueue.main.async {
                    print(operation.responseData?.dictionary?.prettyprintedJSON ?? "")
                    guard let dictResponse = operation.responseData?.dictionary, dictResponse.count > 0 else {
                        return
                    }
                    
                    if let responceCode = dictResponse["code"] ,responceCode as! Int == 1 {
                        self.arrLeads.remove(at: selectedIndex)
                        self.tblConsultas.reloadData()
                    } else if let msg = dictResponse["message"] as? String{
                        UtilityClass.tosta(message: msg, duration: 2.0, vc: self)
                    }
                    else{
                        UtilityClass.tosta(message: "Something wentwrong", duration: 2.0, vc: self)
                    }
                }
        }
        
        appDel.operationQueue.addOperation(operation)
        
    }
    func getBookings() {
        
        let url = "https://ws.aescare.com/medic/bookings?is_my=true"
        //(API.SendResult.getURL()?.absoluteString ?? "")
        let operation =  WebServiceOperation.init(url,nil, .WEB_SERVICE_GET,nil)
        
        operation.completionBlock =
            {
                
                DispatchQueue.main.async {
                    print(operation.responseData?.dictionary?.prettyprintedJSON ?? "")
                    guard let dictResponse = operation.responseData?.dictionary, dictResponse.count > 0 else {
                        return
                    }
                    
                    if let responceCode = dictResponse["code"] ,responceCode as! Int == 1 {
                        if let dataUserDetails = dictResponse.data, dataUserDetails.count > 0 {
                            do{
                                let myAppointment = try JSONDecoder().decode(ModelAppintement.self, from: dataUserDetails)
                                self.arrLeads = myAppointment.leads
                                print(myAppointment)
                                self.tblConsultas.reloadData()
                            }catch _ {
                                
                            }
                        }
                            
                    } else if let msg = dictResponse["message"] as? String{
                        UtilityClass.tosta(message: msg, duration: 2.0, vc: self)
                    }
                    else{
                        UtilityClass.tosta(message: "Something wentwrong", duration: 2.0, vc: self)
                    }
                }
        }
        
        appDel.operationQueue.addOperation(operation)
        
    }
    
    
}
extension URLRequest {

    /**
     Returns a cURL command representation of this URL request.
     */
    public var curlString: String {
        guard let url = url else { return "" }
        var baseCommand = #"curl "\#(url.absoluteString)""#

        if httpMethod == "HEAD" {
            baseCommand += " --head"
        }

        var command = [baseCommand]

        if let method = httpMethod, method != "GET" && method != "HEAD" {
            command.append("-X \(method)")
        }

        if let headers = allHTTPHeaderFields {
            for (key, value) in headers where key != "Cookie" {
                command.append("-H '\(key): \(value)'")
            }
        }

        if let data = httpBody, let body = String(data: data, encoding: .utf8) {
            command.append("-d '\(body)'")
        }

        return command.joined(separator: " \\\n\t")
    }

}
