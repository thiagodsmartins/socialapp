//
//  MinhasConsultasVC.swift
//  AesCare
//
//  Created by Gali Srikanth on 17/10/20.
//

import UIKit

class MinhasConsultasVC: BaseViewController {
    @IBOutlet weak var imgSearch: UIImageView!
    @IBOutlet weak var imgHome: UIImageView!
    @IBOutlet weak var imgResultudos: UIImageView!
    @IBOutlet weak var imgBookDoctor: UIImageView!
    @IBOutlet weak var imgChat: UIImageView!//chat-Inactive
    @IBOutlet weak var imgProfile: UIImageView!//profile-Inactive
    
    
    @IBOutlet weak var vwNavigation: UIView!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var containerSearch: designableView!
    @IBOutlet weak var tblConsultas: UITableView!
    var arrHelps = ["1. Pesquise por especialistas","2. Pesquise por resultados","3. Agende uma consulta","4. Posts médicos","5. Grupos de discussão","6. Busca por procedimento"]
    var gradientLayer: CAGradientLayer!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        createGradientLayer()
        //
    }
     
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        ///// Call Custom Tabbar
         gradientLayer.frame = self.vwNavigation.bounds
    }
    
    func createGradientLayer() {
        gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.vwNavigation.bounds
        gradientLayer.colors = [UIColor(named: "AppPurpleColor")!.cgColor, UIColor(named: "AppMoveColor")!.cgColor]
        self.vwNavigation.layer.addSublayer(gradientLayer)
        self.vwNavigation.bringSubviewToFront(self.btnMenu)
        self.vwNavigation.bringSubviewToFront(self.containerSearch)
    }
    @IBAction func backNavigationPressed(_ sender: Any) {
        backNavigation()
    }
   
}

extension MinhasConsultasVC : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrHelps.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CommonCellTableViewCell = self.tblConsultas.dequeueReusableCell(withIdentifier: "CommonCellTableViewCell") as! CommonCellTableViewCell
        cell.lblInfo.text = arrHelps[indexPath.row]
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
           var lblInfo = ""
           if(indexPath.row == 0){
               imgSearch.image = UIImage(named: "whiteRound")
               lblInfo = "Encontre especialistas na sua cidade e veja avaliações deles"
           }else if(indexPath.row == 1){
            imgHome.image = UIImage(named: "whiteRound")
            lblInfo = "Encontre fotos de resultados e relatos de pacientes que fizeram os procedimentos que voce deseja"
           }else if(indexPath.row == 2){
            imgResultudos.image = UIImage(named: "whiteRound")
               lblInfo = "Marque uma avaliação com o profissional que deseja e o acompanhe o agendamento 5 da sua consulta."
           }else if(indexPath.row == 3){
            imgHome.image = UIImage(named: "whiteRound")
                lblInfo = "Acompanhe os conteúdos produzidos pelos especialistas e encontre todas as informações que busca sobre os procedimentos que você deseja,em posts médicos."
           }else if(indexPath.row == 4){
            imgChat.image = UIImage(named: "whiteRound")
                lblInfo = "Participe dos grupos e converse para tirar dúvidas, pedir indicações e compartilhar experiências."
           }else if(indexPath.row == 5){
              imgHome.image = UIImage(named: "whiteRound")
               lblInfo = "Você pode filtrar o procedimento que você deseja para encontrar com mais facilidade fotos de resultados de procedimentos reais. Confira em resultados."
           }
           
          addTutorialView(info: lblInfo)
       }
    func addTutorialView(info:String) {
        let optionView = HelpIndicatorView()
        
        optionView.frame = self.view.frame
        optionView.tag = 1000
        optionView.delegate = self
        optionView.basicSetup(lblText: info)
        self.view.addSubview(optionView)
        self.view.bringSubviewToFront(optionView)
    }
}
extension MinhasConsultasVC : HelpIndicatorViewDelegate{
    func removePressed() {
        removeImageFromAllButton()
    }
    func removeImageFromAllButton() {
        imgSearch.image = UIImage(named: "search")
        
        imgHome.image = UIImage(named: "home-Inactive")
         imgResultudos.image = UIImage(named: "jornada-Inactive")
         imgBookDoctor.image = UIImage(named: "agendar-Inactive")
         imgChat.image = UIImage(named: "chat-Inactive")
        imgProfile.image = UIImage(named: "profile-Inactive")
    }
}
