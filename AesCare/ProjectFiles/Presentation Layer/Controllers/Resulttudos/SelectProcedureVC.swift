//
//  SelectProcedureVC.swift
//  AesCare
//
//  Created by Gali Srikanth on 12/10/20.
//

import UIKit
import SwiftyJSON
class SelectProcedureVC: BaseViewController {
    @IBOutlet weak var vwNavigation: UIView!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var containerSearch: designableView!
    var selectedAreaStr = ""
    @IBOutlet weak var tblProcedure: UITableView!
    var gradientLayer: CAGradientLayer!
    var arrTreatment : [ModelTreatement]!
    var selectedTreatment = [Bool]()
    override func viewDidLoad() {
        super.viewDidLoad()
        //createGradientLayer()
        getTreatment()
       // selectedAreaStr = selectedAreaStr.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        ///// Call Custom Tabbar

        //gradientLayer.frame = self.vwNavigation.bounds
    }

    func createGradientLayer() {
        gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.vwNavigation.bounds
        gradientLayer.colors = [UIColor(named: "AppPurpleColor")!.cgColor, UIColor(named: "AppMoveColor")!.cgColor]
        self.vwNavigation.layer.addSublayer(gradientLayer)
        self.vwNavigation.bringSubviewToFront(self.btnMenu)
        self.vwNavigation.bringSubviewToFront(self.containerSearch)
    }
    
}
extension SelectProcedureVC : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 90
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommonCellTableViewCell2") as! CommonCellTableViewCell
        return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let mdObjArr = self.arrTreatment{
            return mdObjArr.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CommonCellTableViewCell = self.tblProcedure.dequeueReusableCell(withIdentifier: "CommonCellTableViewCell") as! CommonCellTableViewCell
        
        if let mdObjArr = self.arrTreatment{
            cell.lblInfo.text = mdObjArr[indexPath.row].name
        }
        
        
        cell.btnInstance.setImage(UIImage(named: "unchecked_box"), for: .normal)
        if(selectedTreatment.count > 0 && selectedTreatment[indexPath.row]){
            cell.btnInstance.setImage(UIImage(named: "checked_box"), for: .normal)
        }
        
        cell.btnInstance.tag = indexPath.row
        cell.btnInstance.addTarget(self, action: #selector(btnSelectTreatment(_:)), for: .touchUpInside)
        cell.layoutIfNeeded()
        return cell
    }
    @objc func btnSelectTreatment(_ sender:UIButton){
        selectedTreatment[sender.tag] = !selectedTreatment[sender.tag]
        self.tblProcedure.reloadData()
    }
}

extension SelectProcedureVC{
    
    func getTreatment() {
        if #available(iOS 13.0, *) {
            let keyWindow = UIApplication.shared.connectedScenes
                .filter({$0.activationState == .foregroundActive})
                .map({$0 as? UIWindowScene})
                .compactMap({$0})
                .first?.windows
                .filter({$0.isKeyWindow}).first
            CustomActivityIndicator.sharedInstance.display(onView: keyWindow, done: {
                
            })
        } else {
            
            UIApplication.shared.keyWindow?.rootViewController?.view.endEditing(true)
            CustomActivityIndicator.sharedInstance.display(onView: UIApplication.shared.keyWindow, done: {
                
            })
        }
        
        
        let getUrl :String = BaseUrl + "/treatment?body_parts=" + "\(selectedAreaStr)"
        if let urlString  = URL(string: getUrl){
            var request = URLRequest(url: urlString)
            //request.addValue("U4LsBbfYdKlYTB3kJYKpCKiG", forHTTPHeaderField: "aescare_session_key")
            request.httpMethod = "GET"
            URLSession.shared.dataTask(with: request) { data, response, error in
                //print(String(data: data!, encoding: .utf8)!)
                DispatchQueue.main.async {
                    CustomActivityIndicator.sharedInstance.hide {
                    }
                }
                if let dataTemp = data, dataTemp.count > 0 {
                    
                    do {
                        
                        self.arrTreatment = try JSONDecoder().decode([ModelTreatement].self, from: dataTemp)
                        print(self.arrTreatment as [ModelTreatement])
                        self.refreshUI()
                        
                    } catch let error  {
                        print("Parsing Failed \(error.localizedDescription)")
                    }
                }
            }.resume()
        }
    }
    
    func refreshUI(){
        DispatchQueue.main.async {
            
            for (_,_) in self.arrTreatment.enumerated(){
                self.selectedTreatment.append(false)
            }
            self.tblProcedure.reloadData()
        }
    }
    
}
extension SelectProcedureVC{
    @IBAction func backNavigationPressed(_ sender: Any) {
        backNavigation()
    }
    @IBAction func navigationNext(_ sender: Any) {
        var selectedArea = [Int]()
        for (index,object) in selectedTreatment.enumerated(){
            if(object){
               // selectedArea.append("\(arrTreatment[index].treatment_id!)")
                selectedArea.append(arrTreatment[index].treatment_id!)
            }
        }
        if(selectedArea.count == 0){
            UtilityClass.tosta(message: "Selecione a parte do corpo", duration: 2.0, vc: self)
            return
        }
        
        if #available(iOS 13.0, *) {
            let vc = self.storyboard?.instantiateViewController(identifier: "SendPhotoVC") as! SendPhotoVC
            vc.selectedTreatment = selectedArea
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            // Fallback on earlier versions
            let vc = SendPhotoVC.init(nibName: "SendPhotoVC", bundle: nil)
            vc.selectedTreatment = selectedArea
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
