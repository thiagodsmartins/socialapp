//
//  SelectAreaVC.swift
//  AesCare
//
//  Created by Gali Srikanth on 10/10/20.
//

import UIKit

class SelectAreaVC: BaseViewController {
    
    @IBOutlet weak var vwNavigation: UIView!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var containerSearch: designableView!
    var gradientLayer: CAGradientLayer!
    
    @IBOutlet weak var colViewArea: UICollectionView!
    
    var arrAreaId = ["1","2","3","4","5","6","7","8","9","11","12","13","14","15","16"]
    var arrArea = ["Nariz","Rosto","Olhos","Boca","Pescoço","Cabelo","Mama","Abdômen","BumBum","Braços","Costas","Região intima","Corpo","Pernas","Orelhas"]
    var arrImgArea = ["Nariz","Rosto","Olhos","Boca","Pescoco","Cabelo","Mama","Abdomen","BumBum","Bracos","Costas","Regiao intima","Corpo","Pernas","Orelhas"]
    var arrSelectedArea = [Bool]()
    override func viewDidLoad() {
        super.viewDidLoad()
        //createGradientLayer()
        for _ in arrAreaId.enumerated(){
            arrSelectedArea.append(false)
        }
    
        self.colViewArea.register(UINib(nibName: "FooterColBtnCell", bundle: nil), forSupplementaryViewOfKind:UICollectionView.elementKindSectionFooter, withReuseIdentifier: "FooterColBtnCell")
       
        self.colViewArea.reloadData()
        self.colViewArea.delegate = self
        self.colViewArea.dataSource = self
        // Do any additional setup after loading the view.
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        ///// Call Custom Tabbar
        
        //gradientLayer.frame = self.vwNavigation.bounds
    }

//    func createGradientLayer() {
//        gradientLayer = CAGradientLayer()
//        gradientLayer.frame = self.vwNavigation.bounds
//        gradientLayer.colors = [UIColor(named: "AppPurpleColor")!.cgColor, UIColor(named: "AppMoveColor")!.cgColor]
//        self.vwNavigation.layer.addSublayer(gradientLayer)
//        self.vwNavigation.bringSubviewToFront(self.btnMenu)
//        self.vwNavigation.bringSubviewToFront(self.containerSearch)
//    }
    @IBAction func backNavigationPressed(_ sender: Any) {
        backNavigation()
    }
}
//Mark: collectionView Work
extension SelectAreaVC:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrArea.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SliderCollCell", for: indexPath) as! SliderCollCell
        
        cell.labelMheaderName.text = arrArea[indexPath.row]
        cell.imageviewSlider.image = UIImage(named: arrImgArea[indexPath.row])
        
        
        cell.btnSlider.setImage(UIImage(named: "unchecked_box"), for: .normal)
        if(arrSelectedArea[indexPath.row]){
            cell.btnSlider.setImage(UIImage(named: "checked_box"), for: .normal)
        }
        
        cell.btnSlider.tag = indexPath.row
        cell.btnSlider.addTarget(self, action: #selector(btnSelectArea(_:)), for: .touchUpInside)
        cell.layoutIfNeeded()
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        arrSelectedArea[indexPath.row] = !arrSelectedArea[indexPath.row]
        colViewArea.reloadData()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: (self.colViewArea.frame.size.width / 5), height: (self.colViewArea.frame.size.width / 5))
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    @objc func btnSelectArea(_ sender:UIButton){
        arrSelectedArea[sender.tag] = !arrSelectedArea[sender.tag]
        colViewArea.reloadData()
    }
   
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return CGSize(width:collectionView.frame.size.width, height:90)
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {

        var reusableview = UICollectionReusableView()
        if (kind == UICollectionView.elementKindSectionFooter) {
            let section = indexPath.section
            switch (section) {
            case 0:
                let  firstheader: FooterColBtnCell = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "FooterColBtnCell", for: indexPath) as! FooterColBtnCell
               
                firstheader.btnInstance.addTarget(self, action: #selector(moveNext(_:)), for: .touchUpInside)
                reusableview = firstheader
                //return reusableview
            default:
                return reusableview

            }
        }
        return reusableview
    }

    @objc func moveNext(_ sender:UIButton){
        
        var selectedArea = [String]()
        for (index,object) in arrSelectedArea.enumerated(){
            if(object){
                selectedArea.append(arrAreaId[index])
            }
        }
        if(selectedArea.count == 0){
            UtilityClass.tosta(message: "Selecione a parte do corpo", duration: 2.0, vc: self)
            return
        }
        let stringSelecedAreaStr = selectedArea.joined(separator: ",")
            //",".join(selectedArea)
        if #available(iOS 13.0, *) {
            let vc = self.storyboard?.instantiateViewController(identifier: "SelectProcedureVC") as! SelectProcedureVC
            vc.selectedAreaStr = stringSelecedAreaStr
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            // Fallback on earlier versions
            let vc = SelectProcedureVC.init(nibName: "SelectProcedureVC", bundle: nil)
            vc.selectedAreaStr = stringSelecedAreaStr
            self.navigationController?.pushViewController(vc, animated: true)
        }
        /*
         if #available(iOS 13.0, *) {
         let vc = self.storyboard?.instantiateViewController(identifier: "SendPhotoVC") as! SendPhotoVC
         vc.selectedArea = selectedArea
         self.navigationController?.pushViewController(vc, animated: true)
         } else {
         // Fallback on earlier versions
         let vc = SendPhotoVC.init(nibName: "SendPhotoVC", bundle: nil)
         vc.selectedArea = selectedArea
         self.navigationController?.pushViewController(vc, animated: true)
         }*/
    }
}
