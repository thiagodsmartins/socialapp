//
//  SendPhotoVC.swift
//  AesCare
//
//  Created by Gali Srikanth on 11/10/20.
//

import UIKit

class SendPhotoVC: BaseViewController {
    
    @IBOutlet weak var vwNavigation: UIView!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var containerSearch: designableView!
    var gradientLayer: CAGradientLayer!
    var selectedTreatment = [Int]()
    @IBOutlet weak var tblSendePhoto: UITableView!
    
    var selectedDoctorForSubmit : Medics!
    var mediaImageList_multipartData : [MultiPartDataFormatStructure] = [MultiPartDataFormatStructure]()
    enum User_SendPhotInfo:Int{
        case Procedure = 0
        case Performer_Doctor
        case Evaluation_Medication
        case When_Peroformed_procedure
        case City
        case Investment
        case Experience
        case Titile_Result
        case Description
        case Phone
        case ConfirmationCode
        case Evaluation_Result
        case total
        func getPlaceholder () -> String {
            switch self {
            case .Procedure:
                return "Procedimento" //treatment_name
            case .Performer_Doctor:
                return "Qual médico realizou seu procedimento" //medic_id
            case .Evaluation_Medication:
                return "Avaliação do(a) médico(a)?" //evaluation_medic
            case .When_Peroformed_procedure:
                return "Quando o procedimento foi realizado" //treatment_date
            case .City:
                return "Em qual cidade?"
            case .Investment:
                return "Quanto foi investido?" //treatment_value
            case .Experience:
                return "Sua experiência valeu a pena?" //worth_it
            case .Titile_Result:
                return "Se desejar, insira um titulo para o seu resultudos: (opcional)" //title
            case .Description:
                return "Se desejar, descreva mais detalhes sobre o seu resultudos: (opcional)" //content
            case .Phone:
                return "Seu DDD + Celular (whatsapp)" //phone
            case .ConfirmationCode:
                return "Código de Confirmação"
            case .Evaluation_Result:
                return "Avaliação do(a) resultados?" //evaluation_result
            default:
                return ""
            }
            
        }
    }
    
    
    var arrSendPhotoInfo = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for _ in 0 ..< User_SendPhotInfo.total.rawValue {
            arrSendPhotoInfo.append("")
        }
        
        arrSendPhotoInfo[User_SendPhotInfo.Experience.rawValue] = "OFF"
        self.tblSendePhoto.delegate = self
        self.tblSendePhoto.dataSource = self
        self.tblSendePhoto.reloadData()
        //createGradientLayer()
        
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        ///// Call Custom Tabbar
        //gradientLayer.frame = self.vwNavigation.bounds
    }

    func createGradientLayer() {
        gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.vwNavigation.bounds
        gradientLayer.colors = [UIColor(named: "AppPurpleColor")!.cgColor, UIColor(named: "AppMoveColor")!.cgColor]
        self.vwNavigation.layer.addSublayer(gradientLayer)
        self.vwNavigation.bringSubviewToFront(self.btnMenu)
        self.vwNavigation.bringSubviewToFront(self.containerSearch)
    }
    @IBAction func backNavigationPressed(_ sender: Any) {
        backNavigation()
    }
    /*
    override func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if(textField.tag == User_SendPhotInfo.Phone.rawValue && textField.text?.count != 0 ){
            sendOtp(phone: textField.text!)
        }
        return true
    }*/
}
extension SendPhotoVC : UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return section == 0 ? 0 : 90
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if(section == 1){
            let cell = tableView.dequeueReusableCell(withIdentifier: "CommonCellTableViewCell5") as! CommonCellTableViewCell
            
            return cell
        }
        return nil
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? User_SendPhotInfo.total.rawValue : 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.section == 0 ? UITableView.automaticDimension : 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section == 1){
            let cell: TableCellWithColVW = self.tblSendePhoto.dequeueReusableCell(withIdentifier: "TableCellWithColVW") as! TableCellWithColVW
            cell.delegate = self
            cell.colVWImageAdd.reloadData()
            return cell
        }else{
            if(indexPath.row == User_SendPhotInfo.When_Peroformed_procedure.rawValue){
                let cell: CommonCellTableViewCell = self.tblSendePhoto.dequeueReusableCell(withIdentifier: "CommonCellTableViewCell2") as! CommonCellTableViewCell
                
                cell.btnInstance.removeTarget(nil,action: nil,for: .allEvents)
                
                let type = User_SendPhotInfo.init(rawValue: indexPath.row)
                cell.lblHeader.text = type?.getPlaceholder()
                
                cell.cellSelectionText.placeholder = type?.getPlaceholder()
                cell.cellSelectionText.text = arrSendPhotoInfo[indexPath.row]
                
                
                if(indexPath.row == User_SendPhotInfo.When_Peroformed_procedure.rawValue){
                    cell.cellSelectionText.isUserInteractionEnabled = false
                }else{
                    cell.cellSelectionText.isUserInteractionEnabled = true
                }
                cell.btnInstance.removeTarget(nil,action: nil,for: .allEvents)
                cell.btnInstance.addTarget(self,
                                           action: #selector(self.openConsultationDtae(_:)),
                                           for: .touchUpInside)
                
                return cell
            }else if(indexPath.row == User_SendPhotInfo.Performer_Doctor.rawValue){
                let cell: CommonCellTableViewCell = self.tblSendePhoto.dequeueReusableCell(withIdentifier: "CommonCellTableViewCell2") as! CommonCellTableViewCell
                
                cell.btnInstance.removeTarget(nil,action: nil,for: .allEvents)
                
                let type = User_SendPhotInfo.init(rawValue: indexPath.row)
                cell.lblHeader.text = type?.getPlaceholder()
                
                cell.cellSelectionText.placeholder = type?.getPlaceholder()
                cell.cellSelectionText.text = arrSendPhotoInfo[indexPath.row]
                
                
                if(indexPath.row == User_SendPhotInfo.When_Peroformed_procedure.rawValue){
                    cell.cellSelectionText.isUserInteractionEnabled = false
                }else{
                    cell.cellSelectionText.isUserInteractionEnabled = true
                }
                cell.btnInstance.removeTarget(nil,action: nil,for: .allEvents)
                cell.btnInstance.addTarget(self,
                                           action: #selector(self.opendoctorSearch(_:)),
                                           for: .touchUpInside)
                
                return cell
            } else if(indexPath.row == User_SendPhotInfo.City.rawValue) {
                let cell: CommonCellTableViewCell = self.tblSendePhoto.dequeueReusableCell(withIdentifier: "CommonCellTableViewCell2") as! CommonCellTableViewCell
                
                cell.btnInstance.removeTarget(nil,action: nil,for: .allEvents)
                
                let type = User_SendPhotInfo.init(rawValue: indexPath.row)
                cell.lblHeader.text = type?.getPlaceholder()
                
                cell.cellSelectionText.placeholder = type?.getPlaceholder()
                cell.cellSelectionText.text = arrSendPhotoInfo[indexPath.row]
                
                
                if(indexPath.row == User_SendPhotInfo.City.rawValue){
                    cell.cellSelectionText.isUserInteractionEnabled = false
                }else{
                    cell.cellSelectionText.isUserInteractionEnabled = true
                }
                cell.btnInstance.removeTarget(nil,action: nil,for: .allEvents)
                cell.btnInstance.addTarget(self,
                                           action: #selector(self.openSearchByCity(_:)),
                                           for: .touchUpInside)
                
                return cell
            }else if(indexPath.row == User_SendPhotInfo.Experience.rawValue){
                
                let cell: CommonCellTableViewCell = self.tblSendePhoto.dequeueReusableCell(withIdentifier: "CommonCellTableViewCell3") as! CommonCellTableViewCell
                let type = User_SendPhotInfo.init(rawValue: indexPath.row)
                cell.lblHeader.text = type?.getPlaceholder()
                
                
                cell.btnSwitch.isOn = arrSendPhotoInfo[indexPath.row] == "ON" ? true : false
                cell.btnSwitch.tag = indexPath.row // for detect which row switch Changed
                cell.btnSwitch.addTarget(self, action: #selector(self.switchChanged(_:)), for: .valueChanged)
                
                return cell
            }else if(indexPath.row == User_SendPhotInfo.Evaluation_Medication.rawValue ||
                indexPath.row == User_SendPhotInfo.Evaluation_Result.rawValue){
                let cell: CommonCellTableViewCell = self.tblSendePhoto.dequeueReusableCell(withIdentifier: "CommonCellTableViewCell4") as! CommonCellTableViewCell
                let type = User_SendPhotInfo.init(rawValue: indexPath.row)
                cell.lblHeader.text = type?.getPlaceholder()
                
                cell.vwFloating.tag  = indexPath.row
                cell.vwFloating.rating = Float(arrSendPhotoInfo[indexPath.row]) ?? 0.0
                cell.vwFloating.delegate = self
                //let rating_count = String(cell.vwFloating.rating)
                //print("============")
                //print(rating_count)
                //cell.vwFloating.rating
                
                return cell
            } else{
                let cell: CommonCellTableViewCell = self.tblSendePhoto.dequeueReusableCell(withIdentifier: "CommonCellTableViewCell1") as! CommonCellTableViewCell
                let type = User_SendPhotInfo.init(rawValue: indexPath.row)
                cell.lblHeader.text = type?.getPlaceholder()
                cell.cellSelectionText.isUserInteractionEnabled = true
                cell.cellSelectionText.tag = indexPath.row
                cell.cellSelectionText.text = arrSendPhotoInfo[indexPath.row]
                cell.cellSelectionText.placeholder = type?.getPlaceholder()
                cell.cellSelectionText.removeTarget(nil,
                                                    action: nil,
                                                    for: .allEvents)
                cell.cellSelectionText.addTarget(self, action: #selector(self.textFieldValueChange(_:)), for: .editingChanged)
                
                return cell
            }
        }
    }
    
}

extension SendPhotoVC : PopupCountrySelectionDelegate{
    func didSelectCountryPressed(_ selectedCountry: ModelCountryList) {
        ///
    }
    func didSelectCityPressed(_ selectedCity: ModelSearchedCity){
        arrSendPhotoInfo[User_SendPhotInfo.City.rawValue] = selectedCity.name!
        tblSendePhoto.reloadData()
    }
   
    @objc func openSearchByCity(_ sender:UIButton){
       let optionView = CountrySearchPopUp()
         
         optionView.frame = self.view.frame
        
         optionView.tag = 1000
         optionView.basicSetup()
         optionView.fadeIn()
         optionView.delegate = self
         optionView.isSearchforCountry = false
         self.view.bringSubviewToFront(optionView)
         self.view.addSubview(optionView)
    }
}

extension SendPhotoVC:PopupDoctorSelectionDelegate{
    func didSelectDoctorPressed(_ selectedDoctor: Medics) {
        arrSendPhotoInfo[User_SendPhotInfo.Performer_Doctor.rawValue] = selectedDoctor.name!
        selectedDoctorForSubmit = selectedDoctor
        tblSendePhoto.reloadData()
    }
    
    @objc func opendoctorSearch(_ sender:UIButton){
        let optionView = SearchByDoctorNamePopUp()
        
        optionView.frame = self.view.frame
        
        optionView.tag = 1000
        optionView.basicSetup()
        optionView.fadeIn()
        optionView.delegate = self
        self.view.bringSubviewToFront(optionView)
        self.view.addSubview(optionView)
    }
}

extension SendPhotoVC{
    
    
    @objc func textFieldValueChange(_ txt: UITextField)  {
        if let str = txt.text, str.count > 0 {
            self.arrSendPhotoInfo[txt.tag] = str
        }else{
            self.arrSendPhotoInfo[txt.tag] = ""
        }
    }
   
    @objc func switchChanged(_ sender : UISwitch!){
        arrSendPhotoInfo[sender.tag] = sender.isOn ? "ON" : "OFF"
        
        //isExperienceGood = sender.isOn
        tblSendePhoto.reloadData()
        //print("table row switch Changed \(sender.tag)")
        //print("The switch is \(sender.isOn ? "ON" : "OFF")")
    }
    func isValidData() -> Bool {
        
        guard !arrSendPhotoInfo[User_SendPhotInfo.Procedure.rawValue].isEmpty else {
            showAlertMessage(title: "Ooops", message: ALERT_MESSAGE_MANDETORY_FIELD, vc: self)
            return false
        }
        guard !arrSendPhotoInfo[User_SendPhotInfo.Performer_Doctor.rawValue].isEmpty else {
            showAlertMessage(title: "Ooops", message: ALERT_MESSAGE_MANDETORY_FIELD, vc: self)
            return false
        }
        guard !arrSendPhotoInfo[User_SendPhotInfo.Evaluation_Medication.rawValue].isEmpty else {
            showAlertMessage(title: "Ooops", message: ALERT_MESSAGE_MANDETORY_FIELD, vc: self)
            return false
        }
        guard !arrSendPhotoInfo[User_SendPhotInfo.Evaluation_Medication.rawValue].isEmpty else {
            showAlertMessage(title: "Ooops", message: ALERT_MESSAGE_MANDETORY_FIELD, vc: self)
            return false
        }
        guard !arrSendPhotoInfo[User_SendPhotInfo.When_Peroformed_procedure.rawValue].isEmpty else {
            showAlertMessage(title: "Ooops", message: ALERT_MESSAGE_MANDETORY_FIELD, vc: self)
            return false
        }
        
        
        guard !arrSendPhotoInfo[User_SendPhotInfo.City.rawValue].isEmpty else {
            showAlertMessage(title: "Ooops", message: ALERT_MESSAGE_MANDETORY_FIELD, vc: self)
            return false
        }
        guard !arrSendPhotoInfo[User_SendPhotInfo.Investment.rawValue].isEmpty else {
            showAlertMessage(title: "Ooops", message: ALERT_MESSAGE_MANDETORY_FIELD, vc: self)
            return false
        }
        
        guard !arrSendPhotoInfo[User_SendPhotInfo.Phone.rawValue].isEmpty else {
            showAlertMessage(title: "Ooops", message: ALERT_MESSAGE_MANDETORY_FIELD, vc: self)
            return false
        }
        guard !arrSendPhotoInfo[User_SendPhotInfo.Evaluation_Result.rawValue].isEmpty else {
            showAlertMessage(title: "Ooops", message: ALERT_MESSAGE_MANDETORY_FIELD, vc: self)
            return false
        }
        return true
    }
    @IBAction func navigationNext(_ sender: Any) {
        
        
        if(!isValidData()){
            return
        }
        
        /*
         "is_new": "true", // Always "true"
         
         "treatment_name": "treatment_name", // From field "Procedimento"
         
         "medic_id": 123,
         
         "treatment_value": "485", // From field "Quanto foi investido"
         
         "evaluation_medic": 3, // From field "Avaliação do(a) médico(a)?"
         
         "treatment_date": "", // From field "Quando o procedimento foi realizado?". It must be a date in format yyyy-MM-dd
         
         "worth_it": true, // From field "Sua experiência valeu a pena?"
         
         "title": "title", // From field "Se desejar, insira um título para o seu resultado: (opcional)"
         
         "content": "content", // From field "Se desejar, descreva mais detalhes sobre o seu resultado: (opcional)"
         
         "phone": "41987564548", // From field "Seu DDD + Celular (whatsapp)"
         
         "evaluation_result": 2, // From field "Avaliação do resultado"
         
         "published": "true", // Always "true"
         
         "treatment_ids": [1,2,5] // Treatment ids you got from past screens
         */
     
        var param_Outer:[String:AnyObject] = [String:AnyObject]()
        
        var param:[String:AnyObject] = [String:AnyObject]()
        
        param["is_new"] = "true" as AnyObject
        param["treatment_name"] = arrSendPhotoInfo[User_SendPhotInfo.Procedure.rawValue] as AnyObject
        param["medic_id"] = selectedDoctorForSubmit.medic_id as AnyObject
        param["treatment_value"] = arrSendPhotoInfo[User_SendPhotInfo.Investment.rawValue] as AnyObject
        param["evaluation_medic"] = Float(arrSendPhotoInfo[User_SendPhotInfo.Evaluation_Medication.rawValue]) as AnyObject
            //Int(arrSendPhotoInfo[User_SendPhotInfo.Evaluation_Medication.rawValue]) as AnyObject
        param["treatment_date"] = arrSendPhotoInfo[User_SendPhotInfo.When_Peroformed_procedure.rawValue] as AnyObject
        let worth_it = arrSendPhotoInfo[User_SendPhotInfo.Experience.rawValue] == "ON" ? true : false
        param["worth_it"] = worth_it as AnyObject
        param["title"] = arrSendPhotoInfo[User_SendPhotInfo.Titile_Result.rawValue] as AnyObject
        param["content"] = arrSendPhotoInfo[User_SendPhotInfo.Description.rawValue] as AnyObject
         param["phone"] = arrSendPhotoInfo[User_SendPhotInfo.Phone.rawValue] as AnyObject
        param["evaluation_result"] = Float(arrSendPhotoInfo[User_SendPhotInfo.Evaluation_Result.rawValue]) as AnyObject //Int(arrSendPhotoInfo[User_SendPhotInfo.Evaluation_Result.rawValue]) as AnyObject
        param["published"] = "true" as AnyObject
        
        /*
        var arr = [Int]()
        for(_,object) in selectedTreatment.enumerated(){
            arr.append(object)
        }*/
        param["treatment_ids"] = selectedTreatment as AnyObject
            //(selectedTreatment as Any) as AnyObject
        param_Outer["testimonial"] = param as AnyObject
        print(param_Outer)
        
        sendResult(param: param_Outer)
     }
}
extension SendPhotoVC : FloatRatingViewDelegate{
    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Float) {
        arrSendPhotoInfo[ratingView.tag] = String(rating)
    }
}
extension SendPhotoVC: DatePickerDelegate{
    
    
    @objc func openConsultationDtae(_ sender:UIButton){
           MyBasics.showDatePickerDropDown(PickerType: .date, ParentViewC: self, setRestriction: "")
       }
    func selectedDate(selectedDate: Date) {
        /*
        if(Date().compare(selectedDate) == .orderedAscending){
            UtilityClass.tosta(message: "a data de tratamento não pode ser uma data futura", duration: 2.0, vc: self)
            return
        }*/
        let formatter = DateFormatter()
        formatter.dateFormat = "YYYY-MM-dd"
        arrSendPhotoInfo[User_SendPhotInfo.When_Peroformed_procedure.rawValue] = formatter.string(from: selectedDate)
        tblSendePhoto.reloadData()
    }
    
}
extension SendPhotoVC:MyCelDelegate{
    func didaddImage(_ arrImage: [MultiPartDataFormatStructure]) {
        mediaImageList_multipartData = arrImage
    }
}
///
///Mark : Api call
///
extension SendPhotoVC{
    func sendResult(param : [String:AnyObject]) {
        
        let url = (API.SendResult.getURL()?.absoluteString ?? "")
        let operation =  WebServiceOperation.init(url,param, .WEB_SERVICE_POST,nil)
        
        operation.completionBlock =
            {
                
                DispatchQueue.main.async {
                    print(operation.responseData?.dictionary?.prettyprintedJSON ?? "")
                    guard let dictResponse = operation.responseData?.dictionary, dictResponse.count > 0 else {
                        return
                    }
                    
                    if let responceCode = dictResponse["code"] ,responceCode as! Int == 1 ,let testiMonial = dictResponse["testimonial"] as?  Dictionary<String, Any>,let testId = testiMonial["testimonial_id"] as? Int{
                        //UtilityClass.tosta(message: msg, duration: 2.0, vc: self)
                        var param:[String:AnyObject] = [String:AnyObject]()
                        param["testimonial_id"] = testId as AnyObject
                        self.sendResultPhoto(param: param)
                        
                    } else if let msg = dictResponse["message"] as? String{
                        UtilityClass.tosta(message: msg, duration: 2.0, vc: self)
                    }
                    else{
                        UtilityClass.tosta(message: "Something wentwrong", duration: 2.0, vc: self)
                    }
                }
        }
        
        appDel.operationQueue.addOperation(operation)
        
    }
    
    func sendResultPhoto(param : [String:AnyObject]) {
        
        let url = (API.SendResultPhoto.getURL()?.absoluteString ?? "")
        let operation =  WebServiceOperation.init(url,param, .WEB_SERVICE_MULTI_PART,mediaImageList_multipartData)
        
        operation.completionBlock =
            {
                
                DispatchQueue.main.async {
                    
                    print(operation.responseData?.dictionary?.prettyprintedJSON ?? "")
                    guard let dictResponse = operation.responseData?.dictionary, dictResponse.count > 0 else {
                        return
                    }
                    self.navigateToHome()
                    /*
                     if let responceCode = dictResponse["code"] ,responceCode as! Int == 1 ,let msg = dictResponse["message"] as? String{
                     UtilityClass.tosta(message: msg, duration: 2.0, vc: self)
                     self.navigateToHome()
                     
                     } else if let msg = dictResponse["message"] as? String{
                     UtilityClass.tosta(message: msg, duration: 2.0, vc: self)
                     }
                     else{
                     UtilityClass.tosta(message: "Something wentwrong", duration: 2.0, vc: self)
                     }*/
                }
        }
        
        appDel.operationQueue.addOperation(operation)
    }
    func sendOtp(phone:String){
        let url = "https://ws.aescare.com/user/send-phone-validation-code";
        var param:[String:AnyObject] = [String:AnyObject]()
        param["phone"] = phone as AnyObject
        let operation =  WebServiceOperation.init(url, nil, .WEB_SERVICE_MULTI_PART,nil)
        
        operation.completionBlock =
            {
                
                DispatchQueue.main.async {
                    
                    print(operation.responseData?.dictionary?.prettyprintedJSON ?? "")
                    guard let dictResponse = operation.responseData?.dictionary, dictResponse.count > 0 else {
                        return
                    }
                    
                    if let responceCode = dictResponse["code"] ,responceCode as! Int == 1 ,let msg = dictResponse["message"] as? String{
                        1
                        
                    } else if let msg = dictResponse["message"] as? String{
                        UtilityClass.tosta(message: msg, duration: 2.0, vc: self)
                    }
                    else{
                        UtilityClass.tosta(message: "Something wentwrong", duration: 2.0, vc: self)
                    }
                }
        }
        
        appDel.operationQueue_background.addOperation(operation)
    }
}
