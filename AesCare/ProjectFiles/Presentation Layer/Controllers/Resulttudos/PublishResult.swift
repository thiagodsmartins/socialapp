//
//  PublishResult.swift
//  AesCare
//
//  Created by Gali Srikanth on 10/10/20.
//

import UIKit
import SideMenu
import AMTabView

class PublishResult: BaseViewController, TabItem {
    @IBOutlet weak var vwNavigation: UIView!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var containerSearch: designableView!
    @IBOutlet weak var buttonBack: UIButton!
    
    //var gradientLayer: CAGradientLayer!
    var swipeGesture: UISwipeGestureRecognizer?
    var isFromSideMenu = false
    
    var tabImage: UIImage? {
      return UIImage(named: "icone-publicacao")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isFromSideMenu {
            self.buttonBack.isHidden = false
        }
        else {
            self.buttonBack.isHidden = true
        }
        
        self.swipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(self.openSideMenuWithSwipe(_:)))
        self.swipeGesture?.direction = .right
        
        //createGradientLayer()
        
        self.view.addGestureRecognizer(self.swipeGesture!)
    }
    
    @objc func openSideMenuWithSwipe(_ gestureRecognizer: UISwipeGestureRecognizer) {
        if gestureRecognizer.state == .ended {
            self.menuPressed()
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        ///// Call Custom Tabbar
        //tabbarUISetup()
        //gradientLayer.frame = self.vwNavigation.bounds
    }
    
    func tabbarUISetup(){
        ///// Call Custom Tabbar
        customTabbar.myDelegate = self
        customTabbar.setup(activeFor:"Jornada")
    }
    
//    func createGradientLayer() {
//        gradientLayer = CAGradientLayer()
//        gradientLayer.frame = self.vwNavigation.bounds
//        gradientLayer.colors = [UIColor(named: "AppPurpleColor")!.cgColor, UIColor(named: "AppMoveColor")!.cgColor]
//        self.vwNavigation.layer.addSublayer(gradientLayer)
//        self.vwNavigation.bringSubviewToFront(self.btnMenu)
//        self.vwNavigation.bringSubviewToFront(self.containerSearch)
//    }
    
    private func menuPressed() {
        appDel.topViewControllerWithRootViewController(rootViewController: UIApplication.shared.windows.first?.rootViewController)?.view.endEditing(true)
        
        SideMenuManager.default.leftMenuNavigationController?.settings.presentationStyle = .menuSlideIn
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let menu = storyboard.instantiateViewController(withIdentifier: "SideMenuNavigationController") as! SideMenuNavigationController
        menu.leftSide = true
        
        UIApplication.shared.windows.first?.rootViewController?.present(menu, animated: true, completion: nil)
        
    }
    
    @IBAction func backNavigationPressed(_ sender: Any) {
        backNavigation()
    }
}

extension PublishResult{
    @IBAction func navigateToSurgury(_ sender: Any) {
        if #available(iOS 13.0, *) {
            let vc = self.storyboard?.instantiateViewController(identifier: "SelectAreaVC") as! SelectAreaVC
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            let vc = SelectAreaVC.init(nibName: "SelectAreaVC", bundle: nil)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func navigateToOther(_ sender: Any) {
        if #available(iOS 13.0, *) {
            let vc = self.storyboard?.instantiateViewController(identifier: "SendPhotoVC") as! SendPhotoVC
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            let vc = SendPhotoVC.init(nibName: "SendPhotoVC", bundle: nil)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
