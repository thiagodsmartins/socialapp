//
//  AESCareNotifications.swift
//  AesCare
//
//  Created by Thiago on 23/02/21.


import Foundation
import UIKit
import UserNotifications
import FirebaseMessaging
import Alamofire

class AESCareNotifications: NSObject, MessagingDelegate, UNUserNotificationCenterDelegate {
    private var notification: NotificationCenter!
    private var firebaseToken: String?
    
    var token: String {
        get {
            return self.firebaseToken ?? ""
        }
    }
    
    override init() {
        super.init()
        self.notification = NotificationCenter.default
    }

    /// Request user authorization to receive push notifications
    func requestUserPermissionForNotifications() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound], completionHandler: {
            [weak self] access, error in
            guard error == nil else {
                return
            }

            self?.notificationSettings()
        })
    }

    /// Verify if push notification is enabled
    func notificationSettings() {
        UNUserNotificationCenter.current().getNotificationSettings(completionHandler: {
            settings in
            guard settings.authorizationStatus == .authorized else {
                return
            }
            
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
                UNUserNotificationCenter.current().delegate = self
                Messaging.messaging().delegate = self
            }
        })
    }

    /// Start events observables. When specific event happens, the observers will trigger specifc processes
    func startNotificationsObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(onReceiveRemoteNotification(_:)), name: .didReceiveRemoteNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.onRegisterForRemoteNotificationsWithDeviceToken(_:)), name: .didRegisterForRemoteNotificationsWithDeviceToken, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.onFailToRegisterForRemoteNotificationsWithError(_:)), name: .didFailToRegisterForRemoteNotificationsWithError, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.onPresent(_:)), name: .willPresent, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.onReceive(_:)), name: .didReceive, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.onReceiveRegistrationToken(_:)), name: .didReceiveRegistrationToken, object: nil)
    }

    /// Post a notification to be handled by specific process
    func postNotification(type: Notification.Name, data: [AnyHashable : Any]?) {
        self.notification.post(name: type, object: nil, userInfo: data)
    }
    
    func notificationDataDecodable(_ receivedNotification: [AnyHashable : Any]?) -> NSDictionary? {
        guard let aps = receivedNotification?[AnyHashable("aps")] as? NSDictionary,
              let alert = aps["alert"] as? NSDictionary,
              let _ = alert["body"] as? String,
              let _ = alert["title"] as? String,
              let _ = alert["type"] as? String
        else {
            return nil
        }
        
        return alert
    }
    
    // MARK: - Delegate Functions
    // Called when a notification arrives while app is running on foreground
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        self.postNotification(type: .willPresent, data: nil)
            
        let userInfo = notification.request.content.userInfo
        
        print(userInfo)
        print(notification)
        completionHandler([.sound, .alert, .badge])
    }
    
    // Called when a notification arrives and interaction is made with it while app is running on background
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        self.postNotification(type: .didReceive, data: nil)
        
        let userInfo = response.notification.request.content.userInfo
        
        print(userInfo)
        
        completionHandler()
    }
    
    // Called every time app is launch and monitors if token is refreshed or not
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        let token = ["token" : fcmToken]

        self.postNotification(type: .didReceiveRegistrationToken, data: token as [AnyHashable : Any])
    }
    
    static func localNotification(title: String, subtitle: String, body: String) {
        let notification = UNMutableNotificationContent()
        notification.title = title
        notification.subtitle = subtitle
        notification.body = body
        notification.sound = UNNotificationSound(named: UNNotificationSoundName("local_notification.m4r"))
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 3, repeats: false)
        let request = UNNotificationRequest(identifier: UUID().uuidString, content: notification, trigger: trigger)
        
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }
}

// MARK: - Notification Selectors
extension AESCareNotifications {
    /// Called when a notification is opened and app is not running
    @objc func onReceiveRemoteNotification(_ sender: Notification) {
        print("Remote Notification received!!!")
        let notificationData = self.notificationDataDecodable(sender.userInfo)
        
        print("\(notificationData?["alert"] as? NSDictionary ?? ["alert": "Error"])")
        print("\(notificationData?["title"] as? String ?? "")")
        print("\(notificationData?["body"] as? String ?? "")")
    }
    
    /// Called when the push notification is processed correctly and a unique token identifier is returned
    @objc func onRegisterForRemoteNotificationsWithDeviceToken(_ sender: Notification) {
        print("Registered for remote notification")
    
        self.firebaseToken = Messaging.messaging().fcmToken
    }
    
    /// Called when a error occurs when push notification is initialized
    @objc func onFailToRegisterForRemoteNotificationsWithError(_ sender: Notification) {
        print("Push notification error: \((sender.userInfo?["error"] as? Error)?.localizedDescription ?? "")")
    }
    
    /// Called when app is running on foreground
    @objc func onPresent(_ sender: Notification) {
        print("Notification received while app is running on foreground")
    }
    
    /// Called when app is running on background
    @objc func onReceive(_ sender: Notification) {
        print("Notification received while app is running on background")
    }
    
    /// Called everytime to monitor token state
    @objc func onReceiveRegistrationToken(_ sender: Notification) {
        //self.firebaseToken = sender.userInfo?["token"] as? String
        
        //self.sendToken(self.firebaseToken!)
    }
}

// MARK: - Nofication Requests
extension AESCareNotifications {
    func sendToken(_ token: String, response: @escaping (Bool) -> Void) {
        let headers: HTTPHeaders = ["Content-Type": "multipart/form-data", "Cookie": "aescare_session_key=\(usrerModelOBJ.session_key!)"]
        let url = "\(BaseUrl)/app/user/updatetoken"
        let parameters = ["firebase_token" : token, "app_device_type" : "i"] as Parameters
        
        print("TOKEN GENERATED: \(token) \n")
        print("\(usrerModelOBJ.session_key!)")
        
        Alamofire.upload(multipartFormData: {
            multipartFormData in
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to: url, method: .post, headers: headers, encodingCompletion: {
            encodingResult in
            switch encodingResult {
            case .success(_, _, _):
                response(true)
            case .failure(let encodingError):
                print(encodingError)
                response(false)
            }
        })
    }
}

// MARK: - Notification Name
extension Notification.Name {
    static let didReceiveRemoteNotification = Notification.Name("didReceiveRemoteNotification")
    static let didRegisterForRemoteNotificationsWithDeviceToken = Notification.Name("didRegisterForRemoteNotificationsWithDeviceToken")
    static let didFailToRegisterForRemoteNotificationsWithError = Notification.Name("didFailToRegisterForRemoteNotificationsWithError")
    static let willPresent = Notification.Name("willPresent")
    static let didReceive = Notification.Name("didReceive")
    static let didReceiveRegistrationToken = Notification.Name("didReceiveRegistrationToken")
}
