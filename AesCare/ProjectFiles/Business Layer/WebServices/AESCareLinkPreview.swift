//
//  AESCareLinkPreview.swift
//  AesCare
//
//  Created by Thiago on 02/02/21.
//

import Foundation
import LinkPresentation

@available(iOS 13.0, *)
class AESCareLinkPreview {
    var metaDataProvider: LPMetadataProvider?
    var linkView: LPLinkView?
    
    init() {
        self.metaDataProvider = LPMetadataProvider()
        self.linkView = LPLinkView()
    }
    
    func retriveMetaData(_ url: inout String?, extractLinkFromString: Bool = false, requestHandler: @escaping (LPLinkView?) -> Void) {
        self.metaDataProvider = LPMetadataProvider()
        
        if extractLinkFromString {
            if let link = self.linkDetection(data: url ?? "") {
                self.linkView?.metadata.url = URL(string: link)
                
                guard let urlMetaData = URL(string: link) else {
                    requestHandler(nil)
                    return
                }
                
                self.metaDataProvider?.startFetchingMetadata(for: urlMetaData, completionHandler: {
                    (data, error) in
                    
                    guard let metaData = data, error == nil else {
                        requestHandler(nil)
                        return
                    }
                    
                    print("DATA RESULT: \(metaData.title ?? "No title")")
                    
                    DispatchQueue.main.async { [weak self] in
                        guard let self = self else { return }
                        
                        self.linkView?.metadata = metaData
                        
                        requestHandler(self.linkView)
                    }
                })
            }
            else {
                requestHandler(nil)
            }
        }
        else {
            self.linkView?.metadata.url = URL(string: url ?? "")
            
            guard let urlMetaData = URL(string: url ?? "") else {
                return
            }
            
            self.metaDataProvider?.startFetchingMetadata(for: urlMetaData, completionHandler: {
                (data, error) in
                
                guard let metaData = data, error == nil else {
                    return
                }
                
                print("DATA RESULT: \(metaData.title ?? "No title")")
                
                DispatchQueue.main.async { [weak self] in
                    guard let self = self else { return }
                    
                    self.linkView?.metadata = metaData
                    
                    requestHandler(self.linkView)
                }
            })
        }
        
    }
    
    private func linkDetection(data: String) -> String? {
        let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
        let matches = detector.matches(in: data, options: [], range: NSRange(location: 0, length: data.utf16.count))
        var link: String? = ""
        
        for match in matches {
            guard let range = Range(match.range, in: data) else { continue }
            let url = data[range]
            print("URL::: \(url)")
            link = String(url)
        }
        
        if link?.isEmpty == nil {
            return nil
        }
        else {
            return link
        }
    }
}
