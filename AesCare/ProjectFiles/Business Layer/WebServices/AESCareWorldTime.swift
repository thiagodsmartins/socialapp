//
//  AESCareWorldTime.swift
//  AesCare
//
//  Created by Thiago on 26/01/21.
//

import Foundation
import Alamofire

class AESCareWorldTime {
    static func getRequest(_ url: String?) {
        let time = Alamofire.request(url!)
        
        time.responseJSON(completionHandler: { response in
            debugPrint(response)
            if let itemJson = response.result.value as? Dictionary<String,Any> {
                print(itemJson["datetime"]!)
            }
            
        })
    }
}
