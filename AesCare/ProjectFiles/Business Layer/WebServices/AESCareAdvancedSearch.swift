//
//  AESCareAdvancedSearch.swift
//  AesCare
//
//  Created by Thiago on 25/04/21.
//

import Foundation
import UIKit

class AESCareAdvancedSearch: UIView {
    var blurEffect: UIBlurEffect!
    var blurEffectView: UIVisualEffectView!
    var tableViewAdvancedSearch: UITableView!
    var buttonClose: UIButton!
    var advancedSearchData: [AdvancedSearch]?
    var nibFiles: [UINib]!
    var isDataLoading: Bool!
    var pageNumber = 1
    var searchText = ""
    
    override init(frame: CGRect) {
        super.init(frame: frame)

        self.advancedSearchData = [AdvancedSearch]()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        self.advancedSearchData = [AdvancedSearch]()
    }
    
    override func didMoveToSuperview() {
        UIView.transition(with: self, duration: 1.0, options: .transitionCrossDissolve, animations: {
            self.setupViews()
        })
        
        self.isDataLoading = true
        
        self.requestAdvancedSearch(self.pageNumber, textData: self.searchText, completion: { [weak self]
            data in
            
            if let search = data {
                self?.advancedSearchData!.append(contentsOf: search)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3), execute: {
                    self?.isDataLoading = false
                    self?.pageNumber += self?.pageNumber ?? 1
                    self?.tableViewAdvancedSearch.reloadData()
                })
            }
            else {
                self?.advancedSearchData = []
                
                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3), execute: {
                    self?.isDataLoading = false
                    self?.tableViewAdvancedSearch.reloadData()
                })
            }
        })
    }
    
    private func setupViews() {
        self.setupBlurEffect()
        self.setupTableView()
        self.setupCloseButton()
        self.setupConstraints()
    }
    
    private func setupTableView() {
        self.nibFiles = [UINib]()
        self.nibFiles.append(UINib(nibName: "ResultsDetailsCommentsTableViewCell", bundle: nil))
        self.nibFiles.append(UINib(nibName: "AdvancedSearchMedicInfoTableViewCell", bundle: nil))
        self.nibFiles.append(UINib(nibName: "EmptyTableViewCell", bundle: nil))
        self.nibFiles.append(UINib(nibName: "ResultsTableViewCell", bundle: nil))
        self.nibFiles.append(UINib(nibName: "MedicPostsTableViewCell", bundle: nil))
        
        self.tableViewAdvancedSearch = UITableView()
        self.tableViewAdvancedSearch.allowsSelection = true
        self.tableViewAdvancedSearch.backgroundColor = .clear
        self.tableViewAdvancedSearch.estimatedRowHeight = UITableView.automaticDimension
        self.tableViewAdvancedSearch.separatorStyle = .none
        self.tableViewAdvancedSearch.translatesAutoresizingMaskIntoConstraints = false
        self.tableViewAdvancedSearch.delegate = self
        self.tableViewAdvancedSearch.dataSource = self
        
        self.tableViewAdvancedSearch.register(self.nibFiles[0], forCellReuseIdentifier: "ResultsDetailsCommentsTableViewCell")
        self.tableViewAdvancedSearch.register(self.nibFiles[1], forCellReuseIdentifier: "AdvancedSearchMedicInfoTableViewCell")
        self.tableViewAdvancedSearch.register(self.nibFiles[2], forCellReuseIdentifier: "EmptyTableViewCell")
        self.tableViewAdvancedSearch.register(self.nibFiles[3], forCellReuseIdentifier: "ResultsTableViewCell")
        self.tableViewAdvancedSearch.register(self.nibFiles[4], forCellReuseIdentifier: "MedicPostsTableViewCell")
        
        self.addSubview(self.tableViewAdvancedSearch)
    }
        
    private func setupBlurEffect() {
        self.blurEffect = UIBlurEffect(style: .light)
        self.blurEffectView = UIVisualEffectView(effect: self.blurEffect)
        
        self.blurEffectView.frame = self.bounds
        self.blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        self.addSubview(self.blurEffectView)
    }
    
    private func setupCloseButton() {
        self.buttonClose = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        
        self.buttonClose.setTitle("X", for: .normal)
        self.buttonClose.setTitleColor(.purple, for: .normal)
        self.buttonClose.layer.borderColor = UIColor.purple.cgColor
        self.buttonClose.layer.borderWidth = 2
        self.buttonClose.layer.cornerRadius = self.buttonClose.frame.height / 2
        self.buttonClose.layer.borderColor = UIColor.purple.cgColor
        self.buttonClose.addTarget(self, action: #selector(self.buttonCloseViewPressed(_:)), for: .touchUpInside)
        self.buttonClose.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(self.buttonClose)
    }
    
    private func setupConstraints() {
        NSLayoutConstraint.activate([
            self.tableViewAdvancedSearch.topAnchor.constraint(equalTo: self.topAnchor, constant: 150),
            self.tableViewAdvancedSearch.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20),
            self.tableViewAdvancedSearch.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -100),
            self.tableViewAdvancedSearch.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20),
            
            self.buttonClose.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            self.buttonClose.bottomAnchor.constraint(equalTo: self.tableViewAdvancedSearch.topAnchor, constant: -40),
            self.buttonClose.widthAnchor.constraint(equalToConstant: 30),
            self.buttonClose.heightAnchor.constraint(equalToConstant: 30)
        ])
    }
    
    private func specialty(_ id: Int) -> String {
        switch id {
        case 1:
            return "Cirurgia Plástica"
        case 2:
            return "Dermatologia"
        case 3:
            return "Endocrinologia"
        case 4:
            return "Nutrologia"
        case 5:
            return "Bariátrica"
        case 6:
            return "Cirurgião Dentista"
        case 7:
            return "Ginecologia e Obstetrícia"
        default:
            return "Não Especificada"
        }
    }
}

extension AESCareAdvancedSearch: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.advancedSearchData!.isEmpty {
            return 1
        }
        else {
            return self.advancedSearchData!.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if !self.isDataLoading && !self.advancedSearchData!.isEmpty {
            if self.advancedSearchData![indexPath.row].card_type == "medic" {
                let cell = tableView.dequeueReusableCell(withIdentifier: "AdvancedSearchMedicInfoTableViewCell") as! AdvancedSearchMedicInfoTableViewCell
                cell.labelMedicName.text = self.advancedSearchData![indexPath.row].name_with_title!
                cell.labelMedicSpecialty.text = "Especialidade: \(self.specialty(self.advancedSearchData![indexPath.row].specialty_id ?? 0))"
                cell.labelMedicLocation.text = "\(self.advancedSearchData![indexPath.row].city!.name!)/\(self.advancedSearchData![indexPath.row].city!.state!.name!)"
                cell.imageViewIcon.sd_setImage(with: URL(string: self.advancedSearchData![indexPath.row].cover_media!.image_url!), completed: nil)
                
                return cell
            }
            else if self.advancedSearchData![indexPath.row].card_type == "result" {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ResultsTableViewCell") as! ResultsTableViewCell
                cell.labelResultsTitle.text = self.advancedSearchData![indexPath.row].title!
                cell.setTextToLabel(self.advancedSearchData![indexPath.row].medic!.name_with_title ?? "", label: cell.labelDoctorName, imageName: "medico-icon") // Medic pode ser nil
                cell.starRating.rating = Double(self.advancedSearchData![indexPath.row].evaluation_result ?? 1)
                cell.setTextToLabel(self.advancedSearchData![indexPath.row].payment_type_detail ?? "Não fornecido", label: cell.labelPaymentMethod, imageName: "parcelamento-icon")
                cell.setTextToLabel(self.advancedSearchData![indexPath.row].treatment_value ?? "Não fornecido", label: cell.labelPaymentValue, imageName: "valor-icon")
                cell.setTextToLabel(self.advancedSearchData![indexPath.row].city!.name ?? "", label: cell.labelLocation, imageName: "map-icon")
                cell.imageViewResultsUser.sd_setImage(with: URL(string: self.advancedSearchData![indexPath.row].image_url!), completed: nil)
                return cell
            }
            else if self.advancedSearchData![indexPath.row].card_type == "content" {
                let cell = tableView.dequeueReusableCell(withIdentifier: "MedicPostsTableViewCell") as! MedicPostsTableViewCell
                
                cell.imageViewDoctorIcon.sd_setImage(with: URL(string: self.advancedSearchData![indexPath.row].medic!.image_url!), completed: nil)
                cell.labelPostTitle.text = self.advancedSearchData![indexPath.row].content!.title!
                cell.labelPostDescription.text = self.advancedSearchData![indexPath.row].content!.summary!
                cell.labelMedicName.text = self.advancedSearchData![indexPath.row].medic!.name_with_title!
                cell.labelPostDate.text = self.advancedSearchData![indexPath.row].created_at_formated ?? ""
                cell.imageViewDoctorInfo.sd_setImage(with: URL(string: self.advancedSearchData![indexPath.row].content!.cover_image_url!), completed: nil)
                
                return cell
            }
            else {
                return UITableViewCell()
            }
        }
        else if !self.isDataLoading && self.advancedSearchData!.isEmpty {
            let cell = tableView.dequeueReusableCell(withIdentifier: "EmptyTableViewCell") as! EmptyTableViewCell
            cell.labelMessage.text = "Nenhum dado encontrado"
            
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "EmptyTableViewCell") as! EmptyTableViewCell
            cell.backgroundColor = .clear
            cell.labelMessage.text = "Pesquisando..."
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.advancedSearchData![indexPath.row].card_type == "medic" {
            let rootViewController = self.window!.rootViewController!
            let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "AdvancedSearchMedicInformation") as! AdvancedSearchMedicInformation
            viewController.advancedSearchMedicData = self.advancedSearchData![indexPath.row]
            rootViewController.present(viewController, animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        let verticalPadding: CGFloat = 8
        let maskLayer = CALayer()
        
        maskLayer.cornerRadius = 10
        maskLayer.backgroundColor = UIColor.black.cgColor
        maskLayer.frame = CGRect(x: cell.bounds.origin.x, y: cell.bounds.origin.y, width: cell.bounds.width, height: cell.bounds.height).insetBy(dx: 0, dy: verticalPadding / 2)
        cell.layer.mask = maskLayer
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        
        if maximumOffset - currentOffset <= 10.0 {
            self.requestAdvancedSearch(self.pageNumber, textData: self.searchText, completion: { [weak self]
                data in
                
                if let search = data {
                    self?.advancedSearchData?.append(contentsOf: search)
                    self?.pageNumber += self!.pageNumber
                    
                    DispatchQueue.main.async {
                        UIView.transition(with: (self?.tableViewAdvancedSearch)!, duration: 1.2, options: .transitionCrossDissolve,
                            animations: {
                                self?.tableViewAdvancedSearch.reloadData()
                        }, completion: nil)
                    }
                }
            })
        }
    }
}

extension AESCareAdvancedSearch {
    @objc func buttonCloseViewPressed(_ sender: UIButton) {
        UIView.animate(withDuration: 1.0, animations: {
            self.alpha = 0
        }, completion: {
            finished in
            self.removeFromSuperview()
        })
    }
}
