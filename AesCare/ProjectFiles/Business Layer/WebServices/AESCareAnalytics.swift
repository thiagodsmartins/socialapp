//
//  AESCareAnalytics.swift
//  AesCare
//
//  Created by Thiago on 18/01/21.
//

import Foundation
import Firebase

class AESCareAnalytics {
    private init() {}
    
    enum EventType: String {
        case Signup
        case AppOpen
        case AppClose
        case Login
        case TutorialComplete
        case SuccessOperation
    }
    
    static func initAnalytics() {
        FirebaseApp.configure()
    }
            
    static func event(logName: String, logId: String) {
        Analytics.logEvent(logName, parameters: [
            AnalyticsParameterItemID: logId,
            AnalyticsParameterItemName: "",
            AnalyticsParameterContentType: ""
        ])
    }

    static func event(logName: String, logId: String, logItemName: String) {
        Analytics.logEvent(logName, parameters: [
            AnalyticsParameterItemID: logId,
            AnalyticsParameterItemName: logItemName,
            AnalyticsParameterContentType: ""
        ])
    }
    
    static func event(logName: String, logId: String, logItemName: String, logContentType: String) {
        Analytics.logEvent(logName, parameters: [
            AnalyticsParameterItemID: logId,
            AnalyticsParameterItemName: logItemName,
            AnalyticsParameterContentType: logContentType
        ])
    }
    
    static func event(_ logType: EventType) {
        switch logType {
            case EventType.AppOpen:
                Analytics.logEvent(AnalyticsEventAppOpen, parameters: [
                    AnalyticsParameterItemID: "ID: 001",
                    AnalyticsParameterItemName: "AppOpen",
                    AnalyticsParameterContentType: "Application Open"
                ])
            case EventType.AppClose:
                Analytics.logEvent(AnalyticsEventAppOpen, parameters: [
                    AnalyticsParameterItemID: "ID: 002",
                    AnalyticsParameterItemName: "AppOpen",
                    AnalyticsParameterContentType: "Application Open"
                ])
            case EventType.Login:
                Analytics.logEvent(AnalyticsEventAppOpen, parameters: [
                    AnalyticsParameterItemID: "ID: 003",
                    AnalyticsParameterItemName: "AppLogin",
                    AnalyticsParameterContentType: "Application Login"
                ])
            case EventType.Signup:
                Analytics.logEvent(AnalyticsEventAppOpen, parameters: [
                    AnalyticsParameterItemID: "ID: 004",
                    AnalyticsParameterItemName: "AppSignup",
                    AnalyticsParameterContentType: "Application Signup"
                ])
            case EventType.SuccessOperation:
                Analytics.logEvent(AnalyticsEventAppOpen, parameters: [
                    AnalyticsParameterItemID: "ID: 000",
                    AnalyticsParameterItemName: "App Success",
                    AnalyticsParameterContentType: "Application Sucess Operation"
                ])
            case EventType.TutorialComplete:
                Analytics.logEvent(AnalyticsEventAppOpen, parameters: [
                    AnalyticsParameterItemID: "ID: 005",
                    AnalyticsParameterItemName: "App Complete",
                    AnalyticsParameterContentType: "Application Tutorial Complete"
                ])
        }
    }
}
