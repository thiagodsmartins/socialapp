//
//  AESCareAdvancedSearch+Requests.swift
//  AesCare
//
//  Created by Thiago on 25/04/21.
//

import Foundation
import Alamofire

extension AESCareAdvancedSearch {
    func requestAdvancedSearch(_ page: Int = 1, textData: String, completion: @escaping ([AdvancedSearch]?) -> Void) {
        let formattedString = textData.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = "\(BaseUrl)/assistant/mobile/question?page=\(page)&q=\(formattedString!)"
        
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default).validate(statusCode: 200 ..< 299).responseJSON { AFdata in
            switch AFdata.result {
            case let .success(value):
                print(value)
            case let .failure(error):
                print(error)
            }
                do {
                    guard let jsonObject = try JSONSerialization.jsonObject(with: AFdata.data!) as? [String: Any] else {
                        print("Error: Cannot convert data to JSON object")
                        return
                    }
                    guard let prettyJsonData = try? JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted) else {
                        print("Error: Cannot convert JSON object to Pretty JSON data")
                        completion(nil)
                        return
                    }
                    guard String(data: prettyJsonData, encoding: .utf8) != nil else {
                        print("Error: Could print JSON in String")
                        completion(nil)
                        return
                    }
                    
                    let data = try! JSONDecoder().decode(AdvancedSearchResponse.self, from: AFdata.data!)
                    
                    print(data)
                    
                    if data.answers!.isEmpty {
                        completion(nil)
                    }
                    else {
                        completion(data.answers)
                    }
                } catch {
                    print("Error: Trying to convert JSON data to string")
                    completion(nil)
                    return
                }
            }
    }
}
