//
//  AESCareAuth.swift
//  AesCare
//
//  Created by Thiago on 28/02/21.
//

import Foundation
import GoogleSignIn
import FBSDKCoreKit
import FBSDKLoginKit
import AuthenticationServices
import Alamofire

// MARK: - Delegate SignIn
protocol AESCareAuthSignInDelegate: class {
    func didCompleteGmailLogin(didUserData for: GIDGoogleUser)
    func didCompleteFacebookLogin(didUserToken for: AccessToken)
    func didCompleteAppleLogin(didUserData for: [String : String?])
}

// MARK: - Delegate Facebook
fileprivate protocol FacebookSignInDelegate: class {
    func signFacebook(_ signIn: AccessToken!, didSignInFor user: NSDictionary?)
}

fileprivate protocol AppleSignInDelegate: class {
    func signApple(didSignInFor user: [String : String?])
}

// MARK: - Auth ID's
struct AESCareAuthID {
    static let googleID = "1040862435294-4io0of8fu3pv1thchlhneqpeh9191gge.apps.googleusercontent.com"
    static let facebookID = ""
    static let appleID = ""
    
    private init() { }
}

// MARK: SignIn Class
class AESCareAuth: NSObject {
    var googleUserData: GIDGoogleUser?
    var token: AccessToken?
    weak var delegate: AESCareAuthSignInDelegate!
    
    /// Initialize object for Gmail sign in authentication
    private override init() {
        super.init()
        
        self.googeAuthInitialization()
    }
    
    /// Initialize object for Gmail and Facebook sign in authentication
    init(_ application: UIApplication?, _ launchOptions: [UIApplication.LaunchOptionsKey: Any]?) {
        super.init()
        
        self.googeAuthInitialization()
        self.facebookAuthInitilization(application!, launchOptions)
        self.appleAuthInitialization()
    }
    
    /// Show gmail login menu to select account to login with it
    static func signInWithGmail(_ viewControllerToPresentSignInMenu: UIViewController) {
        GIDSignIn.sharedInstance()?.presentingViewController = viewControllerToPresentSignInMenu
        GIDSignIn.sharedInstance()?.signIn()
    }
    
    /// Show facebook login menu to select account to login with it
    static func signInWithFacebook(_ viewControllerToPresentSignInMenu: UIViewController) {
        Facebook.sharedInstance.presentViewController = viewControllerToPresentSignInMenu
        Facebook.sharedInstance.signInFacebook()
    }
    
    /// Show appleid login menu
    static func signInWithApple(_ viewControllerToPresentSignInMenu: UIViewController) {
        Apple.sharedInstance.signInWithApple(viewControllerToPresentSignInMenu)
    }
    
    /// Log out user from gmail account
    static func signOutFromGmail() {
        GIDSignIn.sharedInstance()?.signOut()
    }
    
    /// Log out user from facebook account
    static func signOutFromFacebook() {
        LoginManager().logOut()
    }
    
    /// Handle login data from gmail client
    static func gmailLoginHandler(url: URL) -> Bool? {
        return GIDSignIn.sharedInstance()?.handle(url) ?? nil
    }
    
    /// Handle login data from facebook client
    static func facebookLoginHandler(_ application: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        return ApplicationDelegate.shared.application(application, open: url, sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplication.OpenURLOptionsKey.annotation])
    
    }
    
    /// Initialize gmail dependencies
    private func googeAuthInitialization() {
        GIDSignIn.sharedInstance()?.clientID = AESCareAuthID.googleID
        GIDSignIn.sharedInstance()?.delegate = self
        //GIDSignIn.sharedInstance()?.restorePreviousSignIn()
    }
    
    /// Initialize facebook dependencies
    private func facebookAuthInitilization(_ application: UIApplication, _ launchOption: [UIApplication.LaunchOptionsKey: Any]?) {
        Facebook.sharedInstance.delegate = self
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOption)
    }
    
    private func appleAuthInitialization() {
        Apple.sharedInstance.delegate = self
    }
}

// MARK: - SignIn Delegate Implementations
extension AESCareAuth: GIDSignInDelegate, FacebookSignInDelegate, AppleSignInDelegate {
    // Called when user selected an account for signin on the app with gmail
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
                print("User has not sign in or has been logged out")
            }
            else {
                print("GMail login error occurred: \(error.localizedDescription)")
            }
            
            return
        }
        
        print("Sign-in complete")
        
        self.googleUserData = user
        loginUserGmail = self.googleUserData
        
        print(self.googleUserData!.profile.name!)
        
        self.authUserWithGmail(self.googleUserData!, response: {
            isRequestSuccessfully in
            if isRequestSuccessfully {
                print("User data saved!!!")
                self.delegate.didCompleteGmailLogin(didUserData: self.googleUserData!)
            }
            else {
                print("User data not saved!!!")
            }
        })
    }
    
    // Called when user signout from app
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        print("User disconnected from gmail")
    }
    
    // Called when user select account to signin on the app with facebook
    func signFacebook(_ signIn: AccessToken!, didSignInFor user: NSDictionary?) {
        self.authWithFacebook(signIn, response: {
            isRequestSuccessfully in
            if isRequestSuccessfully {
                self.delegate.didCompleteFacebookLogin(didUserToken: AccessToken.current!)
            }
        })
    }
    
    func signApple(didSignInFor user: [String : String?]) {
        self.delegate.didCompleteAppleLogin(didUserData: user)
    }
}

// MARK: - SignIn User Authentication Functions
extension AESCareAuth {
    /// Create user account with Gmail data
    private func authUserWithGmail(_ data: GIDGoogleUser, response: @escaping (Bool) -> Void) {
        let parameters: Parameters = ["userId": data.userID!,
                                      "email": data.profile.email!] as Parameters
        let url = "\(BaseUrl)/login/google/mobile"
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default).validate(statusCode: 200 ..< 299).responseJSON { AFdata in

            switch AFdata.result {
            case let .success(value):
                print(value)
            case let .failure(error):
                print(error)
            }
                do {
                    guard let jsonObject = try JSONSerialization.jsonObject(with: AFdata.data!) as? [String: Any] else {
                        print("Error: Cannot convert data to JSON object")
                        response(false)
                        return
                    }
                    guard let prettyJsonData = try? JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted) else {
                        print("Error: Cannot convert JSON object to Pretty JSON data")
                        response(false)
                        return
                    }
                    guard let _ = String(data: prettyJsonData, encoding: .utf8) else {
                        print("Error: Could print JSON in String")
                        response(false)
                        return
                    }

                    usrerModelOBJ = try! JSONDecoder().decode(ModelUser.self, from: prettyJsonData)

                    response(true)
                } catch {
                    print("Error: Trying to convert JSON data to string")
                    response(false)
                    return
                }
            }
    }
    
    /// Create user account with Facebook data
    private func authWithFacebook(_ token: AccessToken, response: @escaping (Bool) -> Void) {
        let header: HTTPHeaders = ["Cookie": ""]
        let url = "\(BaseUrl)/login/facebook"
        
        var dataToken = [String:AnyObject]()
        var parameters = [String : AnyObject]()
        
        dataToken["userID"] = token.userID as AnyObject
        dataToken["accessToken"] = token.tokenString as AnyObject
        dataToken["graphDomain"] = "facebook" as AnyObject
        dataToken["data_access_expiration_time"] = token.dataAccessExpirationDate.timeIntervalSince1970 as AnyObject
        dataToken["signedRequest"] = "" as AnyObject
        dataToken["expiresIn"] = 0 as AnyObject
        
        parameters["authResponse"] = dataToken as AnyObject
        parameters["status"] = "connected" as AnyObject
        
        self.facebookGraphData(token, response: {
            data in
            
            if data != nil {
                Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).validate(statusCode: 200 ..< 599).responseJSON { AFdata in
                    
                    switch AFdata.result {
                    case let .success(value):
                        print(value)
                    case let .failure(error):
                        print(error)
                    }
                        do {
                            guard let jsonObject = try JSONSerialization.jsonObject(with: AFdata.data!) as? [String: Any] else {
                                print("Error: Cannot convert data to JSON object")
                                response(false)
                                return
                            }
                            guard let prettyJsonData = try? JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted) else {
                                print("Error: Cannot convert JSON object to Pretty JSON data")
                                response(false)
                                return
                            }
                            guard let _ = String(data: prettyJsonData, encoding: .utf8) else {
                                print("Error: Could print JSON in String")
                                response(false)
                                return
                            }

                            usrerModelOBJ = try! JSONDecoder().decode(ModelUser.self, from: prettyJsonData)
                            
                            print(usrerModelOBJ.session_key!)
                            
                            print(prettyJsonData)
                            response(true)
                        } catch {
                            print("Error: Trying to convert JSON data to string")
                            response(false)
                            return
                        }
                    }
            }
            else {
                response(false)
            }
        })
    }
    
    /// Acquire facebook user information to send to the server
    private func facebookGraphData(_ token: AccessToken, response: @escaping (NSDictionary?) -> Void) {
        GraphRequest(graphPath: "me", parameters: ["fields": "first_name, last_name, email"], tokenString: token.tokenString, version: nil, httpMethod: .get).start(completionHandler: {
            (connection, result, error) in
            
            if let err = error {
                print("Facebook graph request error: \(err)")
                response(nil)
            }
            else {
                guard let json = result as? NSDictionary else { return }
                print("Facebook graph request successful!")
                response(json)
            }
        })
    }
}

// MARK: - Facebook Class
fileprivate class Facebook {
    static let sharedInstance: Facebook = {
        let instance = Facebook()
        
        return instance
    }()
    
    /// Delegate for showing the result of signin with facebook
    weak var delegate: FacebookSignInDelegate!
    
    /// View controller responsible for showing facebook menu on Safari
    var presentViewController: UIViewController!
    
    private init() { }
    
    /// Show facebook menu and acquire user data for manipulation
    func signInFacebook() {
        LoginManager().logIn(permissions: [.publicProfile, .email], viewController: presentViewController, completion: {
            result in
            
            switch result {
            case .success(let granted, let declined, let token):
                print("Granted Permissions: \(granted)")
                print("Declined Permissions: \(declined)")
                print("Token: \(token!)")
                self.facebookGraphData(token!, response: {
                    facebookUserData in
                    if let data = facebookUserData {
                        self.delegate.signFacebook(token, didSignInFor: data)
                    }
                })
            case .cancelled:
                print("Cancelled")
            case .failed(let error):
                print("Error: \(error.localizedDescription)")
            }
        })
    }
    
    /// Access specific user data for user identification
    private func facebookGraphData(_ token: AccessToken, response: @escaping (NSDictionary?) -> Void) {
        GraphRequest(graphPath: "me", parameters: ["fields": "first_name, last_name, email"], tokenString: token.tokenString, version: nil, httpMethod: .get).start(completionHandler: {
            (connection, result, error) in
            
            if let err = error {
                print("Facebook graph request error: \(err)")
                response(nil)
            }
            else {
                guard let json = result as? NSDictionary else { return }
                print("Facebook graph request successful!")
                response(json)
            }
        })
    }
}

// MARK: - Apple Class
fileprivate class Apple: NSObject {
    static let sharedInstance: Apple = {
       let instance = Apple()
        
        return instance
    }()
    
    var appleViewControllerToShow: UIViewController!
    weak var delegate: AppleSignInDelegate!
    
    private override init() {
        super.init()
    }
    
    /// Show apple id login menu
    func signInWithApple(_ viewControllerToPresentSignInMenu: UIViewController) {
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
        self.appleViewControllerToShow = viewControllerToPresentSignInMenu
        
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.presentationContextProvider = viewControllerToPresentSignInMenu as? ASAuthorizationControllerPresentationContextProviding
        authorizationController.performRequests()
    }
}

// MARK: - Apple SignIn Delegate
extension Apple: ASAuthorizationControllerDelegate,
                 ASAuthorizationControllerPresentationContextProviding  {
    
    // Get the apple id data from user. The data is only returned on the first login
    // subsequent logins will always return nil
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        var appleUserData = [String : String?]()
        
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            appleUserData["id"] = appleIDCredential.user
            appleUserData["name"] = appleIDCredential.fullName?.givenName ?? ""
            appleUserData["surname"] = appleIDCredential.fullName?.familyName ?? ""
            appleUserData["email"] = appleIDCredential.email ?? ""
        } else if let passwordCredential = authorization.credential as? ASPasswordCredential {
            let appleUsername = passwordCredential.user
            let applePassword = passwordCredential.password
            print(appleUsername)
            print(applePassword)
        }
        
        self.delegate.signApple(didSignInFor: appleUserData)
    }
    
    // Handle apple id login error
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        print("Error: \(error.localizedDescription)")
    }
    
    // The view that will show apple login menu
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return appleViewControllerToShow.view.window!
    }
}
