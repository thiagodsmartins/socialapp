//
//  AdvancedSearchDataModel.swift
//  AesCare
//
//  Created by Thiago on 25/04/21.
//

import Foundation

struct AdvancedSearchResponse: Codable {
    let answers: [AdvancedSearch]?
}

// MARK: - Advanced Search Model
struct AdvancedSearch: Codable {
    let name_unaccent: String?
    let next_process_at: String?
    let user_id: Int?
    let processed_instagram_at: String?
    let old_id: Int?
    let next_process_instagram_at: String?
    let crm: String?
    let contact_whatsapp_phone: String?
    let gender: String?
    let city_id: Int?
    let email: String?
    let state_id: Int?
    let phone: String?
    let postal_code: String?
    let address: String?
    let created_at: String?
    let address_number: Int?
    let updated_at: String?
    let facebook_page: String?
    let sbcp_json: AdvancedSearchSbcp?
    let cover_media: AdvancedSearchCoverMedia?
    let city: AdvancedSearchCity?
    let user: AdvancedSearchUserMedic?
    let evaluation_average: Double?
    let evaluation_average_formatted: String?
    let status: String?
    let specialty_id: Int?
    let follow_count: Int?
    let place_id: String?
    let whatsapp_phone: String?
    let latitude: String?
    let posts_count: Int?
    let evaluation_facebook: String?
    let booking_price: Double?
    let longitude: String?
    let medic_posts_count: Int?
    let evaluation_facebook_update_at: String?
    let is_hidden_price: Bool?
    let last_story_at: String?
    let medical_insurance: String?
    let medic_id: Int?
    let posts_instagram_count: Int?
    let evaluation_google: String?
    let key: String?
    let last_post_created_at: String?
    let evaluation_google_update_at: String?
    let count_profile_viewed_month: Int?
    let name: String?
    let name_with_title: String?
    let processed_at: String?
    let instagram_url: String?
    let count_reviews_viewed_month: Int?
    let card_type: String?

    // Advanced Search Results Model
    let title: String?
    let testimonial_id: Int?
    let result_id: Int?
    let liked: Bool?
    let likes_count: Int?
    let views_count: Int?
    let published: Bool?
    let image_id: Int?
    let worth_it: String?
    let image_key: String?
    let payment_type: String?
    let auto_title: String?
    let description: String?
    let keywords: String?
    let medias_count: Int?
    let treatment_date_formated: String?
    let cover_media_key: String?
    let followed: Bool?
    let treatment_value: String?
    let evaluation_result: Int?
    let comments_count: Int?
    let created_at_formated: String?
    let medic_name: String?
    let payment_type_detail: String?
    let image_url: String?
    let medic: AdvancedSearchMedicData?
    let content: AdvancedSearchMedicPostsContent?
}

// MARK: - Advanced Search Medic Card Model
struct AdvancedSearchSbcp: Codable {
    let afiliacao1: String?
    let afiliacao2: String?
    let afiliacao3: String?
    let afiliacao4: String?
    let afiliacao5: String?
    let crm: String?
    let categoria: String?
    let cidade: String?
    let cidade2: String?
    let email: String?
    let endereco: String?
    let endereco2: String?
    let erro: String?
    let especializacoes: String?
    let formacao: String?
    let homepage: String?
    let imagem: String?
    let nome: String?
    let perfilmedico: String?
    let telefone1: String?
    let telefone2: String?
    let uf: String?
    let uf2: String?
    let video: String?
    
    private enum CodingKeys: String, CodingKey {
        case afiliacao1 = "Afiliacao1"
        case afiliacao2 = "Afiliacao2"
        case afiliacao3 = "Afiliacao3 "
        case afiliacao4 = "Afiliacao4"
        case afiliacao5 = "Afiliacao5"
        case crm = "Crm"
        case categoria = "Categoria"
        case cidade = "Cidade"
        case cidade2 = "Cidade2"
        case email = "Email"
        case endereco = "Endereco"
        case endereco2 = "Endereco2"
        case erro = "Erro"
        case especializacoes = "Especializacoes"
        case formacao = "Formacao"
        case homepage = "HomePage"
        case imagem = "Imagem"
        case nome = "Nome"
        case perfilmedico = "PerfilMedico"
        case telefone1 = "Telefone1"
        case telefone2 = "Telefone2"
        case uf = "UF"
        case uf2 = "UF2"
        case video = "Video"
    }
}

struct AdvancedSearchCoverMedia: Codable {
    let image_url: String?
    let thumbnail_url: String?
    let cover_url: String?
}

struct AdvancedSearchCity: Codable {
    let name_unaccent: String?
    let updated_at: String?
    let country_abbr: String?
    let google_place_id: String?
    let processed_at: String?
    let timezone: String?
    let ddd: Int?
    let slug: String?
    let city_id: Int?
    let slug_original: String?
    let name: String?
    let preposition: String?
    let name_pt: String?
    let results_count: Int?
    let state_id: Int?
    let name_pt_unaccent: String?
    let status: String?
    let state_abbr: String?
    let name_en: String?
    let state: AdvancedSearchState?
}

struct AdvancedSearchState: Codable {
    let name_pt: String?
    let state_id: Int?
    let longitude: String?
    let country_abbr: String?
    let name_en: String?
    let name: String?
    let latitude: String?
    let abbr: String
}

struct AdvancedSearchUserMedic: Codable {
    let user_follower_count: Int?
}

// MARK: - Advanced Search Result Card Medic Model
struct AdvancedSearchMedicData: Codable {
    let medic_id: Int?
    let user_id: Int?
    let crm: String?
    let gender: String?
    let count_waiting_list: Int?
    let email: String?
    let phone: String?
    let state_abbr: String?
    let city_name: String?
    let name: String?
    let key: String?
    let medic_key: String?
    let city_id: Int?
    let name_with_title: String?
    let image_url: String?
    let cover_media: AdvancedSearchCoverMedia?
}

struct AdvancedSearchMedicMediaData: Codable {
    let key: String?
    let created_at: String?
    let created_at_formatted: String?
    let video_url: String?
    let image_url: String?
    let thumbnail_url: String?
    let cover_url: String?
}

struct AdvancedSearchMedicPostsContent: Codable {
    let content_id: Int?
    let key: String?
    let url_youtube: String?
    let youtube_video_id: String?
    let cover_image_url: String?
    let title: String?
    let summary: String?
}

// MARK: - Advanced Search Opinions
struct AdvancedSearchMedicAvaluation: Codable {
    let opinions: [AdvancedSearchMedicOpinions]?
}

struct AdvancedSearchMedicOpinions: Codable {
    let opinion: String?
    let related_id: Int?
    let user_id: Int?
    let status: String?
    let created_at: String?
    let evaluation: Int?
    let related_class: String?
    let evaluation_id: Int?
    let testimonial_id: Int?
    let comments_count: Int?
    let user: AdvancedSearchMedicUserInfo?
    let create: String?
    let surgery_date: String?
}

struct AdvancedSearchMedicUserInfo: Codable {
    let last_access_at: String?
    let status: String?
    let user_id: Int?
    let phone: String?
    let role: String?
    let has_photo: Bool?
    let name: String?
    let has_active_app: Bool?
    let email: String?
    let username: String?
    let gender: String?
    let created_in_ip: String?
    let updated_at: String?
    let has_confirmed_email: Bool?
    let review_count: Int?
    let user_follower_count: Int?
    let current_journey_id: Int?
    let processed_at: String?
    let medic_following_count: Int?
    let url_image: String?
}


// MARK: - Advanced Search Medic Informations
struct AdvancedSearchMedicInfo: Codable {
    let medic: AdvancedSearchMedicInfoData?
}

struct AdvancedSearchMedicInfoData: Codable {
    let results_count: Int?
    let evaluations_with_contents_count: Int?
    let posts_count: Int?
    let medic_posts_count: Int?
    let evaluation_average: Double?
    let follow_count: Int?
}
