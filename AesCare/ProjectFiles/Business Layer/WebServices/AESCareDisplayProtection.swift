//
//  AESCareDisplay.swift
//  AesCare
//
//  Created by Thiago on 01/02/21.
//

import Foundation
import UIKit

class AESCareDisplayProtection {
    var screenshotCounter: Int = 60
    var isScreenshotCounterFinish: Bool = true
    var persistentData: [Any]?
    var view: UIView?
    var appDelegate = UIApplication.shared.delegate as! AppDelegate
    var blurEffect: UIBlurEffect?
    var blurView: UIVisualEffectView?
    var displayAction: DisplayAction?
    
    enum DisplayAction: Int {
        case recording = 0
        case screenshot = 1
        case none = 2
    }
    
    init(currentview: UIView) {
        self.view = currentview
        self.persistentData = UserDefaults.standard.array(forKey: "screenshot")
        self.displayAction = DisplayAction.none
        
        if let isCounterFinish = persistentData?[1] as? Bool, isCounterFinish == false {
            if let counter = persistentData?[0] as? Int {
                self.screenshotCounter = counter
                
                DispatchQueue.main.async {
                    self.displayAction = DisplayAction(rawValue: self.persistentData![2] as! Int)!
                    self.blurDisplay(self.view!, display: self.displayAction!)
                }
            }
        }
    }
    
    func timeProportionCalculation(_ number: Int) -> Int {
        let calc = ((number * 4) / 2)
        
        return calc
    }
    
    private func blurDisplay(_ view: UIView, display: DisplayAction) {
        let lblMsg = UILabel(frame: CGRect(x: UIScreen.main.bounds.width / 2, y: UIScreen.main.bounds.height / 2, width: 200, height: 200))
        
        self.isScreenshotCounterFinish = false
        
        switch display {
        case .recording:
            lblMsg.text = self.screenshotCounter < 60 ? "VOCÊ ESTA BLOQUEADO DE UTILIZAR O APP POR \(self.screenshotCounter) SEGUNDOS. NĀO FAÇA GRAVAÇÕES DA DA TELA!" : "VOCÊ ESTA BLOQUEADO DE UTILIZAR O APP POR \(self.screenshotCounter / 60) MINUTOS. NĀO FAÇA GRAVAÇÕES DA TELA!"
        case .screenshot:
            lblMsg.text = self.screenshotCounter < 60 ? "VOCÊ ESTA BLOQUEADO DE UTILIZAR O APP POR \(self.screenshotCounter) SEGUNDOS. NĀO TIRE SCREENSHOTS DA TELA!" : "VOCÊ ESTA BLOQUEADO DE UTILIZAR O APP POR \(self.screenshotCounter / 60) MINUTOS. NĀO TIRE SCREENSHOTS DA TELA!"
        case .none:
            print("No message to show. No ss or rec")
        }
        
        lblMsg.font = lblMsg.font.withSize(20)
        lblMsg.textColor = UIColor.white
        lblMsg.textAlignment = .center
        lblMsg.numberOfLines = 0
        lblMsg.center = CGPoint(x: UIScreen.main.bounds.width / 2, y: UIScreen.main.bounds.height / 2)
        
        blurEffect = UIBlurEffect(style: .dark)
        blurView = UIVisualEffectView(effect: blurEffect)
        blurView?.frame = (self.appDelegate.window?.frame)!
        blurView?.tag = 101
        blurView?.clipsToBounds = true;
        blurView?.layer.borderColor = UIColor.black.cgColor
        blurView?.layer.borderWidth = 1.0;
        blurView?.layer.cornerRadius = 6.0;
        blurView?.contentView.addSubview(lblMsg)
        blurView?.contentView.bringSubviewToFront(lblMsg)
        
        self.appDelegate.window?.addSubview(blurView!)
    
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(self.screenshotCounter), execute: {
            self.screenshotCounter = self.timeProportionCalculation(self.screenshotCounter)
            self.isScreenshotCounterFinish = true
            self.appDelegate.window?.viewWithTag(101)?.removeFromSuperview()
        })
    }
    
    func persistCurrentDisplayState() {
        UserDefaults.standard.set([self.screenshotCounter, self.isScreenshotCounterFinish, self.displayAction!.rawValue], forKey: "screenshot")
    }
    
    func startDisplayProtection() {
        NotificationCenter.default.addObserver(forName: UIApplication.userDidTakeScreenshotNotification, object: nil, queue: .main, using: { notification in
            self.displayAction = .screenshot
            self.blurDisplay(self.view!, display: self.displayAction!)
        })
        
        NotificationCenter.default.addObserver(forName: UIApplication.willTerminateNotification, object: nil, queue: .main, using: { notification in
            self.persistCurrentDisplayState()
        })
        
        NotificationCenter.default.addObserver(forName: UIScreen.capturedDidChangeNotification, object: nil, queue: .main, using: { notification in
            self.displayAction = .recording
            self.blurDisplay(self.view!, display: self.displayAction!)
        })
    }
    
    
}
