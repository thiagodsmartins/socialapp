//
//  AESCarePopupDialog.swift
//  AesCare
//
//  Created by Thiago on 03/05/21.
//

import Foundation
import PopupDialog

class AESCarePopupDialog {
    private init() {}
    
    static func dialog(title: String, message: String, image: UIImage?, viewController: UIViewController, _ event: @escaping () -> Void) {
        let popupDialog = PopupDialog(title: title, message: message, image: image)
        
//        let buttonClose = DefaultButton(title: "Fechar", height: 40, dismissOnTap: false, action: {
//
//        })
        let buttonClose = DefaultButton(title: "Fechar", action: {
            event()
        })
        
        popupDialog.transitionStyle = .fadeIn
        popupDialog.addButton(buttonClose)
            
        viewController.present(popupDialog, animated: true, completion: nil)
    }
}
