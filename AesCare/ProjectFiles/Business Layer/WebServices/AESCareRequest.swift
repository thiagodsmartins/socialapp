//
//  AESCareRequest.swift
//  AesCare
//
//  Created by Thiago on 07/03/21.
//

import Foundation
import UIKit

@propertyWrapper
struct AESCareURL {
    var url: String = ""
    
    init(_ urlPath: String = "") {
        self.url = self.appBuildConfiguration("AesCareAppBuild")!
        self.url += urlPath
    }
    
    var wrappedValue: String {
        get {
            return self.url
        }
        
        set {
            self.url += newValue
        }
    }
    
    private func appBuildConfiguration(_ key: String) -> String? {
        let buildType = (Bundle.main.infoDictionary?[key] as? String)?.replacingOccurrences(of: "\\", with: "")
        
        switch buildType {
        case "Dev":
            return "https://wsbeta.aescare.com"
        case "Prod":
            return "https://ws.aescare.com"
        default:
            return "https://wsbeta.aescare.com"
        }
     }
}

class AESCareRequest: NSObject {
    @AESCareURL var url: String
    private var session: URLSession?
    private var configuration: URLSessionConfiguration?
    private var cache: URLCache?
    
    /// Configure session request. Proxy can be set up and the data privacy
    init(_ proxy: [AnyHashable:Any]? = nil, sessionPrivacy: SessionType = .publicSession){
        super.init()
        
        self.sessionConfiguration(proxy, sessionPrivacy: sessionPrivacy)
        self.sessionSetup()
    }
    
    /// Make request to the server
    func request(_ urlPath: String, requestType: RequestType, header: [String:String]?, body: [String:Any]?, completion: @escaping (Data?, Error?) -> Void) {
        self.url = urlPath
        var request = URLRequest(url: URL(string: self.url)!)
        
        if let header = header {
            for (key, value) in header {
                request.setValue(value, forHTTPHeaderField: key)
            }
        }
        
        request.httpMethod = requestType.requestMethod()
        request.httpBody = nil
        
        self.session?.dataTask(with: request, completionHandler: {
            (data, reponse, error) in
            
            if let error = error {
                completion(nil, error)
            }
            else if let data = data {
                completion(data, nil)
            }
            else {
                completion(nil, error)
            }
        }).resume()
    }
    
    private func sessionConfiguration(_ proxy: [AnyHashable:Any]?, sessionPrivacy: SessionType) {
        switch sessionPrivacy {
        case .privateSession:
            self.configuration = URLSessionConfiguration.default
        case .publicSession:
            self.configuration = URLSessionConfiguration.ephemeral
        }
        
        self.configuration?.allowsCellularAccess = true
        self.configuration?.allowsConstrainedNetworkAccess = true
        self.configuration?.allowsExpensiveNetworkAccess = true
        self.configuration?.connectionProxyDictionary = proxy
    }
    
    private func sessionSetup() {
        self.session = URLSession(configuration: self.configuration!, delegate: self, delegateQueue: nil)
    }
}

extension AESCareRequest {
    enum SessionType {
        /// Keeps cache data, credentials or others sessions in memory. It's never written to disk
        case privateSession
        /// Keeps all session data on disk. Public is the default SessionType
        case publicSession
    }
    
    enum RequestType {
        case get
        case post
        case put
        case delete
        
        func requestMethod() -> String {
            switch self {
            case .get:
                return "GET"
            case .post:
                return "POST"
            case .put:
                return "PUT"
            case .delete:
                return "DELETE"
            }
        }
    }
}

extension AESCareRequest: URLSessionDelegate, URLSessionTaskDelegate {
    func urlSession(_ session: URLSession, didBecomeInvalidWithError error: Error?) {
        
    }
    
    func urlSessionDidFinishEvents(forBackgroundURLSession session: URLSession) {
        
    }
    
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, willCacheResponse proposedResponse: CachedURLResponse, completionHandler: @escaping (CachedURLResponse?) -> Void) {
        if proposedResponse.response.url?.scheme == "https" {
            let updatedResponse = CachedURLResponse(response: proposedResponse.response,
                                                    data: proposedResponse.data,
                                                    userInfo: proposedResponse.userInfo,
                                                    storagePolicy: .allowedInMemoryOnly)
            completionHandler(updatedResponse)
        } else {
            completionHandler(proposedResponse)
        }
    }
    
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        if challenge.previousFailureCount > 0 {
            completionHandler(Foundation.URLSession.AuthChallengeDisposition.cancelAuthenticationChallenge, nil)
        } else if let serverTrust = challenge.protectionSpace.serverTrust {
            completionHandler(Foundation.URLSession.AuthChallengeDisposition.useCredential, URLCredential(trust: serverTrust))
        } else {
            print("unknown state. error: \(String(describing: challenge.error))")
            // do something w/ completionHandler here
        }
    }
    
    func urlSession(session: URLSession, dataTask: URLSessionDataTask, didReceiveResponse response: URLResponse, completionHandler: (URLSession.ResponseDisposition) -> Void) {
        completionHandler(.allow)
    }
    
}
