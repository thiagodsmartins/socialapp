//
//  AESCareConnectivity.swift
//  AesCare
//
//  Created by Thiago on 20/01/21.
//

import Connectivity
import UIKit

class AESCareConnectivity: UIViewController {
    var connectivity: Connectivity?
    var lblConnectionMsg: UILabel?
    var appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    init() {
        super.init(nibName: nil, bundle: nil)
        
        self.lblConnectionMsg = UILabel()
        let height = CGFloat(50)

        self.lblConnectionMsg!.text = ""
        self.lblConnectionMsg!.backgroundColor = .red
        self.lblConnectionMsg!.textAlignment = .center
        self.lblConnectionMsg!.isHidden = true
        self.lblConnectionMsg!.translatesAutoresizingMaskIntoConstraints = false

        self.appDelegate.window?.rootViewController?.view.addSubview(self.lblConnectionMsg!)
        
        NSLayoutConstraint.activate([
            lblConnectionMsg!.leadingAnchor.constraint(equalTo: (self.appDelegate.window?.rootViewController?.view.safeAreaLayoutGuide.leadingAnchor)!),
            lblConnectionMsg!.trailingAnchor.constraint(equalTo: (self.appDelegate.window?.rootViewController?.view.safeAreaLayoutGuide.trailingAnchor)!),
            lblConnectionMsg!.topAnchor.constraint(equalTo: (self.appDelegate.window?.rootViewController?.view.safeAreaLayoutGuide.bottomAnchor)!,constant: -height),
            lblConnectionMsg!.bottomAnchor.constraint(equalTo: (self.appDelegate.window?.rootViewController?.view.safeAreaLayoutGuide.bottomAnchor)!),
        ])
        
        self.connectivity = Connectivity()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func observeConnection() {
        let connectivityChanged: (Connectivity) -> Void = { [weak self] connectivity in
            self?.updateConnectionStatus(connectivity.status)
        }
        
        self.connectivity?.whenConnected = connectivityChanged
        self.connectivity?.whenDisconnected = connectivityChanged
        self.connectivity?.startNotifier()
    }
    
    private func updateConnectionStatus(_ status: Connectivity.Status) {
        switch status {
        case .connected:
            self.lblConnectionMsg?.isHidden = true
        case .connectedViaCellular:
            self.lblConnectionMsg?.isHidden = true
        case .connectedViaCellularWithoutInternet:
            self.lblConnectionMsg?.isHidden = false
            self.lblConnectionMsg?.text = "SEM CONEXĀO"
        case .connectedViaWiFi:
            self.lblConnectionMsg?.isHidden = true
        case .connectedViaWiFiWithoutInternet:
            self.lblConnectionMsg?.isHidden = false
            self.lblConnectionMsg?.text = "SEM CONEXĀO"
        case .notConnected:
            self.lblConnectionMsg?.isHidden = false
            self.lblConnectionMsg?.text = "SEM CONEXĀO"
        case .determining:
            print("Not necessary")
        }
    }
    
    private func showToast(message : String, font: UIFont) {
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width / 2 - 75, y: self.view.frame.size.height - 100, width: 150, height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.font = font
        toastLabel.textAlignment = .center;
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds = true
                
        if let window = UIApplication.shared.delegate?.window {
            var controller = window!.rootViewController
            
            if controller is UINavigationController {
                controller = (controller as! UINavigationController).visibleViewController
                controller?.view.addSubview(toastLabel)
                    
                UIView.animate(withDuration: 5.0, delay: 0.1, options: .curveEaseOut, animations: {
                    toastLabel.alpha = 0.0
                }, completion: { (isCompleted) in
                    toastLabel.removeFromSuperview()
                })
            }
        }
    }
}
