//
//  AESCareErrors.swift
//  AesCare
//
//  Created by Thiago on 08/03/21.
//

import Foundation

enum AESCareError {
    case requestError
    case connectionError
    case conversionError
    case appleSignInError
    case facebookSignInError
    case gmailSignInError
    case unexpectedError
}

extension AESCareError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .requestError:
            return NSLocalizedString("The request cannot be completed", comment: "Request Error")
        case .connectionError:
            return NSLocalizedString("There is a problem with network connection", comment: "Network Error")
        case .conversionError:
            return NSLocalizedString("Data cannot be converted", comment: "Data Conversion Error")
        case .appleSignInError:
            return NSLocalizedString("There is a problem with apple sign in", comment: "Apple Error")
        case .facebookSignInError:
            return NSLocalizedString("There is a problem with facebook sign in", comment: "Facebook Error")
        case .gmailSignInError:
            return NSLocalizedString("There is a problem with gmail sign in", comment: "Gmail Error")
        case .unexpectedError:
            return NSLocalizedString("An unexpected error occurred", comment: "Unexpected Error")
        }
    }
}
