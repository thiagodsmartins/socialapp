//
//  AESCareLinks.swift
//  AesCare
//
//  Created by Thiago on 20/01/21.
//

import Foundation

struct AESCareConstants {
    static var aescareDevURL: String {
        return "TBD"
    }
    
    static var aescareProdURL: String {
        return "TBD"
    }
    
    static var facebookAppstoreURL: String {
        return "https://apps.apple.com/app/facebook/id284882215"
    }
}
