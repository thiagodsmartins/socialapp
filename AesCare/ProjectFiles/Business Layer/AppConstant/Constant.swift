

import Foundation
import UIKit
import GoogleSignIn

let App_Title:String! = "AesCare"


let deviceBounds:CGRect! = UIScreen.main.bounds
//MARK : AppDelegate instant
let appDel = UIApplication.shared.delegate as! AppDelegate
var is_medic  = false
let genericDateFormatter = "dd/MM/yyyy"
var usrerModelOBJ : ModelUser!

let privacyPolicyLink = "https://br.aescare.com/politica-de-privacidad"
let termsAndConditionLink = "https://br.aescare.com/termos-de-uso"

var loginUserGmail: GIDGoogleUser!

///Mark: API
let BaseUrl:String = appBuildConfiguration("AesCareAppBuild")! == "Prod" ? "https://ws.aescare.com" : "https://wsbeta.aescare.com"
//let BaseUrl: String  =  "https://wsbeta.aescare.com"
let BaseImageUrl = "https://br.aescare.com/user/avatar/"

func appBuildConfiguration(_ key: String) -> String? {
        return (Bundle.main.infoDictionary?[key] as? String)?
            .replacingOccurrences(of: "\\", with: "")
 }

public enum ROLE_ID:Int {
    
    case residential = 1
    case commercial = 2
    
    func serverPostValue() -> String {
        return "\(self.rawValue)"
    }
    public static func getRole_ID(_ str:String?) -> ROLE_ID? {
        guard let str = str, !(str.isEmpty) else { return nil }
        guard let val = Int(str) else { return nil }
        return ROLE_ID.init(rawValue: val)
    }
}

var selectedRoleType:ROLE_ID =  .residential
//Mark : Api Url 
public enum API:Int {
    case userNativeLoginByEmail
    case userGoogleLogIn
    case userFacebookLogIn
    case registartion
    
    case fetchCityList
    case forgetPassword
    case logout
    case searchByCityName
    case searchByCountryName
    case profileDetail
    case userPost
    case deletePost
    case userPhoto
    case postUser
    case DoctorDetails
    case groupSuggession
    case LastMessages
    case scheduleAnAppointment
    case createPayment
    case validatePayment
    case PerticularChatMessage
    case SendChatMessage
    case JoinGroup
    case fileMessage
    case SendResult
    case SendResultPhoto
    case GetTreatment
    case ResultaosImages
    case Comments
    case commentOfcomment
    func getURL() -> URL? {
        switch self {
            
        case .userNativeLoginByEmail:
            return URL.init(string: (BaseUrl+"/login"))
        case .userGoogleLogIn:
            return URL.init(string: (BaseUrl+"/login/google/mobile"))
        case .userFacebookLogIn:
            return URL.init(string: (BaseUrl+"/login/facebook?"))
        case .registartion:
            return URL.init(string: (BaseUrl+"/user/register"))
        case .fetchCityList:
            return URL.init(string: (BaseUrl+"/location/city_list"))
        case .forgetPassword:
            return URL.init(string: (BaseUrl+"/user/forgot-password"))
        case .logout:
            return URL.init(string: (BaseUrl+"/logout"))
        case .searchByCityName:
            return URL.init(string: (BaseUrl + "/location/city?name="))
        case .searchByCountryName:
            return URL.init(string: (BaseUrl + "/location/country?name="))
        case .profileDetail:
            return URL.init(string: (BaseUrl + "/user/detail"))
        case .userPost:
            return URL.init(string: (BaseUrl + "/posts"))
        case .deletePost:
            return URL.init(string: (BaseUrl + "/post/status"))
        case .userPhoto:
            return URL.init(string: (BaseUrl + "/user/photo"))
        case .DoctorDetails:
            return URL.init(string: (BaseUrl + "detail?medic_id="))
        case .postUser:
            return URL.init(string: (BaseUrl + "/user"))
            
            
        case .scheduleAnAppointment:
            return URL.init(string: (BaseUrl + "/doctor/booking"))
        case .createPayment:
            return URL.init(string: (BaseUrl + "/billing/payment/new"))
        case .validatePayment:
            return URL.init(string: (BaseUrl + "/billing/payment"))
        case .groupSuggession:
            
            return URL.init(string: (BaseUrl + "/chat/groups_suggestion"))
        case .LastMessages:
            return URL.init(string: (BaseUrl + "/messenger/chats"))
        case .PerticularChatMessage:
            //get_chat_message
            return URL.init(string: (BaseUrl + "/get_chat_message"))
        case .SendChatMessage:
            return URL.init(string: (BaseUrl + "/chat/send_message")) //
            
        case .SendResult:
            return URL.init(string: (BaseUrl + "/testimonial"))
        case .SendResultPhoto:
            return URL.init(string: (BaseUrl + "/testimonial/photo"))
        case .GetTreatment:
            return URL.init(string: (BaseUrl + "/treatment?"))
            
        case .JoinGroup:
            return URL.init(string: (BaseUrl + "/chat/join"))
        ///chat/join
        case .fileMessage:
            return URL.init(string: (BaseUrl + "/file"))
        case .ResultaosImages:
            return URL.init(string: (BaseUrl + "/media/result?result_id="))
        case .Comments:
            return URL.init(string: (BaseUrl + "/comment"))
        case .commentOfcomment:
            return URL.init(string: (BaseUrl + "/comment-comment"))
        }
    }
}

public enum StartingPageMessage: String {
    case slideTitle1 = "1"
    case slideTitle2 = "2"
    case slideTitle3 = "3"
    case slideMessage1 = "4"
    case slideMessage2 = "5"
    case slideMessage3 = "6"
    
    func message() -> String {
        switch self {
        case .slideTitle1:
            return "Informação e relacionamento"
        case .slideTitle2:
            return "Resultados reais publicados pelas pacientes"
        case .slideTitle3:
            return "Encontre médicos e agende uma consulta"
        case .slideMessage1:
            return "A AesCare é a maior comunidade que promove o compartilhamento colaborativo de informaçōes reais sobre tratamentos e cirurgias relacionados à autoimage e saúde"
        case .slideMessage2:
            return "Veja resultados reais de tratamentos e cirurgias com fotos, depoimentos, recomendaçōes e avaliaçōes publicadas pelas própias pacientes"
        case .slideMessage3:
            return "Marque uma consulta com o(os) médico(os) que tem interesse com muito mais facilidade e praticidade através do app"
        }
    }
}

extension UITextView: UITextViewDelegate {
    
    override open var bounds: CGRect {
        didSet {
            self.resizePlaceholder()
        }
    }
        
    /// The UITextView placeholder text
    public var placeholder: String? {
        get {
            var placeholderText: String?
            
            if let placeholderLabel = self.viewWithTag(100) as? UILabel {
                placeholderText = placeholderLabel.text
            }
            
            return placeholderText
        }
        set {
            if let placeholderLabel = self.viewWithTag(100) as! UILabel? {
                placeholderLabel.text = newValue
                placeholderLabel.sizeToFit()
            } else {
                self.addPlaceholder(newValue!)
            }
        }
    }
    
    /// When the UITextView did change, show or hide the label based on if the UITextView is empty or not
    ///
    /// - Parameter textView: The UITextView that got updated
    public func textViewDidChange(_ textView: UITextView) {
        if let placeholderLabel = self.viewWithTag(100) as? UILabel {
            placeholderLabel.isHidden = self.text.count > 0//self.text.characters.count > 0
        }
    }
    
    /// Resize the placeholder UILabel to make sure it's in the same position as the UITextView text
    private func resizePlaceholder() {
        if let placeholderLabel = self.viewWithTag(100) as! UILabel? {
            let labelX = self.textContainer.lineFragmentPadding
            let labelY = self.textContainerInset.top - 2
            let labelWidth = self.frame.width - (labelX * 2)
            let labelHeight = placeholderLabel.frame.height + 5
            
            placeholderLabel.frame = CGRect(x: labelX, y: labelY, width: labelWidth, height: labelHeight)
        }
    }
    
    /// Adds a placeholder UILabel to this UITextView
    private func addPlaceholder(_ placeholderText: String) {
        let placeholderLabel = UILabel()
        
        placeholderLabel.text = placeholderText
        placeholderLabel.sizeToFit()
        
        placeholderLabel.font = self.font
        placeholderLabel.textColor = UIColor.lightGray
        placeholderLabel.tag = 100
        
        placeholderLabel.isHidden = self.text.count > 0//self.text.characters.count > 0
        
        self.addSubview(placeholderLabel)
        self.resizePlaceholder()
        self.delegate = self
    }
    
    
    
}


extension UIView {
    
    func setRoundCorner(cornerRadious:CGFloat) {
        
        self.layer.cornerRadius = cornerRadious
        self.layer.masksToBounds = true
        
    }
    
    func setBorder(width:CGFloat, borderColor:UIColor, cornerRadious:CGFloat) {
        
        self.layer.cornerRadius = cornerRadious
        self.layer.borderWidth = width
        self.layer.borderColor = borderColor.cgColor
        self.layer.masksToBounds = true
        
    }
    
    func animateOpacity() {
        
        let pulseAnimation = CABasicAnimation(keyPath: "opacity")
        pulseAnimation.duration = 1
        pulseAnimation.fromValue = 0
        pulseAnimation.toValue = 1
        pulseAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        pulseAnimation.autoreverses = true
        pulseAnimation.repeatCount = Float.greatestFiniteMagnitude
        self.layer.add(pulseAnimation, forKey: "animateOpacity")
        
    }
    
    
}


var shadowView = UIView()
var mulListV = MultipleSelecetionListPicker()
var listV = ListPickerView()
var dtPickerView = DatePickerView()
class MyBasics: NSObject {
    
    
    class func showPopup(Title:String?, Message:String?, InViewC:UIViewController?) {
        
        let popUpAlert = UIAlertController(title: Title!, message: Message!, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
        popUpAlert.addAction(okAction)
        InViewC?.present(popUpAlert, animated: true, completion: nil)
        
    }
    class func isValidPhoneNumberByCountry(numberStr:String) -> Bool {
        
        let PHONE_REGEX = "^\\+(?:[0-9] ?){10,11}[0-9]$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: numberStr)
        return result
    }
    class func validateEmail(enteredEmail:String) -> Bool {
        
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluate(with: enteredEmail)
        
    }
    
    class func isValidPhoneNumber(numberStr:String) -> Bool {
        
        let PHONE_REGEX = "^[0-9]{10,15}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: numberStr)
        return result
    }
    
    class func isPasswordValid(passwordStr : String) -> Bool
    {
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "^(?=.*[A-Za-z])(?=.*[0-9])(?=.*[!@#$&*%^&~ ]).{8,}$")
        return passwordTest.evaluate(with: passwordStr)
    }
    
    
    class func heightForText(text:String!, viewWidth:CGFloat, font:UIFont) -> CGFloat {
        
        let constraintRect = CGSize(width: viewWidth, height: .greatestFiniteMagnitude)
        let boundingBox = text.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        return boundingBox.height + 4
    }
    
    func makeShadowToChildView(shadowView:UIView)
    {
        shadowView.layer.masksToBounds = false
        shadowView.layer.shadowColor = UIColor.lightGray.cgColor
        shadowView.layer.shadowOffset = CGSize(width: CGFloat(1.0), height: CGFloat(1.0))
        shadowView.layer.shadowOpacity = 0.5
        shadowView.layer.shadowRadius = 4
    }
    
    // MARK: Multiple List picker
    
    class func showMultipleListDropDown(Items:Array<String>,selectedIncludedName:[Bool] ,ParentViewC:UIViewController) {
        
        shadowView = UIView(frame: deviceBounds)
        shadowView.backgroundColor = UIColor.black
        shadowView.alpha = 0.0
        let tapGes = UITapGestureRecognizer(target: self, action: #selector(hideMultipleListView))
        tapGes.numberOfTapsRequired = 1
        shadowView.addGestureRecognizer(tapGes)
        mulListV = Bundle.main.loadNibNamed("ListPickerView", owner: ParentViewC, options: nil)?[1] as! MultipleSelecetionListPicker
        mulListV.frame = CGRect(x: 0, y: deviceBounds.size.height, width: deviceBounds.size.width, height: 227.0)
        ParentViewC.view.addSubview(shadowView)
        ParentViewC.view.addSubview(mulListV)
        ParentViewC.view.endEditing(true)
        mulListV.myDelegate = ParentViewC as? CustomListMultipleDelegate
        mulListV.ReloadPickerView(dataArray: Items,selectedIncludedName:selectedIncludedName)
        UIView.animate(withDuration: 0.3, animations: {
            
            shadowView.alpha = 0.3
            mulListV.frame = CGRect(x: 0, y: deviceBounds.size.height - 227.0, width: deviceBounds.size.width, height: 227.0)
            
        }, completion: nil)
        
    }
    
    @objc class func hideMultipleListView() {
        
        UIView.animate(withDuration: 0.3, animations: {
            shadowView.alpha = 0.0
            mulListV.frame = CGRect(x: 0, y: deviceBounds.size.height, width: deviceBounds.size.width, height: 227.0)
            
        }) { (Bool) in
            //listV.myDelegate?.ListDidHide!()
            shadowView.removeFromSuperview()
            mulListV.removeFromSuperview()
        }
    }
    
    
    
    // MARK: List picker
    
    class func showListDropDown(Items:Array<String>, ParentViewC:UIViewController) {
        
        shadowView = UIView(frame: deviceBounds)
        shadowView.backgroundColor = UIColor.black
        shadowView.alpha = 0.0
        let tapGes = UITapGestureRecognizer(target: self, action: #selector(hideListView))
        tapGes.numberOfTapsRequired = 1
        shadowView.addGestureRecognizer(tapGes)
        listV = Bundle.main.loadNibNamed("ListPickerView", owner: ParentViewC, options: nil)?.first as! ListPickerView
        listV.frame = CGRect(x: 0, y: deviceBounds.size.height, width: deviceBounds.size.width, height: 227.0)
        ParentViewC.view.addSubview(shadowView)
        ParentViewC.view.addSubview(listV)
        ParentViewC.view.endEditing(true)
        listV.myDelegate = ParentViewC as? CustomListDelegate
        listV.ReloadPickerView(dataArray: Items)
        UIView.animate(withDuration: 0.3, animations: {
            
            shadowView.alpha = 0.3
            listV.frame = CGRect(x: 0, y: deviceBounds.size.height - 227.0, width: deviceBounds.size.width, height: 227.0)
            
        }, completion: nil)
        
    }
    
    @objc class func hideListView() {
        
        UIView.animate(withDuration: 0.3, animations: {
            
            shadowView.alpha = 0.0
            listV.frame = CGRect(x: 0, y: deviceBounds.size.height, width: deviceBounds.size.width, height: 227.0)
            
        }) { (Bool) in
            
            //listV.myDelegate?.ListDidHide!()
            shadowView.removeFromSuperview()
            listV.removeFromSuperview()
            
        }
    }
    
    
    // MARK: Date picker
    
    class func showDatePickerDropDown(PickerType:UIDatePicker.Mode, ParentViewC:UIViewController,setRestriction:String) {
        
        shadowView = UIView(frame: deviceBounds)
        shadowView.backgroundColor = UIColor.black
        shadowView.alpha = 0.0
        let tapGes = UITapGestureRecognizer(target: self, action: #selector(hideDatePickerView))
        tapGes.numberOfTapsRequired = 1
        shadowView.addGestureRecognizer(tapGes)
        dtPickerView = Bundle.main.loadNibNamed("ListPickerView", owner: ParentViewC, options: nil)?[2] as! DatePickerView
        dtPickerView.frame = CGRect(x: 0, y: deviceBounds.size.height, width: deviceBounds.size.width, height: 227.0)
        dtPickerView.dtPicker.datePickerMode = PickerType
        ParentViewC.view.addSubview(shadowView)
        ParentViewC.view.addSubview(dtPickerView)
        ParentViewC.view.endEditing(true)
        dtPickerView.delegate = ParentViewC as? DatePickerDelegate
        UIView.animate(withDuration: 0.3, animations: {
            
            shadowView.alpha = 0.3
            dtPickerView.frame = CGRect(x: 0, y: deviceBounds.size.height - 227.0, width: deviceBounds.size.width, height: 227.0)
            
        }, completion: nil)
        
    }
    
    @objc class func hideDatePickerView() {
        
        UIView.animate(withDuration: 0.3, animations: {
            
            shadowView.alpha = 0.0
            dtPickerView.frame = CGRect(x: 0, y: deviceBounds.size.height, width: deviceBounds.size.width, height: 227.0)
            
        }) { (Bool) in
            
            //listV.myDelegate?.ListDidHide!()
            shadowView.removeFromSuperview()
            dtPickerView.removeFromSuperview()
            
        }
    }
    
}

extension UITextField {
    enum UITextFieldImagePosition {
        case leftSide
        case rightSide
    }
    
    func setIcon(_ image: UIImage, viewPosition: UITextFieldImagePosition, viewMode: ViewMode) {
        let iconView = UIImageView(frame:
                      CGRect(x: 10, y: 5, width: 20, height: 20))
        iconView.image = image
        let iconContainerView: UIView = UIView(frame:
                      CGRect(x: 20, y: 0, width: 30, height: 30))
        iconContainerView.addSubview(iconView)
        
        if viewPosition == .leftSide {
            leftView = iconContainerView
            leftViewMode = viewMode
        }
        else if viewPosition == .rightSide {
            rightView = iconContainerView
            rightViewMode = viewMode
        }
    }
    
    func setButton(_ button: UIButton, viewPosition: UITextFieldImagePosition, viewMode: ViewMode) {
        button.frame = CGRect(x: 10, y: 5, width: 20, height: 20)
        
        let buttonContainerView: UIView = UIView(frame:
                      CGRect(x: 20, y: 0, width: 30, height: 30))
        buttonContainerView.addSubview(button)
        
        if viewPosition == .leftSide {
            leftView = buttonContainerView
            leftViewMode = viewMode
        }
        else if viewPosition == .rightSide {
            rightView = buttonContainerView
            rightViewMode = viewMode
        }
    }
}

extension UIViewController {
    func embed(_ viewController:UIViewController, inView view:UIView){
        viewController.willMove(toParent: self)
        viewController.view.frame = view.bounds
        view.addSubview(viewController.view)
        self.addChild(viewController)
        viewController.didMove(toParent: self)
    }
}
