//
//  AppDelegate.swift
//  AesCare
//
//  Created by Gali Srikanth on 07/08/20.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleSignIn
import Kingfisher
import Firebase
import FirebaseMessaging
import FirebaseInstanceID
import SideMenu
//let storyboard = UIStoryboard(name: "Main", bundle: nil)
//let controller = storyboard.instantiateViewController(identifier: "MenuViewController") as! MenuViewController
//
//let menu = SideMenuNavigationController(rootViewController: controller)
//menu.leftSide = true
//menu.animationOptions = .curveEaseIn
//menu.blurEffectStyle = .light
//menu.allowPushOfSameClassTwice = false

@UIApplicationMain
class AppDelegate: UIResponder,
                   UIApplicationDelegate,
                   UIGestureRecognizerDelegate,
                   MessagingDelegate,
                   UNUserNotificationCenterDelegate {
    var aesCareAuth: AESCareAuth?
    
    var splashController: UIViewController?
    var imageBackground: UIImageView?
    var connection: AESCareConnectivity?
    var loginManager: LoginManager?

    var isKeyboardOpen = false

    var aesCareDisplay: AESCareDisplayProtection?
    var aesCareNotifications: AESCareNotifications?
    
    lazy var window:UIWindow? = {
        return UIWindow.init(frame: UIScreen.main.bounds)
    }()
    lazy var operationQueue:OperationQueue = {
        let queue = OperationQueue()
        queue.maxConcurrentOperationCount = OperationQueue.defaultMaxConcurrentOperationCount
        queue.name = "ServerInteractionQueue"
        queue.qualityOfService = .background
        return queue
    }()

    static var kQueueOperationsChanged = "kQueueOperationsChanged"
    
    
    lazy var operationQueue_background:OperationQueue = {
        let queue = OperationQueue()
        queue.maxConcurrentOperationCount = OperationQueue.defaultMaxConcurrentOperationCount
        queue.name = "ServerInteractionQueueBackground"
        queue.qualityOfService = .background
        return queue
    }()
    static var kQueueOperationsChangedBackground = "kQueueOperationsChangedBackground"
    
    
    
    var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent // .default
    }
    
    var visualEffectView = UIVisualEffectView()
    
    var numberOfScreenShots = 10
    var isScreenshotCounterEnded = true
    
    var navigationStack: UINavigationController {
        let navigation = UINavigationController()
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)

        let startPageViewController = mainStoryboard.instantiateViewController(identifier: "StartingPageViewController") as! StartingPageViewController
        let loginViewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        let dashboardController = mainStoryboard.instantiateViewController(identifier: "DashBoardVC") as! DashBoardVC
        
        if !UserDefaults.standard.bool(forKey: "appLaunchedOnce") {
            navigation.setViewControllers([loginViewController, startPageViewController], animated: true)
        }
        else {
            navigation.setViewControllers([dashboardController, loginViewController], animated: true)
        }
        
        navigation.setNavigationBarHidden(true, animated: false)

        return navigation
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
                
        FirebaseApp.configure()
        
    
        self.aesCareAuth = AESCareAuth(application, launchOptions)
        
        self.aesCareAuth?.delegate = self
        //AESCareAuth.delegateFacebook = self
        
        //self.window?.isMultipleTouchEnabled = true
        self.aesCareNotifications = AESCareNotifications()
        self.aesCareDisplay = AESCareDisplayProtection(currentview: (self.window?.rootViewController?.view)!)
        
        self.aesCareDisplay?.startDisplayProtection()
        
        self.aesCareNotifications?.requestUserPermissionForNotifications()
        self.aesCareNotifications?.startNotificationsObserver()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDisappear), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        self.window?.rootViewController?.view.addGestureRecognizer(tap)

        AESCareWorldTime.getRequest("http://worldtimeapi.org/api/timezone/America/Sao_Paulo")

        
        IQKeyboardManager.shared.enable = true
       
        self.connection = AESCareConnectivity()
        
        //AESCareAnalytics.initAnalytics()
        
        //AESCareAnalytics.event(.AppOpen)
        loginManager = LoginManager()
                
        // RETIRADO DAQUI
        
        self.autoLogin()

        /// Operation queue for Api Call Structure
        self.connection?.observeConnection()
        
        self.operationQueue.addObserver(self, forKeyPath: "operations", options: NSKeyValueObservingOptions(rawValue: 0), context: &AppDelegate.kQueueOperationsChanged)
        
        /////// Operation queue for background Api call
        self.operationQueue_background.addObserver(self, forKeyPath: "operationsBackground", options: NSKeyValueObservingOptions(rawValue: 0), context: &AppDelegate.kQueueOperationsChangedBackground)
        
        return true
    }
    
    @objc func dismissKeyboard() {
        self.window?.rootViewController?.view.endEditing(true)
    }
    
    @objc func keyboardWillAppear() {
        self.isKeyboardOpen = true
    }

    @objc func keyboardWillDisappear() {
        self.isKeyboardOpen = false
    }
    
    func checkScreenshot() {
        let ssData = UserDefaults.standard.array(forKey: "screenshot")
        
        self.isScreenshotCounterEnded = false
        let lblMsg = UILabel(frame: CGRect(x: UIScreen.main.bounds.width / 2, y: UIScreen.main.bounds.height / 2, width: 200, height: 200))
        lblMsg.text = "VOCÊ ESTA BLOQUEADO DE UTILIZAR O APP POR \(ssData![1] as! Int) SEGUNDOS. NĀO TIRE SCREENSHOTS DA TELA!"
        lblMsg.font = lblMsg.font.withSize(20)
        lblMsg.textColor = UIColor.white
        lblMsg.textAlignment = .center
        lblMsg.numberOfLines = 0
        lblMsg.center = CGPoint(x: UIScreen.main.bounds.width / 2, y: UIScreen.main.bounds.height / 2)

        let blurEffect = UIBlurEffect(style: .dark)
        let blurView = UIVisualEffectView(effect: blurEffect)
        blurView.frame = self.window!.frame
        blurView.tag = 101
        blurView.clipsToBounds = true;
        blurView.layer.borderColor = UIColor.black.cgColor
        blurView.layer.borderWidth = 1.0;
        blurView.layer.cornerRadius = 6.0;
        blurView.contentView.addSubview(lblMsg)
        blurView.contentView.bringSubviewToFront(lblMsg)
        
        self.window?.addSubview(blurView)
        UIGraphicsBeginImageContextWithOptions((self.window?.rootViewController?.view.bounds.size)!, false, 0)
        self.window?.rootViewController?.view.drawHierarchy(in: (self.window?.rootViewController?.view.bounds)!, afterScreenUpdates: true)
        UIGraphicsEndImageContext()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(self.numberOfScreenShots), execute: {
            self.numberOfScreenShots += 5
            self.isScreenshotCounterEnded = true
            self.window?.viewWithTag(101)?.removeFromSuperview()
        })

    }
        
    func applicationWillResignActive(_ application: UIApplication) {
        self.imageBackground = UIImageView(image: UIImage(named: "launch-image"))
        self.imageBackground?.contentMode = .center
        self.imageBackground?.tag = 100
        self.imageBackground?.frame = self.window!.frame
        self.imageBackground?.backgroundColor = UIColor.white
        self.window?.addSubview(self.imageBackground!)
        
        if self.isKeyboardOpen {
            self.window?.rootViewController?.view.endEditing(true)
        }
    }

  // MARK: - NAVIGATION
     internal func topViewControllerWithRootViewController(rootViewController: UIViewController!) -> UIViewController? {
         
         if (rootViewController == nil) { return nil }
         
         if (rootViewController.isKind(of: (UITabBarController).self)) {
             
             
             return topViewControllerWithRootViewController(rootViewController: (rootViewController as! UITabBarController).selectedViewController)
             
         } else if (rootViewController.isKind(of:(UINavigationController).self)) {
             
             return topViewControllerWithRootViewController(rootViewController: (rootViewController as! UINavigationController).visibleViewController)
             
         } else if (rootViewController.presentedViewController != nil) {
             return topViewControllerWithRootViewController(rootViewController: rootViewController.presentedViewController)
         }
         return rootViewController
     }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "AesCare")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        if let _ = AESCareAuth.gmailLoginHandler(url: url) {
            return true
        }
        else {
            return AESCareAuth.facebookLoginHandler(app, open: url)
        }
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        AppEvents.activateApp()
        self.window?.viewWithTag(100)?.removeFromSuperview()
      }
    
    
    func applicationWillTerminate(_ application: UIApplication) {
        AESCareAnalytics.event(.AppClose)
        
        if !UserDefaults.standard.bool(forKey: "autoLogin") {
            if let _ = UserDefaults.standard.object(forKey: "userData") {
                UserDefaults.standard.removeObject(forKey: "userData")
            }
        }
        
        UserDefaults.standard.set([self.isScreenshotCounterEnded, self.numberOfScreenShots], forKey: "screenshot")
    }
        
    func callLoginByGoogleApi(url:String,param:[String:AnyObject]) {
        
        let operation = WebServiceOperation.init(url, param, .WEB_SERVICE_POST, nil)
        
        operation.completionBlock =
            {
                DispatchQueue.main.async {
                    
                    //print(operation.responseData?.dictionary?.prettyprintedJSON ?? "")
                    guard let dictResponse = operation.responseData?.dictionary, dictResponse.count > 0 else {
                        print("Error")
                        return
                    }
                    
                    if let responceCode = dictResponse["code"] ,responceCode as! Int == 1 , let msg = dictResponse["message"] as? String{
                        if let dataUserDetails = dictResponse.data, dataUserDetails.count > 0 {
                            do{
                                let userModel = try JSONDecoder().decode(ModelUser.self, from: dataUserDetails)
                                let encoder = JSONEncoder()
                                if let encoded = try? encoder.encode(userModel) {
                                    let defaults = UserDefaults.standard
                                    defaults.set(encoded, forKey: "LOGGED_IN_USER_DATA")
                                }
                                
                                let rootViewController = self.window!.rootViewController as! UINavigationController
                                let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                let profileViewController = mainStoryboard.instantiateViewController(withIdentifier: "DashBoardVC") as! DashBoardVC
                                rootViewController.pushViewController(profileViewController, animated: true)
                                
                            }catch let e{
                                print(e.localizedDescription)
                                
                            }
                        }
                    }else if let msg = dictResponse["message"] as? String{
                        
                        //showAlertMessage(title: App_Title, message: msg, vc: self)
                    }else{
                        
                    }
                    
                    DispatchQueue.main.async {
                        /////
                    }
                }
        }
        
        appDel.operationQueue.addOperation(operation)
        
    }
    
    func callLoginByFacebookApi(url:String,param:[String:AnyObject]) {
        
        let operation = WebServiceOperation.init(url, param, .WEB_SERVICE_POST, nil)
        
        operation.completionBlock =
            {
                DispatchQueue.main.async {
                    //print(operation.responseData?.dictionary?.prettyprintedJSON ?? "")
                    guard let dictResponse = operation.responseData?.dictionary, dictResponse.count > 0 else {
                        print("Error")
                        return
                    }
                    
                    if let responceCode = dictResponse["code"] ,responceCode as! Int == 1 , let msg = dictResponse["message"] as? String{
                        if let dataUserDetails = dictResponse.data, dataUserDetails.count > 0 {
                            do{
                                let userModel = try JSONDecoder().decode(ModelUser.self, from: dataUserDetails)
                                let encoder = JSONEncoder()
                                if let encoded = try? encoder.encode(userModel) {
                                    let defaults = UserDefaults.standard
                                    defaults.set(encoded, forKey: "LOGGED_IN_USER_DATA")
                                }
                                
                                //UserDefaults.standard.set(true, forKey: "fbLogin")
                                
                                let rootViewController = self.window!.rootViewController as! UINavigationController
                                let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                let profileViewController = mainStoryboard.instantiateViewController(withIdentifier: "DashBoardVC") as! DashBoardVC
                                rootViewController.pushViewController(profileViewController, animated: true)
                                
                            }catch let e{
                                print(e.localizedDescription)
                                //showAlertMessage(title: App_Title, message: "Something went wrong", vc: self)
                            }
                        }
                    }else if let msg = dictResponse["message"] as? String{
                        //UtilityClass.tosta(message: msg, duration: 2.0, vc: self)
                        //showAlertMessage(title: App_Title, message: msg, vc: self)
                    }else{
                        //showAlertMessage(title: App_Title, message: "Something went wrong", vc: self)
                    }
                    
                    DispatchQueue.main.async {
                        /////
                    }
                }
        }
        
        appDel.operationQueue.addOperation(operation)
        
    }
}


// MARK: - ObServer for Api Call

extension AppDelegate {
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if (((object as? OperationQueue) === self.operationQueue) && (keyPath == "operations") && (context == &AppDelegate.kQueueOperationsChanged)) {
            DispatchQueue.main.async {
                if self.operationQueue.operationCount > 0 {
                    
                    
                    if #available(iOS 13.0, *) {
                        let keyWindow = UIApplication.shared.connectedScenes
                            .filter({$0.activationState == .foregroundActive})
                            .map({$0 as? UIWindowScene})
                            .compactMap({$0})
                            .first?.windows
                            .filter({$0.isKeyWindow}).first
                        CustomActivityIndicator.sharedInstance.display(onView: keyWindow, done: {
                            
                        })
                    } else {
                        
                        UIApplication.shared.keyWindow?.rootViewController?.view.endEditing(true)
                        CustomActivityIndicator.sharedInstance.display(onView: UIApplication.shared.keyWindow, done: {
                            
                        })
                    }
                    
                }else{
                    CustomActivityIndicator.sharedInstance.hide(finish: {
                        if let wd = UIApplication.shared.delegate?.window {
                            var vc = wd!.rootViewController
                            
                            if vc is UINavigationController {
                                vc = (vc as! UINavigationController).visibleViewController
                            }

                            if vc is DashBoardVC {
                                //self.aesCareNotifications?.requestUserPermissionForNotifications()
                            }
                        }
                    })
                }
                
            }
        }
        else if(((object as? OperationQueue) === self.operationQueue) && (keyPath == "operationsBackground") && (context == &AppDelegate.kQueueOperationsChangedBackground)){
           
        }
        else {
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
        }
    }
}

public extension UIWindow {

  func capture() -> UIImage? {

    let blurEffect = UIBlurEffect(style: .dark)
    let blurView = UIVisualEffectView(effect: blurEffect)
    blurView.frame = (self.window?.rootViewController!.view.frame)!
    self.window?.rootViewController!.view.addSubview(blurView)
    
    UIGraphicsBeginImageContextWithOptions((self.window?.rootViewController!.view.frame.size)!, true, 0)
    self.window?.rootViewController!.view.drawHierarchy(in: (self.window?.rootViewController!.view.bounds)!, afterScreenUpdates: true)
    let image = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()

    return image

  }
}

extension AppDelegate {
    func autoLogin() {
        if UserDefaults.standard.bool(forKey: "appLaunchedOnce") {
            if UserDefaults.standard.bool(forKey: "autoLogin") {
                if UserDefaults.standard.bool(forKey: "signIn") {
                    if let data = UserDefaults.standard.object(forKey: "userData") {
                        usrerModelOBJ = try! JSONDecoder().decode(ModelUser.self, from: data as! Data)
                        
                        let rootViewController = self.window!.rootViewController as! UINavigationController
                        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let dashboardViewController = mainStoryboard.instantiateViewController(withIdentifier: "DashBoardVC") as! DashBoardVC
                        
                        if self.window!.rootViewController is UINavigationController {
                            self.window?.rootViewController?.view.isHidden = true
                        }
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(200), execute: {
                            rootViewController.pushViewController(dashboardViewController, animated: false)
                            self.window?.rootViewController?.view.isHidden = false
                        })
                    }
                    else {
                        let rootViewController = self.window!.rootViewController as! UINavigationController
                        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let loginViewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                        rootViewController.pushViewController(loginViewController, animated: true)
                    }
                }
            }
            else {
                let rootViewController = self.window!.rootViewController as! UINavigationController
                let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let loginViewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                rootViewController.pushViewController(loginViewController, animated: true)
            }
        }
    }
}

extension AppDelegate: AESCareAuthSignInDelegate {
    func didCompleteGmailLogin(didUserData for: GIDGoogleUser) {
        let rootViewController = self.window!.rootViewController as! UINavigationController
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let profileViewController = mainStoryboard.instantiateViewController(withIdentifier: "DashBoardVC") as! DashBoardVC
        
        UserDefaults.standard.set(true, forKey: "signIn")
        UserDefaults.standard.set("gmail", forKey: "signInMethod")

        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(200), execute: {
            rootViewController.pushViewController(profileViewController, animated: false)
            self.window?.rootViewController?.view.isHidden = false
        })
    }
    
    func didCompleteFacebookLogin(didUserToken for: AccessToken) {
        let rootViewController = self.window!.rootViewController as! UINavigationController
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let profileViewController = mainStoryboard.instantiateViewController(withIdentifier: "DashBoardVC") as! DashBoardVC
        
        UserDefaults.standard.set(true, forKey: "signIn")
        UserDefaults.standard.set("facebook", forKey: "signInMethod")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(200), execute: {
            rootViewController.pushViewController(profileViewController, animated: false)
            self.window?.rootViewController?.view.isHidden = false
        })
    }
    
    func didCompleteAppleLogin(didUserData for: [String : String?]) {
        let rootViewController = self.window!.rootViewController as! UINavigationController
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let profileViewController = mainStoryboard.instantiateViewController(withIdentifier: "DashBoardVC") as! DashBoardVC
        
        UserDefaults.standard.set(true, forKey: "signIn")
        UserDefaults.standard.set("apple", forKey: "SignInMethod")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(200), execute: {
            rootViewController.pushViewController(profileViewController, animated: false)
            self.window?.rootViewController?.view.isHidden = false
        })
    }
}

// MARK: - PushNotification Functions
extension AppDelegate {
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
        self.aesCareNotifications?.postNotification(type: .didRegisterForRemoteNotificationsWithDeviceToken, data: ["token" : deviceToken])
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        self.aesCareNotifications?.postNotification(type: .didFailToRegisterForRemoteNotificationsWithError, data: ["error" : error])
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        self.aesCareNotifications?.postNotification(type: .didReceiveRemoteNotification, data: userInfo)
    }
}
