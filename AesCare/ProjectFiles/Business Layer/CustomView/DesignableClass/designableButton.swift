//
//  designableButton.swift
//  AesCare
//
//  Created by Gali Srikanth on 08/08/20.
//

import UIKit

@IBDesignable open class designableButton: UIButton {
    
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    @IBInspectable
    public var cornerRadius: CGFloat = 2.0 {
        didSet {
              self.layer.cornerRadius = self.cornerRadius
        }
    }
}
