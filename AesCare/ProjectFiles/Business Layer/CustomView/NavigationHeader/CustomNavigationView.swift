//
//  CustomNavigationView.swift
//  AesCare
//
//  Created by Gali Srikanth on 08/08/20.
//

import UIKit

class CustomNavigationView: UIView {
    @IBOutlet fileprivate var view:UIView!
    @IBOutlet var navigationContainerView: UIView!
    @IBOutlet weak var lableHeader: UILabel!
    @IBOutlet weak var lableSubHeader: UILabel!
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    internal required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private convenience init() {
        self.init(frame: CGRect.zero)
    }
    
    
    
    private func setup() {
        if let view = Bundle.main.loadNibNamed("CustomNavigationView", owner: self, options: nil)?.first as? UIView {
            self.view = view
            self.view.backgroundColor = .clear
            self.view.frame = self.bounds
            self.navigationContainerView.backgroundColor = .black
            self.addSubview(self.view)
            /*
            self.translatesAutoresizingMaskIntoConstraints = false
            self.view.translatesAutoresizingMaskIntoConstraints = false
            
            let consTop = NSLayoutConstraint.init(item: self, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1.0, constant: 0.0)
            consTop.isActive = true
            
            let consRight = NSLayoutConstraint.init(item: self, attribute: NSLayoutConstraint.Attribute.right, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.right, multiplier: 1.0, constant: 0.0)
            consRight.isActive = true
            
            let consBottom = NSLayoutConstraint.init(item: self, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1.0, constant: 0.0)
            consBottom.isActive = true
            
            let consLeft = NSLayoutConstraint.init(item: self, attribute: NSLayoutConstraint.Attribute.left, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.left, multiplier: 1.0, constant: 0.0)
            consLeft.isActive = true
            
            self.addConstraints([consTop,consRight,consBottom,consLeft])
            
            self.view.layoutIfNeeded()
            */
            
        }
    }
}
