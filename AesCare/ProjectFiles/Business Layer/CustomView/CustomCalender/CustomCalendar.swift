

import UIKit
import FSCalendar

/**
  This class is used to represent calendar helper for whole project
 */

protocol selectedDateDelegate:NSObjectProtocol {
    func didSelectDate(_ selectedDate:Date?)
    
}

class CustomCalendar: UIView {

    public static let shared = CustomCalendar()
    @IBOutlet var view:UIView?
    @IBOutlet var cal:FSCalendar!
    //@IBOutlet var lblYear:UILabel!
    //@IBOutlet var lblDay:UILabel!
    @IBOutlet weak var lblYearDateMonth: UILabel!
    
    @IBOutlet weak var btnPrevious: UIButton!
    
    var resultDate:String = ""
    
    weak var delegate:selectedDateDelegate? = nil
   
    fileprivate var selectedDate:Date =  Date()
    
    
    
    var arrMedics:[Medics]!
    
    //var tempArr = ["05/10/2020","06/10/2020","07/10/2020"]
        //["2020-10-05 07:40:00-03:00","2020-10-06 10:40:00-03:00"]
    private override init(frame: CGRect) {
        super.init(frame: frame)
        basicSetup()
    }
    private convenience init() {
        self.init(frame: CGRect.zero)
        
    }
    internal required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.basicSetup()
    }
    
    private func basicSetup() {
        DispatchQueue.main.async {
            self.view = Bundle.main.loadNibNamed("CustomCalendar", owner: self, options: nil)?.first as? UIView
            self.view?.frame = CGRect(x:0 , y:0 ,width:self.bounds.size.width, height:self.bounds.size.height)
            self.addSubview(self.view!)
            self.cal.delegate = self
            self.cal.dataSource = self
        
            let now = Date()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "LLL"
            let nameOfYear = dateFormatter.string(from: now)
            self.lblYearDateMonth.text = nameOfYear
        }
    }
    @IBAction func dismissView(_ btn: Any) {
        self.removeFromSuperview()
        self.delegate = nil
    }
    @IBAction func okPressed(_ btn:UIButton) {
        
        guard let delegate = self.delegate else {
            return
        }
        
        delegate.didSelectDate(self.selectedDate)
        
        self.removeFromSuperview()
        self.delegate = nil
    }
    
    @IBAction func moveNextMonth(_ btn:UIButton) {
        var dateComponents = DateComponents()
        dateComponents.month = 1
         
        if let currentPage = Calendar.current.date(byAdding: dateComponents, to: self.cal.currentPage) {
            self.cal.setCurrentPage(currentPage, animated: true)
           // print(self.cal.date(for: <#T##FSCalendarCell#>))
        }
        
        //        let dA = Date.init()
        //        if (dA.compare(self.cal.currentPage)) == .orderedAscending {
        //
        //            btnPrevious.isHidden = false
        //        }else{
        //             btnPrevious.isHidden = true
        //        }
        
        
    }
    @IBAction func movePrevMonth(_ btn:UIButton) {
        var dateComponents = DateComponents()
        dateComponents.month = -1
        
        
        if let currentPage = Calendar.current.date(byAdding: dateComponents, to: self.cal.currentPage) {
            self.cal.setCurrentPage(currentPage, animated: true)
        }
        //         let dA = Date.init()
        //        if (dA.compare(self.cal.currentPage)) == .orderedAscending {
        //
        //            btnPrevious.isHidden = true
        //        }else{
        //            btnPrevious.isHidden = false
        //        }
    }
    
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+DispatchTimeInterval.milliseconds(10)) {
            self.processDate(Date())
        }
    }
    
    fileprivate func processDate(_ date:Date) {
        self.selectedDate =  date
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        resultDate = formatter.string(from: selectedDate)
      
    }
    
    fileprivate lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    /*
    func minimumDate(for calendar: FSCalendar) -> Date {
        return self.dateFormatter.date(from: "2018-12-10")!
    }
    
    func maximumDate(for calendar: FSCalendar) -> Date {
        return self.dateFormatter.date(from: "2019-02-28")!
    }*/
}

extension CustomCalendar:FSCalendarDelegate {
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        print(calendar.currentPage)
        
        let values = Calendar.current.dateComponents([Calendar.Component.month, Calendar.Component.year], from:calendar.currentPage)
        let month = DateFormatter().monthSymbols[values.month! - 1]
        self.lblYearDateMonth.text = "\(month)"
            //+ " " + "\(values.year!)"
        
    }
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        
        self.removeAndDataProcess(date)
    }
    func removeAndDataProcess(_ date:Date) {
        self.selectedDate =  date
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        resultDate = formatter.string(from: selectedDate)
        guard let delegate = self.delegate else {
            return
        }
        
        delegate.didSelectDate(self.selectedDate)
     
    }
}

// MARK: - FSCalendarDelegateAppearance
extension CustomCalendar:FSCalendarDelegateAppearance{
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, titleDefaultColorFor date: Date) -> UIColor? {
        if let obj = arrMedics, obj.count > 0 {
            for (_ , objectOuter ) in arrMedics.enumerated(){
                if let arrCalendar = objectOuter.calendar, arrCalendar.count > 0 {
                    for (_ , object ) in arrCalendar.enumerated(){
                        
                        if let createdAt = object.created_at, createdAt.count > 0{
                            
                            let dateFormatter1 = DateFormatter()
                            dateFormatter1.dateFormat = "dd/MM/yyyy"
                            let dateString = dateFormatter1.string(from: date as Date)
                            //let dateString = date.toString(dateFormat: "dd/MM/yyyy")
                            let eventDateString = UtilityClass.dateFormatterFlexibleLocal(strDate: createdAt, fromDateFromat: "yyyy-MM-dd'T'HH:mm:ss.SSSXXX", toDateFormat: "dd/MM/yyyy")
                            //print(eventDateString)
                            //print(dateString)
                            if(eventDateString == dateString){
                                return .black
                            }
                        }
                    }
                }
            }
        }
        return .lightGray
        
    }
    
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, eventDefaultColorsFor date: Date) -> [UIColor]? {
        return [UIColor(red: 0.686, green: 0.266, blue: 0.850, alpha: 1.0)]
    }
}

// MARK: - FSCalendarDataSource
extension CustomCalendar : FSCalendarDataSource {
    
    /*
    
    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
        
        if let obj = arrMedics, obj.count > 0 {
            for (_ , objectOuter ) in arrMedics.enumerated(){
                if let arrCalendar = objectOuter.calendar, arrCalendar.count > 0 {
                    for (_ , object ) in arrCalendar.enumerated(){
                        
                        if let createdAt = object.created_at, createdAt.count > 0{
                            
                            let dateFormatter1 = DateFormatter()
                            dateFormatter1.dateFormat = "dd/MM/yyyy"
                            let dateString = dateFormatter1.string(from: date as Date)
                            //let dateString = date.toString(dateFormat: "dd/MM/yyyy")
                            let eventDateString = UtilityClass.dateFormatterFlexibleLocal(strDate: createdAt, fromDateFromat: "yyyy-MM-dd'T'HH:mm:ss.SSSXXX", toDateFormat: "dd/MM/yyyy")
                            print(eventDateString)
                            print(dateString)
                            if(eventDateString == dateString){
                                return 1
                            }
                        }
                    }
                }
            }
        }
        return 0
     
    }*/
   
}

