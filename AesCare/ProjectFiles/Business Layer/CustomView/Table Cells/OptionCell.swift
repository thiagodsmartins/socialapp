//
//  OptionCell.swift
//  AesCare
//
//  Created by Gali Srikanth on 13/08/20.
//

import UIKit

class OptionCell: UITableViewCell {
    @IBOutlet weak var containerOption: UIView!
    @IBOutlet weak var lblInfo:UILabel!
    @IBOutlet weak var btnInstance:UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
