//
//  PublishMediaCell.swift
//  AesCare
//
//  Created by Gali Srikanth on 16/10/20.
//

import UIKit

class PublishMediaCell: UITableViewCell {
    
    @IBOutlet weak var txtVWInstance1: UITextView!
    @IBOutlet weak var imgContent:UIImageView!
    
    
    @IBOutlet weak var colVWImageAdd: UICollectionView!
  
    
    @IBOutlet weak var btnInstance:UIButton!
    @IBOutlet weak var btnInstance2:UIButton!
    @IBOutlet weak var lblInfo:UILabel!
    var imgArr = [UIImage]()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        colVWImageAdd.delegate = self
        colVWImageAdd.dataSource = self
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
extension PublishMediaCell:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imgArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SliderCollCell", for: indexPath) as! SliderCollCell
        //cell.imageviewSlider.layer.cornerRadius = cell.imageviewSlider.frame.height / 2
        //cell.imageviewSlider.clipsToBounds = true
        //cell.imageviewSlider.contentMode = .scaleAspectFit
        cell.setNeedsDisplay()
        
        cell.imageviewSlider.image = imgArr[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 80, height: 80)
    }
   
}
