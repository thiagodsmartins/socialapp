//
//  SingleImageLblCell.swift
//  AesCare
//
//  Created by Gali Srikanth on 12/08/20.
//

import UIKit

class SingleImageLblCell: UITableViewCell {
    @IBOutlet weak var imgContent: UIImageView!
    @IBOutlet weak var lblInfo: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
