//
//  CommonCellTableViewCell.swift


import UIKit
import SkyFloatingLabelTextField

class CommonCellTableViewCell: UITableViewCell {
    @IBOutlet weak var cellText:SkyFloatingLabelTextField!
    @IBOutlet weak var cellSelectionText:UITextField!
    @IBOutlet weak var cellSelectionText2:UITextField!
    
    //@IBOutlet weak var viewSwitch: UISwitch!
    @IBOutlet weak var vwContainer1:UIView!
    @IBOutlet weak var vwContainer2:UIView!
    
    @IBOutlet weak var lblHeader:UILabel!
    @IBOutlet weak var lblHeaderExtra:UILabel!
    @IBOutlet weak var lblInfo:UILabel!
    @IBOutlet weak var lblInfoExtra1:UILabel!
    @IBOutlet weak var lblInfoExtra2:UILabel!
    @IBOutlet weak var lblInfoExtra3:UILabel!
    @IBOutlet weak var lblInfoExtra4:UILabel!
    @IBOutlet weak var lblInfoExtra5:UILabel!
    @IBOutlet weak var lblInfoExtra6:UILabel!
    
    @IBOutlet weak var imgContent:UIImageView!
   
    @IBOutlet weak var btnInstance:UIButton!
    
    @IBOutlet weak var btnInstance2:UIButton!
    @IBOutlet weak var btnInstance3:UIButton!
    @IBOutlet weak var btnInstance4:UIButton!
    @IBOutlet weak var btnInstance5:UIButton!
    
    @IBOutlet weak var heightConstant: NSLayoutConstraint!
    
    @IBOutlet weak var txtVWInstance1: UITextView!
    
    @IBOutlet weak var btnSwitch: UISwitch!
    @IBOutlet weak var vwFloating: FloatRatingView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
}
