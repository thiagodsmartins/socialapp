//
//  TableCellWithColVW.swift
//  AesCare
//
//  Created by Gali Srikanth on 14/10/20.
//

import UIKit
protocol MyCelDelegate: NSObjectProtocol{
    func didaddImage( _ arrImage:[MultiPartDataFormatStructure])
}
class TableCellWithColVW: UITableViewCell {
    @IBOutlet weak var colVWImageAdd: UICollectionView!
    var imagePicker = UIImagePickerController()
    weak var delegate: MyCelDelegate? = nil
    var mediaImageList_multipartData : [MultiPartDataFormatStructure] = [MultiPartDataFormatStructure]()
    var row_imag_ContentArr:[AnyObject] = [AnyObject]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        colVWImageAdd.delegate = self
        colVWImageAdd.dataSource = self
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
extension TableCellWithColVW:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (1 + row_imag_ContentArr.count)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SliderCollCell", for: indexPath) as! SliderCollCell
        //cell.imageviewSlider.layer.cornerRadius = cell.imageviewSlider.frame.height / 2
        //cell.imageviewSlider.clipsToBounds = true
        cell.imageviewSlider.contentMode = .scaleAspectFit
        cell.setNeedsDisplay()
        if(indexPath.row == 0){
            cell.imageviewSlider.image = UIImage(named: "plus")
        } else {
            cell.imageviewSlider.image = row_imag_ContentArr[indexPath.row - 1] as? UIImage
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 80, height: 80)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(indexPath.row == 0){
             self.systemAlertofAttachmentForPublication(header: App_Title, message: "Escolha a sua foto")
        }
    }
    
}
extension TableCellWithColVW:UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
   
    ////alert to represent attachment option
    func systemAlertofAttachmentForPublication(header:String , message:String) {
        
        let alertController:UIAlertController = UIAlertController(title: header, message: message, preferredStyle: .alert)
        
        
        let actionVideoGallery = UIAlertAction(title: "Torar foto", style: .default, handler: { (action) -> Void in
            // Get TextFields text
            self.openCamera()
            
        })
        let actionImageGallery = UIAlertAction(title: "Escolher foto", style: .default, handler: { (action) -> Void in
            // Get TextFields text
            self.openPhotoGallary()
            
        })
        let cancelAction = UIAlertAction(title: "Cancelher foto", style: .destructive, handler: { (action) -> Void in
            
        })
        
        alertController.addAction(actionVideoGallery)
        alertController.addAction(actionImageGallery)
        alertController.addAction(cancelAction)
        
        UIApplication.shared.keyWindow?.rootViewController?.present(alertController, animated: true, completion: nil)
    }
    func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(.camera)){
            self.imagePicker.delegate = self
            self.imagePicker.sourceType = .camera
            self.imagePicker.allowsEditing = true
            UIApplication.shared.keyWindow?.rootViewController?.present(imagePicker, animated: true, completion: nil)
        }else{
            showAlertMessage(title: "Ooops", message: "unable to open Camera", vc: (UIApplication.shared.keyWindow?.rootViewController)!)
        }
    }
    func openPhotoGallary(){
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            self.imagePicker.delegate = self
            self.imagePicker.allowsEditing = true
            self.imagePicker.sourceType = .savedPhotosAlbum//.photoLibrary
            self.imagePicker.mediaTypes = ["public.image"]
            
            UIApplication.shared.keyWindow?.rootViewController?.present(self.imagePicker, animated: true, completion: nil)
        } else {
            showAlertMessage(title: "Ooops", message: "unable to open PhotoAlbum", vc: (UIApplication.shared.keyWindow?.rootViewController)!)
        }
        
    }
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)
        if let pickedimage = info[.editedImage] as? UIImage {
            let data = pickedimage.jpegData(compressionQuality: 0.5 )
            let timeInterval = NSDate().timeIntervalSince1970
            let theFileName : String = "\(timeInterval).jpeg"
            //let multiPartData = [MultiPartDataFormatStructure.init(key: "image", mimeType: MultiPartDataFormatStructure.MIME_TYPE.image_jpeg, data: data, name: theFileName)]
            let multiPartData = MultiPartDataFormatStructure.init(key: "attachments", mimeType: MultiPartDataFormatStructure.MIME_TYPE.image_jpeg, data: data, name: theFileName)
            self.mediaImageList_multipartData.append(multiPartData)
            self.row_imag_ContentArr.append(pickedimage)
            self.colVWImageAdd.reloadData()
            if let delegate = self.delegate {
                delegate.didaddImage(self.mediaImageList_multipartData)
            }
        } else {
            showAlertMessage(title: "Ooops", message: "unable to select image", vc: (UIApplication.shared.keyWindow?.rootViewController)!)
        }
        
        UIApplication.shared.keyWindow?.rootViewController?.dismiss(animated: true, completion: nil)
    }
    
}
