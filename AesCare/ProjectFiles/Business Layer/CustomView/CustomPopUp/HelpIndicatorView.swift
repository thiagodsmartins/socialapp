//
//  HelpIndicatorView.swift
//  AesCare
//
//  Created by Gali Srikanth on 18/10/20.
//

import UIKit
protocol HelpIndicatorViewDelegate: NSObjectProtocol{
    func removePressed()
}
class HelpIndicatorView: UIView {
    @IBOutlet var view:UIView?
    @IBOutlet var lblInfo:UILabel?
    weak var delegate: HelpIndicatorViewDelegate? = nil
    private override init(frame: CGRect) {
        super.init(frame: frame)
    }
    private convenience init() {
        self.init(frame: CGRect.zero)
    }
    internal required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func basicSetup(lblText:String) {
        self.view = Bundle.main.loadNibNamed("HelpIndicatorView", owner: self, options: nil)?.first as? UIView
        self.view?.frame = CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height)
        self.lblInfo?.text = lblText
       
        self.addSubview(self.view!)
      
    }
    @IBAction func remove(_ sender: Any) {
        if let delegate = self.delegate {
            delegate.removePressed()
        }
        self.removeFromSuperview()
    }
}
