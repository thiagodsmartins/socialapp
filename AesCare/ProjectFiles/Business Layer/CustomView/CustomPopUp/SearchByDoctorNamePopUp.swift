//
//  SearchByDoctorNamePopUp.swift
//  AesCare
//
//  Created by Gali Srikanth on 13/10/20.
//

import UIKit
protocol PopupDoctorSelectionDelegate: NSObjectProtocol{
    func didSelectDoctorPressed( _ selectedDoctor:Medics)
}
protocol PopupDoctorSelectionDelegateFromNavigation: NSObjectProtocol{
    func didSelectDoctorPressedForSearch( _ selectedDoctor:Medics)
}
class SearchByDoctorNamePopUp: UIView {
    @IBOutlet weak var tblCountry: UITableView!
    @IBOutlet var view:UIView?
    @IBOutlet var txtSearch:UITextField?
    var arrDoctorList : [Medics]!
    weak var delegate: PopupDoctorSelectionDelegate? = nil
    weak var delegateNavDocSearch: PopupDoctorSelectionDelegateFromNavigation? = nil
    
    private override init(frame: CGRect) {
        super.init(frame: frame)
    }
    private convenience init() {
        self.init(frame: CGRect.zero)
    }
    internal required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    func basicSetup() {
        self.view = Bundle.main.loadNibNamed("SearchByDoctorNamePopUp", owner: self, options: nil)?.first as? UIView
        self.view?.frame = CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height)
        
        txtSearch?.delegate = self
        self.addSubview(self.view!)
        self.txtSearch!.addTarget(self, action: #selector(self.textFieldValueChange(_:)), for: .editingChanged)
    }
}
//
//Mark: table work
//
extension SearchByDoctorNamePopUp:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let arr = arrDoctorList {
            return arr.count
        }
        return 0
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        var cell = tableView.dequeueReusableCell(withIdentifier: "Cell")
        
        if( !(cell != nil))
        {
            cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "Cell")
        }
        
       
        if let arr = arrDoctorList,arr.count > 0{
            let mdOBJTemp = arr[indexPath.row]
            cell!.textLabel?.text = mdOBJTemp.name
        }
        
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.removeFromSuperview()
       
        if let delegate = self.delegate {
            delegate.didSelectDoctorPressed(arrDoctorList[indexPath.row])
        } else if let delegate = self.delegateNavDocSearch {
            delegate.didSelectDoctorPressedForSearch(arrDoctorList[indexPath.row])
        }
    }
}
extension SearchByDoctorNamePopUp:UITextFieldDelegate{
    @objc func textFieldValueChange(_ txt: UITextField)  {
           if let str = txt.text, str.count > 0 {
               self.callDoctorsByNameApi(searchString: str)
        }
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool{
        true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.txtSearch?.resignFirstResponder()
        return true
    }
    @IBAction func remove(_ sender: Any) {
        self.removeFromSuperview()
    }
    @IBAction func search(_ sender: Any) {
        if let searchtxt = txtSearch?.text, searchtxt.count != 0{
            self.callDoctorsByNameApi(searchString: searchtxt)
        }
    }
}
extension SearchByDoctorNamePopUp{
    func callDoctorsByNameApi(searchString:String) {
      
        let getUrl :String = BaseUrl + "/doctors?medic_name=" + searchString + "&order_by=relevance"
        
        let operation = WebServiceOperation.init(getUrl, nil, .WEB_SERVICE_GET, nil)
        
        operation.completionBlock =
            {
                DispatchQueue.main.async {
                    
                    print(operation.responseData?.dictionary?.prettyprintedJSON ?? "")
                    guard let dictResponse = operation.responseData?.dictionary, dictResponse.count > 0 else {
                        
                        return
                    }
                    
                    if let responceCode = dictResponse["code"] ,responceCode as? Int == 1 {
                        if let dataUserDetails = dictResponse.data, dataUserDetails.count > 0 {
                            do{
                                let getParseData = try JSONDecoder().decode(Model_Doctor.self, from: dataUserDetails)
                                self.arrDoctorList = getParseData.medics
                                self.tblCountry.reloadData()
                            }catch let e{
                                print(e.localizedDescription)
                                
                            }
                        }
                    }
                }
        }
        appDel.operationQueue_background.addOperation(operation)
    }
}
