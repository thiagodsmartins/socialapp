//
//  CustomPopupTutorial.swift
//  AesCare
//
//  Created by Gali Srikanth on 11/08/20.
//

import UIKit


protocol PopupViewDelegate: NSObjectProtocol
{
    func didSelectNextPressed( _ view:CustomPopupTutorial,loadFor:String)
    func didSelectSkipPressed(_ view:CustomPopupTutorial,loadFor:String)
}
class CustomPopupTutorial: UIView {
    @IBOutlet weak var tblPopupItem: UITableView!
    @IBOutlet var view:UIView?
    var arrOption: [String] = [String]()
    
    var arrOption1  : [[String:String]] = [[String:String]]()
    weak var delegate: PopupViewDelegate? = nil
    public static let shared = CustomPopupTutorial()
    var isNavigateForCustomTabbar = false
    
    private override init(frame: CGRect) {
         super.init(frame: frame)
         
     }
     private convenience init() {
         self.init(frame: CGRect.zero)
     }
     internal required init?(coder aDecoder: NSCoder) {
         super.init(coder: aDecoder)
     }
    
    func basicSetup() {
        self.view = Bundle.main.loadNibNamed("CustomPopupTutorial", owner: self, options: nil)?.first as? UIView
        self.view?.frame = CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height)
        
        self.tblPopupItem.register(UINib.init(nibName: "SingleImageLblCell", bundle: nil), forCellReuseIdentifier: "SingleImageLblCell")
        self.tblPopupItem.reloadData()
        self.addSubview(self.view!)
    }
    
    
}
extension CustomPopupTutorial{
    @IBAction func moveByNext(_ sender: Any) {
        if let delegate = self.delegate {
            delegate.didSelectNextPressed(self, loadFor: "")
        }
    }
    @IBAction func moveBySkip(_ sender: Any) {
        if let delegate = self.delegate {
            delegate.didSelectSkipPressed(self, loadFor: "")
        }
    }
}
extension CustomPopupTutorial:UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int{
        return (isNavigateForCustomTabbar) ? 2 : 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(isNavigateForCustomTabbar){
            if (section == 1){
                return arrOption1.count
            }
        }
        return arrOption.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if(isNavigateForCustomTabbar){
            if (indexPath.section == 1){
                return 30
            }
        }
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        if(isNavigateForCustomTabbar && indexPath.section == 1){
            let cell: SingleImageLblCell = self.tblPopupItem.dequeueReusableCell(withIdentifier: "SingleImageLblCell") as! SingleImageLblCell
            
            
            cell.imgContent.image =  UIImage(named: arrOption1[indexPath.row]["imageName"]!)
            cell.lblInfo.text = arrOption1[indexPath.row]["itemName"]
            cell.selectionStyle = .none
            cell.backgroundColor = UIColor.clear
            cell.contentView.backgroundColor = UIColor.clear
            
            return cell
            
        }
        else{
            var cell = tableView.dequeueReusableCell(withIdentifier: "Cell")
            
            if( !(cell != nil))
            {
                cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "Cell")
            }
            cell!.textLabel?.text = arrOption[indexPath.row]
            cell!.textLabel?.font = UIFont(name: "OpenSans-SemiBold", size: 14.0)
            cell!.textLabel?.textColor = UIColor(named: "AppTextColor")
            cell!.textLabel?.textAlignment = .center
            cell!.textLabel?.numberOfLines = 0
            cell!.selectionStyle = .none
            cell!.backgroundColor = UIColor.clear
            cell!.contentView.backgroundColor = UIColor.clear
            return cell!
        }
     }
}
