//
//  CountrySearchPopUp.swift
//  AesCare
//
//  Created by Gali Srikanth on 26/09/20.
//

import UIKit
protocol PopupCountrySelectionDelegate: NSObjectProtocol{
    func didSelectCountryPressed( _ selectedCountry:ModelCountryList)
    func didSelectCityPressed( _ selectedCity:ModelSearchedCity)
}
class CountrySearchPopUp: UIView {
    
    @IBOutlet weak var tblCountry: UITableView!
    @IBOutlet var view:UIView?
    @IBOutlet var txtSearch:UITextField?
    var arrSearchCountryList : [ModelCountryList]!
    var arrSearchCityList : [ModelSearchedCity]!
    weak var delegate: PopupCountrySelectionDelegate? = nil
    var isSearchforCountry = false
    private override init(frame: CGRect) {
        super.init(frame: frame)
    }
    private convenience init() {
        self.init(frame: CGRect.zero)
    }
    internal required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func basicSetup() {
        self.view = Bundle.main.loadNibNamed("CountrySearchPopUp", owner: self, options: nil)?.first as? UIView
        self.view?.frame = CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height)
        
        txtSearch?.delegate = self
        self.addSubview(self.view!)
        
        self.txtSearch!.addTarget(self, action: #selector(self.textFieldValueChange(_:)), for: .editingChanged)
    }
    
}
extension CountrySearchPopUp:UITextFieldDelegate {
    @objc func textFieldValueChange(_ txt: UITextField)  {
        if let searchtxt = txt.text, searchtxt.count > 0 {
            if(isSearchforCountry){
                self.getCountryApi(searchText: searchtxt)
            }else{
                self.getCityApi(searchText: searchtxt)
            }
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.txtSearch?.resignFirstResponder()
        return true
    }
    @IBAction func remove(_ sender: Any) {
        self.removeFromSuperview()
    }
    @IBAction func search(_ sender: Any) {
        
        if let searchtxt = txtSearch?.text, searchtxt.count != 0{
            if(isSearchforCountry){
                self.getCountryApi(searchText: searchtxt)
            }else{
                self.getCityApi(searchText: searchtxt)
            }
        }
    }
}
//
//Mark: table work
//
extension CountrySearchPopUp:UITableViewDelegate,UITableViewDataSource{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        true
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(isSearchforCountry){
            if let arr = arrSearchCountryList {
                return arr.count
            }
        }else{
            if let arr = arrSearchCityList {
                return arr.count
            }
        }
        return 0
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var cell = tableView.dequeueReusableCell(withIdentifier: "Cell")
        
        if( !(cell != nil))
        {
            cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "Cell")
        }
        
        if(isSearchforCountry){
            if let arr = arrSearchCountryList,arr.count > 0{
                let mdOBJTemp = arr[indexPath.row]
                cell!.textLabel?.text = mdOBJTemp.name
            }
        }else{
            if let arr = arrSearchCityList,arr.count > 0{
                let mdOBJTemp = arr[indexPath.row]
                cell!.textLabel?.text = mdOBJTemp.name
            }
        }
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.removeFromSuperview()
        if(isSearchforCountry){
            if let delegate = self.delegate {
                delegate.didSelectCountryPressed(arrSearchCountryList[indexPath.row])
            }
        }else{
            if let delegate = self.delegate {
                delegate.didSelectCityPressed(arrSearchCityList[indexPath.row])
            }
        }
    }
}
//
//Mark : Api search country
///
extension CountrySearchPopUp{
    
    func getCountryApi(searchText : String) {
        
        let url = (API.searchByCountryName.getURL()?.absoluteString ?? "") + searchText
        let operation =  WebServiceOperation.init(url,nil, .WEB_SERVICE_GET)
        
        operation.completionBlock =
            {
                DispatchQueue.main.async {
                    
                    if let dataUserDetails = operation.responseData, dataUserDetails.count > 0 {
                        do{
                            
                            self.arrSearchCountryList  = try JSONDecoder().decode([ModelCountryList].self, from: dataUserDetails)
                            
                            //print(self.arrSearchCountryList)
                            self.tblCountry.reloadData()
                        }catch _ {
                            
                            
                        }
                    }
                    else{
                        
                    }
                }
        }
        
        appDel.operationQueue_background.addOperation(operation)
    }
    
    func getCityApi(searchText : String) {
        
        let url = (API.searchByCityName.getURL()?.absoluteString ?? "") + searchText
        let operation =  WebServiceOperation.init(url,nil, .WEB_SERVICE_GET)
        operation.completionBlock =
            {
                DispatchQueue.main.async {
                    
                    if let dataUserDetails = operation.responseData, dataUserDetails.count > 0 {
                        do{
                            
                            self.arrSearchCityList  = try JSONDecoder().decode([ModelSearchedCity].self, from: dataUserDetails)
                            
                            
                            self.tblCountry.reloadData()
                        }catch  {
                            
                        }
                    }
                }
        }
        
        appDel.operationQueue_background.addOperation(operation)
    }
}
