//
//  LoginVC.swift


import Foundation
import UIKit
import Toast_Swift
struct UtilityClass {
    static func tosta(message: String, duration: CGFloat, vc:UIViewController)  {
        var style = ToastStyle()
        
        style.backgroundColor = UIColor(named: "AppTextColor")!
        //style.maxWidthPercentage
        style.messageFont = UIFont.systemFont(ofSize: 15.0)
            //UIFont(name: "System", size: 15.0)!
        style.messageColor = .white
        ToastManager.shared.style = style
        
        vc.view?.makeToast(message, duration: 2.0, position: .bottom)
    }
    static func calculateAge(birthday: String) -> Int{
        
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "dd/MM/yyyy"
        guard let birthdayDate = dateFormater.date(from: birthday) else {
            return 0
        }
        
        let calendar: NSCalendar! = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)
        let now: NSDate! = NSDate()
        guard let presentDate = now as Date?  else {  return 0 }
        
        let calcAge = calendar.components(.day, from: presentDate, to: birthdayDate, options: [])
        guard let age = calcAge.day else { return 0 }
        return age
        
    }
    static func calculateTimeSpan(fromTime: String) -> Bool{
        
        
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "hh:mm a"
        guard let birthdayDate = dateFormater.date(from: fromTime) else {
            return false
        }
        
        
        let calendar = Calendar.current
        let now = birthdayDate
        let eight_today = calendar.date(
          bySettingHour: 12,
          minute: 00,
          second: 0,
          of: now)!

        let four_thirty_today = calendar.date(
          bySettingHour: 23,
          minute: 59,
          second: 0,
          of: now)!

        if now >= eight_today &&
          now <= four_thirty_today
        {
          return true
        }
        return false
    }
    static func dateFormatterFlexibleLocal(strDate:String, fromDateFromat:String, toDateFormat:String)->String
       {
           let dateFormatter = DateFormatter()
           let tempLocale = dateFormatter.locale // save locale temporarily
           
           dateFormatter.dateFormat = fromDateFromat//"dd-MM-yyyy hh:mm a"
           guard let date = dateFormatter.date(from: strDate) else {
               return strDate
           }
           //let date = try dateFormatter.date(from: strDate)!
           dateFormatter.dateFormat = toDateFormat//"hh:mm a"
           dateFormatter.locale = tempLocale // reset the locale
           let dateString = dateFormatter.string(from: date)
           //print("EXACT_DATE : \(dateString)")
           return dateString
       }
}
extension UIDevice {
    var isSimulator:Bool {
        get{
            var isSim = false
            #if arch(i386) || arch(x86_64)
            isSim = true
            #endif
            return isSim
        }
    }
    var iPhoneX: Bool {
        return UIScreen.main.nativeBounds.height == 2436
    }
    var iPhone: Bool {
        return UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone
    }
    var iPad: Bool {
        return UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad
    }
    enum ScreenType: String {
        case iPhone4_4S = "iPhone 4 or iPhone 4S"
        case iPhones_5_5s_5c_SE = "iPhone 5, iPhone 5s, iPhone 5c or iPhone SE"
        case iPhones_6_6s_7_8 = "iPhone 6, iPhone 6S, iPhone 7 or iPhone 8"
        case iPhones_6Plus_6sPlus_7Plus_8Plus = "iPhone 6 Plus, iPhone 6S Plus, iPhone 7 Plus or iPhone 8 Plus"
        case iPhoneX = "iPhone X"
        case iPhoneXR = "iPhone XR"
        case iPhoneXS_MAX = "iPhone XS MAX"
        case iPadPro9_7 = "iPad 9.7"
        case iPadPro10_5 = "iPad 10.5"
        case iPadPro12_9 = "iPad 12.9"
        case unknown
    }
    var screenType: ScreenType {
        switch UIScreen.main.nativeBounds.height {
        case 960:
            return .iPhone4_4S
        case 1136:
            return .iPhones_5_5s_5c_SE
        case 1334:
            return .iPhones_6_6s_7_8
        case 1920, 2208:
            return .iPhones_6Plus_6sPlus_7Plus_8Plus
        case 2436:
            return .iPhoneX
        case 1792:
            return .iPhoneXR
        case 2688:
            return .iPhoneXS_MAX
        case 2048:
            return .iPadPro9_7
        case 2224:
            return .iPadPro10_5
        case 2732:
            return .iPadPro12_9
        default:
            return .unknown
        }
    }
}
extension UILabel {
    open override func awakeFromNib() {
        super.awakeFromNib()
        /*
        if let fontName = self.font?.fontName, let size = self.font?.pointSize {
            if UIDevice.current.iPad {
                if UIDevice.current.screenType == .iPadPro12_9 {
                    self.font = UIFont.init(name: fontName, size: (size + 20.0))
                }
                else if UIDevice.current.screenType == .iPadPro10_5 {
                    self.font = UIFont.init(name: fontName, size: (size + 17.5))
                }
                else if UIDevice.current.screenType == .iPadPro9_7 {
                    self.font = UIFont.init(name: fontName, size: (size + 15.0))
                }
            }else{
                if UIDevice.current.screenType == .iPhone4_4S {
                    self.font = UIFont.init(name: fontName, size: size)
                }
                else if UIDevice.current.screenType == .iPhones_5_5s_5c_SE {
                    self.font = UIFont.init(name: fontName, size: (size + 1.0))
                }
                else if UIDevice.current.screenType == .iPhones_6_6s_7_8 {
                    self.font = UIFont.init(name: fontName, size: (size + 3.0))
                }
                else if UIDevice.current.screenType == .iPhones_6Plus_6sPlus_7Plus_8Plus {
                    self.font = UIFont.init(name: fontName, size: (size + 10.0))
                }
                else if UIDevice.current.screenType == .iPhoneX {
                    self.font = UIFont.init(name: fontName, size: (size + 5.0))
                }
                else if UIDevice.current.screenType == .iPhoneXR {
                    self.font = UIFont.init(name: fontName, size: (size + 12.0))
                }
                else if UIDevice.current.screenType == .iPhoneXS_MAX {
                    self.font = UIFont.init(name: fontName, size: (size + 13.0))
                }
            }
        }*/
    }
}

extension UIButton {
    func makeButtonCornerRadius(){
       let radius = (self.frame.size.height / 2)
           self.layer.cornerRadius = radius
           self.clipsToBounds = true
       }
}

extension String {

    init?(htmlEncodedString: String) {

        guard let data = htmlEncodedString.data(using: .utf8) else {
            return nil
        }

        let options: [NSAttributedString.DocumentReadingOptionKey: Any] = [
            .documentType: NSAttributedString.DocumentType.html,
            .characterEncoding: String.Encoding.utf8.rawValue
        ]
        guard let attributedString = try? NSAttributedString(data: data, options: options, documentAttributes: nil) else {
            return nil
        }

        self.init(attributedString.string)
    }

}

extension String {
    
    var roundOfPrice: String {
        
        let charset = CharacterSet(charactersIn: ".")
        if self.rangeOfCharacter(from: charset) != nil {
           return  self.components(separatedBy: ".").first ?? ""
        }else{
            return self
        }
    }
    var trimSpace: String {
        return self.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    var isValidTenDigitMobileNumber : Bool{
       let p = self.trimSpace
        return p.count >= 10 ? true : false
    }
    
    func toDate(withFormate formet: String) -> Date? {
        let dateFormatter = DateFormatter()
        //let utcTimeZone = TimeZone(secondsFromGMT: 0)
        //dateFormatter.timeZone = utcTimeZone
        dateFormatter.dateFormat = formet
        let date = dateFormatter.date(from: self)
        return date
    }
    
    var isValideEmail: Bool {
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluate(with: self)
    }
    
    var isValidPhoneNumberCountryWise: Bool{
        let PHONE_REGEX = "^\\+(?:[0-9] ?){10,11}[0-9]$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        return  phoneTest.evaluate(with: self)
        
    }
   // MARK: -  PasswordValidation(Minimum 8 characters at least 1 Uppercase Alphabet, 1 Lowercase Alphabet, 1 Number and 1 Special Character)
    var isPasswordValidation: Bool{
         let Password_REGEX = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*?&])[A-Za-z\\d$@$!%*?&]{6,}"
           let Password = NSPredicate(format: "SELF MATCHES %@", Password_REGEX)
           return  Password.evaluate(with: self)
          
       }
    
    //
    
    var containsDigits : Bool {
        return (self.rangeOfCharacter(from: CharacterSet.decimalDigits) != nil)
    }
}

extension UIImageView {
    func setRounded() {
        self.layer.cornerRadius = (self.frame.width / 2)
        self.layer.masksToBounds = true
    }
    //var context = CIContext(options: nil)

    func blurEffect(context:CIContext) {

        let currentFilter = CIFilter(name: "CIGaussianBlur")
        let beginImage = CIImage(image: self.image!)
        currentFilter!.setValue(beginImage, forKey: kCIInputImageKey)
        currentFilter!.setValue(10, forKey: kCIInputRadiusKey)

        let cropFilter = CIFilter(name: "CICrop")
        cropFilter!.setValue(currentFilter!.outputImage, forKey: kCIInputImageKey)
        cropFilter!.setValue(CIVector(cgRect: beginImage!.extent), forKey: "inputRectangle")

        let output = cropFilter!.outputImage
        let cgimg = context.createCGImage(output!, from: output!.extent)
        let processedImage = UIImage(cgImage: cgimg!)
        self.image = processedImage
    }
}
extension UINavigationController {
    //Mark:  - Check in navigation stack
    func containsViewController(ofKind kind: AnyClass) -> Bool {
        return self.viewControllers.contains(where: {$0.isKind(of: kind)})
    }
    //function use to check whether that VC is in the Stack or not , if in tha stack tha pop or Push
    func popPushToVC(ofKind Kind: AnyClass, pushController: UIViewController) {
        if containsViewController(ofKind: Kind) {
            for controller in self.viewControllers{
                if controller.isKind(of: Kind){
                    popToViewController(controller, animated: true)
                    break
                }
            }
        } else{
            pushViewController(pushController, animated: true)
        }
    }
    
    func backToPreviousPage() {
      self.popViewController(animated: true)
        
    }
    
}
extension UIView {
    func makeRound(){
    let radius = (self.frame.size.width / 2) 
        self.layer.cornerRadius = radius
        self.layer.borderWidth = 2.0
        self.layer.borderColor = UIColor.clear.cgColor
        self.clipsToBounds = true
    }
    
    func makeRoundCornerRadius(){
        let radius = 5
        self.layer.cornerRadius = CGFloat(radius)
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor.gray.cgColor
        self.clipsToBounds = true
    }
    /*
    func makeRoundCornerRadiusWithColor(color:UIColor){
        let radius = 5
        self.layer.cornerRadius = CGFloat(radius)
        self.layer.borderWidth = 1.0
        self.layer.borderColor = color.cgColor
        self.clipsToBounds = true
    }*/
    func makeShadow() {
        // Initialization code
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: CGFloat(1.0), height: CGFloat(1.0))
        self.layer.shadowOpacity = 0.5
        self.layer.shadowRadius = 5
    }
    
   
}

extension Date {
 
    func afterMonth(getMonth:Int) -> Date{
        return Calendar.current.date(byAdding: .month, value: getMonth, to: noon)!
    }
    func startOfMonth() -> Date {
        return Calendar.current.date(from: Calendar.current.dateComponents([.year, .month], from: Calendar.current.startOfDay(for: self)))!
    }
    
    func endOfMonth() -> Date {
        return Calendar.current.date(byAdding: DateComponents(month: 1, day: -1), to: self.startOfMonth())!
    }
    static var yesterday: Date { return Date().dayBefore }
    static var tomorrow:  Date { return Date().dayAfter }
    static var nextMonth:  Date { return Date().monthAfter }
    static var nextyear:  Date { return Date().yearAfter }
    var dayBefore: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: noon)!
    }
    var dayAfter: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: noon)!
    }
    var monthAfter: Date {
        return Calendar.current.date(byAdding: .month, value: 1, to: noon)!
    }
    var yearAfter: Date {
        return Calendar.current.date(byAdding: .year, value: 1, to: noon)!
    }
    
    var noon: Date {
        return Calendar.current.date(bySettingHour: 0, minute: 0, second: 0, of: self)!
    }
    var month: Int {
        return Calendar.current.component(.month,  from: self)
    }
    var isLastDayOfMonth: Bool {
        return dayAfter.month != month
    }
}

extension UIImage {
    
    func fixedOrientation() -> UIImage? {
        
        guard imageOrientation != UIImage.Orientation.up else {
            //This is default orientation, don't need to do anything
            return self.copy() as? UIImage
        }
        
        guard let cgImage = self.cgImage else {
            //CGImage is not available
            return nil
        }
        
        guard let colorSpace = cgImage.colorSpace, let ctx = CGContext(data: nil, width: Int(size.width), height: Int(size.height), bitsPerComponent: cgImage.bitsPerComponent, bytesPerRow: 0, space: colorSpace, bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue) else {
            return nil //Not able to create CGContext
        }
        
        var transform: CGAffineTransform = CGAffineTransform.identity
        
        switch imageOrientation {
        case .down, .downMirrored:
            transform = transform.translatedBy(x: size.width, y: size.height)
            transform = transform.rotated(by: CGFloat.pi)
            break
        case .left, .leftMirrored:
            transform = transform.translatedBy(x: size.width, y: 0)
            transform = transform.rotated(by: CGFloat.pi / 2.0)
            break
        case .right, .rightMirrored:
            transform = transform.translatedBy(x: 0, y: size.height)
            transform = transform.rotated(by: CGFloat.pi / -2.0)
            break
        case .up, .upMirrored:
            break
        default:
            break
        }
        
        //Flip image one more time if needed to, this is to prevent flipped image
        switch imageOrientation {
        case .upMirrored, .downMirrored:
            transform = transform.translatedBy(x: size.width, y: 0)
            transform = transform.scaledBy(x: -1, y: 1)
            break
        case .leftMirrored, .rightMirrored:
            transform = transform.translatedBy(x: size.height, y: 0)
            transform = transform.scaledBy(x: -1, y: 1)
        case .up, .down, .left, .right:
            break
        default:
            break
        }
        
        ctx.concatenate(transform)
        
        switch imageOrientation {
        case .left, .leftMirrored, .right, .rightMirrored:
            ctx.draw(self.cgImage!, in: CGRect(x: 0, y: 0, width: size.height, height: size.width))
        default:
            ctx.draw(self.cgImage!, in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
            break
        }
        
        guard let newCGImage = ctx.makeImage() else { return nil }
        return UIImage.init(cgImage: newCGImage, scale: 1, orientation: .up)
    }
    
   
}



extension Data {
    
    var dictionary:[String:Any]? {
        do{
            if let json = try JSONSerialization.jsonObject(with: self, options: JSONSerialization.ReadingOptions.mutableLeaves) as? [String:Any] {
                return json
            }
        }catch let e {
            print(e.localizedDescription)
            return nil
        }
        return nil
    }
}


extension Dictionary where Key: ExpressibleByStringLiteral, Value: Any {
    var prettyprintedJSON: String? {
        do {
            let data: Data = try JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
            return String(data: data, encoding: .utf8)
        } catch let err {
            print(err.localizedDescription)
            return nil
        }
    }
    var data:Data? {
        do {
            let data: Data = try JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
            return data
        } catch let err {
            print(err.localizedDescription)
            return nil
        }
    }
}

extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}


extension UILabel {
   @objc public var substituteFontName : String {
        get {
            return self.font.fontName;
        }
        set {
            let fontNameToTest = self.font.fontName.lowercased();
            var fontName = newValue;
            if fontNameToTest.range(of: "bold") != nil {
                fontName += "-Bold";
            } else if fontNameToTest.range(of: "medium") != nil {
                fontName += "-Medium";
            } else if fontNameToTest.range(of: "light") != nil {
                fontName += "-Light";
            } else if fontNameToTest.range(of: "ultralight") != nil {
                fontName += "-UltraLight";
            }
            self.font = UIFont(name: fontName, size: self.font.pointSize)
        }
    }
}

extension UITextView {
    @objc public var substituteFontName : String {
        get {
            return self.font?.fontName ?? "";
        }
        set {
            let fontNameToTest = self.font?.fontName.lowercased() ?? "";
            var fontName = newValue;
            if fontNameToTest.range(of: "bold") != nil {
                fontName += "-Bold";
            } else if fontNameToTest.range(of: "medium") != nil {
                fontName += "-Medium";
            } else if fontNameToTest.range(of: "light") != nil {
                fontName += "-Light";
            } else if fontNameToTest.range(of: "ultralight") != nil {
                fontName += "-UltraLight";
            }
            self.font = UIFont(name: fontName, size: self.font?.pointSize ?? 17)
        }
    }
}

extension UITextField {
    @objc public var substituteFontName : String {
        get {
            return self.font?.fontName ?? "";
        }
        set {
            let fontNameToTest = self.font?.fontName.lowercased() ?? "";
            var fontName = newValue;
            if fontNameToTest.range(of: "bold") != nil {
                fontName += "-Bold";
            } else if fontNameToTest.range(of: "medium") != nil {
                fontName += "-Medium";
            } else if fontNameToTest.range(of: "light") != nil {
                fontName += "-Light";
            } else if fontNameToTest.range(of: "ultralight") != nil {
                fontName += "-UltraLight";
            }
            self.font = UIFont(name: fontName, size: self.font?.pointSize ?? 17)
        }
    }
}
extension UIView {
    
    private struct AssociatedKeys {
        static var descriptiveName = "AssociatedKeys.DescriptiveName.blurView"
    }
    
    private (set) var blurView: BlurView {
        get {
            if let blurView = objc_getAssociatedObject(
                self,
                &AssociatedKeys.descriptiveName
                ) as? BlurView {
                return blurView
            }
            self.blurView = BlurView(to: self)
            return self.blurView
        }
        set(blurView) {
            objc_setAssociatedObject(
                self,
                &AssociatedKeys.descriptiveName,
                blurView,
                .OBJC_ASSOCIATION_RETAIN_NONATOMIC
            )
        }
    }
    
    class BlurView {
        
        private var superview: UIView
        private var blur: UIVisualEffectView?
        private var editing: Bool = false
        private (set) var blurContentView: UIView?
        private (set) var vibrancyContentView: UIView?
        
        var animationDuration: TimeInterval = 0.1
        
        /**
         * Blur style. After it is changed all subviews on
         * blurContentView & vibrancyContentView will be deleted.
         */
        var style: UIBlurEffect.Style = .light {
            didSet {
                guard oldValue != style,
                    !editing else { return }
                applyBlurEffect()
            }
        }
        /**
         * Alpha component of view. It can be changed freely.
         */
        var alpha: CGFloat = 0 {
            didSet {
                guard !editing else { return }
                if blur == nil {
                    applyBlurEffect()
                }
                let alpha = self.alpha
                UIView.animate(withDuration: animationDuration) {
                    self.blur?.alpha = alpha
                }
            }
        }
        
        init(to view: UIView) {
            self.superview = view
        }
        
        func setup(style: UIBlurEffect.Style, alpha: CGFloat) -> Self {
            self.editing = true
            
            self.style = style
            self.alpha = alpha
            
            self.editing = false
            
            return self
        }
        
        func enable(isHidden: Bool = false) {
            if blur == nil {
                applyBlurEffect()
            }
            
            self.blur?.isHidden = isHidden
        }
        
        private func applyBlurEffect() {
            blur?.removeFromSuperview()
            
            applyBlurEffect(
                style: style,
                blurAlpha: alpha
            )
        }
        
        private func applyBlurEffect(style: UIBlurEffect.Style,
                                     blurAlpha: CGFloat) {
            superview.backgroundColor = UIColor.clear
            
            let blurEffect = UIBlurEffect(style: style)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            
            let vibrancyEffect = UIVibrancyEffect(blurEffect: blurEffect)
            let vibrancyView = UIVisualEffectView(effect: vibrancyEffect)
            blurEffectView.contentView.addSubview(vibrancyView)
            
            blurEffectView.alpha = blurAlpha
            
            superview.insertSubview(blurEffectView, at: 0)
            
            blurEffectView.addAlignedConstrains()
            vibrancyView.addAlignedConstrains()
            
            self.blur = blurEffectView
            self.blurContentView = blurEffectView.contentView
            self.vibrancyContentView = vibrancyView.contentView
        }
    }
    
    private func addAlignedConstrains() {
        translatesAutoresizingMaskIntoConstraints = false
        addAlignConstraintToSuperview(attribute: NSLayoutConstraint.Attribute.top)
        addAlignConstraintToSuperview(attribute: NSLayoutConstraint.Attribute.leading)
        addAlignConstraintToSuperview(attribute: NSLayoutConstraint.Attribute.trailing)
        addAlignConstraintToSuperview(attribute: NSLayoutConstraint.Attribute.bottom)
    }
    
    private func addAlignConstraintToSuperview(attribute: NSLayoutConstraint.Attribute) {
        superview?.addConstraint(
            NSLayoutConstraint(
                item: self,
                attribute: attribute,
                relatedBy: NSLayoutConstraint.Relation.equal,
                toItem: superview,
                attribute: attribute,
                multiplier: 1,
                constant: 0
            )
        )
    }
}
extension Array {
    var data:Data? {
        get{
            do{
                return (try JSONSerialization.data(withJSONObject: self, options: JSONSerialization.WritingOptions.prettyPrinted))
            }catch let e {
                print(e.localizedDescription)
                return nil
            }
        }
    }
    
}

extension UIView {
    func fadeIn(_ duration: TimeInterval = 0.25,_ delay:TimeInterval = 0.0, completion:((Bool) -> ())? = nil) {
        self.alpha = 0.0
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.alpha = 1.0
        }) { (finished:Bool) in
            if let completion = completion {
                completion(finished)
            }
        }
    }
    func fadeOut(_ duration: TimeInterval = 0.25,_ delay:TimeInterval = 0.0, completion:((Bool) -> ())? = nil) {
        self.alpha = 1.0
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.alpha = 0.0
        }) { (finished:Bool) in
            if let completion = completion {
                completion(finished)
            }
        }
    }
}


extension UINavigationController {
    
    ///Get previous view controller of the navigation stack
    func previousViewController() -> UIViewController?{
        
        let lenght = self.viewControllers.count
        
        let previousViewController: UIViewController? = lenght >= 2 ? self.viewControllers[lenght-2] : nil
        
        return previousViewController
    }
    
}
extension UITextField {
    func addDoneCancelToolbar(onDone: (target: Any, action: Selector)? = nil, onCancel: (target: Any, action: Selector)? = nil) {
        let onCancel = onCancel ?? (target: self, action: #selector(cancelButtonTapped))
        let onDone = onDone ?? (target: self, action: #selector(doneButtonTapped))

        let toolbar: UIToolbar = UIToolbar()
        toolbar.barStyle = .default
        toolbar.items = [
            UIBarButtonItem(title: "Cancel", style: .plain, target: onCancel.target, action: onCancel.action),
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil),
            UIBarButtonItem(title: "Done", style: .done, target: onDone.target, action: onDone.action)
        ]
        toolbar.sizeToFit()

        self.inputAccessoryView = toolbar
    }

    // Default actions:
    @objc func doneButtonTapped() { self.resignFirstResponder() }
    @objc func cancelButtonTapped() { self.resignFirstResponder() }
}
extension Notification.Name{
    static let didCompleteSubProjectSync = NSNotification.Name("didCompleteSubProjectSync")
    static let didCompleteComSubProjectSync = NSNotification.Name("didCompleteComSubProjectSync")
    static let didCallProjectSync = NSNotification.Name("didCallProjectSync")
}
extension Collection where Indices.Iterator.Element == Index {
    subscript (safe index: Index) -> Iterator.Element? {
        return indices.contains(index) ? self[index] : nil
    }
}
extension Date {
    
    func isEqualTo(_ date: Date) -> Bool {
        return self == date
    }
    
}
