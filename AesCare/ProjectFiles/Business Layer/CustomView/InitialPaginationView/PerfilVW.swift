//
//  PerfilVW.swift
//  AesCare
//
//  Created by Gali Srikanth on 13/08/20.
//

import UIKit

class PerfilVW: UIView {
    @IBOutlet var view:UIView?
    @IBOutlet weak var tblOption: UITableView!
    var arrData = [String]()
    var selectedIndex = 0
   private override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    private convenience init() {
        self.init(frame: CGRect.zero)
    }
    internal required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
       
    }
   
    func basicSetup() {
        self.view = Bundle.main.loadNibNamed("PerfilVW", owner: self, options: nil)?.first as? UIView
        self.view?.frame = CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height)
        arrData = ["Sou paciente","Sou médico"]
        
        self.tblOption.register(UINib.init(nibName: "OptionCell", bundle: nil), forCellReuseIdentifier: "OptionCell")
        
        self.tblOption.reloadData()
        self.addSubview(self.view!)
    }
}
extension PerfilVW:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrData.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  UIDevice.current.iPad ?  100 : 70
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: OptionCell = self.tblOption.dequeueReusableCell(withIdentifier: "OptionCell") as! OptionCell
        
        cell.lblInfo.text = arrData[indexPath.row]
        let img = (indexPath.row == selectedIndex) ? UIImage(named: "selectCircle") : UIImage(named: "unselectCircle")
        cell.btnInstance.tag = indexPath.row
        cell.btnInstance.setImage(img, for: .normal)
        cell.btnInstance.addTarget(self, action: #selector(btnSelectSaleable), for: .touchUpInside)
       
        
        cell.selectionStyle = .none
        cell.backgroundColor = UIColor.clear
        cell.contentView.backgroundColor = UIColor.clear
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        is_medic = (selectedIndex == 0) ? false : true
        tblOption.reloadData()
    }
    @objc func btnSelectSaleable(_ sender: UIButton!){
        selectedIndex = sender.tag
        is_medic = (selectedIndex == 0) ? false : true
        tblOption.reloadData()
    }
    
}
