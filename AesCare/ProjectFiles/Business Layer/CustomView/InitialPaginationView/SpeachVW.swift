//
//  SpeachVW.swift
//  AesCare
//
//  Created by Gali Srikanth on 13/08/20.
//

import UIKit

class SpeachVW: UIView {
    @IBOutlet var view:UIView?
   
   private override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    private convenience init() {
        self.init(frame: CGRect.zero)
    }
    internal required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
       
    }
   
    func basicSetup() {
        self.view = Bundle.main.loadNibNamed("SpeachVW", owner: self, options: nil)?.first as? UIView
        self.view?.frame = CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height)
      
        self.addSubview(self.view!)
    }
}
