//
//  AgendaVW.swift
//  AesCare
//
//  Created by Gali Srikanth on 08/08/20.
//

import UIKit

class AgendaVW: UIView {
    @IBOutlet var view:UIView?
    @IBOutlet weak var lblInfo: UILabel!
   private override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    private convenience init() {
        self.init(frame: CGRect.zero)
    }
    internal required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
       
    }
   
    func basicSetup() {
        self.view = Bundle.main.loadNibNamed("AgendaVW", owner: self, options: nil)?.first as? UIView
        self.view?.frame = CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height)
        self.lblInfo.text = "Na aesCare pensamos na sua comodidade, \n você pode agendar consultas com médicos \n diretamente pelo app."
        self.addSubview(self.view!)
    }
}
