//
//  BemVindaVW.swift
//  AesCare
//
//  Created by Gali Srikanth on 08/08/20.
//

import UIKit

class BemVindaVW: UIView {
    @IBOutlet var view:UIView?
    @IBOutlet weak var lblInfo: UILabel!
   private override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    private convenience init() {
        self.init(frame: CGRect.zero)
    }
    internal required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
       
    }
   
    func basicSetup() {
        self.view = Bundle.main.loadNibNamed("BemVindaVW", owner: self, options: nil)?.first as? UIView
        self.view?.frame = CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height)
        self.lblInfo.text = "Já somos mais de 600 mil usuárias! \n Faça parte do nosso universo da cirurgia plástica."
        self.addSubview(self.view!)
    }
}
