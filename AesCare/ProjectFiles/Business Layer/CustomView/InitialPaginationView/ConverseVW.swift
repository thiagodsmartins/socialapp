//
//  ConverseVW.swift
//  AesCare
//
//  Created by Gali Srikanth on 08/08/20.
//

import UIKit

class ConverseVW: UIView {
    @IBOutlet var view:UIView?
    @IBOutlet weak var lblInfo: UILabel!
   private override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    private convenience init() {
        self.init(frame: CGRect.zero)
    }
    internal required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
       
    }
   
    func basicSetup() {
        self.view = Bundle.main.loadNibNamed("ConverseVW", owner: self, options: nil)?.first as? UIView
        self.view?.frame = CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height)
        self.lblInfo.text = "Tire dúvidas, pergunte opiniões, participe dos \n grupos e interaja com outras usuárias que \n possuem o mesmo objetivo que você."
        self.addSubview(self.view!)
    }
}
