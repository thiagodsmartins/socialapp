

import UIKit

class CommonCViewCell: UICollectionViewCell {
    @IBOutlet weak var vwContainer: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var btnCell: UIButton!
    
    
    
    @IBOutlet weak var lblHeader: UILabel!
    
    @IBOutlet weak var lblHeaderInfo: UILabel!
    @IBOutlet weak var lblInfo: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
