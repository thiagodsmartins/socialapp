//
//  CustomTabbarView.swift
//  SuperMindSkill
//
//  Created by Abhik on 20/10/19.
//  Copyright © 2020 Abhik. All rights reserved.
//

import UIKit
@objc protocol CustomTabbarDelegate {
    func GetSelectedIndex(Index:Int)
}
class CustomTabbarView: UIView {
    
    var myDelegate:CustomTabbarDelegate?
    
    @IBOutlet var view: UIView?
    
    @IBOutlet var viewHome: UIView?
    @IBOutlet var viewJoranda: UIView?
    @IBOutlet var viewAgendar: UIView?
    @IBOutlet var viewChat: UIView?
    @IBOutlet var viewProfile: UIView?
    
    @IBOutlet var imgHome: UIImageView?
    @IBOutlet var imgJoranda: UIImageView?
    @IBOutlet var imgAgendar: UIImageView?
    @IBOutlet var imgChat: UIImageView?
    @IBOutlet var imgProfile: UIImageView?
    
    
    @IBOutlet var lblHome: UILabel?
    @IBOutlet var lblJoranda: UILabel?
    @IBOutlet var lblAgendar: UILabel?
    @IBOutlet var lblChat: UILabel?
    @IBOutlet var lblProfile: UILabel?
    
    func setup(activeFor:String) -> Void {
        self.view = Bundle.main.loadNibNamed("CustomTabbarView", owner: self, options: nil)?.first as! UIView?
        self.view?.frame = CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height)
        self.addSubview(self.view!)
        
        /*
         if(activeFor == "Home"){
         imgHome?.image = UIImage(named: "home-Active")
         lblHome?.backgroundColor = UIColor(named: "AppPurpleColor")
         }*/
        
        
        
        
        imgHome?.image = UIImage(named: "home-Inactive")
        lblHome?.backgroundColor = .clear
        imgJoranda?.image = UIImage(named: "jornada-Inactive")
        lblJoranda?.backgroundColor = .clear
        imgAgendar?.image = UIImage(named: "agendar-Inactive")
        lblAgendar?.backgroundColor = .clear
        imgChat?.image = UIImage(named: "chat-Inactive")
        lblChat?.backgroundColor = .clear
        
        imgProfile?.image = UIImage(named: "profile-Inactive")
        lblProfile?.backgroundColor = .clear
        if(activeFor == "Home"){
            imgHome?.image = UIImage(named: "home-Active")
            lblHome?.backgroundColor = UIColor(named: "AppPurpleColor")
        }else if(activeFor == "Jornada"){
            imgJoranda?.image = UIImage(named: "jornada-Active")
            lblJoranda?.backgroundColor = UIColor(named: "AppPurpleColor")
        }
        else if(activeFor == "Agendar"){
            imgAgendar?.image = UIImage(named: "agendar-Active")
            lblAgendar?.backgroundColor = UIColor(named: "AppPurpleColor")
        }
        else if(activeFor == "Chat"){
            imgChat?.image = UIImage(named: "chat-Active")
            lblChat?.backgroundColor = UIColor(named: "AppPurpleColor")
        }
        else if(activeFor == "Profile"){
            imgProfile?.image = UIImage(named: "profile-Active")
            lblProfile?.backgroundColor = UIColor(named: "AppPurpleColor")
        }
    }
    @IBAction func selectionTabbar(_ sender: UIButton){
        myDelegate?.GetSelectedIndex(Index: sender.tag)
    }
}
