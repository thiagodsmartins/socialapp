//
//  AESCareConnectivity.swift
//  AesCare
//
//  Created by Thiago on 15/01/21.
//

import Foundation
import Network

@available(iOS 12.0, *)
class AESCareConnectivity {
    var connectionMonitor: NWPathMonitor?
    
    init() {
        self.connectionMonitor = NWPathMonitor()
    }
    
    func startNetworkMonitoring() {
        self.connectionMonitor!.pathUpdateHandler = { path in
            if path.status == .satisfied {
                print("We're connected!")
            } else {
                print("No connection.")
            }

            print(path.isExpensive)
        }
    }
    
}
